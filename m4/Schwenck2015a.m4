dnl -*- autoconf -*-
# Macros needed to find Schwenck2015a and dependent libraries.  They are called by
# the macros in ${top_src_dir}/dependencies.m4, which is generated by
# "dunecontrol autogen"

# Additional checks needed to build Schwenck2015a
# This macro should be invoked by every module which depends on Schwenck2015a, as
# well as by Schwenck2015a itself
AC_DEFUN([SCHWENCK2015A_CHECKS])

# Additional checks needed to find Schwenck2015a
# This macro should be invoked by every module which depends on Schwenck2015a, but
# not by Schwenck2015a itself
AC_DEFUN([SCHWENCK2015A_CHECK_MODULE],
[
  DUNE_CHECK_MODULES([Schwenck2015a],[Schwenck2015a/Schwenck2015a.hh])
])
