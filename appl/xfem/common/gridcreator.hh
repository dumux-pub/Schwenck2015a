#ifndef GRIDCREATOR_HH_
#define GRIDCREATOR_HH_

template <typename Grid, typename Scalar>
class CreateGrid
{
};

template <typename Scalar>
class CreateGrid< Dune::UGGrid<2>, Scalar>
{
public:
static Dune::UGGrid<2> *create(const Dune::FieldVector<Scalar, 2> &upperRight,
        const Dune::FieldVector<int, 2> &cellRes)
    {
    Dune::UGGrid<2> *grid = new Dune::UGGrid<2>;
    Dune::GridFactory<Dune::UGGrid<2> > factory(grid);
//    int cellRes0=cellRes[0]-1;
    const double domainSizeX=1.0;
//    const double domainSizeX=1.6;
//    const double domainSizeX=1.0;
//    const double domainSizeY=domainSizeX/10.0;
    const double domainSizeY=domainSizeX;
//    const double domainSizeY=1.15;
    const int cellRes0=cellRes[0];
    for (int i=0; i<=cellRes0; i++)
    {
        for (int j=0; j<=cellRes[1]; j++)
            {
            Dune::FieldVector<Scalar,2> pos;
            pos[0] = upperRight[0]-Scalar(i)*domainSizeX/cellRes0;
            pos[1] = upperRight[1]-Scalar(j)*domainSizeY/cellRes[1];
            factory.insertVertex(pos);
            }
    }

    for (int i=0; i<cellRes0; i++)
    {
        for (int j=0; j<cellRes[1]; j++)
        {
            std::vector<unsigned int> v(4);
            v[0] = i*(cellRes[1]+1) + j;
            v[1] = i*(cellRes[1]+1) + j+1;
            v[2] = (i+1)*(cellRes[1]+1) + j;
            v[3] = (i+1)*(cellRes[1]+1) + j+1;
            factory.insertElement(Dune::GeometryType(Dune::GeometryType::cube,2), v);
        }
    }

    factory.createGrid();
    grid->loadBalance();
    return (grid);
    }
};
#if HAVE_ALUGRID
template <typename Scalar>
class CreateGrid< Dune::ALUGrid< 2, 2, Dune::cube, Dune::nonconforming >, Scalar>
{
public:
static Dune::ALUGrid< 2, 2, Dune::cube, Dune::nonconforming > *create(const Dune::FieldVector<Scalar, 2> &upperRight,
        const Dune::FieldVector<int, 2> &cellRes)
    {
    Dune::ALUGrid< 2, 2, Dune::cube, Dune::nonconforming > *grid = new Dune::ALUGrid< 2, 2, Dune::cube, Dune::nonconforming >;
    Dune::GridFactory<Dune::ALUGrid< 2, 2, Dune::cube, Dune::nonconforming > > factory(grid);
    for (int i=0; i<=cellRes[0]; i++)
    {
        for (int j=0; j<=cellRes[1]; j++)
            {
            Dune::FieldVector<Scalar,2> pos;
            pos[0] = upperRight[0]-Scalar(i)*2.0/cellRes[0];
            pos[1] = upperRight[1]-Scalar(j)*2.0/cellRes[1];
            factory.insertVertex(pos);
            }
    }

    for (int i=0; i<cellRes[0]; i++)
    {
        for (int j=0; j<cellRes[1]; j++)
        {
            std::vector<unsigned int> v(4);
            v[0] = i*(cellRes[1]+1) + j;
            v[1] = i*(cellRes[1]+1) + j+1;
            v[2] = (i+1)*(cellRes[1]+1) + j;
            v[3] = (i+1)*(cellRes[1]+1) + j+1;
            factory.insertElement(Dune::GeometryType(Dune::GeometryType::cube,2), v);
        }
    }

    factory.createGrid();
    grid->loadBalance();
    return (grid);
    }
};
#endif
template <typename Scalar>
class CreateGrid<Dune::YaspGrid<2>, Scalar>
{
public:
static Dune::YaspGrid<2> *create(const Dune::FieldVector<Scalar, 2> &upperRight,
        const Dune::FieldVector<int, 2> &cellRes)
    {
    // constructor (domain extension, coarse grid resolution, periodic?, overlap)
    Dune::YaspGrid<2> *grid =  new Dune::YaspGrid<2>(upperRight,cellRes,Dune::FieldVector<bool,2>(false),4);
    return (grid);
    }
};

template<typename HostGrid, typename Map, typename Scalar>
class CreateMixedGrid
{
    static const unsigned int dimw_=2;//TODO extract this from HostGrid
    typedef typename HostGrid::LeafGridView GV;
    typedef typename HostGrid::ctype ctype;
    typedef typename Dune::FieldVector<ctype,dimw_> FV;
    typedef typename GV::template Codim<dimw_>::Iterator HostGridVertexIterator;
    typedef typename GV::template Codim<0>::Iterator HostGridElementIterator;

    typedef typename GV::IndexSet::IndexType IndexType;

//    typedef typename std::vector<Scalar> GlobalElementPermeability;

public:
//    typedef typename std::vector<GlobalElementPermeability> GlobalElementPermeabilityVector;

    CreateMixedGrid(const HostGrid &hostgrid,const Map &intersectionPointsMap)
//set artificialSmallAperture only if the aperture is too large and causes problems because the fracture cuts close to element corners/edges
    : matrixGrid_(hostgrid), intersectionPointsMap_(intersectionPointsMap), artificialSmallAperture_(1.0)
    {}


    HostGrid *create()
    {
        HostGrid *grid = new HostGrid;
        Dune::GridFactory<HostGrid> factory(grid);
        bool insertCubes=false;
        bool splitCubes=false;
        bool createBrokenGrid=true;
        bool lowerDim=false;
        /*
         * create grid from the structured quadrilateral grid
         * 1) traverse through the coarse matrix grid and copy all uncut elements
         * 2) for all cut elements introduce simplex elements depending on the
         * fracture geometry.
         */
        //loop over all vertices and copy them
        //TODO make sure every node is only added once
        int numberOfVertices=matrixGrid_.leafIndexSet().size(dimw_);
        typedef typename std::vector<FV> Vertices;
        typedef typename Vertices::iterator VerticesIterator;
        Vertices oldVertices;
        oldVertices.resize(numberOfVertices);


        for (int oldVertexIdx=0;oldVertexIdx<numberOfVertices;++oldVertexIdx){
            HostGridVertexIterator nendit = matrixGrid_.template leafend<dimw_>();
            for (HostGridVertexIterator matrixNodeIt = matrixGrid_.template leafbegin<dimw_>();
                    matrixNodeIt != nendit; ++matrixNodeIt) {
                FV pos;
                pos=matrixNodeIt->geometry().center();
                int index=matrixGrid_.leafIndexSet().index(*matrixNodeIt);
                if (index==oldVertexIdx){
                    oldVertices[index]=pos;
                    factory.insertVertex(pos);
                    break;
                }
            }
        }

        Vertices newVertices;
        typedef Dune::GeometryType::BasicType BasicType;
        const BasicType cube = Dune::GeometryType::cube;
        const BasicType simplex = Dune::GeometryType::simplex;

        //loop over elements
        HostGridElementIterator eendit = matrixGrid_.template leafend<0>();
        for (HostGridElementIterator matrixElementIterator = matrixGrid_.template leafbegin<0>(); matrixElementIterator != eendit; ++matrixElementIterator) {

            //get element Id
            typedef typename HostGrid::Traits::GlobalIdSet::IdType IdType;
            const IdType elementId =matrixGrid_.globalIdSet().id(*matrixElementIterator);

            typedef typename Map::const_iterator IPMapIterator;
            IPMapIterator ipmit=intersectionPointsMap_.find(elementId);

            //if this is an uncut element just add all four nodes and then the element to the new grid
            if (ipmit == intersectionPointsMap_.end()){
                //add now a quadrilateral by using the index of the elements nodes
                std::vector<unsigned int> v(4);
                for (int i=0;i<4;++i){
                    IndexType nodeIdx = matrixGrid_.leafIndexSet().subIndex(*matrixElementIterator,i,dimw_);
                    v[i] = nodeIdx;
                }
                if (!splitCubes){
                    std::cout << "now regular quad uncut element: "<< std::endl;
                    factory.insertElement(Dune::GeometryType(cube,2), v);
                    assert(v[0]!=v[1]);
                    assert(v[0]!=v[2]);
                    assert(v[0]!=v[3]);
                    assert(v[1]!=v[2]);
                    assert(v[1]!=v[3]);
                    assert(v[2]!=v[3]);
                }
                else {
                    for (int iSIdx=0;iSIdx<2;++iSIdx){
                        std::vector<unsigned int> vSimplexUncut(3);

                        if ( (matrixElementIterator->geometry().center())[1]>0.5){
                            vSimplexUncut[0]=v[0];
                            vSimplexUncut[2]=v[3];
                            vSimplexUncut[1]=v[iSIdx+1];
                        }
                        else {
                            for (int nIdx=0;nIdx<3;++nIdx){
                                vSimplexUncut[nIdx]=v[nIdx+iSIdx];
                            }
                        }
                        std::cout << "now regular simplex uncut element: "<< std::endl;
                        factory.insertElement(Dune::GeometryType(simplex,2), vSimplexUncut);
                    }
                }
            }
        }//end loop over all elements and only uncut elements are copied
        /*
         * now treat only cut elements
         */
        bool treatCutElements=true;
        int maxNumberOfCutElements=1000;
        int cutElementIdx=0;
        if (treatCutElements){
        for (HostGridElementIterator matrixElementIterator = matrixGrid_.template leafbegin<0>(); matrixElementIterator != eendit; ++matrixElementIterator) {
            //get element Id
            typedef typename HostGrid::Traits::GlobalIdSet::IdType IdType;
            const IdType elementId =matrixGrid_.globalIdSet().id(*matrixElementIterator);

            typedef typename Map::const_iterator IPMapIterator;
            IPMapIterator ipmit=intersectionPointsMap_.find(elementId);

            if (ipmit != intersectionPointsMap_.end()){
                ++cutElementIdx;
                if (cutElementIdx>maxNumberOfCutElements) break;
                typedef typename Map::mapped_type IntersectionPoints;
                IntersectionPoints intersectionPoints=(((*ipmit).second));

                typedef typename Dune::FieldVector<Scalar,2> FV;
                FV check(-1000.0);
                int numberOfIP=0;
                for (int j=0;j<4;j++){//index 4 and 5 only store the midpoints/virtual exit points
                    /*
                     * if there is an empty entry there are no more relevant entries after that
                     * for ending fractures the projected exit is not to be used!
                     */
                    if (Dumux::arePointsEqual(intersectionPoints[j].intersection,check)){
                        break;
                    }
                    ++numberOfIP;
                }

                /*
                 * This increments the number of intersection points by one if there is an entry
                 * at position 5 and there was only one real intersection with the boundary so far,
                 * i.e., ending fracture with projected exit at position 5.
                 */
                typedef typename IntersectionPoints::value_type Intersection;
                typedef typename std::vector<Intersection> StandardVectorOfIntersections;
                StandardVectorOfIntersections pointsNotOnCorners;//there will be later numberOfIP entries TODO check if this is true for cuts through a corner!
                std::vector<FV> xfMiddle;
                xfMiddle.clear();

                xfMiddle.push_back(intersectionPoints[4].intersection);

                /*
                 * lmax is 1 for ending fractures, 2 for single fractures, 3 for 3-crossings and 4 for 4-crossings.
                 * Two not-crossing fractures have an lmax of 2.
                 */
                for (int i=0;i<numberOfIP;i++){
                    pointsNotOnCorners.push_back(intersectionPoints[i]);
                }
                if(!Dumux::arePointsEqual(intersectionPoints[1].intersection,check) &&
                        !Dumux::arePointsEqual(intersectionPoints[5].intersection,check) ){//this indicates two not-crossing fractures, STRANGE not necessarily
                    xfMiddle.push_back(intersectionPoints[5].intersection);
                    /*
                     * there are 4 points on the boundary of the element,
                     * but there are also two centre points, so that the for each xfMiddle there are only two pointsNotOnCorners.
                     */
                }
                assert(xfMiddle.size()==1);
                /*
                 * for ending fractures the sub-triangulation is the same as for one single not bending fracture through the element
                 * the fracture is extrapolated to the end of the element and a virtual exit point is determined
                 * this happens in the gridCoupling and in line 62,
                 * so that the vector pointsNotOnCorners does look the same as in the single not bending fracture case
                 */
                typedef Dune::GeometryType GT;
                const GT subSimplex(simplex,dimw_);
                /*
                 * this scheme is based on the DUNE node ordering as defined for the generic reference element cube
                 * create subtriangles depending on the fracture geometry
                 * for straight, bending and ending fractures always 6 subtriangles are generated
                 * for 3-crossings 7 subtriangles are generated
                 * for 4-crossings 8 subtriangles are generated
                 * and for two not-crossing fractures 10 subtriangles are generated
                 */
                if (lowerDim){
                    StandardVectorOfIntersections sortedPoints;
                    sortedPoints=Dumux::sortPoints(pointsNotOnCorners, *matrixElementIterator);
                    //TODO check here how the DFM is implemented (lower dim elements on the edges
                    assert(xfMiddle.size()<2);
                    if (xfMiddle.size()==1){
                        FV global(matrixElementIterator->geometry().global(xfMiddle[0]));
                        factory.insertVertex( global );
                        ++numberOfVertices;
                        newVertices.push_back( global );
                        int centerIndex=(numberOfVertices-1);
                        /*
                         * if the simplex corner is an old quadrilateral corner use the corresponding index
                         * else add a new vertex and use the new index
                         * keep in mind that this new index then also has to be used for the next simplex
                         * and there must not be inserted another vertex
                         */
                        std::vector<unsigned int> v(3);
                        int initialNotCornerIdx=-1;
                        //initial v[0] gets the center and keeps it always
                        v[0]=centerIndex;//at position zero always the new center node index is inserted
                        //initially v[1] check if sortedPoints[0] is a corner or not
                        global=matrixElementIterator->geometry().global(sortedPoints[0].intersection);
                        bool isCorner=false;
                        for (int k=0;k<4;++k){
                            if (Dumux::arePointsEqual(matrixElementIterator->geometry().local(matrixElementIterator->geometry().corner(k)),sortedPoints[0].intersection) ) {
                                v[1]= matrixGrid_.leafIndexSet().subIndex(*matrixElementIterator,k,dimw_);
                                isCorner=true;
                            }
                        }
                        if (!isCorner){
                            //test if this new vertex was already inserted from another macro quadrilateral element
                            bool alreadyInserted=false;
                            int oldIndex=matrixGrid_.leafIndexSet().size(dimw_)-1;
                            for (VerticesIterator nvIt=newVertices.begin();nvIt!=newVertices.end();++nvIt){
                                ++oldIndex;
                                if ( Dumux::arePointsEqual(*nvIt, global) ){
                                    alreadyInserted=true;
                                    break;
                                }
                            }
                            if (alreadyInserted) v[1]=oldIndex;
                            else{
                                factory.insertVertex(global);
                                ++numberOfVertices;
                                newVertices.push_back( global );
                                v[1]=(numberOfVertices-1);
                                initialNotCornerIdx=v[1];
                            }
                        }

                        for (int i=0;i<4+numberOfIP;i++){
                            int iPlusOne=( i<3+numberOfIP ? i+1 : 0);
                            global=matrixElementIterator->geometry().global(sortedPoints[iPlusOne].intersection);
                            isCorner=false;
                            for (int k=0;k<4;++k){
                                if (Dumux::arePointsEqual(matrixElementIterator->geometry().local(matrixElementIterator->geometry().corner(k)),sortedPoints[iPlusOne].intersection) ) {
                                    v[2]= matrixGrid_.leafIndexSet().subIndex(*matrixElementIterator,k,dimw_);
                                    isCorner=true;
                                }
                            }
                            if (!isCorner){
                                if (initialNotCornerIdx>=0) v[2]=initialNotCornerIdx;
                                else{
                                    //test if this new vertex was already inserted from another macro quadrilateral element
                                    bool alreadyInserted=false;
                                    int oldIndex=matrixGrid_.leafIndexSet().size(dimw_)-1;
                                    for (VerticesIterator nvIt=newVertices.begin();nvIt!=newVertices.end();++nvIt){
                                        ++oldIndex;
                                        if ( Dumux::arePointsEqual(*nvIt,global) ){
                                            alreadyInserted=true;
                                            break;
                                        }
                                    }
                                    if (alreadyInserted) {
                                        v[2]=oldIndex;
                                    }
                                    else{
                                        factory.insertVertex( global );
                                        ++numberOfVertices;
                                        newVertices.push_back( global );
                                        v[2]=(numberOfVertices-1);
                                    }

                                }
                            }
                            std::cout << "now regular simplex uncut element: "<< std::endl;
                            assert(v[0]!=v[1]);
                            assert(v[0]!=v[2]);
                            assert(v[1]!=v[2]);
                            factory.insertElement(Dune::GeometryType(simplex,2), v);
//                            globalElementPermeabilityVector_.push_back(globalElementMatrixPermeability);
                            //last iter v[2] is v[1] for the next simplex
                            v[1]=v[2];
                        }//end loop over simplices
                    }//end if (xfMiddle.size()==1)
                }//end if lower dimension grid creation

                else{//create equi-dimensional grid
                    StandardVectorOfIntersections sortedPoints;
                    bool isEndingFracture=false;
                    /*
                     * if (numberOfIP==2) implement the special case of a straight fracture with changing aperture" <<std::endl;
                     */
                    if (numberOfIP==1){
                        ++numberOfIP;
                        isEndingFracture=true;
                        pointsNotOnCorners.push_back(intersectionPoints[5]);
                    }
                    sortedPoints=Dumux::sortPoints(pointsNotOnCorners, *matrixElementIterator);
                    std::vector<unsigned int> vCrossing;
                    vCrossing.clear();
                    typedef typename std::vector<typename StandardVectorOfIntersections::value_type::FieldVector> StandardVectorOfFieldVectors;
                    StandardVectorOfFieldVectors explodedCenter;
                    explodedCenter.clear();
                    this->explodeCrossing(*matrixElementIterator,sortedPoints,xfMiddle[0],explodedCenter);
                    this->splitNodes(sortedPoints,xfMiddle[0]);

                    //debug output
//                    if (cutElementIdx==9){
                        std::cout << "sorted points: ";
                        for (int spIdx=0;spIdx<sortedPoints.size();++spIdx){
                            std::cout << sortedPoints[spIdx]<<"\t";
                        }
                        std::cout << std::endl;
//                    }
//                    FV oneNull(0.0);
//                    oneNull[0]=1.0;
//                    if (Dumux::arePointsEqual(sortedPoints[1].intersection,oneNull)){
//                        sortedPoints.push_back(sortedPoints[0]);
//                        sortedPoints.erase(sortedPoints.begin());
//                    }
//                    std::cout << "sorted points: ";
//                    for (int spIdx=0;spIdx<sortedPoints.size();++spIdx){
//                        std::cout << sortedPoints[spIdx]<<"\t";
//                    }
//                    std::cout << std::endl;


                    assert(xfMiddle.size()==1);
                    if (xfMiddle.size()==1){
                        //TODO use red corner crossing treatment
                        FV global(matrixElementIterator->geometry().global(explodedCenter[0]));
                        if (cutElementIdx==9) std::cout << "global: " << global << std::endl;
                        //check if already inserted
                        bool alreadyInserted=false;
                        int oldIndex=matrixGrid_.leafIndexSet().size(dimw_)-1;
                        for (VerticesIterator nvIt=newVertices.begin();nvIt!=newVertices.end();++nvIt){
                            ++oldIndex;
                            if ( Dumux::arePointsEqual(*nvIt, global) ){
                                alreadyInserted=true;
                                break;
                            }
                        }
                        std::vector<unsigned int> v(3);
                        if (alreadyInserted) v[0]=oldIndex;
                        else{//this was not already inserted
                            factory.insertVertex( global );
                            ++numberOfVertices;
                            std::cout << "F: " << (numberOfVertices-1) << std::endl;
                            newVertices.push_back( global );
                            //initial v[0] gets the center and keeps it always
                            v[0]=(numberOfVertices-1);//at position zero always the new center node index is inserted
                        }

                        /*
                         * if the simplex corner is an old quadrilateral corner use the corresponding index
                         * else add a new vertex and use the new index
                         */

                        int initialNotCornerIdx=-1;
                        vCrossing.push_back(v[0]);
                        //initially v[1] check if sortedPoints[0] is a corner or not
                        global=matrixElementIterator->geometry().global(sortedPoints[0].intersection);
                        bool iIsCorner=false;
                        for (int k=0;k<4;++k){
                            //helperfunction equalToCorner cannot be used because the index of the corner is also needed
                            if (Dumux::arePointsEqual(matrixElementIterator->geometry().local(matrixElementIterator->geometry().corner(k)),sortedPoints[0].intersection) ) {
                                v[1]= matrixGrid_.leafIndexSet().subIndex(*matrixElementIterator,k,dimw_);
                                iIsCorner=true;
                                break;
                            }
                        }
                        /*
                         * set iIsCorner to true also for projected intersection points of ending fractures
                         * so that it is never added
                         */
                        if (!(sortedPoints[0].apertureOnEdge>0.0)) iIsCorner=true;
                        if (!iIsCorner){
                            //test if this new vertex was already inserted from another macro quadrilateral element
                            bool alreadyInserted=false;
                            int oldIndex=matrixGrid_.leafIndexSet().size(dimw_)-1;
                            for (VerticesIterator nvIt=newVertices.begin();nvIt!=newVertices.end();++nvIt){
                                ++oldIndex;
                                if ( Dumux::arePointsEqual(*nvIt, global) ){
                                    alreadyInserted=true;
                                    break;
                                }
                            }
                            if (alreadyInserted) v[1]=oldIndex;
                            else{//this is not a corner and was not already inserted
                                factory.insertVertex(global);
                                ++numberOfVertices;
                                std::cout << "E: " << (numberOfVertices-1) << std::endl;
                                newVertices.push_back( global );
                                v[1]=(numberOfVertices-1);
                                initialNotCornerIdx=v[1];
                            }
                        }

                        /*
                         * 4 + number of intersections simplex matrix elements
                         * + number of intersections quadrilateral fracture elements
                         * numberOfIP is here minimum two (virtual/projected exit
                         * for ending fractures is also counted)
                         */
                        int numberOfSubelements=4+2*numberOfIP;
                        /*
                         * 4 simplex matrix elements
                         * + 1 quadrilateral fracture element
                         * + 1 quadrilateral matrix closure element
                         * + 1 because the virtual exit point should be skipped
                         */

                        if (isEndingFracture) numberOfSubelements=7;
                        /*
                         * add one (for the crossing element) to the total number of created elements
                         * TODO the case of a straight fracture which changes the aperture is not
                         * properly handled here
                         */
                        int centerIdx=0;
                        bool isFractureElement=false;
                        bool isEndingClosureElement=false;
                        int oldCenterZeroIdx=-1;
                        std::vector<unsigned int> vCube(4);
                        for (int i=0;i<numberOfSubelements;i++){
//                            if (cutElementIdx==9 && i>=3) break;
                            int iPlusOne=( i<(numberOfSubelements-1) ? i+1 : 0);
                            std::cout << "i: " << i << "\t +1: " << iPlusOne << std::endl;
                            /*
                             * initial check if i or (i+1) are corners
                             */
                            bool iPlusOneIsCorner=false;
                            iIsCorner=false;
                            for (int k=0;k<4;++k){
                                if (Dumux::arePointsEqual(matrixElementIterator->geometry().local(matrixElementIterator->geometry().corner(k)),sortedPoints[i].intersection) ) {
                                    iIsCorner=true;
                                }
                                if (Dumux::arePointsEqual(matrixElementIterator->geometry().local(matrixElementIterator->geometry().corner(k)),sortedPoints[iPlusOne].intersection) ) {
                                    v[2]= matrixGrid_.leafIndexSet().subIndex(*matrixElementIterator,k,dimw_);
                                    iPlusOneIsCorner=true;
                                }
                                //do not break before, because both indices have to be checked separately if they represent a corner
                                if (iIsCorner && iPlusOneIsCorner) break;
                            }
                            /*
                             * if i is a corner and (i+1) is the virtual exit
                             * then (i+1) is incremented by one (which is then again a corner)
                             * and isEndingClosureElement is set to true
                             */
                            if (iIsCorner && !iPlusOneIsCorner && !(sortedPoints[iPlusOne].apertureOnEdge>0.0)) {
                                isEndingClosureElement=true;
                                iPlusOne = ( iPlusOne<(numberOfSubelements-1) ? iPlusOne+1 : 0);
                                iPlusOneIsCorner=true;
                            }
                            else if (!iIsCorner && iPlusOneIsCorner && !(sortedPoints[i].apertureOnEdge>0.0)) {
//                                std::cout << "virtual Exit at i: " << std::endl;
                                ++i;
                                if (i==numberOfSubelements) break;
                                iIsCorner=true;
                                iPlusOne = ( i<(numberOfSubelements-1) ? i+1 : 0);
                                iPlusOneIsCorner=false;
                                for (int k=0;k<4;++k){
                                    if (Dumux::arePointsEqual(matrixElementIterator->geometry().local(matrixElementIterator->geometry().corner(k)),sortedPoints[iPlusOne].intersection) ) {
                                        v[2]= matrixGrid_.leafIndexSet().subIndex(*matrixElementIterator,k,dimw_);
                                        iPlusOneIsCorner=true;
                                        break;
                                    }
                                }
                            }
                            std::cout << "i: " << i << "\t " << sortedPoints[i] << "\n +1: " << iPlusOne << "\t "<< sortedPoints[iPlusOne]<< std::endl;
                            std::cout << "iIsCorner: " << iIsCorner << "\t iPlusOneIsCorner"<< iPlusOneIsCorner<<std::endl;

                            global=matrixElementIterator->geometry().global(sortedPoints[iPlusOne].intersection);
                            std::cout << "now HERE" <<std::endl;

                            if (!isEndingClosureElement){
                                std::cout << "now HERE B" <<std::endl;
                                if (!iPlusOneIsCorner){
                                    std::cout << "now HERE C" <<std::endl;
                                    if (initialNotCornerIdx>=0) {
                                        v[2]=initialNotCornerIdx;
                                        std::cout << "GG: " << v[2] << "\t sizeOfOldVertices: "<<matrixGrid_.leafIndexSet().size(dimw_)-1 << std::endl;
                                    }
                                    else{
                                        std::cout << "now HERE D" <<std::endl;
                                        if (!iIsCorner){
                                            std::cout << "now HERE E" <<std::endl;
                                            /*
                                             * both nodes on the boundary are not corners,
                                             * i.e., this must be a fracture element
                                             */
                                            isFractureElement=true;
                                            if (centerIdx<explodedCenter.size()-1){
                                                assert(centerIdx+1<explodedCenter.size());
                                                FV globalNextCenter(matrixElementIterator->geometry().global(explodedCenter[centerIdx+1]));

                                                //test if this new vertex was already inserted from another macro quadrilateral element
                                                bool alreadyInserted=false;
                                                int oldIndex=matrixGrid_.leafIndexSet().size(dimw_)-1;
                                                for (VerticesIterator nvIt=newVertices.begin();nvIt!=newVertices.end();++nvIt){
                                                    ++oldIndex;
                                                    if ( Dumux::arePointsEqual(*nvIt, globalNextCenter) ){
                                                        alreadyInserted=true;
                                                        break;
                                                    }
                                                }
                                                vCube[1]=v[0];
                                                if (centerIdx==0){
                                                    oldCenterZeroIdx=v[0];
                                                }
                                                if (!alreadyInserted) {
                                                factory.insertVertex( globalNextCenter );
                                                ++numberOfVertices;
                                                std::cout << "D: " << (numberOfVertices-1) << std::endl;
                                                newVertices.push_back( globalNextCenter );
                                                v[0]=(numberOfVertices-1);
                                                }
                                                else{
                                                    v[0]=oldIndex;
                                                }
                                                vCube[0]=v[0];
                                                vCrossing.push_back(v[0]);
                                                ++centerIdx;
                                            }
                                            else {//(centerIdx >= explodedCenter.size()-1)
                                                vCube[1]=v[0];
                                                assert(oldCenterZeroIdx>=0);
                                                vCube[0]=oldCenterZeroIdx;
                                                v[0]=oldCenterZeroIdx;
                                            }
                                        }
                                        else {// (i+1) is not a corner, but i is a corner
                                            isFractureElement=false;
                                        }
                                        //test if this new vertex was already inserted from another macro quadrilateral element
                                        bool alreadyInserted=false;
                                        int oldIndex=matrixGrid_.leafIndexSet().size(dimw_)-1;
                                        for (VerticesIterator nvIt=newVertices.begin();nvIt!=newVertices.end();++nvIt){
                                            ++oldIndex;
                                            if ( Dumux::arePointsEqual(*nvIt,global) ){
                                                alreadyInserted=true;
                                                break;
                                            }
                                        }
                                        if (alreadyInserted) {
                                            v[2]=oldIndex;
                                            std::cout << "CC: " << v[2] << "\t sizeOfOldVertices: "<<matrixGrid_.leafIndexSet().size(dimw_)-1 << std::endl;
                                        }
                                        else{
                                            factory.insertVertex( global );
                                            ++numberOfVertices;
                                            std::cout << "C: " << (numberOfVertices-1) << std::endl;
                                            newVertices.push_back( global );
                                            v[2]=(numberOfVertices-1);
                                        }
                                    }
                                }
                            }
                            else {//if (isEndingClosureElement){
                                std::cout << "now is ending closure element"<<std::endl;
                                if (centerIdx<explodedCenter.size()-1){
                                    assert(centerIdx+1<explodedCenter.size());
                                    FV globalNextCenter(matrixElementIterator->geometry().global(explodedCenter[centerIdx+1]));
                                    vCube[1]=v[0];
                                    if (centerIdx==0){
                                        oldCenterZeroIdx=v[0];
                                    }

                                    oldIndex=matrixGrid_.leafIndexSet().size(dimw_)-1;
                                    for (VerticesIterator nvIt=newVertices.begin();nvIt!=newVertices.end();++nvIt){
                                        ++oldIndex;
                                        if ( Dumux::arePointsEqual(*nvIt,globalNextCenter) ){
                                            alreadyInserted=true;
                                            break;
                                        }
                                    }
                                    if (alreadyInserted) {
                                        v[0]=oldIndex;
                                    }
                                    else{
                                       factory.insertVertex( globalNextCenter );
                                        ++numberOfVertices;
                                        newVertices.push_back( globalNextCenter );
                                        v[0]=(numberOfVertices-1);
                                        std::cout << "B: " << (numberOfVertices-1) << std::endl;
                                    }

                                    vCube[0]=v[0];
                                    vCrossing.push_back(v[0]);
                                    ++centerIdx;
                                }
                                else {
                                    vCube[1]=v[0];
                                    assert(oldCenterZeroIdx>=0);
                                    vCube[0]=oldCenterZeroIdx;
                                    v[0]=oldCenterZeroIdx;
                                }
                                //test if this new vertex was already inserted from another macro quadrilateral element
                                bool alreadyInserted=false;
                                int oldIndex=0;
                                for (VerticesIterator ovIt=oldVertices.begin();ovIt!=oldVertices.end();++ovIt){
                                    if ( Dumux::arePointsEqual(*ovIt,global) ){
                                        alreadyInserted=true;
                                        break;
                                    }
                                    ++oldIndex;
                                }
                                if (!alreadyInserted){
                                    oldIndex=matrixGrid_.leafIndexSet().size(dimw_)-1;
                                    for (VerticesIterator nvIt=newVertices.begin();nvIt!=newVertices.end();++nvIt){
                                        ++oldIndex;
                                        if ( Dumux::arePointsEqual(*nvIt,global) ){
                                            alreadyInserted=true;
                                            break;
                                        }
                                    }
                                }
                                if (alreadyInserted) {
                                    v[2]=oldIndex;
                                    std::cout << "AA: " << v[2] << "\t sizeOfOldVertices: "<<matrixGrid_.leafIndexSet().size(dimw_)-1 << std::endl;
                                }
                                else{
                                    factory.insertVertex( global );
                                    ++numberOfVertices;
                                    newVertices.push_back( global );
                                    v[2]=(numberOfVertices-1);
                                    std::cout << "A: " << v[2] << "\t sizeOfOldVertices: "<<matrixGrid_.leafIndexSet().size(dimw_)-1 << std::endl;
                                }
                            }

                            if (isFractureElement || isEndingClosureElement){//add a quadrilateral
                                vCube[3]=v[1];
                                vCube[2]=v[2];
                                if (insertCubes==true){
                                    if (isEndingClosureElement) {
                                        std::cout << "now isEndingClosureElement"<<std::endl;
                                        factory.insertElement(Dune::GeometryType(cube,2), vCube);
                                        for (int opIdx=0;opIdx<4;++opIdx){
                                            auto vPos=(vCube[opIdx] < oldVertices.size() ? oldVertices[(vCube[opIdx])] : newVertices[(vCube[opIdx]-oldVertices.size() )] );
                                            std::cout << vCube[opIdx] << "\t"<< vPos << "\t";
                                        }
                                        std::cout << std::endl;
                                    }
                                    else if( !createBrokenGrid) {
                                        std::cout << "now isFractureElement"<<std::endl;
                                        factory.insertElement(Dune::GeometryType(cube,2), vCube);
                                    }
                                }
                                else{
                                    std::cout << "now isEndingClosureElement simplices"<<std::endl;
                                    for (int opIdx=0;opIdx<4;++opIdx){
                                        auto vPos=(vCube[opIdx] < oldVertices.size() ? oldVertices[(vCube[opIdx])] : newVertices[(vCube[opIdx]-oldVertices.size() )] );
                                        std::cout << vCube[opIdx] << "\t"<< vPos << "\t";
                                    }
                                    std::cout << std::endl;
                                    /*
                                     * adding two simplices instead of one quadrilateral
                                     */
                                    Vertices cubePolygon(4);
                                    for (unsigned int idx=0;idx<4;++idx){
                                        if ( vCube[idx]<oldVertices.size() ){
                                            cubePolygon[idx]=oldVertices[(vCube[idx])];
                                        }
                                        else{
                                            cubePolygon[idx]=newVertices[(vCube[idx]-oldVertices.size())];
                                        }
                                    }

                                    std::vector<unsigned int> vSimplexA(3);
                                    std::vector<unsigned int> vSimplexB(3);
                                    for (int tempNode=0;tempNode<3;++tempNode){
                                        vSimplexA[tempNode]=vCube[tempNode];
                                        vSimplexB[tempNode]=vCube[tempNode+1];
                                    }
                                    if( isFractureElement && !createBrokenGrid) {
                                        factory.insertElement(Dune::GeometryType(simplex,2), vSimplexA);
                                        factory.insertElement(Dune::GeometryType(simplex,2), vSimplexB);
                                    }

                                    else if (isEndingClosureElement) {
                                        Vertices simplexPolygonA(3);
                                        for (unsigned int idx=0;idx<3;++idx){
                                            if (vSimplexA[idx]<oldVertices.size()){
                                                simplexPolygonA[idx]=oldVertices[(vSimplexA[idx])];
                                            }
                                            else{
                                                simplexPolygonA[idx]=newVertices[(vSimplexA[idx]-oldVertices.size())];
                                            }
                                        }
                                        Vertices simplexPolygonB(3);
                                        for (unsigned int idx=0;idx<3;++idx){
                                            if (vSimplexB[idx]<oldVertices.size()){
                                                simplexPolygonB[idx]=oldVertices[(vSimplexB[idx])];
                                            }
                                            else{
                                                simplexPolygonB[idx]=newVertices[(vSimplexB[idx]-oldVertices.size())];
                                            }
                                        }

                                        double sumSimplexAreas=Dumux::polygonArea(simplexPolygonA)+Dumux::polygonArea(simplexPolygonB);
                                        double cubeArea=Dumux::polygonArea(cubePolygon);
                                        /*
                                         * if the sum of the two simplex areas is larger than the
                                         * cube area, i.e., the cube is concave, the triangulation cut
                                         * has to be exactly the other one.
                                         */
                                        if ( sumSimplexAreas > cubeArea) {
                                            auto vSimplexATemp=vSimplexA;
                                            auto vSimplexBTemp=vSimplexB;
                                            vSimplexATemp[2]=vSimplexB[2];
                                            vSimplexBTemp[0]=vSimplexA[0];
                                            auto simplexPolygonATemp=simplexPolygonA;
                                            auto simplexPolygonBTemp=simplexPolygonB;
                                            simplexPolygonATemp[2]=simplexPolygonB[2];
                                            simplexPolygonBTemp[0]=simplexPolygonA[0];
                                            if (Dumux::polygonArea(simplexPolygonATemp)+Dumux::polygonArea(simplexPolygonBTemp)<sumSimplexAreas ){
                                                vSimplexA=vSimplexATemp;
                                                vSimplexB=vSimplexBTemp;
                                            }
                                        }
                                        std::cout << "SA: " << vSimplexA[0] << "\t" << vSimplexA[1] << "\t" << vSimplexA[2] << std::endl;
                                        std::cout << "SB: " << vSimplexB[0] << "\t" << vSimplexB[1] << "\t" << vSimplexB[2] << std::endl;
                                        for (int opIdx=0;opIdx<v.size();++opIdx){
                                            auto vPos=(v[opIdx] < oldVertices.size() ? oldVertices[(v[opIdx])] : newVertices[(v[opIdx]-oldVertices.size() )] );
                                            std::cout << v[opIdx] << "\t"<< vPos << "\t";
                                        }
                                        std::cout << std::endl;
                                        factory.insertElement(Dune::GeometryType(simplex,2), vSimplexA);
                                        factory.insertElement(Dune::GeometryType(simplex,2), vSimplexB);
                                    }
                                }//end insert two simplices instead of one cube

                                v[1]=v[2];
                                isFractureElement=false;
                                isEndingClosureElement=false;
                            }//end if (isFractureElement || isEndingClosureElement) and thus, add a quadrilateral
                            else{//add a simplex
                                std::cout << "now regular cut element: "<< std::endl;
                                for (int opIdx=0;opIdx<3;++opIdx){
                                    auto vPos=(v[opIdx] < oldVertices.size() ? oldVertices[(v[opIdx])] : newVertices[(v[opIdx]-oldVertices.size() )] );
                                    std::cout << v[opIdx] << "\t"<< vPos << "\t";
                                }
                                std::cout << std::endl;
                                assert(v[0]!=v[1]);
                                assert(v[0]!=v[2]);
                                assert(v[2]!=v[1]);
                                factory.insertElement(Dune::GeometryType(simplex,2), v);
                                std::cout << "this is a check line" << std::endl;
                                //last iter v[2] is v[1] for the next simplex
                                v[1]=v[2];
                            }
                        }//end loop over subelements
                        /*
                         * add the closing crossing element
                         */
                        if( !createBrokenGrid) {
                            if (explodedCenter.size()==3){
                                assert(explodedCenter.size()==vCrossing.size());
                                factory.insertElement(Dune::GeometryType(simplex,2), vCrossing);
                            }
                            else if (explodedCenter.size()==4){
                                assert(explodedCenter.size()==vCrossing.size());
                                auto tempEntryCrossing=vCrossing[2];
                                vCrossing[2]=vCrossing[3];
                                vCrossing[3]=tempEntryCrossing;
                                if (insertCubes)
                                    factory.insertElement(Dune::GeometryType(cube,2), vCrossing);
                                else {
                                    for (int iSIdx=0;iSIdx<2;++iSIdx){
                                        std::vector<unsigned int> vSimplexCrossing(3);
                                        for (int nIdx=0;nIdx<3;++nIdx){
                                            vSimplexCrossing[nIdx]=vCrossing[nIdx+iSIdx];
                                        }
                                        factory.insertElement(Dune::GeometryType(simplex,2), vSimplexCrossing);
                                    }
                                }
                            }
                        }
                    }//end if xfMiddle.size==1
                }//end else create equidim grid

            }//else treatment for cut elements
        }//end loop over all coarse quadrilateral elements
        }//end if treat cut elements
        std::cout << "this is another check line" << std::endl;
        factory.createGrid();
        grid->loadBalance();
        return (grid);
    }//end create function

//    GlobalElementPermeabilityVector getPermeabilityVector(){
//        return (globalElementPermeabilityVector_);
//    }
private:
    const HostGrid &matrixGrid_;
    const Map &intersectionPointsMap_;

    /*
     * splits the old node, which is an intersection with an edge and not a corner,
     * into two nodes. The smaller (order on the projected element boundary) one is
     * stored in first node and returned the other one is written into the oldNode
     * given in the function call.
     */
    template<typename StandardVectorOfIntersections>
    void splitNodes(StandardVectorOfIntersections &sortedPoints,
            const typename StandardVectorOfIntersections::value_type::FieldVector &center){

        typedef typename StandardVectorOfIntersections::value_type Intersection;
        typedef typename Intersection::FieldVector FV;
        StandardVectorOfIntersections sortedPointsNew;
        /*
         * iterate over all sorted points but skip corners
         * and projected/virtual intersections in elements
         * with ending fractures
         */
        for (typename StandardVectorOfIntersections::iterator it=sortedPoints.begin();it!=sortedPoints.end();++it ){
            if ( Dumux::equalToCorner((*it).intersection) || Dumux::arePointsEqual((*it).apertureOnEdge,-1.0)) {
                sortedPointsNew.push_back(*it);
                continue;
            }
            else{
                assert((*it).apertureOnEdge>0.0);
                FV oldNode=(*it).intersection;
                /*
                 * this aperture is a value with respect to the local
                 * coordinate system
                 */
                typename FV::value_type apertureOnEdge=(*it).apertureOnEdge;
                typename FV::value_type apertureGlobal=(*it).apertureGlobal;
                /*
                 * this is for visualization purposes of hydrocoin where the actual matrix grid needs to be finer
                 * than the aperture of the fracture
                 */
                apertureOnEdge*=artificialSmallAperture_;
                //add or subtract the projected fracture aperture to the old node
                Intersection firstNode;
                firstNode.intersection=oldNode;
                firstNode.apertureOnEdge=apertureOnEdge;
                firstNode.apertureGlobal=apertureGlobal;
                Intersection secondNode(firstNode);
                if(Dumux::arePointsEqual(oldNode[0],0.0)){
                    firstNode[1]+= (apertureOnEdge/2.0);
                    secondNode[1]-= (apertureOnEdge/2.0);
                }
                else if(Dumux::arePointsEqual(oldNode[0],1.0)){
                    firstNode[1]-= (apertureOnEdge/2.0);
                    secondNode[1]+= (apertureOnEdge/2.0);
                }
                else if(Dumux::arePointsEqual(oldNode[1],0.0)){
                    firstNode[0]-= (apertureOnEdge/2.0);
                    secondNode[0]+= (apertureOnEdge/2.0);
                }
                else {
                    firstNode[0]+= (apertureOnEdge/2.0);
                    secondNode[0]-= (apertureOnEdge/2.0);
                }
                sortedPointsNew.push_back(firstNode);
                sortedPointsNew.push_back(secondNode);
            }//end else only for points which are not corners
        }//end iteration over all sorted points
        sortedPoints.swap(sortedPointsNew);
    }//end splitNodes

    template<typename EG, typename StandardVectorOfIntersections>
    void explodeCrossing(const EG &eg, const StandardVectorOfIntersections &sortedPoints,
            const typename StandardVectorOfIntersections::value_type::FieldVector &centerOld,
            std::vector<typename StandardVectorOfIntersections::value_type::FieldVector> &explodedCrossingPoints){
        typedef typename StandardVectorOfIntersections::value_type::FieldVector FV;
        StandardVectorOfIntersections notOnCorners;
        //iterate over all sorted points but skip corners
        for (typename StandardVectorOfIntersections::const_iterator it=sortedPoints.begin();it!=sortedPoints.end();++it ){
            if ( !Dumux::equalToCorner((*it).intersection) ) {
                notOnCorners.push_back(*it);
            }
        }
        typename StandardVectorOfIntersections::value_type::FieldVector center=eg.geometry().global(centerOld);
        /*
         * iterate over all elements attached to this crossing
         */
        for (typename StandardVectorOfIntersections::iterator it=notOnCorners.begin();it!=notOnCorners.end();++it ){
            FV oldNode=(*it).intersection;
            /*
             * for every old node (intersection) find the global length of the corresponding edge
             */
            typename FV::value_type apertureI=(*it).apertureGlobal;
            /*
             * correct association of aperture
             * for the case of ending fracture with projected exit and no associated aperture
             */
            if (!((*it).apertureGlobal>0.0)){
                ++it;
                if (it == notOnCorners.end())  {it=notOnCorners.begin();}
                apertureI=(*it).apertureGlobal;
                if (it == notOnCorners.begin() ) {
                    it=notOnCorners.end();
                }
                --it;
            }//end correct association of aperture
            /*
             * this is for visualization purposes of hydrocoin where the actual matrix grid needs to be finer
             * than the aperture of the fracture
             */
            apertureI*=artificialSmallAperture_;
            assert(apertureI>0.0);
            /*
             * calculate the fracture direction vector
             */
            FV f(center);
            oldNode=eg.geometry().global(oldNode);
            f-=oldNode;
            /*
             * calculate two points, tmpA and tmpB,
             * which lie on the negative normal side of the two-dimensional fracture boundary
             * of the fracture element i
             */
            FV tmpA(Dumux::normal(f));
            tmpA*=(-0.5)*apertureI;
            tmpA+=center;
            FV tmpB(tmpA);
            tmpB+= f;
            /*
             * Go to the next element.
             * If the recent element is the last, the next is the first in the list.
             */
            ++it;
            if (it == notOnCorners.end())  {it=notOnCorners.begin();}

            /*
             * calculate the fracture direction vector
             * for the (i+1)th element
             */
            /*
             * correct association of aperture
             * for the case of ending fracture with projected exit and no associated aperture
             */
            apertureI=(*it).apertureGlobal;
            if (!((*it).apertureGlobal>0.0)){
                ++it;
                if (it == notOnCorners.end())  {it=notOnCorners.begin();}
                apertureI=(*it).apertureGlobal;
                if (it == notOnCorners.begin() ) {
                    it=notOnCorners.end();
                }
                --it;
            }//end correct association of aperture
            /*
             * this is for visualization purposes of hydrocoin where the actual matrix grid needs to be finer
             * than the aperture of the fracture
             */
            apertureI*=artificialSmallAperture_;
            assert(apertureI>0.0);
            f=center;
            f-=eg.geometry().global((*it).intersection);
            /*
             * calculate two points, tmpC and tmpD,
             * which lie on the positive normal side of the two-dimensional fracture boundary
             * of the fracture element (i+1)
             */
            FV tmpC(Dumux::normal(f));
            tmpC*=0.5*apertureI;
            tmpC+=center;
            FV tmpD(tmpC);
            tmpD+= f;
            /*
             * calculate the greenCorner in the positive normal direction of the fracture
             * the "real" geometrical intersection of neighboring fractures (with thickness)
             * gives the green corners. Use the intersectionOfLines function but do not test,
             * if the intersection is between the boundaries.
             */
            FV greenCorner;
            Dumux::intersectionOfLines(tmpA,tmpB,tmpC,tmpD,greenCorner);
            greenCorner=eg.geometry().local(greenCorner);
            explodedCrossingPoints.push_back(greenCorner);
            if (it == notOnCorners.begin() ) {
                it=notOnCorners.end();
            }
            --it;
        }//end iteration over all sorted points

        explodedCrossingPoints.insert(explodedCrossingPoints.begin(),explodedCrossingPoints.back());
        explodedCrossingPoints.pop_back();
        assert(explodedCrossingPoints.size()>1);
    }//end explodeCrossing


private:
    typename FV::value_type artificialSmallAperture_;
//    GlobalElementPermeabilityVector globalElementPermeabilityVector_;
};
#endif /* GRIDCREATOR_HH_ */
