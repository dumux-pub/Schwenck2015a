#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/quadraturerules.hh>
#include<dune/pdelab/common/geometrywrapper.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>

/** a local operator for solving the equation
 * TODO: doc me!
 */
template<typename B, typename Soil>
class MatrixLocalOperatorDarcy : //derived from the following and using the CRTP-Trick!
  public Dune::PDELab::NumericalJacobianApplyVolume<MatrixLocalOperatorDarcy<B, Soil> >,
  public Dune::PDELab::NumericalJacobianVolume<MatrixLocalOperatorDarcy<B, Soil> >,
  public Dune::PDELab::NumericalJacobianApplyBoundary<MatrixLocalOperatorDarcy<B, Soil> >,
  public Dune::PDELab::NumericalJacobianBoundary<MatrixLocalOperatorDarcy<B, Soil> >,
  public Dune::PDELab::FullVolumePattern,
  public Dune::PDELab::LocalOperatorDefaultFlags

  {
  public:
  // pattern assembly flags
  enum { doPatternVolume = true };
  // residual assembly flags
  enum { doAlphaVolume  = true };
  enum { doAlphaBoundary  = true };

  MatrixLocalOperatorDarcy (const B &b, const Soil &soil, unsigned int intorder=3)  // needs boundary cond. type
    :
    Dune::PDELab::NumericalJacobianApplyVolume<MatrixLocalOperatorDarcy<B, Soil> >(1e-1),
    Dune::PDELab::NumericalJacobianVolume<MatrixLocalOperatorDarcy<B, Soil> >(1e-1),
    Dune::PDELab::NumericalJacobianApplyBoundary<MatrixLocalOperatorDarcy<B, Soil> >(1e-1),
    Dune::PDELab::NumericalJacobianBoundary<MatrixLocalOperatorDarcy<B, Soil> >(1e-1),
        b_(b), soil_(soil), intorder_(intorder)
  {}

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {
      // extract some types
      typedef typename LFSU::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::DomainFieldType DF;
      typedef typename LFSU::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::RangeFieldType RF;
      typedef typename LFSU::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::JacobianType JacobianType;
      typedef typename LFSU::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::RangeType RangeType;
      typedef typename LFSU::Traits::SizeType size_type;

      // dimensions
      const int dim = EG::Geometry::dimension;
      const int dimw = EG::Geometry::dimensionworld;

      Dune::FieldMatrix<typename X::value_type,dim,dim> K=soil_.intrinsicPermeability();

      // select quadrature rule
      Dune::GeometryType gt = eg.geometry().type();
      const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder_);
      //the local operator is elementwise defined and therefore the loop over the quadrature points is elementwise
      for (typename Dune::QuadratureRule<DF,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it){
          // evaluate basis functions on reference element
          std::vector<RangeType> phi(lfsu.size());
          lfsu.finiteElement().localBasis().evaluateFunction(it->position(),phi);

          // evaluate gradient of basis functions on reference element
          std::vector<JacobianType> js(lfsu.size());
          lfsu.finiteElement().localBasis().evaluateJacobian(it->position(),js);
          // transform gradients from reference element to real element
          const Dune::FieldMatrix<DF,dimw,dim> jac = eg.geometry().jacobianInverseTransposed(it->position());
          std::vector<Dune::FieldVector<RF,dim> > gradphi(lfsu.size());
          for (size_type i=0; i<lfsu.size(); i++)
          {jac.mv(js[i][0],gradphi[i]);}
          // compute gradient of u
          Dune::FieldVector<RF,dim> gradu(0.0);
          for (size_type i=0; i<lfsu.size(); i++){
              gradu.axpy(x(lfsu,i),gradphi[i]);}

          RF factor = it->weight()*eg.geometry().integrationElement(it->position());
//          std::cout << "integration element standard: " << eg.geometry().integrationElement(it->position())<<std::endl;

          Dune::FieldVector<RF,dim> vector1(0.0);
          K.mv(gradu,vector1); //K * grad p
          for (size_type i=0; i<lfsu.size(); i++){
              r.accumulate(lfsu,i,( vector1 * gradphi[i])*factor);}

      }//end loop over quadrature points
  }//end alpha_volume

  // boundary integral
  // for Neumann b.c.
  template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_boundary (const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s) const
  {
      // some types
      typedef typename LFSV::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::DomainFieldType DF;
      typedef typename LFSV::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::RangeFieldType RF;
      typedef typename LFSV::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::RangeType RangeType;
      typedef typename LFSV::Traits::SizeType size_type;

      // dimensions
      const int dim = IG::dimension;

      // select quadrature rule for face
      Dune::GeometryType gtface = ig.geometryInInside().type();
      const Dune::QuadratureRule<DF,dim-1>&
      rule = Dune::QuadratureRules<DF,dim-1>::rule(gtface,intorder_);

      // loop over quadrature points and integrate normal flux
      for (typename Dune::QuadratureRule<DF,dim-1>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
      {
          // position of quadrature point in local coordinates of element
          Dune::FieldVector<DF,dim> local = ig.geometryInInside().global(it->position());
          // skip rest if we are on Dirichlet boundary
          if (!b_.isNeumann(ig,it->position())) continue;
          // evaluate basis functions at integration point
          std::vector<RangeType> phi(lfsv_s.size());
          lfsv_s.finiteElement().localBasis().evaluateFunction(local,phi);

          // evaluate flux boundary condition
          RF j=b_.template neumannFlux<RF>(ig,it->position());
//          std::cout << "Neumann flux: " << j << "\t at " << ig.geometry().global(it->position()) <<  std::endl;
          // integrate j
          RF factor = it->weight()*ig.geometry().integrationElement(it->position());
          for (size_type i=0; i<lfsv_s.size(); i++)
              {r_s.accumulate(lfsv_s,i, j*phi[i]*factor);}
        }
}

private:
  const B &b_;
  const Soil &soil_;
  unsigned int intorder_;
};
