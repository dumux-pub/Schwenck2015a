#ifndef MODIFIEDBASISFUNCTION_HH_
#define MODIFIEDBASISFUNCTION_HH_

namespace Dumux{



class edgeColoring
{
    typedef typename std::vector<int> Colors;
    typedef typename std::vector<int> SubpolygonIndices;
    typedef typename Colors::const_iterator ColorsIterator;
    typedef typename SubpolygonIndices::const_iterator SubpolygonIndicesIterator;
public:
    //constructor
    edgeColoring()
    {}

    template<typename IntersectionPoints, typename Subpolygons>
    void edgeColoringInitialize(const IntersectionPoints &intersectionPoints, const Subpolygons &subpolygons)
    {
        /*
         * store the edges of each node
         */
        this->storeSubpolygonsToNodes(subpolygons);
        /*
         * color the edges - here stored via the corners
         */
        this->setEdgeColors(intersectionPoints);
        /*
         *
         */
        this->setPolygonColors(subpolygons);
    }

    template<typename Subpolygons>
    void edgeColoringUpdate(const Subpolygons &subpolygons)
    {
        /*
         * store the edges of each node
         */
        this->storeSubpolygonsToNodes(subpolygons);
        /*
         *
         */
        this->setPolygonColors(subpolygons);
    }


    template<typename IntersectionPoints>
    void setEdgeColors(const IntersectionPoints &intersectionPoints){
        typedef typename IntersectionPoints::value_type::ctype ctype;
        vectorOfCornerColors_.resize(4);
        const Dune::ReferenceElement<ctype,2>& genericCube = Dune::ReferenceElements<ctype, 2>::cube();
        for (int dofIdx=0; dofIdx<4; dofIdx++){//TODO this is fixed for 4 dofs
            for (int ipIdx=0;ipIdx<4;++ipIdx){
                if ( Dumux::arePointsEqual( (intersectionPoints[ipIdx])[0],(genericCube.position( dofIdx, 2 ))[0] )
                        || Dumux::arePointsEqual( (intersectionPoints[ipIdx])[1],(genericCube.position( dofIdx, 2 ))[1] )
                ){
                    vectorOfCornerColors_[dofIdx].push_back( (intersectionPoints[ipIdx]).color );
                }
            }
        }
    }

    template<typename Subpolygons>
    void setPolygonColors(const Subpolygons &subpolygons){
        typedef typename Subpolygons::value_type::value_type::value_type ctype;
        vectorOfSubpolygonColors_.resize(subpolygons.size());
        const Dune::ReferenceElement<ctype,2>& genericCube = Dune::ReferenceElements<ctype, 2>::cube();
        typedef typename Subpolygons::const_iterator SpsIt;
        int subpolygonIdx=0;
        for (SpsIt spsit=subpolygons.begin(); spsit!=subpolygons.end(); spsit++){//iterate over all subpolygons
            for (int dofIdx=0; dofIdx<4; dofIdx++){//TODO this is fixed for 4 dofs
                /*
                 * iterate over all colors of every dof
                 * within this subpolygon
                 * and add them to this subpolygon
                 */
                if (Dumux::pointInPolygon(subpolygons[subpolygonIdx],genericCube.position( dofIdx, 2 ))){
                    for (ColorsIterator cIt= (vectorOfCornerColors_[dofIdx]).begin();
                            cIt!=(vectorOfCornerColors_[dofIdx]).end();++cIt){
                        vectorOfSubpolygonColors_[subpolygonIdx].push_back(*cIt);
                    }
                }
            }
            ++subpolygonIdx;
        }
    }

    template<typename Subpolygons>
    void storeSubpolygonsToNodes(const Subpolygons &subpolygons){
        vectorOfCornerSubpolygons_.resize(4);
        typedef typename Subpolygons::const_iterator SpsIt;
        typedef typename Subpolygons::value_type Subpolygon;
        typedef typename Subpolygon::value_type::value_type Scalar;
        typedef typename Subpolygon::const_iterator SpIt;
        const Dune::ReferenceElement<Scalar,2>& genericCube = Dune::ReferenceElements<Scalar, 2>::cube();

        for (int dofIdx=0; dofIdx<4; dofIdx++){//TODO this is fixed for 4 dofs
            int subpolygonIdx=0;
            for (SpsIt spsit=subpolygons.begin(); spsit!=subpolygons.end(); spsit++){//iterate over all subpolygons
                for (SpIt spit=(*spsit).begin();spit!=(*spsit).end();spit++ ){//iterate over all nodes of this subpolygon
                    if ( Dumux::arePointsEqual( (*spit)[0],(genericCube.position( dofIdx, 2 ))[0] )//test if at least one node is on an edge belonging to the the dof
                    || Dumux::arePointsEqual( (*spit)[1],(genericCube.position( dofIdx, 2 ))[1] )
                    ){
                        vectorOfCornerSubpolygons_[dofIdx].push_back(subpolygonIdx);
                        break;
                    }
                }
                ++subpolygonIdx;
            }
        }
    }

    bool dofIsOnEdge(const int &dofIdx, const int globalColor) const{
        for (ColorsIterator cIt= (vectorOfCornerColors_[dofIdx]).begin();
                cIt!=(vectorOfCornerColors_[dofIdx]).end();++cIt){
            if (*cIt==globalColor) return (true);
        }
        return (false);
    }
    bool polygonIsOnSpaceEdge(const int &subpolygonIdx, const int globalColor) const{
        for (ColorsIterator cIt= (vectorOfSubpolygonColors_[subpolygonIdx]).begin();
                cIt!=(vectorOfSubpolygonColors_[subpolygonIdx]).end();++cIt){
            if (*cIt==globalColor) return (true);
        }
        return (false);
    }
    bool polygonIsOnDofEdge(const int &dofIdx, const int &subpolygonIdx) const{
        for (SubpolygonIndicesIterator cIt= (vectorOfCornerSubpolygons_[dofIdx]).begin();
                cIt!=(vectorOfCornerSubpolygons_[dofIdx]).end();++cIt){
            if (*cIt==subpolygonIdx) return (true);
        }
        return (false);
    }

private:
    std::vector<Colors> vectorOfCornerColors_;
    std::vector<Colors> vectorOfSubpolygonColors_;
    std::vector<SubpolygonIndices> vectorOfCornerSubpolygons_;
};

/*
 * EG is here the entity (element) and not the wrapper
 */
template<typename MultiDomainGrid, typename EG, typename Map, typename LFSU>
class modifiedBasisFunction
{
    static const int dim_=MultiDomainGrid::dimension;
    static const int dimw_=MultiDomainGrid::dimensionworld;
    // select the two components (assume Galerkin scheme U=V)
    typedef typename LFSU::template Child<0>::Type LBGFS;       // LBGFS is the base GFS
    typedef typename LBGFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LBGFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainType D;
    typedef typename D::value_type Scalar;
    typedef typename LBGFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename LBGFS::Traits::SizeType sizeType;
    typedef typename MultiDomainGrid::Traits::GlobalIdSet::IdType IdType;
    typedef typename MultiDomainGrid::ctype ctype;
    typedef typename Dune::FieldVector<ctype,dimw_> FV;
    typedef typename std::vector<FV> Subpolygon;
    typedef typename std::vector<Subpolygon> Subpolygons;
    typedef typename std::vector<FV> PointsNotOnCorners;
    typedef typename std::vector<FV> StdVectorOfFieldVectors;
    typedef typename StdVectorOfFieldVectors::const_iterator StdVectorOfFieldVectorsIterator;
    typedef typename Map::mapped_type IntersectionPoints;
    typedef typename IntersectionPoints::value_type Intersection;
    typedef typename std::vector<Intersection> StandardVectorOfIntersections;
    typedef typename std::vector<Intersection>::const_iterator StandardVectorOfIntersectionsIterator;

public:
    modifiedBasisFunction (const MultiDomainGrid &multiDomainGrid, const EG &eg,const Map &intersectionPointsMap, const LFSU &lfsu, const bool dirty=false)
    : multiDomainGrid_(multiDomainGrid), eg_(eg), intersectionPointsMap_(intersectionPointsMap), lfsu_(lfsu),
      globalToLocalSubspace_(6,-1), localToGlobalSubspace_(6,-1), localSubspaceToGlobalColor_(6,-1),
      endingFracture_(false), specialTreatmentEndingFractureNodes_(4,false), numberOfSpaces_(0), numberOfSubpolygons_(0), dirty_(dirty)
    {
        typedef typename LFSU::template Child<0>::Type LGFSChild0;
        typedef typename LFSU::template Child<1>::Type LGFSChild1;
        typedef typename LFSU::template Child<2>::Type LGFSChild2;
        typedef typename LFSU::template Child<3>::Type LGFSChild3;
        typedef typename LFSU::template Child<4>::Type LGFSChild4;
        typedef typename LFSU::template Child<5>::Type LGFSChild5;
        const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();
        const LGFSChild1 &lgfsChild1 = lfsu.template child<1>();
        const LGFSChild2 &lgfsChild2 = lfsu.template child<2>();
        const LGFSChild3 &lgfsChild3 = lfsu.template child<3>();
        const LGFSChild4 &lgfsChild4 = lfsu.template child<4>();
        const LGFSChild5 &lgfsChild5 = lfsu.template child<5>();

        /*
         * For straight fractures the number of spaces is equal to
         * the number of sub-polygons.
         * For every other situation the number of sub-polygons is
         * number of spaces minus one.
         */
        numberOfSpaces_=(lgfsChild0.size()+lgfsChild1.size()+lgfsChild2.size()+lgfsChild3.size()+lgfsChild4.size()+lgfsChild5.size())/4;
        numberOfSubpolygons_=(numberOfSpaces_>2 ? numberOfSpaces_-1  : numberOfSpaces_);
        assert(numberOfSpaces_<6);

        /*
         * global to local space mapping
         * global is between 0 and 5
         * local is between 0 and (numberOfSpaces-1)
         * global 0 maps always to local 0
         * global color 0 maps always to local enriched space 1
         */
        globalToLocalSubspace_[0]=0;
        localToGlobalSubspace_[0]=0;
        int localSubspaceCounter=1;
        if (lgfsChild1.size()!=0) {
            globalToLocalSubspace_[1]=localSubspaceCounter;
            localToGlobalSubspace_[localSubspaceCounter]=1;
            localSubspaceToGlobalColor_[localSubspaceCounter]=0;
            ++localSubspaceCounter;
        }
        if (lgfsChild2.size()!=0) {
            globalToLocalSubspace_[2]=localSubspaceCounter;
            localToGlobalSubspace_[localSubspaceCounter]=2;
            localSubspaceToGlobalColor_[localSubspaceCounter]=1;
            ++localSubspaceCounter;
        }
        if (lgfsChild3.size()!=0) {
            globalToLocalSubspace_[3]=localSubspaceCounter;
            localToGlobalSubspace_[localSubspaceCounter]=3;
            localSubspaceToGlobalColor_[localSubspaceCounter]=2;
            ++localSubspaceCounter;
        }
        if (lgfsChild4.size()!=0) {
            globalToLocalSubspace_[4]=localSubspaceCounter;
            localToGlobalSubspace_[localSubspaceCounter]=4;
            localSubspaceToGlobalColor_[localSubspaceCounter]=3;
            ++localSubspaceCounter;
        }
        if (lgfsChild5.size()!=0) {
            globalToLocalSubspace_[5]=localSubspaceCounter;
            localToGlobalSubspace_[localSubspaceCounter]=5;
            localSubspaceToGlobalColor_[localSubspaceCounter]=4;;
            ++localSubspaceCounter;
        }

        /*
         * lgfs_size_ stores in the corresponding position the size of the
         * enriched subspace.
         */
        lgfs_size_.clear();
        lgfs_size_.push_back((lfsu_.template child<0>()).size());
        lgfs_size_.push_back((lfsu_.template child<1>()).size());
        lgfs_size_.push_back((lfsu_.template child<2>()).size());
        lgfs_size_.push_back((lfsu_.template child<3>()).size());
        lgfs_size_.push_back((lfsu_.template child<4>()).size());
        lgfs_size_.push_back((lfsu_.template child<5>()).size());
        //TODO here it is assumed that every space has the same size!

        const IdType elementId =multiDomainGrid_.globalIdSet().id(eg_);

        pointsNotOnCorners_.clear();
        pointsOnCorners_.clear();
        xfMiddle_.clear();
        IntersectionPoints intersectionPoints=(((*intersectionPointsMap_.find(elementId)).second));

        //extract matrix element local coordinates from intersectionPointsMap_(*ipmIt).second
        FV check(-1000.0);
        unsigned int i=0;
//        assert(!Dumux::arePointsEqual(intersectionPoints[1].intersection,check) );//TODO this indicates an ending fracture, which is not implemented yet
        endingFracture_=Dumux::arePointsEqual(intersectionPoints[1].intersection,check);
        while ( !Dumux::arePointsEqual(intersectionPoints[i].intersection,check) && i<4 ){
            if ( !Dumux::equalToCorner(intersectionPoints[i].intersection) ){
                pointsNotOnCorners_.push_back(intersectionPoints[i]);
            }
            else {
                pointsOnCorners_.push_back(intersectionPoints[i]);
//                std::cout << "intersectionPoints[i].intersection "<<intersectionPoints[i].intersection<<std::endl;
            }
            i++;
        }
        xfMiddle_.push_back(intersectionPoints[4].intersection);
        //only if field one is empty, field five contains an intersection point on the boundary
        //in all other cases there are two non-crossing fractures and position 5 stores the second midpoint.
        if ( !Dumux::arePointsEqual(intersectionPoints[5].intersection,check)){
            if (Dumux::arePointsEqual(intersectionPoints[1].intersection,check)){
                pointsNotOnCorners_.push_back(intersectionPoints[5]);
            }
            else {
                xfMiddle_.push_back(intersectionPoints[5].intersection);
            }
        }
        assert (xfMiddle_.size()==1);//TODO two parallel fracture are not yet implemented (case v,vi)

        StandardVectorOfIntersections sortedPoints;//TODO modifiy this for ending fractures!
        sortedPoints=Dumux::sortPoints(pointsNotOnCorners_, eg_);

        Subpolygon subpolygon;


        for (unsigned i=0;i<numberOfSubpolygons_;i++){
            subpolygon.clear();
            createSubPolygon(sortedPoints, xfMiddle_[0], i, subpolygon);
            subpolygons_.push_back( subpolygon );
        }

        //TODO check if this has to be adapted/recalculated if the subpolygons_ are changed later
        edgeColors_.edgeColoringInitialize(intersectionPoints,subpolygons_);

        /*
         * for ending fractures store the indices of the dofs which are not on
         * the edge which is cut by the fracture
         * only the intersectionPoints[0] has to be used because the other intersection point
         * is only the projected exit and not a real intersection
         */
        const Dune::ReferenceElement<RF,2>& genericCube = Dune::ReferenceElements<RF, 2>::cube();
        if (endingFracture_){
            int counter=0;
            for (sizeType dofIdx=0; dofIdx<lgfs_size_[0]; dofIdx++){
                if ( !Dumux::arePointsEqual( (intersectionPoints[0])[0],(genericCube.position( dofIdx, 2 ))[0] )
                && !Dumux::arePointsEqual( (intersectionPoints[0])[1],(genericCube.position( dofIdx, 2 ))[1] )){
                    specialTreatmentEndingFractureNodes_[dofIdx]=true;
                    counter++;
                }
            }
            assert(counter==2);
        }

        this->fillVectorOfSortedSubpolygons();

}//end constructor

    template<typename Phi,typename FV>
    Phi modify(Phi phi, const FV &xQP, const int numberOfSpace) const {//phi here is a copy and does not modify the phi in local operator
        //map numberOfSpace global into local
        const int numberOfSpaceLocal=globalToLocalSubspace_[numberOfSpace];
        for (sizeType i=0; i<lgfs_size_[numberOfSpace]; i++){
            //multiplier is equal to one if QP and DOF are in the same subploygon and zero else
            if (!Dumux::pointInPolygon(vectorOfSortedSubpolygons_[numberOfSpaceLocal][i],xQP)){
                phi[i]*=0.0;
            }
        }// end loop over every dof (node) to manipulate the basisfunctions
        return (phi);
    }

    template<typename Phi, typename FV>
    Phi jump(Phi phi, const FV &xQP, const int numberOfSpace ) const {//phi here is a copy and does not modify the phi in local operator
        const int numberOfSpaceLocal=globalToLocalSubspace_[numberOfSpace];
        //TODO make phiEnriched known in this class,
        //so that there is not the possibility that it gets changed in the local operator and then a different jump is computed
        //is this possible at all? because phiEnriched may change elementwise!?

        for (sizeType i=0; i<lgfs_size_[numberOfSpace]; i++){
            /*
             * the jump is zero if the evaluation point is not part of the subpolygon boundary
             * or if the basis is not modified or set to always zero for ending fractures
             */
            if ( (!Dumux::pointInPolygon(vectorOfSortedSubpolygons_[numberOfSpaceLocal][i],xQP))
                    /*
                     * this is only necessary for the unmodified basis which has always support on the full reference element
                     * the basis with no support anyhow gets zero
                     */
                    || (endingFracture_ && specialTreatmentEndingFractureNodes_[i])
            ){
                phi[i]*=0.0;
            }
            //if the evaluation point lies on the boundary the jump is the positive/negative value at that point
            //depending on the normal direction and the number of the space
            else {
                FV normal=fN(xQP);
                normal*=normalEpsilon<Scalar>();
                normal+=xQP;
                if ( Dumux::pointInPolygon(vectorOfSortedSubpolygons_[numberOfSpaceLocal][i],normal) ){
                    phi[i]*=(-1.0);
                }
            }
        }
        return (phi);
    }

    template<typename Phi, typename FV>
    Phi average(Phi phi, const FV &xQP, const int numberOfSpace) const {//phi here is a copy and does not modify the phi in local operator
        const int numberOfSpaceLocal=globalToLocalSubspace_[numberOfSpace];
        for (sizeType i=0; i<lgfs_size_[numberOfSpace]; i++){
            //the average is zero if the evaluation point is not part of the subpolygon boundary
            if ( !Dumux::pointInPolygon(vectorOfSortedSubpolygons_[numberOfSpaceLocal][i],xQP) ){
                phi[i]*=0.0;
            }
            /*
             * if the node is not modified for an ending fracture the average is the value itself
             * if the evaluation point lies on the boundary the standard average is calculated
             */
            else if ( !endingFracture_ || (endingFracture_ && !specialTreatmentEndingFractureNodes_[i])){
                phi[i]*=(0.5);
            }
        }
            //   phiEnrichedAverage[i]=phiEnriched[i]*HDOF_[i]*(-2.0)*DOF_area_[i];
            //alternative average weighted with the area of the triangle/trapezium
        return (phi);
    }

    template<typename Phi,typename FV>
    Phi phiPlus(Phi phi, const FV &xQP, const int numberOfSpace) const {//phi here is a copy and does not modify the phi in local operator
        //map numberOfSpace global into local
        const int numberOfSpaceLocal=globalToLocalSubspace_[numberOfSpace];
        FV normal=fN(xQP);
        normal*=normalEpsilon<Scalar>();
        normal+=xQP;
        for (sizeType i=0; i<lgfs_size_[numberOfSpace]; i++){
            //multiplier is equal to one if QP and DOF are in the same subploygon and zero else
            if (!Dumux::pointInPolygon(vectorOfSortedSubpolygons_[numberOfSpaceLocal][i],normal)){
                phi[i]*=0.0;
            }
        }// end loop over every dof (node) to manipulate the basisfunctions
        return (phi);
    }
    template<typename Phi,typename FV>
    Phi phiMinus(Phi phi, const FV &xQP, const int numberOfSpace) const {//phi here is a copy and does not modify the phi in local operator
        //map numberOfSpace global into local
        const int numberOfSpaceLocal=globalToLocalSubspace_[numberOfSpace];
        FV normal=fN(xQP);
        normal*=(-1.0)*normalEpsilon<Scalar>();
        normal+=xQP;
        for (sizeType i=0; i<lgfs_size_[numberOfSpace]; i++){
            //multiplier is equal to one if QP and DOF are in the same subploygon and zero else
            if (!Dumux::pointInPolygon(vectorOfSortedSubpolygons_[numberOfSpaceLocal][i],normal)){
                phi[i]*=0.0;
            }
        }// end loop over every dof (node) to manipulate the basisfunctions
        return (phi);
    }


    /*
     * Find closest point between line spanned by a,b and point c.
     * Here a is always the centre, b is one point of pointsNotOnCorner and
     * c is the local point to which the normal vector is calculated.
     */
    template<typename FV>
    FV fN(const FV &local) const {
        FV normal;

        if (dirty_){
            //        std::cout << "local: " << local << std::endl;
            for (int k=0;k<subpolygons_.size();++k){
                for (int pcIdx=0;pcIdx<subpolygons_[k].size();++pcIdx){
                    //                std::cout << "check polygon: " << subpolygons_[k][pcIdx] <<std::endl;
                    int pcIdxNext= (pcIdx<subpolygons_[k].size()-1 ? pcIdx+1: 0);
                    //                if ( Dumux::pointOnLine(eg_.geometry().global(subpolygons_[k][pcIdx]),eg_.geometry().global(subpolygons_[k][pcIdxNext]),eg_.geometry().global(local)) ){
                    if ( Dumux::arePointsEqual(subpolygons_[k][pcIdx],local) ){
                        //                    FV ab=eg_.geometry().global(subpolygons_[k][pcIdx]);
                        FV ab=subpolygons_[k][pcIdx];
                        ab*=-1.0;
                        if (!Dumux::equalToCorner(subpolygons_[k][pcIdxNext])){
                            //                        ab+=eg_.geometry().global(subpolygons_[k][pcIdxNext]);
                            ab+=subpolygons_[k][pcIdxNext];
                            //                        std::cout << "neighbour point: " << subpolygons_[k][pcIdxNext] <<std::endl;
                        }
                        else {
                            int pcIdxPrevious= (pcIdx==0 ? subpolygons_[k].size()-1: pcIdx-1);
                            assert(!Dumux::equalToCorner(subpolygons_[k][pcIdxPrevious]));
                            //                        ab+=eg_.geometry().global(subpolygons_[k][pcIdxPrevious]);
                            ab+=subpolygons_[k][pcIdxPrevious];
                            //                        std::cout << "neighbour point: " << subpolygons_[k][pcIdxPrevious] <<std::endl;
                        }
                        normal[0]=ab[1];
                        normal[0]*=-1.0;
                        normal[1]=ab[0];
                        normal/=normal.two_norm();
                        return (normal);
                    }
                }
            }
        }
        //if xQP was not in one of the polygons then use old calculation
        StandardVectorOfIntersections b;
        b.clear();
        assert(pointsNotOnCorners_.size()+pointsOnCorners_.size()>0);
        b= (pointsNotOnCorners_.size()!=0 ? pointsNotOnCorners_ : pointsOnCorners_);
        if (pointsNotOnCorners_.size()!=0){
            b.insert (b.end(),pointsOnCorners_.begin(),pointsOnCorners_.end());
        }

        for (int k=0;k<b.size();k++){
            if ( Dumux::pointOnLine(xfMiddle_[0],b[k].intersection,local) ){
                FV ab=b[k].intersection-xfMiddle_[0];
                /*
                 * the orientation of the normal vector is arbitrary!
                 * but always mathematically positive oriented, i.e., pointing counter-clockwise
                 * because one point always lies on the boundary of the unit square and the other point inside.
                 */
                normal[0]=ab[1];
                normal[0]*=-1.0;
                normal[1]=ab[0];
                normal/=normal.two_norm();
                break;
            }//end if
        }//end loop over pointsNotOnCorners
        return (normal);
    }//end fN


    /*
     * The normalEpsilon has to be larger than the equalityEpsilon, because otherwise the decision if one point
     * lies on the positive or negative side of a fracture is not unique.
     * This function has to be public, because it is also used in MatrixSolution.hh.
     */
    template<typename Scalar>
    Scalar normalEpsilon() const {
        Scalar equalityEpsilon=Dumux::equalityEpsilon<Scalar>();
        equalityEpsilon*=10.0;
        return (equalityEpsilon);
    }

    private:

    /*
     * This functions creates a subpolygon by "walking" on the references element along the faces,
     * starting at (0,0). The i'th subpolygon is spanned by the i'th and the (i+1)'th point in sortedPoints
     *  which are not corners, the corner points in between and the midpoint. The zero'th point in
     *  sortedPoints, which is not a corner is defined as also the last point in sortedPoints, which is not a corner.
     *  A special case occurs if pointsOnCorners_.size!=0, i.e., the fracture cuts through a corner.
     */
    template<typename PolygonCornerVector, typename StdVecOfIntersections>
    void createSubPolygon(const StdVecOfIntersections &sortedPoints,
            const FV &midpoint,//typename StdVecOfIntersections::value_type::FieldVector
            const unsigned numberOfSubpolygon, PolygonCornerVector &polygonCornerVector){
        /*
         * sortedPoints.size()+pointsOnCorners_.size() -4 is the number of subpolygons available
         */
//        for (typename SortedPoints::const_iterator it=sortedPoints.begin();it!=sortedPoints.end();it++){
//            std::cout << "corner: " << *it << std::endl;
//        }
//        std::cout << "pointsOnCorners_.size(): " << pointsOnCorners_.size() << std::endl;

        assert(sortedPoints.size()+pointsOnCorners_.size()-numberOfSubpolygon>=5);
        typedef typename StdVecOfIntersections::const_iterator StdVecOfIntersectionsIterator;
//        typedef typename StdVecOfIntersections::value_type Intersection;
//        typedef typename Intersection::FieldVector FV;
//        typedef typename std::vector<FV> StdVecOfFV;
//        typedef typename StdVecOfFV::const_iterator StdVecOfFVIterator;
        int counter=0;
        if (numberOfSubpolygon==0){
            std::vector<FV> rest;
            for (StdVecOfIntersectionsIterator spit=sortedPoints.begin();spit!=sortedPoints.end();spit++){
                if (counter==0) {
                    polygonCornerVector.push_back((*spit).intersection);
                }
                else {
                    rest.push_back((*spit).intersection);
                }
                if ( !Dumux::equalToCorner((*spit).intersection) ) {
                    counter++;
                    rest.clear();
                    rest.push_back((*spit).intersection);
                }
                else {
                    for (StandardVectorOfIntersectionsIterator pocit=pointsOnCorners_.begin();pocit!=pointsOnCorners_.end();pocit++) {
                        if ( Dumux::arePointsEqual((*spit).intersection,(*pocit).intersection) ){
                            counter++;
                            rest.clear();
                            rest.push_back((*spit).intersection);
                            break;
                        }
                    }
                }
            }//end iteration over sortedPoints entries

            polygonCornerVector.push_back(midpoint);
            for (StdVectorOfFieldVectorsIterator spit=rest.begin();spit!=rest.end();spit++){
                polygonCornerVector.push_back(*spit);
            }

        }//end if-case zero'th subpolygon
        else {
            for (StdVecOfIntersectionsIterator spit=sortedPoints.begin();spit!=sortedPoints.end();spit++){
                /*
                 * Check if the point is not a corner
                 * or if the corner is contained in the pointsOnCorners_ vector
                 * and then increase the counter by one.
                 */
                if ( !Dumux::equalToCorner((*spit).intersection) ) {
                    counter++;
                }
                else {
                    for (StdVecOfIntersectionsIterator pocit=pointsOnCorners_.begin();pocit!=pointsOnCorners_.end();pocit++) {
                        if ( Dumux::arePointsEqual((*spit).intersection,(*pocit).intersection) ){
                            counter++;
                            break;
                        }
                    }
                }

                //if the counter - numbeOfSubpolygon == zero then the numberOfSubpolgon'th subpolygon is found
                //and the points are added until the next none-corner point is found
                if (counter-numberOfSubpolygon==0){
                    polygonCornerVector.push_back((*spit).intersection);
                }

                else if (counter-numberOfSubpolygon==1){//the last point is added to the polgonCornerPointVector. And the midpoint to close the polygon.
                    polygonCornerVector.push_back((*spit).intersection);
                    polygonCornerVector.push_back(midpoint);
                    counter++;
                }
            }//end iteration over entries in sortetPoints vector
        }
    }//end void function createSubPolygon
public:
    template<typename FVLocal>
    void insertSubpolygonPoint(const FVLocal &edgeIntersection, const FVLocal &xQP){
//        std::cout << "edge intersection " << edgeIntersection <<std::endl;
        //find the polygons (always 2) to which the point has to be added
        for (int k=0;k<subpolygons_.size();++k){
            if (Dumux::pointInPolygon(subpolygons_[k],edgeIntersection)){
//                std::cout << "edge intersection " << edgeIntersection << " is in this subpolygon." <<std::endl;
                /*
                 * add the point at appropriate position
                 * for the first additional node this is between the center
                 * and the edgeIntersection.
                 * for every further additional node this has to be in between either
                 * the center and a new node, the edge intersection and a new node or two
                 * new nodes.
                 * The function "closestPointOnLine" cannot be used in local coordinates
                 * because of the non-linear transformation.
                 */
                Subpolygon newSubpolygon;
//                std::cout << "------------old subpolygon: "<< std::endl;
                for (int pcIdx=0;pcIdx<subpolygons_[k].size();++pcIdx){
                    newSubpolygon.push_back(subpolygons_[k][pcIdx]);
//                    std::cout << subpolygons_[k][pcIdx] << std::endl;
                    int pcIdxNext= (pcIdx<subpolygons_[k].size()-1 ? pcIdx+1: 0);
                    if ( Dumux::pointOnLine(eg_.geometry().global(subpolygons_[k][pcIdx]),eg_.geometry().global(subpolygons_[k][pcIdxNext]),eg_.geometry().global(xQP)) ){
//                        std::cout << "insert now the new point: "<<xQP << std::endl;
                        newSubpolygon.push_back(xQP);
                    }
                }
                subpolygons_[k].swap(newSubpolygon);
                //Debug outout
//                std::cout << "------------new subpolygon: "<< std::endl;
//                for (int pcIdx=0;pcIdx<subpolygons_[k].size();++pcIdx){
//                    std::cout << subpolygons_[k][pcIdx] << std::endl;
//                }
                edgeColors_.edgeColoringUpdate(subpolygons_);
            }
        }
        fillVectorOfSortedSubpolygons();
    }

    void createSimplicesFromSubpolygons(){
        //divide every subpolygon in simplices
//        std::cout << "number of subpolygons: " << subpolygons_.size()<<std::endl;
        for (int spIdx=0; spIdx<subpolygons_.size(); ++spIdx){
            auto subpolygonArea=Dumux::polygonArea(subpolygons_[spIdx]);
//            std::cout << "subpolygon: " << spIdx <<"---------------------"<< std::endl;
            int numberOfCornersInsideReferenceCube=0;
            int numberOfCornerOnReferenceCube=0;
            int numberOfSimplices=0;
            int initialPointIdx=-1;
            int middleCornerIdx=-1;
            int numberOfPointsInSubpolygon=subpolygons_[spIdx].size();
            Subpolygon simplex;
//            std::cout<<subpolygons_[spIdx].size() << " corners in subpolygon: \n";
            for (int cIdx=0;cIdx<numberOfPointsInSubpolygon;++cIdx){
//                std::cout << "\t"<<subpolygons_[spIdx][cIdx];
                if (!Dumux::arePointsEqual(subpolygons_[spIdx][cIdx][0],0.0)
                && !Dumux::arePointsEqual(subpolygons_[spIdx][cIdx][0],1.0)
                && !Dumux::arePointsEqual(subpolygons_[spIdx][cIdx][1],0.0)
                && !Dumux::arePointsEqual(subpolygons_[spIdx][cIdx][1],1.0) ){
                    ++numberOfCornersInsideReferenceCube;
                    ++numberOfSimplices;
                    if (initialPointIdx==-1){
                        initialPointIdx=cIdx;
                    }
                }
                else if(Dumux::equalToCorner(subpolygons_[spIdx][cIdx])){
                    ++numberOfCornerOnReferenceCube;
                    ++numberOfSimplices;
                    //the middle corner is always the second consecutive corner
                    if(middleCornerIdx==-1){
                        int tempMiddleCornerIdx1=(cIdx+1) % numberOfPointsInSubpolygon;
                        int tempMiddleCornerIdx2=(cIdx+2) % numberOfPointsInSubpolygon;
                        if (Dumux::equalToCorner(subpolygons_[spIdx][tempMiddleCornerIdx1]) && Dumux::equalToCorner(subpolygons_[spIdx][tempMiddleCornerIdx2])){
                            middleCornerIdx=tempMiddleCornerIdx1;
                        }
                    }
                }
            }
//            std::cout << std::endl;
//            std::cout << "numberOfCornerOnReferenceCube: " << numberOfCornerOnReferenceCube<< "\t dirty: " << dirty_ << std::endl;
            if (numberOfCornerOnReferenceCube==3 && dirty_){//dirty new alternative?!
                //TODO check for problems for concave shaped polygons
                simplex.push_back(subpolygons_[spIdx][middleCornerIdx]);
                simplex.push_back(subpolygons_[spIdx][middleCornerIdx]);
                simplex.push_back(subpolygons_[spIdx][middleCornerIdx]);
                int centerIdx=(middleCornerIdx+2+(numberOfPointsInSubpolygon-3)/2) % numberOfPointsInSubpolygon;
                //creating sub-triangles in two steps
                for (int stepIdx=0;stepIdx<2;++stepIdx){
                int simplexIdx1=centerIdx;
                    simplex[1]=subpolygons_[spIdx][simplexIdx1];
                    const int nOfTrianglesForThisHalf=(numberOfSimplices+stepIdx)/2;//for odd number of triangles always the first loop gets one triangle less
                    for (int triangleIdx=0;triangleIdx<nOfTrianglesForThisHalf;++triangleIdx){
                        if(stepIdx==0){
                            simplexIdx1=(centerIdx+triangleIdx) % numberOfPointsInSubpolygon;
                            simplex[1]=subpolygons_[spIdx][simplexIdx1];
                            int tempIdx=(simplexIdx1+1) % numberOfPointsInSubpolygon;
                            simplex[2]=subpolygons_[spIdx][tempIdx];

                        }
                        else{
                            simplexIdx1=((centerIdx-triangleIdx)<0 ? numberOfPointsInSubpolygon+(centerIdx-triangleIdx) : (centerIdx    -triangleIdx));
                            simplex[1]=subpolygons_[spIdx][simplexIdx1];
                            int tempIdx=(simplexIdx1==0 ? numberOfPointsInSubpolygon-1 : simplexIdx1-1 );
                            simplex[2]=subpolygons_[spIdx][tempIdx];
                        }
                        assert(simplex.size()==3);
                        simplices_.push_back(simplex);
                        const GT subSimplex(simplex_,2);
                        auto simplexGlobal(simplex);
                        for (int c=0;c<3;++c){
                            simplexGlobal[c]=eg_.geometry().global(simplex[c]);
                        }
                        ElementGeometry subElement(subSimplex,simplexGlobal);
                        subSimplices.push_back(subElement);
                    }
                }
            }
//            else if (numberOfCornerOnReferenceCube==2){
//                //TODO check for problems for concave shaped polygons
//
//            }
            else{
                simplex.clear();
                //initial simplex is always trivial
                assert(initialPointIdx>-1);
                simplex.push_back(subpolygons_[spIdx][initialPointIdx]);
                bool backwards=false;
                int nextIdx=(initialPointIdx+1) % numberOfPointsInSubpolygon;
                /*
                 * if the next point does not lie on the reference cube boundary
                 * choose the other direction
                 */
                if (!Dumux::arePointsEqual(subpolygons_[spIdx][nextIdx][0],0.0)
                && !Dumux::arePointsEqual(subpolygons_[spIdx][nextIdx][0],1.0)
                && !Dumux::arePointsEqual(subpolygons_[spIdx][nextIdx][1],0.0)
                && !Dumux::arePointsEqual(subpolygons_[spIdx][nextIdx][1],1.0) ){
                    backwards=true;
                    nextIdx=(initialPointIdx==0 ? numberOfPointsInSubpolygon-1 : initialPointIdx-1);
                }
                simplex.push_back(subpolygons_[spIdx][nextIdx]);
                if (backwards){
                    nextIdx=(nextIdx==0 ? numberOfPointsInSubpolygon-1 : nextIdx-1);
                }
                else {
                    nextIdx=(nextIdx+1) % numberOfPointsInSubpolygon;
                }
                simplex.push_back(subpolygons_[spIdx][nextIdx]);
                simplices_.push_back(simplex);
                const GT subSimplex(simplex_,2);
                auto simplexGlobal(simplex);
                for (int c=0;c<3;++c){
                    simplexGlobal[c]=eg_.geometry().global(simplex[c]);
                }
                assert(simplex.size()==3);
                ElementGeometry subElement(subSimplex,simplexGlobal);
                subSimplices.push_back(subElement);
                /*
                 * for convex subpolygons triangulation is trivial
                 * as well as triangulation if only two simplices have
                 * to be created
                 */

                if (Dumux::isConvex(subpolygons_[spIdx]) || numberOfSimplices==2){
                    //                std::cout << "---------------convex!"<<std::endl;
                    for (int sIdx=1; sIdx<numberOfSimplices; ++sIdx){
                        simplex[1]=simplex[0];
                        if(backwards){
                            initialPointIdx=(initialPointIdx+1) % numberOfPointsInSubpolygon;
                        }
                        else{
                            initialPointIdx=(initialPointIdx==0 ? numberOfPointsInSubpolygon-1 : initialPointIdx-1);
                        }
                        simplex[0]=subpolygons_[spIdx][initialPointIdx];
                        simplices_.push_back(simplex);
                        const GT subSimplex(simplex_,2);
                        auto simplexGlobal(simplex);
                        for (int c=0;c<3;++c){
                            simplexGlobal[c]=eg_.geometry().global(simplex[c]);
                        }
                        ElementGeometry subElement(subSimplex,simplexGlobal);
                        subSimplices.push_back(subElement);
                        //                        std::cout << sIdx << "th simplex: " << simplex[0] <<"\t" << simplex[1]<<"\t" << simplex[2]<<"\t area: "<< Dumux::polygonArea(simplices_[sIdx]) <<std::endl;
                    }
                }//end isConvex
                else{//is not convex
                    for (int sIdx=1; sIdx<numberOfSimplices; ++sIdx){
                        /*
                         * if there is no intersection between the boundary edge
                         * and the newly created edge inside the polygon keep going as for convex
                         * subpolygon
                         */
                        int tempInitialPointIdx=initialPointIdx;
                        if(backwards){
                            tempInitialPointIdx=(tempInitialPointIdx+1) % numberOfPointsInSubpolygon;
                        }
                        else{
                            tempInitialPointIdx=(tempInitialPointIdx==0 ? numberOfPointsInSubpolygon-1 : tempInitialPointIdx-1);
                        }
                        Subpolygon tempSimplex;
                        tempSimplex.push_back(subpolygons_[spIdx][tempInitialPointIdx]);
                        tempSimplex.push_back(simplex[0]);
                        tempSimplex.push_back(simplex[2]);
                        FV intersectionPoint;
                        if (!Dumux::intersectionOfLines(simplex[1],simplex[0],simplex[2],subpolygons_[spIdx][tempInitialPointIdx],intersectionPoint    ) ){
                            if (Dumux::polygonArea(tempSimplex)>(5.0e-3*subpolygonArea)
                                    || numberOfCornerOnReferenceCube==1){//
                                initialPointIdx=tempInitialPointIdx;
                                simplex[1]=simplex[0];
                                simplex[0]=subpolygons_[spIdx][initialPointIdx];
                            }
                            else {//test the next and choose the one with the larger area
                                auto thisSimplexArea=Dumux::polygonArea(tempSimplex);
                                auto nextTempSimplex(simplex);
                                nextTempSimplex[1]=simplex[2];
                                auto nextTempIdx(nextIdx);
                                if (backwards){
                                    nextTempIdx=(nextIdx==0 ? numberOfPointsInSubpolygon-1 : nextIdx-1);
                                }
                                else {
                                    nextTempIdx=(nextIdx+1) % numberOfPointsInSubpolygon;
                                }
                                nextTempSimplex[2]=subpolygons_[spIdx][nextTempIdx];
                                assert(Dumux::polygonArea(nextTempSimplex)>(5.0e-3*subpolygonArea));
                                if (thisSimplexArea<Dumux::polygonArea(nextTempSimplex)){//reverse
                                    nextIdx=nextTempIdx;
                                    simplex=nextTempSimplex;
                                }
                                else{//do the same as in the standard if above
                                    initialPointIdx=tempInitialPointIdx;
                                    simplex[1]=simplex[0];
                                    simplex[0]=subpolygons_[spIdx][initialPointIdx];
                                }
                            }
                        }
                        //special case for non-convex polygon
                        else{
                            simplex[1]=simplex[2];
                            if (backwards){
                                nextIdx=(nextIdx==0 ? numberOfPointsInSubpolygon-1 : nextIdx-1);
                            }
                            else {
                                nextIdx=(nextIdx+1) % numberOfPointsInSubpolygon;
                            }
                            simplex[2]=subpolygons_[spIdx][nextIdx];
                        }
                        simplices_.push_back(simplex);
                        const GT subSimplex(simplex_,2);
                        auto simplexGlobal(simplex);
                        for (int c=0;c<3;++c){
                            simplexGlobal[c]=eg_.geometry().global(simplex[c]);
                        }
                        ElementGeometry subElement(subSimplex,simplexGlobal);
                        subSimplices.push_back(subElement);
                    }
                }//end is not convex
            }//end else to dirty new alternativ
            /*
             * check if the area of the subpolygon is the same as
             * the sum over all simplices created from this subpolygon
             * to make sure no intersections are produced
             */
            auto sumOfSimplexAreas=0.0;
            for (int sIdx=simplices_.size()-numberOfSimplices;sIdx<simplices_.size();++sIdx){
                sumOfSimplexAreas+=Dumux::polygonArea(simplices_[sIdx]);
            }
            sumOfSimplexAreas-=subpolygonArea;
            sumOfSimplexAreas/=subpolygonArea;
            assert(Dumux::arePointsEqual(std::abs(sumOfSimplexAreas),0.0,(1.0e-3*subpolygonArea)));
        }
    }
private:
    void fillVectorOfSortedSubpolygons(){
        /*
         * create nullSpace gives simply always false for any interior reference element point.
         */
        Subpolygon nullSpace;
        {
            FV temp(-1.0);
            nullSpace.push_back(temp);
            temp[0]=-2.0;
            nullSpace.push_back(temp);
            temp[1]=-2.0;
            nullSpace.push_back(temp);
            temp[0]=-1.0;
            nullSpace.push_back(temp);
        }
        /*
         * create fullSpace gives simply always true for any interior reference element point.
         */
        Subpolygon fullSpace;
        {
            FV temp(1.0);
            fullSpace.push_back(temp);
            temp[0]=0.0;
            fullSpace.push_back(temp);
            temp[1]=0.0;
            fullSpace.push_back(temp);
            temp[0]=1.0;
            fullSpace.push_back(temp);
        }


        /*
         * vectorOfSortedSubpolygons_ is a vector which holds for the basic space
         * for every dof (corner) the subpolygon which contains this corner.
         * The enriched spaces and associated polygons are stored analogously,TODO doc me!
         */
        const Dune::ReferenceElement<RF,2>& genericCube = Dune::ReferenceElements<RF, 2>::cube();
        vectorOfSortedSubpolygons_.clear();
        vectorOfSortedSubpolygons_.resize(numberOfSpaces_);
        for (sizeType dofIdx=0; dofIdx<lgfs_size_[0]; dofIdx++){
            for (sizeType spaceIdx=0;spaceIdx<numberOfSpaces_;spaceIdx++){
                for (sizeType subpolygonIdx=0; subpolygonIdx<numberOfSubpolygons_; subpolygonIdx++){
                    /*
                     * this is space 0 which gets always the subpolygon which contains the dof
                     * and for the simple case of only two spaces present the next local space
                     * gets always the opposite subpolygon.
                     * this if also handles the case of an ending fracture.
                     */
                    if (spaceIdx==0){
                        const int neighbourSpaceIdx=spaceIdx+1;
                        /* For corner cutting fractures the cut corner is contained in both adjacent subpolygons.
                         * Only one (arbitrary) subpolygon is associated to the corner.
                         * The first if captures the usual case (no cut corner).
                         * If a corner is cut (so that it is contained in two adjacent subpolygons),
                         * the basic space of this corner has support in the smaller subpolygon.
                         * If the area is equal the basic space has support on the subpolygon with the smaller index.
                         */
                        if (endingFracture_ && specialTreatmentEndingFractureNodes_[dofIdx]){
                            assert(numberOfSpaces_==2);
                            vectorOfSortedSubpolygons_[spaceIdx].push_back(fullSpace);
                            vectorOfSortedSubpolygons_[neighbourSpaceIdx].push_back(nullSpace);
                            break;
                        }
                        else if (Dumux::pointInPolygon(subpolygons_[subpolygonIdx],genericCube.position( dofIdx, 2 ))){
                            bool addToVectorOfSortedSubpolygons=false;
                            int neighbourSubpolygonIdx=(subpolygonIdx+1==numberOfSubpolygons_ ? 0 : subpolygonIdx+1 );
                            if (!Dumux::pointInPolygon(subpolygons_[neighbourSubpolygonIdx],genericCube.position( dofIdx, 2 )) ){
                                addToVectorOfSortedSubpolygons=true;
                            }
                            else if (Dumux::arePointsEqual( polygonArea(subpolygons_[subpolygonIdx]),polygonArea(subpolygons_[neighbourSubpolygonIdx]) ) ){
                                if (subpolygonIdx<neighbourSubpolygonIdx){
                                    addToVectorOfSortedSubpolygons=true;
                                }
                            }
                            else if (polygonArea(subpolygons_[subpolygonIdx])<polygonArea(subpolygons_[neighbourSubpolygonIdx])){
                                addToVectorOfSortedSubpolygons=true;
                            }
                            if (addToVectorOfSortedSubpolygons==true){
                                vectorOfSortedSubpolygons_[spaceIdx].push_back(subpolygons_[subpolygonIdx]);
                                if (numberOfSpaces_==2){
                                    vectorOfSortedSubpolygons_[neighbourSpaceIdx].push_back(subpolygons_[neighbourSubpolygonIdx]);
                                }
                                break;
                            }
                        }
                    }//end if space zero
                    else if(numberOfSpaces_!=2){
                        /*
                         * those are the areas where support is absolutely necessary
                         */
                        if ( (!Dumux::pointInPolygon(subpolygons_[subpolygonIdx],genericCube.position( dofIdx, 2 )))
                                && (edgeColors_.dofIsOnEdge(dofIdx,localSubspaceToGlobalColor_[spaceIdx]))
                                && (edgeColors_.polygonIsOnSpaceEdge(subpolygonIdx,localSubspaceToGlobalColor_[spaceIdx]))
                        ){
                            vectorOfSortedSubpolygons_[spaceIdx].push_back(subpolygons_[subpolygonIdx]);
                            break;
                        }
                    }//end else - other spaces than zero
                }//end loop over subpolygons
            }//end loop over spaces

            if(numberOfSpaces_>3){
                for (sizeType spaceIdx=1;spaceIdx<numberOfSpaces_;spaceIdx++){
                    for (sizeType subpolygonIdx=0; subpolygonIdx<numberOfSubpolygons_; subpolygonIdx++){
                        /*
                         * those are the areas to complete the basis for every dof to a full one
                         * the choice which space gets the missing area of support is somehow arbitrary
                         */
                        if( (!edgeColors_.dofIsOnEdge(dofIdx,localSubspaceToGlobalColor_[spaceIdx]))//choose between the available spaces
                                && (!Dumux::pointInPolygon(subpolygons_[subpolygonIdx],genericCube.position( dofIdx, 2 )))//exclude basic subpolygon (always in space zero)
                                && (!edgeColors_.polygonIsOnDofEdge(dofIdx,subpolygonIdx))//exclude all subpolygons which have non-zero support on element edge
                        ){
                            //determine the number of already chosen polygons with support
                            int numberOfSupportPolygons=0;
                            for (sizeType spaceIdx=0;spaceIdx<numberOfSpaces_;spaceIdx++){
                                numberOfSupportPolygons+=vectorOfSortedSubpolygons_[spaceIdx].size();
                            }
                            numberOfSupportPolygons-=(numberOfSpaces_*dofIdx);
                            if (numberOfSupportPolygons < numberOfSubpolygons_ ){//limit the number of support
                                vectorOfSortedSubpolygons_[spaceIdx].push_back(subpolygons_[subpolygonIdx]);
                            }
                        }
                    }
                }
            }
            if(numberOfSpaces_>=3){
                for (sizeType spaceIdx=1;spaceIdx<numberOfSpaces_;spaceIdx++){
                    for (sizeType subpolygonIdx=0; subpolygonIdx<numberOfSubpolygons_; subpolygonIdx++){
                        /*
                         * this adds the null spaces, this is also necessary for bending fractures
                         */
                        if( (vectorOfSortedSubpolygons_[spaceIdx].size() < dofIdx+1 )){
                            vectorOfSortedSubpolygons_[spaceIdx].push_back(nullSpace);
                        }
                    }
                }
            }
        }//end loop over dofs


        bool doPrint=false;
        if(doPrint){
            for (sizeType l=0;l<numberOfSpaces_;l++){
                std::cout << "local space: "<< l << "\n";
                for (sizeType m=0; m<lgfs_size_[localToGlobalSubspace_[l]]; m++){
                    std::cout << "dof "<< m << " at " << genericCube.position( m, 2 )<< " in:   ";
                    for (sizeType n=0; n<vectorOfSortedSubpolygons_[l][m].size(); n++){
                        std::cout << vectorOfSortedSubpolygons_[l][m][n] <<" , " ;
                    }
                    std::cout << "\n";
                }
                std:: cout << "-------------------------------\n\n";
            }
        }
    }

    const MultiDomainGrid &multiDomainGrid_;
    const EG &eg_;
    const Map &intersectionPointsMap_;
    const LFSU &lfsu_;
    std::vector<sizeType> lgfs_size_;

        /*
         * vectorOfSortedSubpolygons_ is a vector which holds for the basic space
         * for every dof (corner) the subpolygon which contains this corner.
         * The enriched spaces and associated polygons are stored analogously,TODO doc me!
         */
    std::vector<Subpolygons> vectorOfSortedSubpolygons_;
    StandardVectorOfIntersections pointsNotOnCorners_;
    StandardVectorOfIntersections pointsOnCorners_;
    std::vector<FV> xfMiddle_;
    std::vector<int> globalToLocalSubspace_;
    std::vector<int> localToGlobalSubspace_;
    std::vector<int>  localSubspaceToGlobalColor_;
    bool endingFracture_;
    std::vector<bool> specialTreatmentEndingFractureNodes_;
    sizeType numberOfSpaces_;
    sizeType numberOfSubpolygons_;
    Subpolygons subpolygons_;
    Dumux::edgeColoring edgeColors_;

    typedef Dune::MultiLinearGeometry<ctype,dim_,dimw_> ElementGeometry;
    typedef Dune::GeometryType GT;
    typedef typename GT::BasicType BasicType;
    const BasicType simplex_ = Dune::GeometryType::simplex;
    Subpolygons simplices_;
    const bool dirty_;
public:
    std::vector<ElementGeometry> subSimplices;
};//end class

}//end namespace Dumux
#endif /* MODIFIEDBASISFUNCTION_HH_ */
