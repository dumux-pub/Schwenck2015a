#ifndef GRIDCOUPLING_HH_
#define GRIDCOUPLING_HH_

namespace Dumux{
template<typename Intersection>
bool compareIntersections (const Intersection &first, const Intersection & second)
{
    return ( first.color < second.color );
}

template<typename HostGrid,typename GridPointerFracture>
class gridCoupling{
public:
    typedef typename GridPointerFracture::value_type FractureGrid;
    typedef Dune::mdgrid::FewSubDomainsTraits<HostGrid::dimension,5> MDGridTraits;//4 is the maximum number of subdomains
    typedef Dune::MultiDomainGrid<HostGrid, MDGridTraits> MultiDomainGrid;
    typedef typename MultiDomainGrid::LeafGridView MDGV;
    typedef typename MDGV::IntersectionIterator MatrixElementIntersectionIterator;
    typedef typename MultiDomainGrid::ctype ctype;
    typedef typename MultiDomainGrid::Traits::GlobalIdSet::IdType HostGridIdType;
    static const unsigned int dimworld_=MultiDomainGrid::dimensionworld;
    typedef Dune::FieldVector<ctype,dimworld_> FV;

private:
    struct Intersection{

    public:
        typedef typename FV::value_type ctype;
        typedef FV FieldVector;
        FV intersection;
        int color;
        ctype apertureGlobal;
        ctype apertureOnEdge;

        Intersection(): intersection(-1000.0),color(0),apertureGlobal(-1.0),apertureOnEdge(-1.0)
        {}

        // Copy constructor
        Intersection(const Intersection &source) :  intersection(source.intersection),
                color(source.color),apertureGlobal(source.apertureGlobal),
                apertureOnEdge(source.apertureOnEdge)
        {}

        friend std::ostream& operator<< (std::ostream &o, const Intersection &i)
        {
            o << i.intersection[0] <<" "<<i.intersection[1]<< " color: " << i.color
                    << " aperture global: "<< i.apertureGlobal << std::endl;
            return (o);
        }

        ctype &operator [] (int index)
        {
            assert(index==0 || index==1);
            return (intersection[index]);
        }
        ctype operator [] (int index) const
        {
            assert(index==0 || index==1);
            return (intersection[index]);
        }
    };
public:

    typedef typename std::vector<Intersection> IntersectionPoints;
    typedef std::map<HostGridIdType, IntersectionPoints > IntersectionPointsMap;
    typedef typename std::pair<HostGridIdType,IntersectionPoints> IntersectionPointsPair;
    typedef typename IntersectionPointsMap::iterator IntersectionPointsMapIterator;

    typedef typename FractureGrid::LeafGridView FGV;
    typedef typename FGV::IntersectionIterator FractureIntersectionIterator;
    typedef typename FGV::IndexSet::IndexType FractureNodeIndexType;
    typedef std::list<FractureNodeIndexType> FractureNodeIdxList;
    typedef typename FractureGrid::Traits::template Codim<0>::EntitySeed FGES;
    typedef std::vector<FGES> VectorOfEntitySeeds;
    typedef std::map<HostGridIdType, VectorOfEntitySeeds > CouplingMap;
    typedef typename FractureGrid::Traits::GlobalIdSet::IdType FractureGridIdType;

gridCoupling(HostGrid &hostgrid, GridPointerFracture *fractureGridPointer)
    : hostgrid_(hostgrid), fractureGridPointer_(fractureGridPointer), fractureGrid_(**fractureGridPointer)
{}

//TODO: at the moment non-crossing, bending fractures are not allowed and have to be avoided manually in the grid file (case vi).
template<typename FractureNodeMap>
void createMultiDomainGrid(MultiDomainGrid& multiDomainGrid, const FractureNodeMap &fractureNodeMap)
{
    const int worlddim=MultiDomainGrid::dimensionworld;
    MDGV mdgv = multiDomainGrid.leafGridView();
    assert(worlddim==2);
    FGV fgv=fractureGrid_.leafGridView();

    typedef typename FGV::template Codim<0>::Iterator FractureElementIterator;
    typedef typename MDGV::template Codim<0>::Iterator HostGridIterator;

    typedef typename Dune::ReferenceElements<ctype,worlddim> GRE;
    multiDomainGrid.startSubDomainMarking();
    const FV check(-1000.0);//check means no entry
    /*
     * The intersectionPointsMap stores for every 2D element id (even in a 3D world) the entry point coordinates at (0 to (worlddim-1))
     * and the exit point coordinates at (worlddim to (2dim-1)).
     * at the moment this iteration loop is very cheap, because the coarsest fracture grid is used, i.e., in the case of a single fracture only one element is present
     * which is refined later.
     */
    //iteration over all fracture elements and then loop over every intersection (which is a node in this case).
    FractureElementIterator fractureElementEndIt = fractureGrid_.template leafend<0>();//This is a loop over the grid because node ids are used
    for (FractureElementIterator fractureElementIterator = fractureGrid_.template leafbegin<0>();
            fractureElementIterator != fractureElementEndIt; ++fractureElementIterator) {
        /*
         * only do this for real elements, i.e., simply skip all virtual elements
         */
        if (fractureGridPointer_->parameters(*fractureElementIterator)[7] < -0.5){
            ctype apertureGlobal=fractureGridPointer_->parameters(*fractureElementIterator)[0];
            int edgeColor=fractureGridPointer_->parameters(*fractureElementIterator)[8];
            assert(edgeColor>=0);
            std::vector<FV> interface(2);//interface stores the global geometrical position of both fracture element nodes
            std::vector<int> xtypeVector(2);
            FractureIntersectionIterator isItEnd = fgv.iend(*fractureElementIterator);
            //loop over the nodes (which are intersections in the Dune terminology) of this coarse fracture element
            for (FractureIntersectionIterator isIt = fgv.ibegin(*fractureElementIterator); isIt != isItEnd; ++isIt)
            {
                FV globalFractureNode=isIt->geometry().center();
                interface[isIt->indexInInside()]=globalFractureNode;
                FractureGridIdType nodeId = fractureGrid_.globalIdSet().subId(*(isIt->inside()), isIt->indexInInside(), 1);
                typename FractureNodeMap::const_iterator it=fractureNodeMap.find(nodeId);
                xtypeVector[isIt->indexInInside()]=(*it).second;
            }//end loop over fracture element nodes
            std::cout << "now fracture: " << interface[0] << "\t" << interface[1]<<std::endl;
            //Traverse the elements of the rock matrix multiDomainGrid and find the elements which are intersected.
            HostGridIterator eendit = multiDomainGrid.template leafend<0>();
            for (HostGridIterator matrixElementIterator = multiDomainGrid.template leafbegin<0>(); matrixElementIterator != eendit; ++matrixElementIterator) {
                int xtype = -50;
                //find out how many intersections this element is going to have
                FV localInterfaceStart = matrixElementIterator->geometry().local(interface[0]);
                FV localInterfaceEnd = matrixElementIterator->geometry().local(interface[1]);
                Dune::GeometryType gt = matrixElementIterator->geometry().type();
                /*
                 * if one fracture element node lies within this matrix element
                 * check what kind of crossing this element has to get
                 * -1 : ending
                 * -20: bending
                 * -30: 3-crossing
                 * -40: 4-crossing
                 * Skip nodes on the global boundary (xtype=0), i.e.,
                 * treat them later a straight fractures
                 */
                /*
                 * a macroscopic fracture section (element) which is so small
                 * that it is totally included in one matrix element is not allowed at the moment
                 */
                assert(!( ( GRE::general(gt).checkInside(localInterfaceStart) && xtypeVector[0]!=0 ) &&
                        (GRE::general(gt).checkInside(localInterfaceEnd) && xtypeVector[1]!=0) ));
                Dune::FieldVector<ctype,worlddim> endInInside;
                if( GRE::general(gt).checkInside(localInterfaceStart) && xtypeVector[0]!=0 ){
                    xtype=xtypeVector[0];
                    endInInside=localInterfaceStart;
                }
                else if ( GRE::general(gt).checkInside(localInterfaceEnd) && xtypeVector[1]!=0){
                    xtype=xtypeVector[1];
                    endInInside=localInterfaceEnd;
                }
//                /*
//                 * The following part is not necessary but leads to an improved performance.
//                 * It calculates the shortest distance between a fracture line and the matrix element centre.
//                 * Only if the distance is shorter than the outer radius, an intersection with this element can (but does not have to) occur.
//                 *
//                 */
                bool isInside=false;
                if (GRE::general(gt).checkInside(localInterfaceStart)
                        || GRE::general(gt).checkInside(localInterfaceEnd)){
                    isInside=true;
                }
                FV globalElementCenter = matrixElementIterator->geometry().center(); //global coordinates of element center
                if (!isInside){
                    ctype distance=Dumux::absDistance(interface[0], interface[1], globalElementCenter);
                    ctype lambda=Dumux::lambda(interface[0], interface[1], globalElementCenter);
                    ctype outerRadius=0.0;
                    for (int cornerIdx=0;cornerIdx<4;++cornerIdx){
                        FV outerRadiusVector=matrixElementIterator->geometry().corner(cornerIdx);
                        outerRadiusVector-=globalElementCenter;
                        outerRadius=std::max(outerRadiusVector.two_norm(),outerRadius);
                    }

                    /*
                     * If the shortest distance of the fracture line to the element center is smaller than the outer radius, this might lead to an intersection.
                     * But it is only relevant if the shortest distance point is within the actual fracture element
                     * (and not on the virtually extended fracture line)
                     * or one of the end fracture nodes lies within this matrix element.
                     * The boundaries (1.0 and 0.0) are included because this test is anyway only to get a better performance
                     * - better to test too often than to miss one intersection.
                     */
                    isInside= ( (distance<outerRadius) );
//                            && Dumux::isBetweenOrEqual(lambda, 0.0,1.0) );
//                    if (!isInside) std::cout << "distance: " << distance << "\t outerRadius: " << outerRadius << "\t lambda: " <<lambda <<std::endl;
                }
                //end of the performance part

                /*
                 * isInside is true if the shortest distance point between fractureElementIterator
                 * and matrixElementIterator.center lies within the outer circle around the element
                 */
                if (isInside){
                    HostGridIdType matrixElementId = multiDomainGrid.globalIdSet().id(*matrixElementIterator);//global matrix element id
                    std::pair<IntersectionPointsMapIterator,bool> ret;
                    int numberOfIntersections=0;
                    FV temp(-1000.0);
                    Intersection emptyIntersection;
                    IntersectionPoints intersectionPoints(6,emptyIntersection);
                    //loop over edges of coarse rock matrix grid
                    MatrixElementIntersectionIterator mElIsItEnd = mdgv.iend(*matrixElementIterator);
                    for (MatrixElementIntersectionIterator mElIsIt = mdgv.ibegin(*matrixElementIterator); mElIsIt != mElIsItEnd; ++mElIsIt){
                        Intersection intersection;
                        FV localCorner0=matrixElementIterator->geometry().local( mElIsIt->geometry().corner(0) );
                        FV localCorner1=matrixElementIterator->geometry().local( mElIsIt->geometry().corner(1) );
                        /*
                         * Dumux::intersectionOfLines calculates the geometrical position of the intersection
                         * and returns false if there is no intersection.
                         *
                         * For general quadrilateral elements the global-local transformation is NOT affine,
                         * i.e., the intersection calculated in local coordinates will not yield the same
                         * result as in global coordinates.
                         *
                         * The intersection is therefore calculated in global coordinates and then transformed into
                         * local.
                         */
                        FV tempGlobalIntersection(0.0);
                        const bool isGlobalIntersection=(Dumux::intersectionOfLines(mElIsIt->geometry().corner(0), mElIsIt->geometry().corner(1), interface[0], interface[1], tempGlobalIntersection) );

                        if(isGlobalIntersection){
                            intersection.intersection= matrixElementIterator->geometry().local(tempGlobalIntersection);
                            ctype edgeLengthGlobal=(mElIsIt->geometry().corner(0)-mElIsIt->geometry().corner(1)).two_norm();
                            assert(apertureGlobal<edgeLengthGlobal);
                            intersection.apertureGlobal=apertureGlobal;
                            ctype apertureLocal=this->projectApertureOnEdge(apertureGlobal,interface[0]-interface[1],mElIsIt->geometry().corner(0)-mElIsIt->geometry().corner(1));
                            intersection.apertureOnEdge=apertureLocal/edgeLengthGlobal;

                            /*
                             * treatment for corner cut elements:
                             * 1) get the normal of the fracture by the definition of
                             * (indexInInside 0 - indexInInside 1) x n = +1
                             * for the macro fracture grid
                             * 2) do not enrich the element on the positive side
                             * 3) enrich only the cut corner node on the negative side
                             *  - this is not done here but in modifiedbasisfunction.hh
                             */
                            if (Dumux::arePointsEqual(intersection.intersection,localCorner0) || Dumux::arePointsEqual(intersection.intersection,localCorner1)){
                                std::cout << "start corner cut treatment"<< std::endl;
                                //check if on positive side
                                bool doEnrich=(true ? Dumux::signedDistance(interface[0],interface[1],globalElementCenter)<0.0 : false);
                                std::cout << "signed distance: " << doEnrich << std::endl;

                            }

                            //if this assert fails the element is cut at a corner, which is not correctly implemented at the moment
                            assert(!Dumux::arePointsEqual(intersection.intersection,localCorner0) && !Dumux::arePointsEqual(intersection.intersection,localCorner1));
                            numberOfIntersections++;
                            //only if an intersection with this edge is detected, it has to be tested if this is a virtual one.
                            bool virtualIntersection=false;
                            /*
                             * This is just a clean up of the intersection stored.
                             * Due to epsilon comparisons it might happen that an intersection close to a corner is found which is actually
                             * outside this matrix element.
                             * It is then set to 0 or 1 so that it lies on the boundary of the reference element. //TODO this assumes cubes!
                             */
                            for (unsigned int i=0;i<worlddim;i++){
                                if (Dumux::arePointsEqual(intersection[i],0.0)) {
                                    intersection[i]=0.0;
                                }
                                else if (Dumux::arePointsEqual(intersection[i],1.0)) {
                                    intersection[i]=1.0;
                                }
                            }
                            /*
                             * test for virtual intersections
                             * TODO this has to be extended for a node which lies on an edge and for edge aligned fractures
                             */
                            for (int cornerI=0;cornerI<matrixElementIterator->geometry().corners();cornerI++){
                                if (  Dumux::arePointsEqual(intersection.intersection,matrixElementIterator->geometry().corner(cornerI) ) ){
                                    virtualIntersection=true;
                                    break;
                                }
                            }
                            assert(!virtualIntersection);//TODO at the moment corner cut elements are not allowed
                            if (!virtualIntersection){
                                /*
                                 * check, if there is already an entry in the map resulting from a previous fracture
                                 */
                                int storedEntriesIndex=0;
                                IntersectionPointsMapIterator ipmIt=intersectionPointsMap_.find(matrixElementId);
                                if (ipmIt!=intersectionPointsMap_.end()){//there is already an entry
                                    //read entries for matrix element id into intersectionPoints vector
                                    intersectionPoints=(*ipmIt).second;
                                    //find the first empty field in the intersectionPoints vector
                                    FV temp(-1000.0);
                                    while ( !Dumux::arePointsEqual(intersectionPoints[storedEntriesIndex].intersection,temp) ){
                                        storedEntriesIndex++;
                                    }
                                    /*
                                     * delete entry in map, so that the new intersectionPoints vector can be added later
                                     * this is because the intersections for one fracture with this matrix element are
                                     * temporarily stored in intersectionPoints but only added once after this loop
                                     */
                                    intersectionPointsMap_.erase(ipmIt);
                                    assert(storedEntriesIndex!=0);//If storedEntriesIndex is 0, this is an error because there was an entry in the map but no value written in position zero.
                                }
                                /*
                                 * if crossing type is -50 there is no end node in this element, i.e.,
                                 * one straight fractures has to occur
                                 * (or a fracture ending on the global boundary
                                 * is disguised).
                                 *
                                 * In a second traversal there can be still
                                 * three case:
                                 * a fracture ends or bends here or another straight fracture occurs.
                                 * But this is not allowed and the matrix grid has to be refined!
                                 *
                                 * if crossing type is -1 this is an ending node
                                 */
                                if (xtype==-50 || xtype==-1){
                                    assert(storedEntriesIndex<=1);//two straight or two bending fractures are not allowed
                                    if (storedEntriesIndex==1){assert(xtype==-20);}//if there is already an entry in the map this must belong to a bending fracture
                                    //only add the element to the subspace once
                                    if (numberOfIntersections==1) {multiDomainGrid.addToSubDomain(edgeColor,*matrixElementIterator);}
                                    //but store entry, middle and (projected) exit point
                                    intersection.color=edgeColor;
                                    intersectionPoints[numberOfIntersections-1+storedEntriesIndex]=intersection;
                                    /*
                                     * this is a straight fracture and the midpoint (virtual) position
                                     * is stored at entry four (or five if there was already an entry in four)
                                     * this has to be done in global coordinates because the straight fracture
                                     * might actually not be straight in local coordinates (for quadrilaterals
                                     * with not-affine transformations).
                                     */
                                    if (numberOfIntersections==2){
                                        if ( Dumux::arePointsEqual(intersectionPoints[4].intersection,temp) ) {
                                            intersectionPoints[4].intersection*=0.0;
                                            intersectionPoints[4].intersection+=matrixElementIterator->geometry().global(intersectionPoints[0].intersection);
                                            intersectionPoints[4].intersection+=matrixElementIterator->geometry().global(intersectionPoints[1].intersection);
                                            intersectionPoints[4].intersection*=0.5;
                                            intersectionPoints[4].intersection=matrixElementIterator->geometry().local(intersectionPoints[4].intersection);

                                        }
                                        else {
                                            intersectionPoints[5].intersection*=0.0;
                                            intersectionPoints[5].intersection+=matrixElementIterator->geometry().global(intersectionPoints[2].intersection);
                                            intersectionPoints[5].intersection+=matrixElementIterator->geometry().global(intersectionPoints[3].intersection);
                                            intersectionPoints[5].intersection*=0.5;
                                            intersectionPoints[5].intersection=matrixElementIterator->geometry().local(intersectionPoints[5].intersection);
                                        }
                                    }
                                    else if(xtype==-1){
                                        intersectionPoints[4].intersection=endInInside;
                                        //this check is only necessary to avoid a division by zero
                                        ctype m= ( !Dumux::arePointsEqual( ( intersectionPoints[4] )[0] , ( intersectionPoints[0] )[0] ) ?
                                                ( (intersectionPoints[4])[1]-(intersectionPoints[0])[1] ) / ( (intersectionPoints[4])[0]-(intersectionPoints[0])[0] )
                                                : 1000.0);
                                        FV projectedExit(-1.0);
                                        //m is 1000.0 for a nearly vertical fracture
                                        if ( Dumux::arePointsEqual(m,1000.0) ){
                                            projectedExit[0]=((intersectionPoints)[0])[0];
                                            projectedExit[1]= !Dumux::arePointsEqual( ( intersectionPoints[0] )[1],1.0);
                                        }
                                        /*
                                         * this simply solves linear equation for the projection of the exit point
                                         * this is in local coordinates!
                                         * be aware that the fracture is in general not straight in local coordinates,
                                         *  i.e., the projected exit is only a linear approximation.
                                         */
                                        else{
                                            ctype b= (intersectionPoints[0])[1] - m*((intersectionPoints[0])[0]);
                                            std::cout << "m: " << m << std::endl;

                                            if ( !Dumux::arePointsEqual((intersectionPoints[0])[0],0.0) && Dumux::isBetweenOrEqual(b, 0.0, 1.0) ){
                                                projectedExit[0]=0.0;
                                                projectedExit[1]=b;
                                            }
                                            else if ( !Dumux::arePointsEqual((intersectionPoints[0])[0],1.0) && Dumux::isBetweenOrEqual(m+b, 0.0, 1.0) ){
                                                projectedExit[0]=1.0;
                                                projectedExit[1]=m+b;
                                            }
                                            else if ( !Dumux::arePointsEqual((intersectionPoints[0])[1],0.0) && Dumux::isBetweenOrEqual(b/m*-1.0, 0.0, 1.0) ){
                                                projectedExit[0]=b/m*-1.0;
                                                projectedExit[1]=0.0;
                                            }
                                            else if ( !Dumux::arePointsEqual((intersectionPoints[0])[1],1.0) && Dumux::isBetweenOrEqual((1.0-b)/m, 0.0, 1.0) ){
                                                projectedExit[0]=(1.0-b)/m;
                                                projectedExit[1]=1.0;
                                            }
                                        }//end else
                                        intersectionPoints[5].intersection=projectedExit;
                                    }//end if ending fracture case
                                }//end if straight and ending fractures
                                else //there is at least one crossing (bend)
                                {
                                    assert(numberOfIntersections==1);//if numberOfIntersections is not one, i.e., here is no crossing, but the node has a crossing value
                                    multiDomainGrid.addToSubDomain(edgeColor,*matrixElementIterator);
                                    intersection.color=edgeColor;
                                    intersectionPoints[storedEntriesIndex]=intersection;
                                    /*
                                     * store the crossing point at the right location and
                                     * associate the right branch for two not-crossing bending fractures
                                     */
                                    if (storedEntriesIndex==0) {intersectionPoints[4].intersection= endInInside;}
                                    /*
                                     * nothing more has to be done for 3/4-crossings
                                     * only two not-crossing bending fractures have to be treated
                                     */
                                    else if (xtype==-20){
                                        if (Dumux::arePointsEqual(intersectionPoints[4].intersection,endInInside) ){//this branch belongs to fracture intersection stored in 0
                                            intersectionPoints[1]=intersection;
                                        }
                                        else if (Dumux::arePointsEqual(intersectionPoints[2].intersection,temp)){
                                            assert(xtype!=-20);//two not crossing fractures are not allowed
                                            intersectionPoints[2]=intersection;
                                            intersectionPoints[5].intersection=endInInside;
                                        }
                                        else {
                                            intersectionPoints[3]=intersection;
                                        }
                                    }
                                }//end crossing case
                            }//end real intersection
                        }//end if there is an intersection with this edge
                    }//end loop over edges
                    //adding new entry to the map if an intersection occurred
                    if (numberOfIntersections>0){
                        //sort intersection points according to color (enrichment space)
                        for (typename IntersectionPoints::iterator ipIt=intersectionPoints.begin();
                                ipIt!=intersectionPoints.end();ipIt++){
                        }
                        typename IntersectionPoints::iterator ipIt=intersectionPoints.begin();
                        int k=0;
                        while ( !Dumux::arePointsEqual( (*ipIt).intersection,temp) && k<4){
                            ++ipIt;
                            ++k;
                        }
                        std::sort( intersectionPoints.begin(),ipIt,Dumux::compareIntersections<Intersection> );
                        ret=intersectionPointsMap_.insert ( IntersectionPointsPair(matrixElementId,intersectionPoints) );
                        assert(ret.second);
                    }//end if number of intersections>0
                }//end if inside (performance part)
            }//end iteration over rock matrix elements
        }//end if of real/virtual elements
    }//end iteration over fracture elements

    multiDomainGrid.preUpdateSubDomains();
    multiDomainGrid.updateSubDomains();
    multiDomainGrid.postUpdateSubDomains();


}//end void createMultiDomainGrid

IntersectionPointsMap getIntersectionPointsMap(){
    return (intersectionPointsMap_);
}

void createFractureMatrixCoupling(const MultiDomainGrid& multiDomainGrid){
    typedef typename MultiDomainGrid::SubDomainGrid SubDomainGrid;
    typedef typename MultiDomainGrid::LeafGridView MDGV;
    MDGV mdgv = multiDomainGrid.leafGridView();
    const SubDomainGrid &sdg0 = multiDomainGrid.subDomain(0);
    const SubDomainGrid &sdg1 = multiDomainGrid.subDomain(1);
    const SubDomainGrid &sdg2 = multiDomainGrid.subDomain(2);
    const SubDomainGrid &sdg3 = multiDomainGrid.subDomain(3);
    const SubDomainGrid &sdg4 = multiDomainGrid.subDomain(4);

    typedef typename FGV::template Codim<0>::Iterator FractureElementIterator;
    typedef typename MultiDomainGrid::Traits::GlobalIdSet::IdType HostGridIdType;
    FGV fgv = fractureGrid_.leafGridView();
    const int worlddim=MultiDomainGrid::dimensionworld;
    typedef typename Dune::ReferenceElements<ctype,worlddim> GRE;
    FractureNodeIdxList fractureNodeIdxList;
    VectorOfEntitySeeds vectorOfEntitySeeds;
    typedef typename std::pair<HostGridIdType,VectorOfEntitySeeds> CouplingMapPair;
    typedef typename CouplingMap::iterator CouplingMapIterator;

    //traverse all elements and check if the element belongs to at least one subdomain
    typedef typename MDGV::template Codim<0>::Iterator HostGridIterator;
    HostGridIterator eendit = multiDomainGrid.template leafend<0>();
    for (HostGridIterator matrixElementIterator = multiDomainGrid.template leafbegin<0>(); matrixElementIterator != eendit; ++matrixElementIterator) {
        if(
                sdg0.containsMultiDomainEntity(*matrixElementIterator)
                || sdg1.containsMultiDomainEntity(*matrixElementIterator)
                || sdg2.containsMultiDomainEntity(*matrixElementIterator)
                || sdg3.containsMultiDomainEntity(*matrixElementIterator)
                || sdg4.containsMultiDomainEntity(*matrixElementIterator)
        )
        {
            HostGridIdType matrixElementId = multiDomainGrid.globalIdSet().id(*matrixElementIterator);//global matrix element id
            vectorOfEntitySeeds.clear();
            //traverse the elements in the fracture network
            FractureElementIterator feendit = fractureGrid_.template leafend<0>();//This is a loop over the grid because node ids are used
            for (FractureElementIterator fractureElementIterator = fractureGrid_.template leafbegin<0>();
                    fractureElementIterator != feendit; ++fractureElementIterator) {
                /*
                 * only do this for real elements, i.e., simply skip all virtual elements
                 */
                typedef typename GridPointerFracture::value_type::Traits::template Codim<0>::EntityPointer ElementPointer;
                ElementPointer entityPtr(*fractureElementIterator);
                while (entityPtr->hasFather()){
                    entityPtr=entityPtr->father();
                }
                if (fractureGridPointer_->parameters(*entityPtr)[7] < -0.5){//skip virtual elements
                    //traverse all nodes of this fracture element and check if at least one is inside the matrix element
                    bool couplingThisFractureElement=false;
                    FractureIntersectionIterator isItEnd = fgv.iend(*fractureElementIterator);
                    for (FractureIntersectionIterator isIt = fgv.ibegin(*fractureElementIterator); isIt != isItEnd; ++isIt){
                        Dune::GeometryType gt = matrixElementIterator->geometry().type();
                        if ( GRE::general(gt).checkInside( matrixElementIterator->geometry().local( isIt->geometry().center() ) ) ){
                            couplingThisFractureElement=true;
                            break;
                        }
                    }//end loop over fracture nodes

                    if (couplingThisFractureElement){
//                        std::cout << "matrix element id: " << matrixElementId << " couples to fracture id: " << fractureGrid_.globalIdSet().id(*fractureElementIterator)<<std::endl;
                        FGES fges=fractureElementIterator->seed();
                        vectorOfEntitySeeds.push_back(fges);
                    }
                }//end if virtual elements
            }//end loop over fracture elements
            /*
             * if no node lies within the matrix element check which fracture cuts this element.
             * One fracture has to be found because this element is enriched!
             */
            bool onlyOne=false;
            if(vectorOfEntitySeeds.size()==0){
                onlyOne=true;
                for (FractureElementIterator fractureElementIterator = fgv.template begin<0>(); fractureElementIterator != feendit; ++fractureElementIterator) {
                    /*
                     * only do this for real elements, i.e., simply skip all virtual elements
                     */
                    typedef typename GridPointerFracture::value_type::Traits::template Codim<0>::EntityPointer ElementPointer;
                    ElementPointer entityPtr(*fractureElementIterator);
                    while (entityPtr->hasFather()){
                        entityPtr=entityPtr->father();
                    }
                    if (fractureGridPointer_->parameters(*entityPtr)[7] < -0.5){//skip virtual elements
                        FractureIntersectionIterator isItEnd = fgv.iend(*fractureElementIterator);
                        FractureIntersectionIterator isItBegin = fgv.ibegin(*fractureElementIterator);
                        FV matrixElementGlobalFractureStart, matrixElementGlobalFractureEnd;
                        for (FractureIntersectionIterator isIt =isItBegin ; isIt != isItEnd; ++isIt){
                            if (isIt==isItBegin) {
                                matrixElementGlobalFractureStart=isIt->geometry().center();
                            }
                            else {
                                matrixElementGlobalFractureEnd=isIt->geometry().center();
                            }
                        }
                        MatrixElementIntersectionIterator mElIsItEnd = mdgv.iend(*matrixElementIterator);
                        for (MatrixElementIntersectionIterator mElIsIt = mdgv.ibegin(*matrixElementIterator); mElIsIt != mElIsItEnd; ++mElIsIt){
                            FV intersection(0.0);

                            /*
                             * Dumux::intersectionOfLines calculates the geometrical position
                             * of the intersection in local matrix element coordinates if there is one
                             * and returns false otherwise
                             * This check has to be performed in global coordinates because if there are non-regular
                             * distorted cells the intersection algorithm might NOT show an intersection in local coordinates
                             * although there is one in global coordinates.
                             */
                            if (Dumux::intersectionOfLines(mElIsIt->geometry().corner(0), mElIsIt->geometry().corner(1),
                                    matrixElementGlobalFractureStart, matrixElementGlobalFractureEnd, intersection) ){
                                intersection=(*matrixElementIterator).geometry().local(intersection);//TODO check if this intersection is used at all
//                                std::cout << "matrix element id: " << matrixElementId << " couples to fracture id: " << fractureGrid_.globalIdSet().id(*fractureElementIterator)<<std::endl;
                                FGES fges=fractureElementIterator->seed();
                                vectorOfEntitySeeds.push_back(fges);
                                /*
                                 * TODO check if virtual intersections (touched elements) have to be handled in this function, too.
                                 *
                                 * TODO handle the case of one straight fracture which has no node within the matrix element and one ending fracture
                                 *   and two not-crossing fractures - one straight (no node within this matrix element) and one bending.
                                 *
                                 *   REMARK: the case of two not-crossing fractures (straight, with both no node within this matrix element) should already be captured
                                 */
                                break;
                            }
                        }
                    }//end if virtual elements
                }//end loop over fracture elements
            }//end if vector of entity seeds is zero
            if (onlyOne){
                assert(vectorOfEntitySeeds.size()==1);
            }
            assert(vectorOfEntitySeeds.size()!=0);

            std::pair<CouplingMapIterator,bool> ret;
            ret=couplingMap_.insert ( CouplingMapPair(matrixElementId,vectorOfEntitySeeds) );
            assert(ret.second);
        }//end loop over only elements which are contained in at least one of the subdomains
    }//end loop over matrix elements
}//end createFractureMatrixCoupling

CouplingMap getCouplingMap(){
    return (couplingMap_);
}

private:

    template<typename FV>//, typename EG
    ctype projectApertureOnEdge(const ctype &aperture, const FV fractureVector, const FV edgeVector){
        ctype theta= std::acos((fractureVector*edgeVector)/(fractureVector.two_norm()*edgeVector.two_norm()));
        return (aperture/std::sin(theta));
    }

    HostGrid &hostgrid_;
    //intersectionPointsMap_ stores the relevant coupling coordinates for every affected element Id in local matrix element coordinates
    IntersectionPointsMap intersectionPointsMap_;
    GridPointerFracture *fractureGridPointer_;
    //couplingMap_ stores for every matrix element id which is intersected the fracture node indices to which this element is coupled
    CouplingMap couplingMap_;
    FractureGrid &fractureGrid_;

};//end class
}//end namespace dumux

#endif /* GRIDCOUPLING_HH_ */
