//============================================================================
// Author      : Alex Tatomir, Nicolas Schwenck
//============================================================================

#ifndef ART_READER_1D_HH
#define ART_READER_1D_HH

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <algorithm>
#include <iomanip>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>
#include <dune/grid/uggrid.hh>

#define PLOT 0 // plot the vertices, edges, elements

namespace Dumux
{
template <typename FractureGridType>
class artReader;

//comparison function
template <typename FractureGridType>
struct compareEdges
{
    compareEdges(artReader<FractureGridType> &ar)
    : ar_(ar)
    {}
    artReader<FractureGridType> &ar_;
    bool operator()(int i1, int i2)
    { return ( ar_.edges_vector_[i1].angle < ar_.edges_vector_[i2].angle ); }
};

template<typename Vertex>
bool compareVertexForColoring (const Vertex &first, const Vertex & second)
{
    return ( (first.connectivity).size() > (second.connectivity).size() );
}

template <typename FractureGridType>
class artReader {

private:
    typedef typename FractureGridType::ctype Scalar;

    struct Vertex{
        //struct default is public
        Scalar kfn;
        Scalar kft;
        Scalar k12;
        Dune::FieldVector<Scalar, 2> position;
        typedef typename std::list<int> Connectivity;
        Connectivity connectivity;
        typedef typename Connectivity::const_iterator ConIt;
        typedef typename std::list<int> Colors;
        Colors colors;
        typedef typename Colors::iterator ColIt;
        typedef typename Colors::const_iterator const_ColIt;

        Vertex(): kfn(-1.0),kft(-1.0),k12(0.0),position(-1.0)
        {}

        // Copy constructor
        Vertex(const Vertex &vSource) :  kfn(vSource.kfn),kft(vSource.kft),k12(vSource.k12),
                position(vSource.position),connectivity(vSource.connectivity),colors(vSource.colors)
        {}

        friend std::ostream& operator<< (std::ostream &o, const Vertex &i)
        {
            o << i.position[0] <<" "<<i.position[1]<< " kfn: " << i.kfn << " kft: " << i.kft << " k12: " << i.k12
                                << "\n connectivity: ";
            for (ConIt cIt=(i.connectivity).begin();cIt!=(i.connectivity).end();cIt++){
                o << *cIt << " ";
            }
            for (const_ColIt cIt=(i.colors).begin();cIt!=(i.colors).end();cIt++){
                o << *cIt << " ";
            }
            return (o);
        }

        Scalar &operator [] (int index)
        {
            assert(index==0 || index==1);
            return (position[index]);
        }
        Scalar operator [] (int index) const
        {
            assert(index==0 || index==1);
            return (position[index]);
        }

        /*
         * addition and substraction of two vertices will only add/substract
         * the geometrical positions of both points.
         * No averaging of permeabilities is carried out!
         */
        void operator+= (const Vertex &b)
        {
            for (int i=0;i<2;i++){
                (*this)[i]+=b[i];
            }
        }

        void operator-= (const Vertex &b)
        {
            for (int i=0;i<2;i++){
                (*this)[i]-=b[i];
            }
        }

        Scalar two_norm () const
        {
            return ( (this->position).two_norm() );
        }

        template<typename EI>
        void addEdgeIndex(const EI &ei)
        {
            connectivity.push_back(ei);
            connectivity.sort();
        }

        template<typename C>
        void addColor(const C &color)
        {
            colors.push_back(color);
            colors.sort();
        }
    };

    struct Edge{
        int artId;
        Scalar aperture;
        Scalar kfn;
        Scalar kft;
        Scalar k12;
        Dune::FieldVector<int, 2> points;
        Scalar angle;
        Scalar li;
        Dune::FieldVector<Scalar, 2> ti;
        int color;

        Edge(): artId(-1),aperture(-1.0),kfn(-1.0),kft(-1.0),k12(0.0),points(-1.0),angle(-1.0),li(-1.0),ti(-1.0),color(-1)
        {}

        friend std::ostream& operator<< (std::ostream &o, const Edge &i)
        {
            return (o << i.points[0] <<" "<<i.points[1]<< " " << i.aperture <<"\n");
        }

        int &operator [] (int index)
        {
            assert(index==0 || index==1);
            return (points[index]);
        }
        int operator [] (int index) const
        {
            assert(index==0 || index==1);
            return (points[index]);
        }
    };

    friend struct compareEdges<FractureGridType>;

    typedef Dune::FieldVector<Scalar, 3>    Coordinates;
    typedef std::vector<Vertex>        VerticesVector;
    typedef typename std::vector<Vertex>::iterator   VerticesVectorIt;

    typedef std::vector<Edge>         EdgesVector;

    typedef std::list<int> ArtIdxList;
    typedef typename ArtIdxList::iterator ArtIdxListIterator;
    typedef Dune::GridPtr<FractureGridType> GridPointer;

    /* Function art_readARTfile reads the file formats obtained with the
     * ART mesh generator   */
public:
    artReader(const char *art_fileName)
    {
        std::cout<<boost::format("Opening %s ")%art_fileName;
        std::cout<<std::endl;
        std::ifstream inFile(art_fileName);

        //initialize
        Vertex vertex_;
        Edge  edge_;
        std::string jump;

        while(inFile >> jump)
        {
            //do nothing with the first lines in the code
            //start reading information only after getting to %Vertices
            if(jump == "VertexNumber:")
            {
                inFile >> jump;
                Scalar dummy = atof(jump.c_str());
                vertex_number = dummy;
                break;
            }
        }
        while(inFile >> jump)
        {
            if(jump == "EdgeNumber:")
            {
                inFile >> jump;
                Scalar dummy = atof(jump.c_str());
                edge_number = dummy;
                break;
            }
        }
        while(inFile >> jump)
        {
            if(jump == "FaceNumber:")
            {
                inFile >> jump;
                Scalar dummy = atof(jump.c_str());
                face_number = dummy;
                break;
            }
        }
        while(inFile >> jump)
        {
            if(jump == "ElementNumber:")
            {
                inFile >> jump;
                Scalar dummy = atof(jump.c_str());
                element_number = dummy;
                break;
            }
        }
     // finished reading the header information: total number of vertices, etc..
        while(inFile >> jump)
        {
            //jump over the lines until the ones with the vertices "% Vertices: x y z"
            if(jump == "Vertices:")
            {
                break;
            }
        }
        while(inFile >> jump)
        {
            //skip words until "z" from the line "% Vertices: x y z"
            if(jump == "z")
            {
                std::cout<<"Start reading the vertices\n";
                break;
            }
        }

        while(inFile >> jump)
        {
            if(jump == "$")
            {
                std::cout<<"Finished reading the vertices\n";
                break;
            }
            Scalar dummy = atof(jump.c_str());
            vertex_[0] = dummy;
            for (int k=1; k<3; k++)
            {
                inFile >> jump;
                dummy = atof(jump.c_str());
                if (k!=2) {vertex_[k] = dummy;}
            }
            inFile >> jump;
            dummy = atof(jump.c_str());
            vertex_.kfn=dummy;
            inFile >> jump;
            dummy = atof(jump.c_str());
            vertex_.kft=dummy;
            inFile >> jump;
            dummy = atof(jump.c_str());
            vertex_.k12=dummy;

            vertices_.push_back(vertex_);
        }
    //Reading Edges
        while(inFile >> jump)
        {
            //jump over the characters until the ones with the edges
            if(jump == "permeability):")
            {
                std::cout<<"\nStart reading the edges\n";
                break;
            }
        }
        while(inFile >> jump)
        {
            if(jump == "$")
            {
                std::cout<<"Finished reading the edges\n";
                break;
            }
            Scalar dummy = atof(jump.c_str());
            edge_.artId=dummy;
            for (int k=0; k<2; k++)
            {
                inFile >> jump;
                dummy = atof(jump.c_str());
                edge_[k] = dummy;
            }
            inFile >> jump;
            dummy = atof(jump.c_str());
            edge_.aperture=dummy;
            inFile >> jump;
            dummy = atof(jump.c_str());
            edge_.kfn=dummy;
            inFile >> jump;
            dummy = atof(jump.c_str());
            edge_.kft=dummy;
            edges_vector_.push_back(edge_);
        }
    }//end constructor

    void outputARTtoScreen()
    {
    ////////OUTPUT for verification
    //////////////////////////////////////////////////////////////////
            std::cout<<"\nprinting VERTICES\n";
            for (int i = 0; i < vertices_.size(); i++)
            {
                for (int j=0; j < 3; j++)
                std::cout << vertices_[i][j]<<"\t";
                std::cout << std::endl;
            }

            std::cout<<"\nprinting ELEMENTS\n";
            for (int i = 0; i < edges_vector_.size(); i++)
            {
                for (int j=0; j < 2; j++){
                    std::cout<<edges_vector_[i][j]<<"\t";
                }
                std::cout<<(edges_vector_[i]).aperture<<"\t";
                std::cout<<(edges_vector_[i]).kfn<<"\t";
                std::cout<<(edges_vector_[i]).kft;
            }
            std::cout<<std::endl;

            std::cout<<"\nTotal number of vertices "<<vertex_number<<std::endl;
            std::cout<<"Total number of edges: "<<edge_number<<std::endl;
            std::cout<<"Total number of elements: "<<element_number<<std::endl;


            if (vertices_.size() != vertex_number )
            {
                std::cout<<"The total given number of vertices: "<<vertex_number
                        <<" is not the same with the read number of entries: "
                        <<vertices_.size()<<"\n";
            }
            if (edges_vector_.size() != edge_number )
            {
                std::cout<<"The total given number of edges: "<<edge_number
                        <<" is not the same with the read number of entries: "
                        <<edges_vector_.size()<<"\n";
            }
    }

    void edgeColoring()
    {
        /*
         * create map which stores to every vertex index a
         * list with all connected edge indices
         *
         * loop over all edges and add the edge index to a
         * vector part of the vertex struct
         */
        for (int edge=0;edge<edges_vector_.size();edge++){
            for (int edgePoint=0;edgePoint<2;edgePoint++){
                (vertices_[ edges_vector_[edge].points[edgePoint] ]).addEdgeIndex(edge);
            }
        }
        /*
         * copy vertex vector in a vertex list and sort it
         * by degree (descending)
         */
        VerticesVector verticesVector(vertices_);
        std::sort(verticesVector.begin(),verticesVector.end(),Dumux::compareVertexForColoring<Vertex>);
        typedef typename std::list<int>::iterator ListIterator;//TODO make this the actual connectivity/colors type

        /*
         * loop over every vertex and set the colors to all adjacent edges
         */
        for (VerticesVectorIt vlIt=verticesVector.begin();vlIt!=verticesVector.end();vlIt++){
            std::set<int> availableColors{0,1,2,3,4};
            //determine which colors are already used at adjacent elements
            for (ListIterator cIt=((*vlIt).connectivity).begin();cIt!=((*vlIt).connectivity).end();cIt++){
                if ((edges_vector_[*cIt]).color!=-1){//already a color set
//                    std::cout << "already a color set"<<std::endl;
                    /*
                     * if this i a bending fracture, use the same color for both branches,
                     * i.e., delete all other colors from the list of available colors
                     */
                    if ( ((*vlIt).connectivity).size()==2 ) {
                        std::cout << "this is a bending fracture"<<std::endl;
                        availableColors.clear();
                        availableColors.insert((edges_vector_[*cIt]).color);
                    }
                    /*
                     * this is not a bending fracture, i.e., delete all already used
                     * colors from the list of available colors
                     * 1) for all associated edges
                     * 2) but also for the vertex associated to this edge
                     *  other than the vertex on which the iterator is right now
                     *  this however is edge specific and therefore checked
                     *  during the actual color setting in the loop below
                     */
                    else {
                        availableColors.erase((edges_vector_[*cIt]).color);
                    }
                }
            }
            //set colors
            for (ListIterator cIt=((*vlIt).connectivity).begin();cIt!=((*vlIt).connectivity).end();cIt++){
                //set colors for uncolored elements
                if ((edges_vector_[*cIt]).color==-1){//no color set yet
                    //for this edge available colors depends also on the next vertex
                    std::set<int> edgeAvailableColors(availableColors);
//                    std::cout << "for this edge erase: \n";
                    for (int vertexIdx=0;vertexIdx<2;++vertexIdx){
                        if ( ((vertices_[((edges_vector_[*cIt]).points[vertexIdx])]).colors).size()!=0 ){//((vertices_[((edges_vector_[*cIt]).points[0])]).colors)
                            for (typename Vertex::ColIt colIt=((vertices_[((edges_vector_[*cIt]).points[vertexIdx])]).colors).begin();
                                    colIt!=((vertices_[((edges_vector_[*cIt]).points[vertexIdx])]).colors).end();
                                    ++colIt){
//                                std::cout << *colIt  << "\t";
                                edgeAvailableColors.erase(*colIt);
                            }
//                            std::cout <<std::endl;
                        }
                    }

                    assert(edgeAvailableColors.size()!=0);
                    (edges_vector_[*cIt]).color=*edgeAvailableColors.begin();
                    for (int vertexIdx=0;vertexIdx<2;++vertexIdx){
                        (vertices_[((edges_vector_[*cIt]).points[vertexIdx])]).addColor( *edgeAvailableColors.begin() );
                    }
                    if ( ((*vlIt).connectivity).size()!=2 ) {
                        availableColors.erase((edges_vector_[*cIt]).color);
                    }
                }
                //debug output
//                std::cout << "associated colors: "<<std::endl;
//                for (ListIterator colIt=((vertices_[((edges_vector_[*cIt]).points[0])]).colors).begin();colIt!=((vertices_[((edges_vector_[*cIt]).points[0])]).colors).end();colIt++){
//                     std::cout << *colIt << "\t";
//                }
//                std::cout <<std::endl;
            }//end loop over edges associated to this vertes to set colors

        }//end loop over all vertices
    }

GridPointer createGrid(std::string simulationNameString)
{
    // set up the grid factory
    Dune::FieldVector<Scalar,2> position;

    // Plot the vertices
#if PLOT
    std::cout<<"*================*"<<std::endl;
    std::cout<<"* Vertices"<<std::endl;
    std::cout<<"*================*"<<std::endl;
#endif
    for (int k=0; k<vertex_number; k++)
    {
        //Printing the vertices vector
#if PLOT
        std::cout<<vertices_[k][0]<<"\t\t";
        std::cout<<vertices_[k][1]<<std::endl;
//        std::cout<<vertices_[k][2]<<std::endl;
#endif
        for (int i=0; i<2; i++)//use only x,y coordinates and skip z
        {
            position[i]=vertices_[k][i];
        }
        factory_.insertVertex(position);
    }
#if PLOT
    std::cout<<"*================*"<<std::endl;
    std::cout<<"* Edges"<<std::endl;
    std::cout<<"*================*"<<std::endl;
#endif

#if PLOT
    for (int k=0; k<edge_number; k++)
    {

        //Printing the Edge vector
        std::cout<<"idx: "<<(edges_vector_[k]).artId<<" connects vertices:\t";
        std::cout<<edges_vector_[k][0]<<"\t\t";
        std::cout<<edges_vector_[k][1]<< " with aperture "<< (edges_vector_[k]).aperture;
        std::cout<< "  and permeabilities: " << (edges_vector_[k]).kfn << "\t"<<(edges_vector_[k]).kft<<std::endl;
    }
#endif

    //***********************************************************************//
    //Create the Elements in Dune::GridFactory //
    //***********************************************************************//

    /*
     * In the first position either -1 or the first "real" element id which was connected to this crossing is stored.
     * This entry is deleted after the inserting-virtual-elements loop.
     * The following entries store the ids of all the virtual elements connected to this node
     * sorted by the associated angle.
     */
    typedef std::map<int, ArtIdxList> CrossingNodeIdToElementIdMap;
    typedef CrossingNodeIdToElementIdMap::iterator CniteimIterator;
    CniteimIterator cniteimIt;
    CrossingNodeIdToElementIdMap crossingNodeIdToElementIdMap;

    Edge newVirtualEdge;
    typedef typename Dune::FieldVector<Scalar, 2> FV2D;
    int edge_number_increment=0;

    /*
     * Inserting the virtual elements at crossings/bendings
     * Because the art format gives the fracture network in the coarsest possible way
     * every node which is not on the global boundary or represents an in-the-domain
     * ending fracture has to be treated and virtual elements have to be introduced.
     */
    for (int i=0; i<edge_number; i++)//loop over all edges (elements for 1d in 2d environment)
    {
        /*
         * edges_vector is a standard vector of a struct. The struct has a random access operator
         * which gives at 0 and 1 the index of the corresponding vertex
         */
        for (int j=0;j<2;j++){//use only entries 0,1
            int jOpposite= (1 ? j==0 : 0);
            int tmpIdx= edges_vector_[i][j];
            cniteimIt=crossingNodeIdToElementIdMap.find(tmpIdx);
            /*
             * if vertex index is not found, then add the vertex index to the map
             * and set the associated value to the element index to which this node belongs
             */
            if (cniteimIt == crossingNodeIdToElementIdMap.end() )
            {
                (crossingNodeIdToElementIdMap[tmpIdx]).push_back(i);
            }
            /*
             * TODO: is that really true?: if vertex index is found, then
             * it still has to be checked if
             * this is a node inside the domain and it is not an ending fracture.
             * For every crossing virtual elements have to be introduced.
             */
            else
            {
                Scalar cipEpsilon=Dumux::equalityEpsilon<Scalar>()/100.0;
                for (int k=0; k<2; k++)//use only x,y coordinates and skip z
                {
                    position[k]=vertices_[ tmpIdx ][k];
                }
                /*
                 * Once for the first fracture which ended here,
                 * but couldn't be treated in the first if loop, because
                 * at that moment it was not clear if it's just an ending fracture.
                 */
                if ( (crossingNodeIdToElementIdMap[tmpIdx]).front() != -1){
                    /*
                     * inserting node at the same position.
                     * To avoid assertion errors in Alberta the vertex is slightly moved
                     * in the direction of the fracture element
                     * This is done right before the insertion of the newVirtualEdge into the edges_vector_
                     */
                    vertices_.push_back(vertices_[ tmpIdx ]);
                    vertex_number++;
                    /*
                     * Insert the new virtual element
                     * edges_vector_[i][j] is the index of the new crossing node.
                     * This index is always stored in position [0] of the new virtual edge,
                     * i.e.
                     */
                    newVirtualEdge[0]=tmpIdx;
                    newVirtualEdge[1]=(vertices_.size()-1);
                    newVirtualEdge.aperture= (edges_vector_[i]).aperture;

                    newVirtualEdge.kfn = vertices_[ tmpIdx ].kfn;
                    newVirtualEdge.kft = vertices_[ tmpIdx ].kft;
                    newVirtualEdge.k12 = vertices_[ tmpIdx ].k12;

                    int jOld=1;
                    int jOldOpposite=0;
                    if (edges_vector_[ (crossingNodeIdToElementIdMap[tmpIdx]).front() ][0]==tmpIdx){
                        jOldOpposite=1;
                        jOld=0;
                    }
                    /*
                     * calculating the angle of this virtual fracture element
                     * with the position of the nodes of the connected real element
                     */
                    newVirtualEdge.angle=orientationAngle(vertices_[tmpIdx], vertices_[edges_vector_[ (crossingNodeIdToElementIdMap[tmpIdx]).front() ][jOldOpposite]]);
                    FV2D changeInPosition(Dumux::fractureOrientationVector<FV2D>(newVirtualEdge.angle));
                    changeInPosition*=cipEpsilon;
//                    std::cout << " changeInPosition: " << changeInPosition << "\t new position: "<<position-changeInPosition << std::endl;
                    vertices_[vertices_.size()-1].position=position-changeInPosition;
                    factory_.insertVertex(vertices_[vertices_.size()-1].position);
                    edges_vector_.push_back(newVirtualEdge);
                    edge_number_increment++;
                    /*
                     * add the element index to the list of elements which are connected to this node
                     */
                    (crossingNodeIdToElementIdMap[tmpIdx]).push_back(edges_vector_.size()-1);

                    /*
                     * change the old element node index
                     * crossingNodeIdToElementIdMap[tmpIdx] gives the element index
                     * of the first element which shares this node
                     */
                    edges_vector_[ (crossingNodeIdToElementIdMap[tmpIdx]).front() ][jOld] = (vertices_.size()-1);
                    /*
                     *  change the entry in the map so that this if loop is carried out only once
                     */
                    (crossingNodeIdToElementIdMap[tmpIdx]).front()=-1;
                }
                /*
                 * For every virtual element one additional node has to be
                 * inserted at the same geometrical position of the old node.
                 * This is done by adding a vertex to the vertex list at the end.
                 * The new index is (size-1).
                 * The old centre node remains the same with the same index.
                 */
                vertices_.push_back(vertices_[ tmpIdx ]);
                vertex_number++;
                /*
                 * calculating the angle of this virtual fracture element
                 * with the position of the nodes of the connected real element
                 */
                newVirtualEdge.angle=orientationAngle(vertices_[tmpIdx], vertices_[edges_vector_[i][jOpposite]]);
                /*
                 * Insert the new virtual element with the same aperture as the old one.
                 */
                newVirtualEdge[0]=tmpIdx;
                newVirtualEdge[1]=(vertices_.size()-1);
                newVirtualEdge.aperture= (edges_vector_[i]).aperture;
                newVirtualEdge.kfn = vertices_[ tmpIdx ].kfn;
                newVirtualEdge.kft = vertices_[ tmpIdx ].kft;
                newVirtualEdge.k12 = vertices_[ tmpIdx ].k12;
                FV2D changeInPosition(Dumux::fractureOrientationVector<FV2D>(newVirtualEdge.angle));
                changeInPosition*=cipEpsilon;
//                std::cout << " changeInPosition: " << changeInPosition << "\t new position: "<<position-changeInPosition << std::endl;
                vertices_[vertices_.size()-1].position=position-changeInPosition;
                factory_.insertVertex(vertices_[vertices_.size()-1].position);
                edges_vector_.push_back(newVirtualEdge);
                edge_number_increment++;
                /*
                 * add the element index to the list of elements which are connected to this node
                 */
                (crossingNodeIdToElementIdMap[tmpIdx]).push_back(edges_vector_.size()-1);
                /*
                 * change the old element node index
                 */
                edges_vector_[i][j] = (vertices_.size()-1);

            }
        }
    }
    edge_number+=edge_number_increment;

    std::vector<int> removeEntriesFromMap;
    for (CrossingNodeIdToElementIdMap::iterator cniteimIt=crossingNodeIdToElementIdMap.begin(); cniteimIt!=crossingNodeIdToElementIdMap.end(); cniteimIt++)
    {
        /*
         * Mark all entries of nodes which are not crossings for removal, i.e.,
         * no ids of elements connected to this node are stored in the map.
         */
        if (((*cniteimIt).second).size()==1) {
            removeEntriesFromMap.push_back((*cniteimIt).first);//the entry should not be removed while iterating over the map!
        }
        /*
         * remove first entry in list (which is -1) so that
         * every value in the map contains a sorted list of element ids connected
         * to the node idx (the key in the map)
         * and sort by the corresponding angle
         */
        else {
            ((*cniteimIt).second).pop_front();
            // using function as comparison
            compareEdges<FractureGridType> cmp(*this);
            ((*cniteimIt).second).sort(cmp);
        }
    }
    /*
     * remove all entries from map which are marked for removal
     */
    typename std::vector<int>::iterator vecIt;
    for (vecIt=removeEntriesFromMap.begin();vecIt!=removeEntriesFromMap.end();vecIt++ ){
        CniteimIterator cniteimIt=crossingNodeIdToElementIdMap.find(*vecIt);
        crossingNodeIdToElementIdMap.erase(cniteimIt);
    }

    /*
     * iterate over all crossings
     */
    for (CrossingNodeIdToElementIdMap::iterator cniteimIt=crossingNodeIdToElementIdMap.begin(); cniteimIt!=crossingNodeIdToElementIdMap.end(); cniteimIt++)
    {
        FV2D crossing(0);
        std::vector<FV2D> vectorOfGreenCorners;
        for (int l=0;l<2;l++){
            crossing[l]=(vertices_[(*cniteimIt).first])[l];
        }
//        std::cout << "crossing at: " << crossing << std::endl;
        /*
         * iterate over all elements attached to this crossing
         */
        for (ArtIdxList::iterator listIterator=((*cniteimIt).second).begin(); listIterator!=((*cniteimIt).second).end();listIterator++){
            auto tmpEdge=edges_vector_[*listIterator];
            /*
             * calculate the fracture direction vector, fi, from the angle
             */
            FV2D fi=Dumux::fractureOrientationVector<FV2D>(tmpEdge.angle);
            Scalar apertureI=tmpEdge.aperture;
//            std::cout << "fi: " << fi << "\t a: " << apertureI << std::endl;
            /*
             * calculate two points, tmpA and tmpB,
             * which lie on the negative normal side of the two-dimensional fracture boundary
             * of the fracture element i
             */
            FV2D tmpA(Dumux::normal(fi));
            tmpA*=(-0.5)*apertureI;
            tmpA+=crossing;
            FV2D tmpB(tmpA);
            tmpB+= fi;
            /*
             * Go to the next element.
             * If the recent element is the last, the next is the first in the list.
             */
            listIterator++;
            if (listIterator == ((*cniteimIt).second).end() ) {listIterator=((*cniteimIt).second).begin();}

            tmpEdge=edges_vector_[*listIterator];
            /*
             * calculate the fracture direction vector, fi, from the angle
             * for the (i+1)th element
             */
            apertureI=tmpEdge.aperture;
            fi=Dumux::fractureOrientationVector<FV2D>(tmpEdge.angle);
//            std::cout << "fi: " << fi << "\t a: " << apertureI << std::endl;
            /*
             * calculate two points, tmpC and tmpD,
             * which lie on the positive normal side of the two-dimensional fracture boundary
             * of the fracture element (i+1)
             */
            FV2D tmpC(Dumux::normal(fi));
            tmpC*=0.5*apertureI;
            tmpC+=crossing;
            FV2D tmpD(tmpC);
            tmpD+= fi;
            /*
             * calculate the greenCorner in the positive normal direction of the fracture
             * the "real" geometrical intersection of neighbouring fractures (with thickness)
             * gives the green corners. Use the intersectionOfLines function but do not check,
             * if the intersection happens between the given points.
             * It has to be checked, however, if the lines are parallel!
             */
            FV2D greenCorner;
            Dumux::intersectionOfLines(tmpA,tmpB,tmpC,tmpD,greenCorner);
//            std::cout << "intersection? " << Dumux::intersectionOfLines(tmpA,tmpB,tmpC,tmpD,greenCorner) << std::endl;
            vectorOfGreenCorners.push_back(greenCorner);
//            std::cout << "green corners old: " << greenCorner << std::endl;
            if (listIterator == ((*cniteimIt).second).begin() ) {
                listIterator=((*cniteimIt).second).end();
            }
            listIterator--;
        }//end iteration over all fracture elements attached to this crossing

        FV2D lastGreenCorner=vectorOfGreenCorners.back();
        /*
         * iterate over all elements attached to this crossing
         */
        int greenCornerIdx=0;
        for (ArtIdxList::iterator listIterator=((*cniteimIt).second).begin(); listIterator!=((*cniteimIt).second).end();listIterator++){
            auto &tmpEdge=edges_vector_[*listIterator];
            FV2D ti(Dumux::normal(vectorOfGreenCorners[greenCornerIdx]-lastGreenCorner));
            /*
             * scale it, so that the norm has the length of crossection
             */
            Scalar di= (vectorOfGreenCorners[greenCornerIdx]-lastGreenCorner).two_norm();
//            std::cout << "di: " << di << std::endl;
            ti*= (di/ti.two_norm());
//            std::cout << "ti: " << ti << std::endl;
            tmpEdge.ti=ti;
            /*
             * calculate the fracture normal vector, ni, from the angle
             */
            FV2D fi=Dumux::fractureOrientationVector<FV2D>(tmpEdge.angle);

            Scalar apertureI=tmpEdge.aperture;
            FV2D ni=Dumux::normal(fi);
            ni*=0.5*apertureI;
            /*
             * determine the real geometrical position of pi
             */
            FV2D pi1(vectorOfGreenCorners[greenCornerIdx]);
            pi1+=ni;
            FV2D pi2(lastGreenCorner);
            pi2-=ni;
            FV2D pi= ( (pi1-crossing).two_norm() > (pi2-crossing).two_norm() ? pi1 : pi2);
            FV2D piOld= ( (pi1).two_norm() > (pi2).two_norm() ? pi1 : pi2);
//            std::cout << "pi old: " << piOld << "\t pi new: " << pi << std::endl;
            /*
             * calculate the real distance between pi and pI
             */
            Scalar li= (pi-crossing).two_norm();
            tmpEdge.li=li;
            /*
             * increment the greenCorner
             */
            lastGreenCorner=vectorOfGreenCorners[greenCornerIdx];
            greenCornerIdx++;
        }

    }//end iteration over all crossings


    /*
     * CrossingCheckMap checks for double crossings which cannot be handled by Alberta.
     * It iterates over every element and then over every node and county the number of "visits",
     * starting with (-1) for the first visit, counting to (-2) for the second visit.
     * For the third visit a new node is inserted but the corresponding old node index is not stored.
     * This connection is computed and stored in fractureNetworkCouplingMap because DUNE indices have to be used
     * and not the ART indices.
     */

    typedef std::map<int, int> CrossingCheckMap;
    CrossingCheckMap::iterator it;
    CrossingCheckMap crossingCheckMap;

    for (int i=0; i<edge_number; i++)//loop over all egdes (elements for 1d in 2d environment)
    {
        std::vector<unsigned int> nodeIdx(2);
//        Dune::FieldVector<Scalar,3> point(0);
#if PLOT
        std::cout<<"====================================="<<std::endl;
        std::cout<<"globalElemIdx "<<i<<std::endl;
        std::cout<<"====================================="<<std::endl;
#endif
        /*
         * eges_vector is a standard vector of a struct. The struct has a random access operator
         * which gives at 0 and 1 the index of the corresponding vertex
         */
        for (int j=0;j<2;j++){//use only entries 0,1
        it=crossingCheckMap.find(edges_vector_[i][j]);
        /*
         * if vertex index is not found, then add the vertex index to the map and set the associated value to -1
         */
        if (it == crossingCheckMap.end() )
        {
            crossingCheckMap[edges_vector_[i][j]]=-1;
        }
        /*
         * if vertex index is found and the value is -1, then set the associated value to -2
         */
        else if (crossingCheckMap[edges_vector_[i][j]]==-1)
        {
            crossingCheckMap[edges_vector_[i][j]]=-2;
        }
        /*
         * the vertex is already associated to two elements,
         * i.e. the vertex has to be added again because Alberta cannot handle crossings properly
         * and all future appearances of this vertex index have to be replaced by the new vertex index
         */
        else if (crossingCheckMap[edges_vector_[i][j]]==-2)
        {
            for (int k=0; k<2; k++)//use only x,y coordinates and skip z
            {
                position[k]=vertices_[ edges_vector_[i][j] ][k];
            }
            factory_.insertVertex(position);
            vertices_.push_back(vertices_[ edges_vector_[i][j] ]); //add vertex to the vertex list at the end. The new index is (size-1).
            equalIdxList_.push_back (edges_vector_[i][j]);//every time a vertex has to be doubled, the ArtIdx is added to this list.
            crossingCheckMap[edges_vector_[i][j]]=(vertices_.size()-1);//change the value of the old vertex-index-key to the new vertex index
            edges_vector_[i][j]=(vertices_.size()-1);//change the vertex index in the edge to the new vertex index
            crossingCheckMap[edges_vector_[i][j]]=-1;//change the value to the new vertex-index-key to -1
        }
        /*
         * This else loop is necessary because only if more than 2 edges share the same node
         * - every second node an additional "virtual" node is introduced in the else-if loop above.
         */
        else //if crossingCheckMap[edges_vector_[i][j]]>0
        {
            int temp=crossingCheckMap[edges_vector_[i][j]];
            crossingCheckMap[edges_vector_[i][j]]=-2;
            edges_vector_[i][j]=temp;
            crossingCheckMap[edges_vector_[i][j]]=-2;
        }

        /*
         * re-store the indices from the struct in a simple standard vector
         * because this is required by the grid factory
         */
        nodeIdx[j]=edges_vector_[i][j];
        }
        const Dune::GeometryType type( Dune::GeometryType::simplex, 1 );
        factory_.insertElement(type, nodeIdx);
        equalIdxList_.sort();//sort list
        equalIdxList_.unique();//remove doubled entries (works only on sorted lists).
    }

    /*
     * temporary DGF file is written
     */
    FractureGridType *fractureGrid = factory_.createGrid();
    typedef typename FractureGridType::LeafGridView FGV;
    typedef Dune::DGFWriter< FGV > DGFWriter;
    FGV fgv = fractureGrid->leafGridView();
    DGFWriter dgfWriter(fgv);
    dgfWriter.write("tempFGV.dgf");

    /*
     * add permeability and aperture information to the DGF file
     */
    std::string inbuf;
    std::fstream input_file("tempFGV.dgf", std::ios::in);
    simulationNameString+=".dgf";
    std::ofstream output_file(simulationNameString);

    while (!input_file.eof())
    {
        std::getline(input_file, inbuf);
        int spot = inbuf.find("CUBE");
        if(spot >= 0 )
        {
            inbuf+="\nPARAMETERS 9";
            output_file << inbuf << std::endl;
            std::getline(input_file, inbuf);
            for (int edges_vector_index=0;edges_vector_index<edges_vector_.size();edges_vector_index++)
            {
                /*
                 * write aperture, kfn, kft, 0, -1.0, -1.0, -1.0, -1.0 for normal elements and
                 * aperture, kIx, kIy, k12, l, t_x, t_y, angle for virtual elements
                 * angle is -1.0 for real elements
                 */

                std::string aperture = boost::lexical_cast<std::string>(edges_vector_[edges_vector_index].aperture);
                std::string kfn = boost::lexical_cast<std::string>(edges_vector_[edges_vector_index].kfn);
                std::string kft = boost::lexical_cast<std::string>(edges_vector_[edges_vector_index].kft);
                std::string k12 = boost::lexical_cast<std::string>(edges_vector_[edges_vector_index].k12);

                std::string li = boost::lexical_cast<std::string>(edges_vector_[edges_vector_index].li);
                std::string tix = boost::lexical_cast<std::string>( (edges_vector_[edges_vector_index].ti)[0] );
                std::string tiy = boost::lexical_cast<std::string>( (edges_vector_[edges_vector_index].ti)[1] );
                std::string theta = boost::lexical_cast<std::string>(edges_vector_[edges_vector_index].angle);
                std::string color = boost::lexical_cast<std::string>(edges_vector_[edges_vector_index].color);

                inbuf+=" " +aperture+ " " +kfn + " " + kft + " " + k12 + " " + li + " " + tix + " " + tiy + " " + theta + " " + color  ;
                output_file << inbuf << std::endl;
//                std::cout << inbuf << std::endl;
                std::getline(input_file, inbuf);
            }
        }
        output_file << inbuf << std::endl;
    }
    GridPointer gridPtr = GridPointer(simulationNameString);
    remove( "tempFGV.dgf" );
    return (gridPtr);
}//End createGrid()


//TODO This Docu is wrong!

// -1 if j is an unmodified row
// -3/-4 if j is a sum row with a 3-/4-crossing
// larger than zero to indicate the row to which this is added

/*
 * fractureNetworkCouplingMap is a map which is necessary for fracture networks with crossing fractures.
 * It stores the Id of the DUNE fracture grid node as key whenever there is a crossing.
 * In general this works for every number of crossings at one point, though the XFEM implementation does not.
 * For every geometric position at the key of the lowest Dune id the number of connected elements is stored (with a negative sign).
 * For the other Dune ids a -1 is stored as value.
 *
 * example:
 * % Vertices: x y z
 *
 *  1.000   0.   0.00000
 *  1.000   0.   0.00000
 *  0.000  -1.0  0.00000
 *  0.000   0.   0.00000
 *  0.000   1.0  0.00000
 *  1.000   1.0  0.00000
 *
 * % Edges (Indices to List of Points):
 * 0 4
 * 1 4
 * 2 4
 * 1 2
 * 5 4
 * 1 3
 * 2 3
 *
 * will lead to the following duneIdMap (where the ids are somehow arbitrary):
 *  5  -> -4
 *  9  -> -3
 * 13  -> -3
 * 17  ->  5
 * 25  ->  9
 * 33  -> 13
 *
 * nodeId  5   position: 0.0 0.0
 * nodeId 17   position: 0.0 0.0
 * nodeId  9   position: 0.2 1.0
 * nodeId 25   position: 0.2 1.0
 * nodeId 13   position: 1.0 0.2
 * nodeId 33   position: 1.0 0.2
 *
 */
typedef typename FractureGridType::LeafGridView FGV;
typedef typename FractureGridType::Traits::GlobalIdSet::IdType FractureGridIdType;
typedef typename std::map<FractureGridIdType,int> DuneIdMap;//the type of the value of this map has to be int.
//Even though sometimes also ids are stored also negative values must be stored.
//Since The FractureGridIdType can be unsigned int, this would lead to problems.
typedef typename DuneIdMap::iterator DuneIdMapIterator;
typedef typename std::pair<FractureGridIdType,int> Pair;
template<typename GridPointerFracture, typename GB>
DuneIdMap fractureNetworkCouplingMap(GridPointerFracture *fractureGridPointer, const GB &gb){
    FractureGridType &grid_= **fractureGridPointer;
    FGV fgv = grid_.leafGridView();
    DuneIdMap couplingMap;
    //loop over every fracture network node
    for (VerticesVectorIt vertexIt = vertices_.begin(); vertexIt != vertices_.end(); ++vertexIt) {

        //get global position of ArdIdx node *it1
        Dune::FieldVector<Scalar,2> position;
        for (int i=0; i<2; i++)//use only x,y coordinates and skip z and the rest of the vertex struct
        {position[i]=(*vertexIt)[i];}
        bool onGlobalBoundary=gb.onGlobalBoundary(position);
        DuneIdMap couplingMapTemp;
        std::pair<DuneIdMapIterator,bool> retCouplingMapTemp;
        //iterate over fracture grid nodes
        int numberOfNodes=0;
        /*
         * First a loop over every element in the leaf view is started.
         * Then over every intersection is looped and checked which of the nodes has the same
         * position as stored in position from the equalIdxList.
         * This node id is then added to the couplingMapTemp as key with the value zero.
         * TODO DOC THIS FUNCTION MORE
         */
        typedef typename FGV::template Codim<0>::Iterator ElementIterator;
        typedef typename FGV::IntersectionIterator IntersectionIterator;
        for (ElementIterator fractureElementIt = fgv.template begin<0>(); fractureElementIt != fgv.template end<0>(); ++fractureElementIt) {
            for (IntersectionIterator isIt = fgv.ibegin(*fractureElementIt); isIt != fgv.iend(*fractureElementIt); ++isIt){
                //find the Dune node Id which corresponds to the ArtIdx by comparing the geometrical position
                if (Dumux::arePointsEqual(isIt->geometry().center(),position,1.0e-6)){
                    FractureGridIdType nodeId = grid_.globalIdSet().subId(*(isIt->inside()), isIt->indexInInside(), 1);
                    retCouplingMapTemp=couplingMapTemp.insert ( Pair(nodeId,-1) );
                    if (retCouplingMapTemp.second==true){
                        if ( onGlobalBoundary ){
                            couplingMapTemp[nodeId]=0;
                            break;
                        }
                        numberOfNodes++;
                    }
                }//end loop over every fracture intersections (nodes)
            }//end if over virtual elements
        }//end loop over fracture elements

        if (numberOfNodes==6) numberOfNodes++;
        if ( !onGlobalBoundary && numberOfNodes!=1){
            for (ElementIterator fractureElementIt = fgv.template begin<0>(); fractureElementIt != fgv.template end<0>(); ++fractureElementIt) {
                if (fractureGridPointer->parameters(*fractureElementIt)[7] < -0.5 ){//loop over real elements && isCrossingNode
                    for (IntersectionIterator isIt = fgv.ibegin(*fractureElementIt); isIt != fgv.iend(*fractureElementIt); ++isIt){
                        //find the Dune node Id which corresponds to the ArtIdx by comparing the geometrical position
                        if (Dumux::arePointsEqual(isIt->geometry().center(),position,1.0e-6)){
                            FractureGridIdType nodeId = grid_.globalIdSet().subId(*(isIt->inside()), isIt->indexInInside(), 1);
                            numberOfNodes--;
                            couplingMapTemp[nodeId]=-10;
                        }
                    }//end loop over every fracture intersections (nodes)
                }//end else over real elements
            }//end loop over fracture elements

            /*
             * loop over the temporary couplingMap which contains all crossing node ids
             * The first id gets the number of nodes (multiplied with -1) as value.
             * Every further key (node id) gets the first node id.
             */
            FractureGridIdType temp=-1000;
            /*
             * in the final map not the number of nodes are stored but the number (multiplied with (-1) )
             * of connecting fracture elements, i.e.,
             * ending:      -1
             * bending:     -2
             * 3-crossing:  -3
             * 4-crossing:  -4
             * and a node on the global boundary 0.
             * Nodes on between virtual and real elements get the number of the corresponding crossing
             * multiplied by 10, i.e,
             * bending:     -20
             * 3-crossings: -30
             * 4-crossings: -40
             */

            numberOfNodes++;
            bool first=true;
            for (DuneIdMapIterator it=couplingMapTemp.begin() ; it != couplingMapTemp.end(); it++ ){
//                std::cout << "numberOfNodes: " << numberOfNodes<<std::endl;
                if ((*it).second ==-10){
                    (*it).second = -10*numberOfNodes;
                }
                else{
                    if (first) {
                        first=false;
                        (*it).second =(-numberOfNodes);
                        temp=(*it).first;
                    }
                    else{
                        assert(temp>0);
                        (*it).second = temp;
                    }
                }
            }
        }//end if loop for non ending nodes (boundary and inner)
        couplingMap.insert(couplingMapTemp.begin(),couplingMapTemp.end());

    }//end loop over artIdxList

//    for (DuneIdMapIterator it=couplingMap.begin();it!=couplingMap.end();it++ )
//    {
//        std::cout << "id: " << (*it).first << "\t \t value: " << (*it).second << std::endl;
//    }
    return (couplingMap);
}

/*
 * update function for CouplingMap which converts Ids to indices
 * Because of performance optimization on a coarse grid the IDs are stored.
 * After each refinement those IDs are converted to indices with this function.
 */
typedef typename FGV::IndexSet::IndexType FractureGridIdxType;
typedef std::map<FractureGridIdxType,int> DuneIdxMap;
typedef std::map<FractureGridIdType,FractureGridIdType> DuneIdToIdxMap;
typename DuneIdToIdxMap::iterator idToIdxIt;

DuneIdxMap gridCouplingMapToIdx(FractureGridType& grid_, DuneIdMap& couplingMap){
    typedef typename std::pair<FractureGridIdType,int> IdxPair;
    typedef typename FGV::template Codim<1>::Iterator NodeIterator;
    DuneIdxMap duneIdxMap;
    DuneIdToIdxMap duneIdToIdxMap;
    /*
     * Because the number of nodes is usually much larger than the number of entries in the map,
     * the node iterator loop should be outside of the map loop!
     * this is a loop over the grid (not the gridview) because ids are needed
     */
    for (NodeIterator nodeIterator = grid_.template leafbegin<1>(); nodeIterator != grid_.template leafend<1>(); ++nodeIterator) {
        /*
         * every node is in the coupling map,
         * but only positive numbers and associated have (-3 or -4)
         * have to be copied to the duneIdxMap
         */
        FractureGridIdType nodeId = grid_.globalIdSet().id(*nodeIterator);
        DuneIdMapIterator it=couplingMap.find(nodeId);
        if (it!=couplingMap.end()){//nodeId is found
            if ( (*it).second>0 || (*it).second==-3 || (*it).second==-4){
                FractureGridIdxType nodeIdx = grid_.leafIndexSet().index(*nodeIterator);
                if((*it).second<0){//and it is the smallest Id of connected nodes
                    duneIdxMap.insert ( IdxPair(nodeIdx,(*it).second) );
                    duneIdToIdxMap[nodeId]=nodeIdx;
                }
                else{
                    idToIdxIt=duneIdToIdxMap.find((*it).second);
                    if (idToIdxIt!=duneIdToIdxMap.end()){
                        duneIdxMap.insert ( IdxPair(nodeIdx,(*idToIdxIt).second) );
                    }
                    else std::cout << "Error in gridCouplingMaptoIdx!!!"<<std::endl;
                }
            }
        }
    }
    return (duneIdxMap);
}

private:
/*
 * calculates the mathematical positive angle between the x-axis and the vector which points towards the crossing
 * it operates on global coordinates
 */

template<typename FV>
Scalar orientationAngle(const FV &crossing, const FV &notCrossing) {
    assert((crossing.position).dimension==2);
    FV fracture(crossing);
    fracture -= notCrossing;
    fracture[0]/=fracture.two_norm();
    fracture[1]/=fracture.two_norm();
    Scalar angle= std::acos( std::abs(fracture[0]) );
    /*
     * make the angle unique,
     * remark: a fracture in the first quadrant pointing towards zero
     * fullfills x<0 and y<0.
     * 1st quadrant: alpha= pi+acos(fx)
     * 2nd quadrant: alpha= 2pi-acos(fx)
     * 3rd quadrant: alpha= acos(fx)
     * 4th quadrant: alpha= pi-acos(fx)
     */
    if (fracture[0]<=0 && fracture[1]<=0 ) {angle+=M_PI;}//first quadrant
    else if (fracture[0]>=0 && fracture[1]<=0 ) {angle=2.0*M_PI-angle;}//second quadrant
    else if (fracture[0]<=0 && fracture[1]>=0) {angle=M_PI-angle;}//fourth quadrant
    return (angle);
}// end orientationAngle


    VerticesVector vertices_;
    EdgesVector edges_vector_;
//  FacesVector faces_vector_ ;
    int vertex_number;
    int edge_number;
    int face_number; //in 2D
    int element_number; //in 3D
    Dune::GridFactory< FractureGridType > factory_;
    ArtIdxList equalIdxList_; //list of every node (ArtIdx) which is doubled
}; //End ArtReader

}//end namespace Dumux

#endif // ART_READER_1D_HH
