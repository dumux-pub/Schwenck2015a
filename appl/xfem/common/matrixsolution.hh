#ifndef MATRIXSOLUTION_HH_
#define MATRIXSOLUTION_HH_

#include<dune/geometry/type.hh>

namespace Dumux{

template<typename MultiDomainGrid, typename Map, typename MultiGFS, typename EnrichedProblem, typename X>
class MatrixSolution{

    typedef typename MultiDomainGrid::LeafGridView MDGV;
    typedef typename MultiDomainGrid::ctype ctype;
    typedef typename MDGV::template Codim<0>::Iterator HostGridIterator;
    typedef typename Dune::PDELab::LocalFunctionSpace<MultiGFS> MultiLFSU;
    typedef typename EnrichedProblem::Traits::TrialLocalFunctionSpace EPLFS;
    typedef typename EPLFS::template Child<0>::Type LBFS;       // LBFS is the local base function space within the XFEM area (EnrichedProblem)
    typedef typename EPLFS::template Child<1>::Type LEFS0;
    typedef typename EPLFS::template Child<2>::Type LEFS1;
    typedef typename EPLFS::template Child<3>::Type LEFS2;
    typedef typename EPLFS::template Child<4>::Type LEFS3;
    typedef typename EPLFS::template Child<5>::Type LEFS4;

    static const int dimw_=MultiDomainGrid::dimensionworld;
    static const int dim_=MultiDomainGrid::dimension;
    typedef typename Dune::FieldVector<ctype,dimw_> FV;

    typedef typename LBFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits FTraits;
    typedef typename FTraits::DomainType D;
    typedef typename FTraits::RangeFieldType RF;
    typedef typename FTraits::RangeType R;
    typedef typename FTraits::DomainFieldType DF;
    typedef typename FTraits::JacobianType JacobianType;
    typedef typename LBFS::Traits::SizeType sizeType;

    typedef typename X::field_type Scalar;


//TODO adapt this for more enriched spaces
    typedef typename Dune::PDELab::GridFunctionSubSpace<MultiGFS,Dune::TypeTree::template TreePath<0> > SHGFS;
    typedef typename SHGFS::Traits::GridViewType GVB;
    typedef typename GVB::template Codim< 0 >::Entity E;
    typedef Dune::PDELab::ElementGeometry<E> EG;
    typedef typename Dumux::modifiedBasisFunction<MultiDomainGrid, E, Map, MultiLFSU> MBF;

    typedef Dune::PDELab::LFSIndexCache<MultiLFSU> MultiLFSCache;
    typedef typename X::template LocalView<MultiLFSCache> XView;//X is an ISTLBlockVectorContainer
    typedef typename X::template ConstLocalView<MultiLFSCache> ConstXView;

    //typedefs for global evaluation of pressure via grid function
    typedef Dune::PDELab::DiscreteGridFunction<SHGFS,X> MDGF;
    typedef Dune::PDELab::GridFunctionToFunctionAdapter<MDGF> MGF;
    typedef typename MGF::Traits::RangeType RangeTypeGF;

public:
    /*
     * TODO doc me!
     */
MatrixSolution(MultiDomainGrid& multiDomainGrid, Map& intersectionPointsMap,  MultiGFS& multigfs,EnrichedProblem& ep, X& x) //x FE solution
    : multiDomainGrid_(multiDomainGrid), intersectionPointsMap_(intersectionPointsMap), multigfs_(multigfs), ep_(ep),x_(x),
      gradientDistance_(Dumux::equalityEpsilon<typename X::field_type>()),
      multilfsu_(multigfs),shgfs_(multigfs),mdgf_(shgfs_,x),mgf_(mdgf_)
{}
private:
/*
 * This gets the global coordinate as input and iterates
 * over the matrix grid to find the element in which global lies.
 * It then calculates the local coordinates and returns the pointer to this element.
 * It also detects if this matrix element is cut by a fracture (elementIsEnriched == true).
 */
HostGridIterator findElement(const FV &global, FV &local, bool &elementIsEnriched){

    typedef typename MultiDomainGrid::LeafGridView MDGV;
    MDGV mdgv=multiDomainGrid_.leafGridView();

    //iterate over the whole matrix multidomaingrid and find the element in which global lies.
    HostGridIterator eendit = multiDomainGrid_.template leafend<0>();
    HostGridIterator matrixElementIterator = multiDomainGrid_.template leafbegin<0>();
    for (; matrixElementIterator != eendit; ++matrixElementIterator) {
        elementIsEnriched=ep_.condition()(mdgv.indexSet().subDomains(*matrixElementIterator));
        multilfsu_.bind(*matrixElementIterator);
        local=matrixElementIterator->geometry().local(global);
        typedef typename Dune::ReferenceElements<ctype,dimw_> GRE;
        Dune::GeometryType gt = matrixElementIterator->geometry().type();
        if (GRE::general(gt).checkInside(local) ){
            break;
        }
    }
    assert(matrixElementIterator!=eendit);
    return (matrixElementIterator);
}

/*
 * this constructs the local basis and
 * is just a convenience function to get also gradientDistance_ filled.
 */
template<typename MEIT>
MBF constructMBF(const MEIT &meit, const FV &global, const FV &local){
    MBF mbf(multiDomainGrid_, *meit, intersectionPointsMap_, multilfsu_);
    FV fractureNormal=mbf.fN(local);
    fractureNormal*=mbf.template normalEpsilon<Scalar>();//make the normal small
    fractureNormal+=local;
    gradientDistance_=(global-meit->geometry().global(fractureNormal)).two_norm();
    return (mbf);
}

RF dfn(){
    return(gradientDistance_);
}
protected:
RF evaluatePressureGlobal(const FV &global) {
    //only for enriched elements evaluate the complex basis
    FV local;
    RF p_fe=0.0;
    bool elementIsEnriched=false;
    auto meit=this->findElement(global,local,elementIsEnriched);
    if (elementIsEnriched){
        MBF mbf=constructMBF(meit,global,local);
        p_fe=this->evaluatePressureLocal(local,mbf);
    }
    else {
        //for standard elements evaluate a grid function
        RangeTypeGF pM;
        mgf_.evaluate(global,pM);
        p_fe=pM;
    }
    return (p_fe);
}//end evaluate function

private:
RF evaluatePressureLocal(const FV &local, const MBF &mbf) {

    MultiLFSCache multilfs_cache_(multilfsu_);//TODO move this to the constructor or even before!
    ConstXView x_view_(x_);

    RF p_fe(0.0);//TODO check what happens if p_fe is returned without a proper value (p_fe=0.0)?
    EPLFS eplfs(multilfsu_,ep_);

    const LBFS& lbfs = eplfs.template child<0>();
    const LEFS0& lefs0 = eplfs.template child<1>();
    const LEFS1& lefs1 = eplfs.template child<2>();
    const LEFS2& lefs2 = eplfs.template child<3>();
    const LEFS3& lefs3 = eplfs.template child<4>();
    const LEFS4& lefs4 = eplfs.template child<5>();

    eplfs.bind();

    std::vector<R> xl(lbfs.maxSize());        // local coefficients
    multilfs_cache_.update();
    x_view_.bind(multilfs_cache_);
    x_view_.read(xl);// get local coefficients of the solution

    std::vector<R> b(lbfs.maxSize()); // shape function values

    R p_fe_temp(0.0);
    {
        p_fe_temp*=(0.0);
        // evaluate finite element function at local coordinate (standard basis function)
        //evaluating left hand side
        lbfs.finiteElement().localBasis().evaluateFunction(local,b);
        b=mbf.modify(b,local,0);
        for (int i=0; i<lbfs.size(); i++){
            p_fe_temp.axpy(xl[lbfs.localIndex(i)],b[i]);
        }
        p_fe+=p_fe_temp;
    }
    // evaluate finite element function at local coordinate (enriched basis function)
    //TODO: works only if lblfs.size=lelfs.size, i.e. only ONE additional dof at every standard dof, i.e. no fracture crossings and only one fracture per element
    if (lefs0.size()>0){
        std::vector<R> e(lefs0.maxSize()); // shape function values
        //evaluating left hand side
        p_fe_temp*=0.0;
        lefs0.finiteElement().localBasis().evaluateFunction(local,e);
        e=mbf.modify(e,local,1);
        for (int i=0; i<lefs0.size(); i++){
            p_fe_temp.axpy(xl[lefs0.localIndex(i)],e[i]);
        }
        p_fe+=p_fe_temp;
    }
    if (lefs1.size()>0){
        std::vector<R> e(lefs1.maxSize()); // shape function values
        //evaluating left hand side
        p_fe_temp*=0.0;
        lefs1.finiteElement().localBasis().evaluateFunction(local,e);
        e=mbf.modify(e,local,2);
        for (int i=0; i<lefs1.size(); i++){
            p_fe_temp.axpy(xl[lefs1.localIndex(i)],e[i]);
        }
        p_fe+=p_fe_temp;
    }
    if (lefs2.size()>0){
        std::vector<R> e(lefs2.maxSize()); // shape function values
        //evaluating left hand side
        p_fe_temp*=0.0;
        lefs2.finiteElement().localBasis().evaluateFunction(local,e);
        e=mbf.modify(e,local,3);
        for (int i=0; i<lefs2.size(); i++){
            p_fe_temp.axpy(xl[lefs2.localIndex(i)],e[i]);
        }
        p_fe+=p_fe_temp;
    }
    if (lefs3.size()>0){
        std::vector<R> e(lefs3.maxSize()); // shape function values
        //evaluating left hand side
        p_fe_temp*=0.0;
        lefs3.finiteElement().localBasis().evaluateFunction(local,e);
        e=mbf.modify(e,local,4);
        for (int i=0; i<lefs3.size(); i++){
            p_fe_temp.axpy(xl[lefs3.localIndex(i)],e[i]);
        }
        p_fe+=p_fe_temp;
    }
    if (lefs4.size()>0){
        std::vector<R> e(lefs4.maxSize()); // shape function values
        //evaluating left hand side
        p_fe_temp*=0.0;
        lefs4.finiteElement().localBasis().evaluateFunction(local,e);
        e=mbf.modify(e,local,5);
        for (int i=0; i<lefs4.size(); i++){
            p_fe_temp.axpy(xl[lefs4.localIndex(i)],e[i]);
        }
        p_fe+=p_fe_temp;
    }
return (p_fe);
}//end evaluate function

RF evaluateAverage(const FV &global) {

    FV local;
    bool elementIsEnriched=false;
    auto meit=this->findElement(global,local,elementIsEnriched);
    assert (elementIsEnriched);
    MBF mbf=constructMBF(meit,global,local);

    FV fractureNormal=mbf.fN(local);
    fractureNormal*=mbf.template normalEpsilon<Scalar>();//make the normal small

    FV local_left(local);
    local_left+=fractureNormal;
    FV local_right(local);
    local_right-=fractureNormal;

    RF p_fe_left=this->evaluatePressureLocal(local_left,mbf);
    RF p_fe_right=this->evaluatePressureLocal(local_right,mbf);
    RF p_fe_average=0.5*(p_fe_left+p_fe_right);//p_fe is the average of the solution left and right of the fracture

return (p_fe_average);
}//end evaluate function

RF evaluatePPlus(const FV &global) {

    FV local;
    bool elementIsEnriched=false;
    auto meit=this->findElement(global,local,elementIsEnriched);
    assert (elementIsEnriched);
    MBF mbf=constructMBF(meit,global,local);

    FV fractureNormal=mbf.fN(local);
    fractureNormal*=mbf.template normalEpsilon<Scalar>();//make the normal small

    local+=fractureNormal;
    RF p_fe=this->evaluatePressureLocal(local,mbf);

return (p_fe);
}//end evaluate function

RF evaluatePMinus(const FV &global) {

    FV local;
    bool elementIsEnriched=false;
    auto meit=this->findElement(global,local,elementIsEnriched);
    assert (elementIsEnriched);
    MBF mbf=constructMBF(meit,global,local);

    FV fractureNormal=mbf.fN(local);
    fractureNormal*=mbf.template normalEpsilon<Scalar>();//make the normal small

    local-=fractureNormal;

    RF p_fe=this->evaluatePressureLocal(local,mbf);

return (p_fe);
}//end evaluate function
//protected:
public:
//get the element centre coordinates
template<typename VV,typename Soil>
void evaluateVelocities(const Soil &soil, VV &velocitiesVector) {
    typedef typename Dune::FieldVector<RF,dimw_> GradP;
    typedef typename std::vector<GradP> GradPhi;

    MultiLFSCache multilfs_cache_(multilfsu_);
    ConstXView x_view_(x_);
       std::ofstream vmatrix;
       vmatrix.open ("vMatrix.dat");
    //Traverse the elements of the rock matrix multiDomainGrid
    HostGridIterator eendit = multiDomainGrid_.template leafend<0>();
    for (HostGridIterator matrixElementIterator = multiDomainGrid_.template leafbegin<0>(); matrixElementIterator != eendit; ++matrixElementIterator) {
//        const int worlddim = EG::Geometry::dimensionworld;
//        RF u_fe(0.0);//TODO check what happens if u_fe is returned without a proper value (u_fe=0.0)?

        multilfsu_.bind(*matrixElementIterator);
        EPLFS eplfs(multilfsu_,ep_);

        const LBFS& lbfs = eplfs.template child<0>();
        const LEFS0& lefs0 = eplfs.template child<1>();
        const LEFS1& lefs1 = eplfs.template child<2>();
        const LEFS2& lefs2 = eplfs.template child<3>();
        const LEFS3& lefs3 = eplfs.template child<4>();
        const LEFS4& lefs4 = eplfs.template child<5>();

        bool normalElement=true;
        if (lefs0.size()+lefs1.size()+lefs2.size()+lefs3.size()+lefs4.size() > 0){
            normalElement=false;
        }


        eplfs.bind();

        std::vector<R> xl(lbfs.maxSize());        // local coefficients
        multilfs_cache_.update();
        x_view_.bind(multilfs_cache_);
        x_view_.read(xl);// get local coefficients of the solution

        if (normalElement){

            Dune::FieldVector<ctype,dimw_> globalElementCenter = matrixElementIterator->geometry().center();

            FV localElementCenter=matrixElementIterator->geometry().local(globalElementCenter);

            // evaluate gradient of basis functions on reference element
            std::vector<JacobianType> js(lbfs.size());
            lbfs.finiteElement().localBasis().evaluateJacobian(localElementCenter,js);
            // transform gradients from reference element to real element
            const Dune::FieldMatrix<DF,dimw_,dim_> jac = matrixElementIterator->geometry().jacobianInverseTransposed(localElementCenter);
            GradPhi gradphi(lbfs.size());
            for (sizeType i=0; i<lbfs.size(); i++){
                jac.mv(js[i][0],gradphi[i]);
            }

            GradP gradP(0.0);
            for (sizeType i=0; i<lbfs.size(); i++){
                gradP.axpy(xl[lbfs.localIndex(i)],gradphi[i]);
            }
            const Dune::FieldMatrix<Scalar,dimw_,dimw_> K=soil.intrinsicPermeability();
            FV u(0.0);
            K.umv(gradP,u);

            std::cout << "u("<<globalElementCenter<<")= " << u << std::endl;
            vmatrix << globalElementCenter<<"\t " << u << std::endl;
            velocitiesVector.push_back(u);
        }// end here loop for normal elements


        else{

            MBF mbf(multiDomainGrid_, *matrixElementIterator, intersectionPointsMap_, multilfsu_);

            //get element Id
            typedef typename MultiDomainGrid::Traits::GlobalIdSet::IdType IdType;
            const IdType elementId =multiDomainGrid_.globalIdSet().id(*matrixElementIterator);

            typedef typename LBFS::Traits::SizeType sizeType;

            typedef typename Map::mapped_type IntersectionPoints;
            IntersectionPoints intersectionPoints=(((*intersectionPointsMap_.find(elementId)).second));
            sizeType children=(lbfs.size()+lefs0.size()+lefs1.size()+lefs2.size()+lefs3.size()+lefs4.size())/4;
            assert(children<6);
            if (children>2) --children;

            typedef typename std::vector<FV> PointsNotOnCorners;
            PointsNotOnCorners pointsNotOnCorners(children);//TODO check if this is true for cuts through a corner!
            std::vector<FV> xfMiddle;
            xfMiddle.clear();
            FV check(-1000.0);//check means no entry
            xfMiddle.push_back(intersectionPoints[4].intersection);

            /*
             * lmax is 1 for ending fractures, 2 for single fractures, 3 for 3-crossings and 4 for 4-crossings.
             * Two not-crossing fractures have an lmax of 2.
             */
        //    int lmax=pointsNotOnCorners.size();
            for (int i=0;i<pointsNotOnCorners.size();i++){
                pointsNotOnCorners[i]=(intersectionPoints[i].intersection);
            }
            if (Dumux::arePointsEqual(intersectionPoints[1].intersection,check) ){//this indicates an ending fracture
                pointsNotOnCorners[1]=(intersectionPoints[5].intersection);
        //        lmax=1;
            }
            else if( !Dumux::arePointsEqual(intersectionPoints[5].intersection,check) ){//this indicates two not-crossing fractures
                xfMiddle.push_back(intersectionPoints[5].intersection);
                /*
                 * there are 4 points on the boundary of the element,
                 * but there are also two centre points, so that the for each xfMiddle there are only two pointsNotOnCorners.
                 */
        //        lmax=2;
            }

            typedef typename std::vector<FV> SortedPoints;
            /*
             * for ending fractures the sub-triangulation is the same as for one single not bending fracture through the element
             * the fracture is extrapolated to the end of the element and a virtual exit point is determined
             * this happens in the gridCoupling and in line 62,
             * so that the vector pointsNotOnCorners does look the same as in the single not bending fracture case
             */
            SortedPoints sortedPoints;
            sortedPoints=Dumux::sortPoints<FV>(pointsNotOnCorners, *matrixElementIterator);
            typedef Dune::GeometryType::BasicType BasicType;
            const BasicType simplex = Dune::GeometryType::simplex;
            typedef Dune::GeometryType GT;
            const GT subSimplex(simplex,dimw_);
            std::vector<FV> listOfCornerCoordinates(3);//this is a vector which stores all corners of the used subtriangle
            /*
             * this scheme is based on the DUNE node ordering as defined for the generic reference element cube
             * create subtriangles depending on the fracture geoemtry
             * for straight, bending and ending fractures always 6 subtriangles are generated
             * for 3-crossings 7 subtriangles are generated
             * for 4-crossings 8 subtriangles are generated
             * and for two not-crossing fractures 10 subtriangles are generated
             */
            assert(xfMiddle.size()<2);
            if (xfMiddle.size()==1){
                listOfCornerCoordinates[0]=matrixElementIterator->geometry().global(xfMiddle[0]);
                for (int i=0;i<4+children;i++){
                    listOfCornerCoordinates[1]=matrixElementIterator->geometry().global(sortedPoints[i]);
                    listOfCornerCoordinates[2]= (i<3+children ? matrixElementIterator->geometry().global(sortedPoints[i+1]) : matrixElementIterator->geometry().global(sortedPoints[0]) );

                    // calculate the centroid for the subtriangle
                    Dune::FieldVector<ctype,dimw_> positionGlobal= (listOfCornerCoordinates[0]+listOfCornerCoordinates[1]+listOfCornerCoordinates[2]);
                    positionGlobal/=3.0;
                    FV position=matrixElementIterator->geometry().local(positionGlobal);

                    GradP gradP(0.0);
                    {
                    // evaluate gradient of basis functions on reference element
                    std::vector<JacobianType> js(lbfs.size());
                    lbfs.finiteElement().localBasis().evaluateJacobian(position,js);
                    // transform gradients from reference element to real element
                    const Dune::FieldMatrix<DF,dimw_,dim_> jac = matrixElementIterator->geometry().jacobianInverseTransposed(position);
                    GradPhi gradphi(lbfs.size());
                    for (sizeType i=0; i<lbfs.size(); i++){
                        jac.mv(js[i][0],gradphi[i]);
                    }
                    GradPhi gradPhiModified(lbfs.size());
                    gradPhiModified=mbf.modify(gradphi,position,0);

                    GradP gradPTemp(0.0);
                    for (sizeType i=0; i<lbfs.size(); i++){
                        gradPTemp.axpy(xl[lbfs.localIndex(i)],gradPhiModified[i]);
                    }
                    gradP+=gradPTemp;
                    }

                    // evaluate finite element function at local coordinate (enriched basis function)
                    //TODO: works only if lblfs.size=lelfs.size, i.e. only ONE additional dof at every standard dof, i.e. no fracture crossings and only one fracture per element
                    if (lefs0.size()>0){
                     std::vector<JacobianType> js(lefs0.size());
                         lefs0.finiteElement().localBasis().evaluateJacobian(position,js);
                         // transform gradients from reference element to real element
                         const Dune::FieldMatrix<DF,dimw_,dim_> jac = matrixElementIterator->geometry().jacobianInverseTransposed(position);
                         GradPhi gradphi(lefs0.size());
                         for (sizeType i=0; i<lefs0.size(); i++){
                             jac.mv(js[i][0],gradphi[i]);
                         }
                         GradPhi gradPhiModified(lefs0.size());
                         gradPhiModified=mbf.modify(gradphi,position,1);

                         GradP gradPTemp(0.0);
                         for (sizeType i=0; i<lefs0.size(); i++){
                             gradPTemp.axpy(xl[lefs0.localIndex(i)],gradPhiModified[i]);
                         }
                         gradP+=gradPTemp;
                    }

                    // evaluate finite element function at local coordinate (enriched basis function)
                    //TODO: works only if lblfs.size=lelfs.size, i.e. only ONE additional dof at every standard dof, i.e. no fracture crossings and only one fracture per element
                    if (lefs1.size()>0){
                     std::vector<JacobianType> js(lefs1.size());
                         lefs1.finiteElement().localBasis().evaluateJacobian(position,js);
                         // transform gradients from reference element to real element
                         const Dune::FieldMatrix<DF,dimw_,dim_> jac = matrixElementIterator->geometry().jacobianInverseTransposed(position);
                         GradPhi gradphi(lefs1.size());
                         for (sizeType i=0; i<lefs1.size(); i++){
                             jac.mv(js[i][0],gradphi[i]);
                         }
                         GradPhi gradPhiModified(lefs1.size());
                         gradPhiModified=mbf.modify(gradphi,position,2);

                         GradP gradPTemp(0.0);
                         for (sizeType i=0; i<lefs1.size(); i++){
                             gradPTemp.axpy(xl[lefs1.localIndex(i)],gradPhiModified[i]);
                         }
                         gradP+=gradPTemp;
                    }
                    // evaluate finite element function at local coordinate (enriched basis function)
                    //TODO: works only if lblfs.size=lelfs.size, i.e. only ONE additional dof at every standard dof, i.e. no fracture crossings and only one fracture per element
                    if (lefs2.size()>0){
                     std::vector<JacobianType> js(lefs2.size());
                         lefs2.finiteElement().localBasis().evaluateJacobian(position,js);
                         // transform gradients from reference element to real element
                         const Dune::FieldMatrix<DF,dimw_,dim_> jac = matrixElementIterator->geometry().jacobianInverseTransposed(position);
                         GradPhi gradphi(lefs2.size());
                         for (sizeType i=0; i<lefs2.size(); i++){
                             jac.mv(js[i][0],gradphi[i]);
                         }
                         GradPhi gradPhiModified(lefs2.size());
                         gradPhiModified=mbf.modify(gradphi,position,3);

                         GradP gradPTemp(0.0);
                         for (sizeType i=0; i<lefs2.size(); i++){
                             gradPTemp.axpy(xl[lefs2.localIndex(i)],gradPhiModified[i]);
                         }
                         gradP+=gradPTemp;
                    }
                    // evaluate finite element function at local coordinate (enriched basis function)
                    //TODO: works only if lblfs.size=lelfs.size, i.e. only ONE additional dof at every standard dof, i.e. no fracture crossings and only one fracture per element
                    if (lefs3.size()>0){
                     std::vector<JacobianType> js(lefs3.size());
                         lefs3.finiteElement().localBasis().evaluateJacobian(position,js);
                         // transform gradients from reference element to real element
                         const Dune::FieldMatrix<DF,dimw_,dim_> jac = matrixElementIterator->geometry().jacobianInverseTransposed(position);
                         GradPhi gradphi(lefs3.size());
                         for (sizeType i=0; i<lefs3.size(); i++){
                             jac.mv(js[i][0],gradphi[i]);
                         }
                         GradPhi gradPhiModified(lefs3.size());
                         gradPhiModified=mbf.modify(gradphi,position,4);

                         GradP gradPTemp(0.0);
                         for (sizeType i=0; i<lefs3.size(); i++){
                             gradPTemp.axpy(xl[lefs3.localIndex(i)],gradPhiModified[i]);
                         }
                         gradP+=gradPTemp;
                    }
                    // evaluate finite element function at local coordinate (enriched basis function)
                    //TODO: works only if lblfs.size=lelfs.size, i.e. only ONE additional dof at every standard dof, i.e. no fracture crossings and only one fracture per element
                    if (lefs4.size()>0){
                     std::vector<JacobianType> js(lefs4.size());
                         lefs4.finiteElement().localBasis().evaluateJacobian(position,js);
                         // transform gradients from reference element to real element
                         const Dune::FieldMatrix<DF,dimw_,dim_> jac = matrixElementIterator->geometry().jacobianInverseTransposed(position);
                         GradPhi gradphi(lefs4.size());
                         for (sizeType i=0; i<lefs4.size(); i++){
                             jac.mv(js[i][0],gradphi[i]);
                         }
                         GradPhi gradPhiModified(lefs4.size());
                         gradPhiModified=mbf.modify(gradphi,position,5);

                         GradP gradPTemp(0.0);
                         for (sizeType i=0; i<lefs4.size(); i++){
                             gradPTemp.axpy(xl[lefs4.localIndex(i)],gradPhiModified[i]);
                         }
                         gradP+=gradPTemp;
                    }


                    const Dune::FieldMatrix<Scalar,dimw_,dimw_> K=soil.intrinsicPermeability();
                    FV u(0.0);
                    K.umv(gradP,u);
                    u*=-1.0;
    std::cout << "u("<<positionGlobal<<")= " << u << std::endl;
                    velocitiesVector.push_back(u);

                }//end loop over all subelements
            }//end if for all cases except two-not crossing fractures
        }//end else
    }//end loop over all elements
    vmatrix.close();
}//end evaluate function

private:
    MultiDomainGrid& multiDomainGrid_;
    Map& intersectionPointsMap_;
    MultiGFS& multigfs_;
    EnrichedProblem& ep_;
    X& x_;
    RF gradientDistance_;

    MultiLFSU multilfsu_;
    SHGFS shgfs_;
    MDGF mdgf_;
    MGF mgf_;
//    MultiLFSCache multilfs_cache_;
//    ConstXView x_view_;

};//end class MatrixSolution
}//end namespace Dumux
#endif /* MATRIXSOLUTION_HH_ */
