#ifndef REFERENCE_PROPERTIES_HH_
#define REFERENCE_PROPERTIES_HH_

namespace Dumux{
template<typename ParameterTree, typename Scalar>
class ReferenceProperties{

public:
    typedef Scalar RF;

    ReferenceProperties(const ParameterTree &inputParameters) //: inputParameters_(inputParameters)
    {

//TODO check what is better: naming of density and viscosity reference parameters or fluid properties? This may depend on the system (incompressible/compressible etc.)
    rhoReference_= inputParameters.template get<RF>("Fluid.density");
    muReference_= inputParameters.template get<RF>("Fluid.viscosity");
    uReference_= inputParameters.template get<RF>("Reference.velocity");
    dReference_= inputParameters.template get<RF>("Reference.length");

    kReference_= (muReference_ * dReference_)/(rhoReference_*uReference_);
    pReference_= (rhoReference_ * uReference_ * uReference_);

}//end constructor

    RF density() const {
        return (rhoReference_);
    }//end density function
    RF viscosity() const {
        return (muReference_);
    }//end viscosity function
    RF velocity() const {
        return (uReference_);
    }//end velocity function
    RF length() const {
        return (dReference_);
    }//end length function
    RF permeability() const {
        return (kReference_);
    }//end permeability function
    RF pressure() const {
        return (pReference_);
    }//end permeability function
    RF gravity() const {
        return (9.81);
    }//end gravity function

private:
    RF rhoReference_;
    RF muReference_;
    RF uReference_;
    RF dReference_;
    RF kReference_;
    RF pReference_;


};//end class
}//end namespace dumux

#endif /* REFERENCE_PROPERTIES_HH_ */
