//============================================================================
// Author      : Alex Tatomir, Nicolas Schwenck
//============================================================================

#ifndef ART_READER_1D_HH
#define ART_READER_1D_HH

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>
#include <dune/grid/uggrid.hh>

#define PLOT 0 // plot the vertices, edges, elements

namespace Dumux
{

struct EdgePoints{
    int artId;
    double aperture;
    double kfn;
    double kft;
    Dune::FieldVector<int, 2> points;
    friend std::ostream& operator<< (std::ostream &o, const EdgePoints &i)
    {
        return o << i.points[0] <<" "<<i.points[1]<< " " << i.aperture <<"\n";
    }

    int &operator [] (int index)
    {
        assert(index==0 || index==1);
        return (points[index]);
    }
    int operator [] (int index) const
    {
        assert(index==0 || index==1);
        return (points[index]);
    }
};

template <typename FractureGridType>
class artReader {


    typedef Dune::FieldVector<double, 3>    Coordinates;
    typedef std::vector<Coordinates>        VerticesVector;

    typedef std::vector<Dumux::EdgePoints>         EdgesVector;

    typedef std::list<int> ArtIdxList;
    typedef typename ArtIdxList::iterator ArtIdxListIterator;
    typedef Dune::GridPtr<FractureGridType> GridPointer;

    /* Function art_readARTfile reads the file formats obtained with the
     * ART mesh generator   */
public:
    artReader(const char *art_fileName)
    {
        std::cout<<boost::format("Opening %s ")%art_fileName;
        std::cout<<std::endl;
        std::ifstream inFile(art_fileName);

        //initialize
        Coordinates coordinate_;
        Dumux::EdgePoints  edge_points_;
        std::string jump;

        while(inFile >> jump)
        {
            //do nothing with the first lines in the code
            //start reading information only after getting to %Vertices
            if(jump == "VertexNumber:")
            {
                inFile >> jump;
                double dummy = atof(jump.c_str());
                vertex_number = dummy;
                break;
            }
        }
        while(inFile >> jump)
        {
            if(jump == "EdgeNumber:")
            {
                inFile >> jump;
                double dummy = atof(jump.c_str());
                edge_number = dummy;
                break;
            }
        }
        while(inFile >> jump)
        {
            if(jump == "FaceNumber:")
            {
                inFile >> jump;
                double dummy = atof(jump.c_str());
                face_number = dummy;
                break;
            }
        }
        while(inFile >> jump)
        {
            if(jump == "ElementNumber:")
            {
                inFile >> jump;
                double dummy = atof(jump.c_str());
                element_number = dummy;
                break;
            }
        }
     // finished reading the header information: total number of vertices, etc..
        while(inFile >> jump)
        {
            //jump over the lines until the ones with the vertices "% Vertices: x y z"
            if(jump == "Vertices:")
            {
                break;
            }
        }
        while(inFile >> jump)
        {
            //skip words until "z" from the line "% Vertices: x y z"
            if(jump == "z")
            {
                std::cout<<"Start reading the vertices\n";
                break;
            }
        }

        while(inFile >> jump)
        {
            if(jump == "$")
            {
                std::cout<<"Finished reading the vertices\n";
                break;
            }
            double dummy = atof(jump.c_str());
            coordinate_[0] = dummy;
            for (int k=1; k<3; k++)
            {
                inFile >> jump;
                dummy = atof(jump.c_str());
                coordinate_[k] = dummy;
            }
            vertices_.push_back(coordinate_);
        }
    //Reading Edges
        while(inFile >> jump)
        {
            //jump over the characters until the ones with the edges
            if(jump == "permeability):")
            {
                std::cout<<"\nStart reading the edges\n";
                break;
            }
        }
        while(inFile >> jump)
        {
            if(jump == "$")
            {
                std::cout<<"Finished reading the edges\n";
                break;
            }
            double dummy = atof(jump.c_str());
            edge_points_.artId=dummy;
            for (int k=0; k<2; k++)
            {
                inFile >> jump;
                dummy = atof(jump.c_str());
                edge_points_[k] = dummy;
            }
            inFile >> jump;
            dummy = atof(jump.c_str());
            edge_points_.aperture=dummy;
            inFile >> jump;
            dummy = atof(jump.c_str());
            edge_points_.kfn=dummy;
            inFile >> jump;
            dummy = atof(jump.c_str());
            edge_points_.kft=dummy;
            edges_vector_.push_back(edge_points_);
        }
    }//end constructor

    void outputARTtoScreen()
    {
    ////////OUTPUT for verification
    //////////////////////////////////////////////////////////////////
            std::cout<<"\nprinting VERTICES\n";
            for (int i = 0; i < vertices_.size(); i++)
            {
                for (int j=0; j < 3; j++)
                std::cout << vertices_[i][j]<<"\t";
                std::cout << std::endl;
            }

            std::cout<<"\nprinting ELEMENTS\n";
            for (int i = 0; i < edges_vector_.size(); i++)
            {
                for (int j=0; j < 2; j++){
                    std::cout<<edges_vector_[i][j]<<"\t";
                }
                std::cout<<(edges_vector_[i]).aperture<<"\t";
                std::cout<<(edges_vector_[i]).kfn<<"\t";
                std::cout<<(edges_vector_[i]).kft;
            }
            std::cout<<std::endl;

            std::cout<<"\nTotal number of vertices "<<vertex_number<<std::endl;
            std::cout<<"Total number of edges: "<<edge_number<<std::endl;
            std::cout<<"Total number of elements: "<<element_number<<std::endl;


            if (vertices_.size() != vertex_number )
            {
                std::cout<<"The total given number of vertices: "<<vertex_number
                        <<" is not the same with the read number of entries: "
                        <<vertices_.size()<<"\n";
            }
            if (edges_vector_.size() != edge_number )
            {
                std::cout<<"The total given number of edges: "<<edge_number
                        <<" is not the same with the read number of entries: "
                        <<edges_vector_.size()<<"\n";
            }
    }

GridPointer createGrid(std::string simulationNameString)
{
    // set up the grid factory
    Dune::FieldVector<double,2> position;

    // Plot the vertices
#if PLOT
    std::cout<<"*================*"<<std::endl;
    std::cout<<"* Vertices"<<std::endl;
    std::cout<<"*================*"<<std::endl;
#endif
    for (int k=0; k<vertex_number; k++)
    {
        //Printing the vertices vector
#if PLOT
        std::cout<<vertices_[k][0]<<"\t\t";
        std::cout<<vertices_[k][1]<<"\t\t";
        std::cout<<vertices_[k][2]<<std::endl;
#endif
        for (int i=0; i<2; i++)//use only x,y coordinates and skip z
        {
            position[i]=vertices_[k][i];
        }
        factory_.insertVertex(position);
    }
#if PLOT
    std::cout<<"*================*"<<std::endl;
    std::cout<<"* Edges"<<std::endl;
    std::cout<<"*================*"<<std::endl;
#endif

#if PLOT
    for (int k=0; k<edge_number; k++)
    {

        //Printing the Edge vector
        std::cout<<"idx: "<<(edges_vector_[k]).artId<<" connects vertices:\t";
        std::cout<<edges_vector_[k][0]<<"\t\t";
        std::cout<<edges_vector_[k][1]<< " with aperture "<< (edges_vector_[k]).aperture;
        std::cout<< "  and permeabilities: " << (edges_vector_[k]).kfn << "\t"<<(edges_vector_[k]).kft<<std::endl;
    }
#endif

    //***********************************************************************//
    //Create the Elements in Dune::GridFactory //
    //***********************************************************************//
    typedef std::map<int, int> CrossingCheckMap;
    CrossingCheckMap::iterator it;
    CrossingCheckMap crossingCheckMap;

    for (int i=0; i<edge_number; i++)
    {
        std::vector<unsigned int> nodeIdx(2);
//        Dune::FieldVector<double,3> point(0);
#if PLOT
        std::cout<<"====================================="<<std::endl;
        std::cout<<"globalElemIdx "<<i<<std::endl;
        std::cout<<"====================================="<<std::endl;
#endif

        for (int j=0;j<2;j++){//use only entries 0,1
//          std::cout << "edges_vector line: "<<i<<"  vertex(" <<j<<"): "<<  edges_vector_[i][j] << std::endl;
        it=crossingCheckMap.find(edges_vector_[i][j]);
        if (it == crossingCheckMap.end() )//if vertex index is not found, then add the vertex index to the map and set the associated value to -1
        {
//          std::cout << "vertex "<<edges_vector_[i][j]<<" is not yet in the map. Map entry is set to ";//-1" << std::endl;
            crossingCheckMap[edges_vector_[i][j]]=-1;
//          std::cout << crossingCheckMap.find(edges_vector_[i][j])->second <<std::endl;
        }
        else if (crossingCheckMap[edges_vector_[i][j]]==-1)//if vertex index is found and the value is one, then set the associated value to -2
        {
//          std::cout << "vertex "<<edges_vector_[i][j]<<" was already once in the map. Map entry is set to -2" << std::endl;
            crossingCheckMap[edges_vector_[i][j]]=-2;
        }

        else if (crossingCheckMap[edges_vector_[i][j]]==-2)
            //the vertex is already associated to two elements,
             // i.e. the vertex has to be added again because Alberta cannot handle crossings properly
             // and all future appearances of this vertex index have to be replaced by the new vertex index
        {
            for (int k=0; k<2; k++)//use only x,y coordinates and skip z
            {
                position[k]=vertices_[ edges_vector_[i][j] ][k];
            }
            factory_.insertVertex(position);
            vertices_.push_back(vertices_[ edges_vector_[i][j] ]); //add vertex to the vertex list at the end. The new index is (size-1).
//            equalIdxList_.insert(IdPair(edges_vector_[i][j],(vertices_.size()-1)));
            equalIdxList_.push_back (edges_vector_[i][j]);//every time a vertex has to be doubled, the ArtIdx is added to this list.
            crossingCheckMap[edges_vector_[i][j]]=(vertices_.size()-1);//change the value of the old vertex-index-key to the new vertex index
//            std::cout <<"vertex "<<edges_vector_[i][j];
            edges_vector_[i][j]=(vertices_.size()-1);//change the vertex index in the edge to the new vertex index
            crossingCheckMap[edges_vector_[i][j]]=-1;//change the value to the new vertex-index-key to -1
//            std::cout << " was already twice in the map. Map entry is set to: " << (vertices_.size()-1)<<std::endl;
        }
        else //if crossingCheckMap[edges_vector_[i][1]]>0
        {
            int temp=crossingCheckMap[edges_vector_[i][j]];
            crossingCheckMap[edges_vector_[i][j]]=-2;
            edges_vector_[i][j]=temp;
            crossingCheckMap[edges_vector_[i][j]]=-2;
//          std::cout << "vertex "<<edges_vector_[i][j]<<" was already once in the map. Map entry is set to -2 "<<std::endl;
        }


        nodeIdx[j]=edges_vector_[i][j];//use only entries 0,1
        }
        const Dune::GeometryType type( Dune::GeometryType::simplex, 1 );
        factory_.insertElement(type, nodeIdx);
        equalIdxList_.sort();//sort list
        equalIdxList_.unique();//remove doubled entries (works only on sorted lists).
    }

    //temporary DGF file is written
    FractureGridType *fractureGrid = factory_.createGrid();
    typedef typename FractureGridType::LeafGridView FGV;
    typedef Dune::DGFWriter< FGV > DGFWriter;
    FGV fgv = fractureGrid->leafView();
    DGFWriter dgfWriter(fgv);
    dgfWriter.write("tempFGV.dgf");

    //add permeability and aperture information to the DGF file
    std::string inbuf;
    std::fstream input_file("tempFGV.dgf", std::ios::in);
    simulationNameString+=".dgf";
    std::ofstream output_file(simulationNameString);

    while (!input_file.eof())
    {
        std::getline(input_file, inbuf);
        int spot = inbuf.find("CUBE");
        if(spot >= 0 )
        {
            inbuf+="\nPARAMETERS 3";
            output_file << inbuf << std::endl;
            std::getline(input_file, inbuf);
            for (int edges_vector_index=0;edges_vector_index<edges_vector_.size();edges_vector_index++)
            {
                std::string aperture = boost::lexical_cast<std::string>(edges_vector_[edges_vector_index].aperture);
                std::string kfn = boost::lexical_cast<std::string>(edges_vector_[edges_vector_index].kfn);
                std::string kft = boost::lexical_cast<std::string>(edges_vector_[edges_vector_index].kft);
                inbuf+=" " +aperture+ " " +kfn + " " + kft ;
                output_file << inbuf << std::endl;
                std::getline(input_file, inbuf);
            }
        }
        output_file << inbuf << std::endl;
    }

    GridPointer gridPtr = GridPointer(simulationNameString);
    remove( "tempFGV.dgf" );
    return (gridPtr);
}//End createGrid()



// -1 if j is an unmodified row
// -3/-4 if j is a sum row with a 3-/4-crossing
// larger than zero to indicate the row to which this is added

/*
 * fractureNetworkCouplingMap is a map which is necessary for fracture networks with crossing fractures.
 * It stores the Id of the DUNE fracture grid node as key whenever there is a crossing.
 * In general this works for every number of crossings at one point, though the XFEM implementation does not.
 * For every geometric position at the key of the lowest Dune id the number of connected elements is stored (with a negative sign).
 * For the other Dune ids a -1 is stored as value.
 *
 * example:
 * % Vertices: x y z
 *
 *  1.000   0.   0.00000
 *  1.000   0.   0.00000
 *  0.000  -1.0  0.00000
 *  0.000   0.   0.00000
 *  0.000   1.0  0.00000
 *  1.000   1.0  0.00000
 *
 * % Edges (Indices to List of Points):
 * 0 4
 * 1 4
 * 2 4
 * 1 2
 * 5 4
 * 1 3
 * 2 3
 *
 * will lead to the following duneIdMap (where the ids are somehow arbitrary):
 *  5  -> -4
 *  9  -> -3
 * 13  -> -3
 * 17  ->  5
 * 25  ->  9
 * 33  -> 13
 *
 * nodeId  5   position: 0.0 0.0
 * nodeId 17   position: 0.0 0.0
 * nodeId  9   position: 0.2 1.0
 * nodeId 25   position: 0.2 1.0
 * nodeId 13   position: 1.0 0.2
 * nodeId 33   position: 1.0 0.2
 *
 */
typedef typename FractureGridType::LeafGridView FGV;
typedef typename FractureGridType::Traits::GlobalIdSet::IdType FractureGridIdType;
typedef typename std::map<FractureGridIdType,int> DuneIdMap;//the type of the value of this map has to be int.
//Even though sometimes also ids are stored also negative values must be stored.
//Since The FractureGridIdType can be unsigned int, this would lead to problems.
typedef typename DuneIdMap::iterator DuneIdMapIterator;
typedef typename std::pair<FractureGridIdType,int> Pair;
DuneIdMap fractureNetworkCouplingMap(FractureGridType &grid_){
    FGV fgv = grid_.leafView();
    DuneIdMap couplingMap;
    for (ArtIdxListIterator it1=equalIdxList_.begin(); it1 != equalIdxList_.end();it1++)//iterate over every doubled node
    {
        //get global position of ArdIdx node *it1
        Dune::FieldVector<double,2> position;
        for (int i=0; i<2; i++)//use only x,y coordinates and skip z
        {position[i]=vertices_[*it1][i];}
//        std::cout << "doubled node at: " << position << std::endl;

        DuneIdMap couplingMapTemp;
        //iterate over fracture grid nodes
        int k=0;
        //First a loop over every element in the leaf view is started.
        //Then over every intersection is looped and checked where more than two nodes are at the same position (i.e., there is a crossing).
        typedef typename FGV::template Codim<0>::Iterator ElementIterator;
        for (ElementIterator fractureElementIt = fgv.template begin<0>(); fractureElementIt != fgv.template end<0>(); ++fractureElementIt) {
            typedef typename FGV::IntersectionIterator IntersectionIterator;
            for (IntersectionIterator isIt = fgv.ibegin(*fractureElementIt); isIt != fgv.iend(*fractureElementIt); ++isIt){
                //find the Dune node Id which corresponds to the ArtIdx by comparing the geometrical position
                if (isIt->geometry().center()== position){
                    FractureGridIdType nodeId = grid_.localIdSet().subId(*(isIt->inside()), isIt->indexInInside(), 1);
                    k++;
                    couplingMapTemp.insert ( Pair(nodeId,0) );
                    Dune::dverb << "intersectionId "<< nodeId<<std::endl;
                }
            }//end loop over every fracture intersections (nodes)
        }//end loop over fracture elements

//        std::cout << "couplingMapTemp:\n";
        FractureGridIdType temp=-1;
        for (DuneIdMapIterator it=couplingMapTemp.begin() ; it != couplingMapTemp.end(); it++ ){
            if (it==couplingMapTemp.begin()) {
                (*it).second =(-k);
                temp=(*it).first;
            }
            else{
                assert(temp>0);
                (*it).second = temp;
            }
//            std::cout  << (*it).first << "   " << (*it).second  <<std::endl;
        }
        couplingMap.insert(couplingMapTemp.begin(),couplingMapTemp.end());

        ////////////////////////////////////// control output
//        typedef typename FGV::template Codim<1>::Iterator NodeIterator;
//        for (NodeIterator fractureNodeIt = grid_.template leafbegin<1>(); fractureNodeIt != grid_.template leafend<1>(); ++fractureNodeIt) {
//            if (Dumux::arePointsEqual(fractureNodeIt->geometry().center(),position) ){
//                FractureGridIdType nodeId = grid_.globalIdSet().id(*fractureNodeIt);
//                std::cout << "nodeId "<< nodeId<<"   position: " << position << std::endl;
//                Dune::dverb << "nodeId "<< nodeId<<"   position: " << position << std::endl;
//            }
//        }
        //////////////////////////////////////
    }//end loop over artIdxList
    return (couplingMap);
}

/*
 * update function for CouplingMap which converts Ids to indices
 * Because of performance optimization on a coarse grid the IDs are stored.
 * After each refinement those IDs are converted to indices with this function.
 */
typedef typename FGV::IndexSet::IndexType FractureGridIdxType;
typedef std::map<FractureGridIdxType,int> DuneIdxMap;
typedef std::map<FractureGridIdType,FractureGridIdType> DuneIdToIdxMap;
typename DuneIdToIdxMap::iterator idToIdxIt;

DuneIdxMap gridCouplingMapToIdx(FractureGridType& grid_, DuneIdMap& couplingMap){
    typedef typename std::pair<FractureGridIdType,int> IdxPair;
    typedef typename FGV::template Codim<1>::Iterator NodeIterator;
    DuneIdxMap duneIdxMap;
    DuneIdToIdxMap duneIdToIdxMap;
    /*
     * Because the number of nodes is usually much larger than the number of entries in the map,
     * the node iterator loop should be outside of the map loop!
     * this is a loop over the grid (not the gridview) because ids are needed
     */
    for (NodeIterator nodeIterator = grid_.template leafbegin<1>(); nodeIterator != grid_.template leafend<1>(); ++nodeIterator) {
        //check if nodeId is in couplingMap
        FractureGridIdType nodeId = grid_.globalIdSet().id(*nodeIterator);
        DuneIdMapIterator it=couplingMap.find(nodeId);
        if (it!=couplingMap.end()){//nodeId is found
            FractureGridIdxType nodeIdx = grid_.leafIndexSet().index(*nodeIterator);
            if((*it).second<0){//and it is the smallest Id of connected nodes
                duneIdxMap.insert ( IdxPair(nodeIdx,(*it).second) );
                duneIdToIdxMap[nodeId]=nodeIdx;
            }
            else{
                idToIdxIt=duneIdToIdxMap.find((*it).second);
                if (idToIdxIt!=duneIdToIdxMap.end()){
                    duneIdxMap.insert ( IdxPair(nodeIdx,(*idToIdxIt).second) );
                }
                else std::cout << "Error in gridCouplingMaptoIdx!!!"<<std::endl;
            }
        }
    }
    return (duneIdxMap);
}


//public:
private:
    VerticesVector vertices_;
    EdgesVector edges_vector_;
//  FacesVector faces_vector_ ;
    int vertex_number;
    int edge_number;
    int face_number; //in 2D
    int element_number; //in 3D
    Dune::GridFactory< FractureGridType > factory_;
    ArtIdxList equalIdxList_; //list of every node (ArtIdx) which is doubled
}; //End ArtReader

}//end namespace Dumux

#endif // ART_READER_1D_HH
