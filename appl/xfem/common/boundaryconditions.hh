#include <dune/common/array.hh>

namespace Dumux{

template<typename FV>
bool onTop(const FV &x) {
    return (x[1]>1.0-Dumux::equalityEpsilon<typename FV::value_type>());
}
template<typename FV>
bool onBottom(const FV &x) {
    return (x[1]<-1.0+Dumux::equalityEpsilon<typename FV::value_type>());
}
template<typename FV>
bool onLeft(const FV &x) {
    return (x[0]<-1.0+Dumux::equalityEpsilon<typename FV::value_type>());
}
template<typename FV>
bool onRight(const FV &x) {
    return (x[0]>1.0-Dumux::equalityEpsilon<typename FV::value_type>());
}

template<typename FV>
bool injection(const FV &x) {
    return (x[0]<-0.5 && x[1]<-0.5);
}

template<typename FV>
bool extraction(const FV &x) {
    return (x[0]>0.5 && x[1]>0.5);
}

template<typename RF, typename FV>
RF standardBC(const FV &x){
//    return ( (x[1] < 0.0) ?  (1.0) : (2.0) );
    if ( Dumux::onTop(x) || Dumux::onBottom(x) || Dumux::onLeft(x) || Dumux::onRight(x) ){
//        return ( std::abs(x[0])* (((1.0e4*std::abs(x[1])+1.0)*0.5) + 1.0) );
//        return ( (((1.0e4*std::abs(x[1])+1.0)*0.5) + 1.0) );
//        return ( (((std::abs(x[1])+1.0)*0.5) + 1.0) );
//        return ( (((x[0]+1.0)*0.5) + 1.0) );
//        return ( 2.0);
        if ( x[1]>0 ) return(4.0);
        else  return(1.0);

//        if (x[1]<0.0) return (1.0);
//        else return (2.0);
    }
    /*
     * this should give the linear interpolation of the boundary values
     * whatever this means for complex domains with complex Dirichlet value distributions
     */
    else{
//        return (((x[1]+1.0)*0.5) + 1.0);
                return( -1.0);
    }

    //      y = (xg[0]< 0.0) ?  (1.0) : (2.0);
//      y = (xg[0]< 1.0e-6) ?  (1.0) : (10.0);
//      y = (xg[0]< 0.5) ?  (1.0) : (10.0);
//      y=xg[0];
//      if (x[1]<0.4-1E-6 || ( x[1]<0.9-1.0e-6 && x[0]>1.0-1.0e-6 )  ) {y=1.0;}
//      else if (x[0]<1E-6 ){y=x[1]+0.6;} //north and south
//      else y=x[1]*6.0-4.4;//{y=(10.0/6.0)*x[1]+(1.0/3.0);}{y=2.0;}
//      if (x[1]<0.4-1E-6 || ( x[1]<0.9-1.0e-6 && x[0]>1.0-1.0e-6 )  ) {y=1.0;}
//      else {y=2.0;} //north and south

    //new bc
/*      RF p0=1.0;
  RF y0=0.4;
  RF y1=0.4;
  RF x0=0.0;
  RF x1=1.0;
  RF m=(y1-y0)/(x1-x0); //dy/dx

  if (x[1]<y0-1E-6 || ( x[1]<y1-1.0e-6 && x[0]>1.0-1.0e-6 )  )//below the fracture
  {y=p0 + ( x[1] -(y0 +m*(x[0] -x0) ) );}

  else {y=p0 - ( x[1] -(y0 +m*(x[0] -x0) ) );}
*/
    //      y=1.0;
    //      y = (x[1]< 0.4) ?  (1.0) : (2.0);
    //      y = (x[1]< 0.4) ?  (1.0) : ((1.0/3.0)+(x[1]/0.6));
    //      if (x[1]<0.4-1E-6 || ( x[1]<0.9-1.0e-6 && x[0]>1.0-1.0e-6 )  ) {y=1.0;}
    //      else if (x[0]<1E-6 ){y=x[1]+0.6;} //north and south
    //      else y=x[1]*6.0-4.4;//{y=(10.0/6.0)*x[1]+(1.0/3.0);}{y=2.0;}
//    y = (xg[1]< 0.0) ?  (1.0) : (2.0);//((1.0/3.0)+(x[1]/0.6))
    //        y = (xg[1]< 0.0) ?  (1.0) : (2.0);//((1.0/3.0)+(x[1]/0.6))
    //        y=1.0;
    /*    y = (xg[0]< -1.0+1.0e-6 || xg[0]>1.0-1.0e-6 ) ?  (1.0) : (2.0);
            y=xg[0];
        y=0.0;
        if (x[1]<0.4-1E-6 || ( x[1]<0.9-1.0e-6 && x[0]>1.0-1.0e-6 )  ) {y=1.0;}
        else if (x[0]<1E-6 ){y=x[1]+0.6;} //north and south
        else y=x[1]*6.0-4.4;//{y=(10.0/6.0)*x[1]+(1.0/3.0);}{y=2.0;}
            if (x[1]<0.4-1E-6 || ( x[1]<0.9-1.0e-6 && x[0]>1.0-1.0e-6 )  ) {y=1.0;}
            else {y=2.0;} //north and south

    //            y=xg[1]*reference_.gravity()/(reference_.velocity()*reference_.velocity());//hydrostatic pressure, higher at top!
        //new bc
        RF p0=1.0;
        RF y0=0.4;
        RF y1=0.4;
        RF x0=0.0;
        RF x1=1.0;
        RF m=(y1-y0)/(x1-x0); //dy/dx
    */

    //    if ( (xg[0]<0.0 && xg[1]<0.0) )
    //        if ( (xg[1]<0.1) )
    //        {

    //    std::cout << "bc at x=" <<xg[0]<< " y=" << xg[1] << "  is set to: " << y << std::endl;
    //        }

    //    else if (x[1]< y0-1E-6 +m*(x[0]-x0) )
    //    {y=p0 - ( x[1] -(y0 +m*(x[0] -x0) ) );
    //    std::cout << "bc at x=" <<x[0]<< " y=" << x[1] << "  is set to: " << y << std::endl;
    //    }
    //    else if (x[0]<1E-6 || x[0]>1-1E-6)
    //    {y=1.0;
    //    std::cout << "bc at x=" <<x[0]<< " y=" << x[1] << "  is set to: " << y << std::endl;
    //    }
    //    else
    //        {y=1.0;
    //        std::cout << "bc at x=" <<xg[0]<< " y=" << xg[1] << "  is set to: " << y << std::endl;
    //        }
}//end standardBC

template<typename RF, typename FV>
RF pfEqualsAverageSingle(const FV &x){
    //boundary conditions
    const RF pLeft=1.0;
    const RF pRight=1.0;
    //soil parameters
    const RF alphaF=2.0;
    const RF k1=1.0;
    const RF k2=1.0;
    //chif is the position of the fracture
    RF chif=(x[1]<0.0 ? 0.7 : -0.7);
    RF length=2.0;
    RF offset=1.0;
    //normalize
    chif+=offset;
    chif/=length;
    RF chi=(x[0]+offset)/length;
//compute the values for p+ and p- and pf
    RF b1=pLeft;
    RF denominator2=(alphaF*k1 + k2*(2.0*k1+alphaF*chif) -alphaF*k1*chif);
    RF b2=pRight+ (alphaF*k1*(pLeft-pRight))/denominator2;
    RF m2= -(alphaF*k1*(pLeft-pRight))/denominator2;
    RF m1= -(alphaF*k2*(pLeft-pRight))/(k1*(alphaF+2.0*k2-alphaF*chif)+alphaF*k2*chif);
    RF p=0.0;
if(chi==chif){
    p=( (k1*pLeft-k2*pRight)/(k1-k2) ) -
            ( (k1*k2*(pLeft-pRight)*(alphaF*k1+k2) )/( (k1-k2)*(alphaF*k1+2.0*k1*k2-chif*alphaF*(k1-k2)) ) );
}
else{
    p=(chi<chif ? m1*chi+b1 : m2*chi+b2);
}
    return ( p );
}

}//end namespace Dumux


struct BCType : public Dune::PDELab::DirichletConstraintsParameters
{

    template<typename I>
    bool isDirichlet(const I &ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> &x) const
    {
        auto xg = ig.geometry().global(x);
        // no bc
        if (!ig.boundary()){
            Dune::dinfo << "At intersection centre: "<< xg << "  no bc" << std::endl;
            return (false); // no bc on subdomain interface
        }
        // Dirichlet
//        else if ( Dumux::onLeft(xg) || Dumux::onRight(xg) ){//top and bottom Dirichlet
        else if ( Dumux::onTop(xg) || Dumux::onBottom(xg) ){//top and bottom Dirichlet
//        else if ( Dumux::onBottom(xg) ){
//        else if( Dumux::injection(xg) || Dumux::extraction(xg) ){
            Dune::dinfo << "At intersection centre: "<< xg << " Dirichlet bc" << std::endl;
            return (true);//  || Dumux::onTop(xg) || Dumux::onBottom(xg)
        }
        // Neumann
        return (false);
    }
    template<typename Scalar, typename I>
    Scalar neumannFlux(const I & ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
    {
//        auto xg = ig.geometry().global(x);
        assert (this->isNeumann(ig,x));
        return ( (0.0) );
    }
    template<typename I>
    bool isNeumann(const I &ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> &x) const
    {
//        auto xg = ig.geometry().global(x);
        if (!ig.boundary()) return (false); // no bc
        //returns true if no Dirichlet
        return ( !(this->isDirichlet(ig,x)) );//this assumes there are either Dirichlet or Neumann boundaries
    }
};

template<typename GB>
struct FractureBCType : public Dune::PDELab::DirichletConstraintsParameters
{
public:
    FractureBCType(const GB &gb): gb_(gb){};
private:
    template<typename FV>
    bool onBoundary(const FV &xg) const{
        return (gb_.onGlobalBoundary(xg));
    }
public:
    template<typename I>
    bool isDirichlet(const I & ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
    {
        auto xg = ig.geometry().global(x);
        if (!ig.boundary()){
            Dune::dinfo << xg << "  no bc" << std::endl;
//          std::cout << xg << "  no bc" << std::endl;
            return (false); // no bc on subdomain interface
        }
        else if (!this->onBoundary(xg)) return (false);
//        else if ( Dumux::onTop(xg) || Dumux::onBottom(xg) || Dumux::onLeft(xg) || Dumux::onRight(xg) ){
////        else if ( Dumux::onRight(xg) || Dumux::onLeft(xg)){
////        else if ( Dumux::onBottom(xg) ){
////            Dune::dinfo << xg << " Dirichlet bc" << std::endl;
//          std::cout << xg <<  " Dirichlet bc" << std::endl;
//            return (true);
//        }
        return (false);//Neumann boundary or internal node
    }

    template<typename I>
    bool isNeumann(const I & ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
    {
        auto xg = ig.geometry().global(x);
        if (!ig.boundary()) return (false); // no bc
        else if (!this->onBoundary(xg)) {
            return (false);
        }
        //returns true if no Dirichlet
        return ( !(this->isDirichlet(ig,x)) );//this assumes there are either Dirichlet or Neumann boundaries
    }
    template<typename Scalar, typename I>
    Scalar neumannFlux(const I & ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
    {
//        auto xg = ig.geometry().global(x);
        assert (this->isNeumann(ig,x));
        return ( (0.0) );
    }
    template<typename I>
    bool isEnding(const I & ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
    {
        auto xg = ig.geometry().global(x);
        auto local = ig.geometryInInside().global(x);//element local (transformation from intersection local to element local)
        bool tripleNode=Dumux::arePointsEqual(ig.inside()->geometry().integrationElement(local),0.0);
//        std::cout << "xg: " << xg << "\t is B: "<<this->onBoundary(xg)<<"\t is D: " << this->isDirichlet(ig,x)<< "\t is N: "  <<this->isNeumann(ig,x)<<std::endl;
        if (!ig.boundary()){
            return (false); // no bc on subdomain interface
        }
        else if (tripleNode){//catch here the case of duplicated nodes due to alberta limiations of crossing handling
            return (false); // no bc on subdomain interface
        }

        else if (this->onBoundary(xg)) {
            return (false);
        }
        else if (!(this->isDirichlet(ig,x)) && !(this->isNeumann(ig,x)) ){
//            std::cout << "xg: " << xg << "\t is E!"<<std::endl;
            return (true);
        }
        return ( false );//returns false for all inner boundaries or Dirichlet or Neumann boundaries
    }
private:
    const GB &gb_;
};

/*
 * this struct returns always false for Dirichlet bc
 * and in addition always true for Neumann boundaries, i.e.,
 * there is always no flow for the enriched element boundaries.
 */

struct EmptyBCType : public Dune::PDELab::DirichletConstraintsParameters
{
    template<typename I>
    bool isDirichlet(const I & ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
    {
        Dune::dinfo << ig.geometry().center() << "  no bc" << std::endl;
        return (false); // no bc on subdomain interface
    }

    template<typename I>
    bool isNeumann(const I & ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
    {
        auto xg = ig.geometry().global(x);
        if (!ig.boundary()) return (false); // no bc
        //returns true if no Dirichlet
        return ( !(this->isDirichlet(ig,x)) );//this assumes there are either Dirichlet or Neumann boundaries
    }
};

/** \brief A function that defines Dirichlet boundary conditions AND
    its extension to the interior */
template<typename GV, typename Reference, typename RF>
class BCDirichlet : public Dune::PDELab::GridFunctionBase<Dune::PDELab::
        GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, BCDirichlet<GV,Reference,RF> > {

public:
    typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

    //! construct from grid view
    BCDirichlet (const GV &gv, const Reference &reference) : gv_(gv),reference_(reference) {}

    //! evaluate extended function on element
    inline void evaluate (const typename Traits::ElementType& e,
            const typename Traits::DomainType& xlocal, typename Traits::RangeType& y) const
    {
        const int dimworld = Traits::GridViewType::Grid::dimensionworld;
        typedef typename Traits::GridViewType::Grid::ctype ctype;
        Dune::FieldVector<ctype,dimworld> xg = e.geometry().global(xlocal);
        //check global coordinates and set the Dirichlet value (y=...)
        //if vertex is not on a boundary with type Dirichlet the value can be set but will NOT be used
        y = Dumux::standardBC<RF>(xg);
        return;
    }
    //! get a reference to the grid view
    inline const GV& getGridView () {return (gv_);}
private:
    const GV &gv_;
    const Reference &reference_;
};

template<typename GV, typename Reference, typename RF>
class BCDirichletFracture : public Dune::PDELab::GridFunctionBase<Dune::PDELab::
        GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, BCDirichletFracture<GV,Reference,RF> >
    {
public:
    typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

    //! construct from grid view
    BCDirichletFracture (const GV &gv, const Reference &reference) : gv_(gv),reference_(reference) {}

    //! evaluate extended function on element
    inline void evaluate (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal,
            typename Traits::RangeType& y) const
    {
        const int dimworld = Traits::GridViewType::Grid::dimensionworld;
        typedef typename Traits::GridViewType::Grid::ctype ctype;
        Dune::FieldVector<ctype,dimworld> xg = e.geometry().global(xlocal);

        //check global coordinates and set the Dirichlet value (y=...)
        //if vertex is not on a boundary with type Dirichlet the value can be set but will NOT be used
        if ( Dumux::onTop(xg) || Dumux::onBottom(xg) || Dumux::onLeft(xg) || Dumux::onRight(xg) ){
            y=Dumux::standardBC<RF>(xg);
//            y = 0.5;
        }
//        else y=-1.0*xg[0];
        else y=3.0;

        return;
    }

    //TODO add function to evaluate derivative(s)
    //! Evaluate derivatives of all shape functions at given position
    /**
     * \note Only required for Traits::diffOrder >= 2
     */
//    void evaluate
//    ( const typename Traits::ElementType& e, const Dune::array<std::size_t, Traits::dimDomain>& directions,
//      const typename Traits::DomainType& in,
//      typename Traits::RangeType& out) const{
//
//        assert(directions[0]==1);
//        const int dimworld = Traits::GridViewType::Grid::dimensionworld;
//        typedef typename Traits::GridViewType::Grid::ctype ctype;
//        //Dune::FieldVector<ctype,dimworld> xg = e.geometry().global(in);
//        auto jacobian=e.geometry().jacobianTransposed(in);
//
//        Dune::FieldVector<ctype,dimworld> deriv(0.);
//        deriv[0]=0.5;
//        jacobian.mv(deriv,out);
//        return;
//    }

    //! get a reference to the grid view
    inline const GV& getGridView () {return (gv_);}
private:
    const GV &gv_;
    const Reference &reference_;
};

      /** \brief A function that defines Dirichlet boundary conditions AND
      its extension to the interior */
template<typename GV, typename RF>
class BCDirichletEnriched : public Dune::PDELab::GridFunctionBase<Dune::PDELab::
        GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, BCDirichletEnriched<GV,RF> >
{
    const GV& gv;
public:
    typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

    //! construct from grid view
    BCDirichletEnriched (const GV& gv_) : gv(gv_) {}

    //! evaluate extended function on element
    inline void evaluate (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal,
            typename Traits::RangeType& y) const
    {
        const int dimworld = Traits::GridViewType::Grid::dimensionworld;
        typedef typename Traits::GridViewType::Grid::ctype ctype;
        Dune::FieldVector<ctype,dimworld> xg = e.geometry().global(xlocal);
        Dune::FieldVector<ctype,dimworld> f(0.0);
        f[0]=1.0;//fracture end on right boundary
        //check global coordinates and set the Dirichlet value (y=...)
        //if vertex is not on a boundary with type Dirichlet the value can be set but will NOT be used
        if (Dumux::onTop(xg) || Dumux::onBottom(xg) || Dumux::onLeft(xg) || Dumux::onRight(xg) ) {
//            y = 2*Dumux::standardBC<RF>(f) - Dumux::standardBC<RF>(xg);
//            if (xg[1]<0.0) y= (2.0);
//            else y=(1.0);
//            if (xg[0]<0.0) y= (2.0);
//            else y=1.0;
            y=Dumux::standardBC<RF>(xg);
        }
        else{
            y=0.0;
        }
        return;
    }

    //! get a reference to the grid view
    inline const GV& getGridView () {return (gv);}
};
