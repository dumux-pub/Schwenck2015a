#ifndef HELPERFUNCTIONS_HH_
#define HELPERFUNCTIONS_HH_

#include <dune/common/float_cmp.hh>
#include <algorithm>

namespace Dumux{

template <typename K, typename V>
std::multimap<V, K> invertMap(std::multimap<K, V>& forwardMap)
{
    std::multimap<V, K> inverseMap;
    for (typename std::multimap<K,V>::iterator it1=forwardMap.begin(); it1 != forwardMap.end();it1++)
    {
        inverseMap.insert(std::pair<V,K>((*it1).second,(*it1).first));
    }
    return (inverseMap);
}

//TODO: DOC ME!
//the equality epsilon has to be smaller than the normalEpsilon in modified basis function!!!
template<typename Scalar>
Scalar equalityEpsilon() {
    return (1.0e-8);//TODO scale equalityEpsilon with the smallest fracture cell size/aperture -> epsilon must be smaller
}//end equalityEpsilon


// Usable AlmostEqual function
template<typename Scalar>
bool almostEqual2sComplement(const Scalar a, const Scalar b, int maxUlps=5)

{
    // Make sure maxUlps is non-negative and small enough that the
    // default NAN won't compare as equal to anything.
    assert(maxUlps > 0 && maxUlps < 4 * 1024 * 1024);

    // Make aInt lexicographically ordered as a twos-complement int
    int aInt = *(int*)&a;
    if (aInt < 0)
        aInt = 0x80000000 - aInt;

    // Make bInt lexicographically ordered as a twos-complement int
    int bInt = *(int*)&b;
    if (bInt < 0)
        bInt = 0x80000000 - bInt;

    int intDiff = std::abs(aInt - bInt);

    if (intDiff <= maxUlps)
        return (true);

    return (false);
}



/*
 * Don't ever try to compare floating point numbers! Use the "arePointsEqual" booleans.
 * arePointsEqual for Dune::FieldVectors returns true if the two norm of the difference
 * of two geometrical points of the same dimension
 * differ by a maximum of equalityEpsilon
 *
 * if different input types are used always the first is used as type for the epsilon!
 *
 * TODO doc me regarding relative epsilon scaling
 */


template<typename RFA, typename RFB, int dim >
bool arePointsEqual(const Dune::FieldVector<RFA,dim> &a, const Dune::FieldVector<RFB,dim> &b, const RFA &epsilon=Dumux::equalityEpsilon<RFA>() ) {
    assert(a.size()==b.size());
    RFA relEpsilon=epsilon;
    RFA min=std::min(std::abs(a.two_norm()), std::abs(b.two_norm()) );
    if (  min >Dumux::equalityEpsilon<RFA>() ){
        relEpsilon*= min;
    }
    return ( (a-b).two_norm()  < relEpsilon );
}//end arePointsEqual


template<typename RFA, typename RFB, int dim=1>
bool arePointsEqual(const RFA &a, const RFB &b, const RFA &epsilon=Dumux::equalityEpsilon<RFA>()){
    RFA relEpsilon=epsilon;
    RFA min = std::min(std::abs(a), std::abs(b) );
    if ( min >Dumux::equalityEpsilon<RFA>() ){
        relEpsilon*=min;
    }
    return (std::abs(a-b)  < relEpsilon);
}//end arePointsEqual

/*
 * calculates the coefficient which is necessary to go from point a along the line
 * spanned by a,b to the closest point to x, i.e., x does not have to be on the line a-b
 */
template<typename FV>
typename FV::value_type lambda(const FV &a, const FV &b, const FV &x){
    if (Dumux::arePointsEqual(a,x)){
        return (0.0);
    }
    else if(Dumux::arePointsEqual(b,x)){
        return (1.0);
    }
    FV ab=b-a;
    return ( ( a.two_norm2()+(x*ab)-(a*b) )/ab.two_norm2() );
}//end lambda

//calculates the normal vector from a line spanned by a,b pointing towards x
template<typename FV>
FV closestPointOnLine(const FV &a, const FV &b, const FV &x){
    FV ab=b-a;//ab points outwards from the centre to the point which is not a corner
    //calculate closest point
    typename FV::value_type lambda=Dumux::lambda(a,b,x);
//    std::cout << "lambda: " << lambda << std::endl;
//    assert(lambda>=0.0 && lambda<=1.0);
    FV closestPoint=ab;
    closestPoint*=lambda;
    closestPoint+=a;
    return (closestPoint);
}//end closestPointOnLine

/*
 * TODO doc me! calculates the normal mathematically positive oriented
 * if the vector f points from zero to one
 * the orientation of the normal vector is arbitrary!
 * but in this implementation always mathematical positive oriented, i.e., pointing counter-clockwise
 */
template<typename FV>
FV normal(const FV &f0, const FV &f1) {
    assert(FV::dimension==2);
    FV normal;
    FV f=f1-f0;
    normal[0]=f[1];
    normal[0]*=-1.0;
    normal[1]=f[0];
    normal/=normal.two_norm();
    return (normal);
}//end normal

template<typename FV>
FV normal(const FV &f0) {
    FV normal(f0);
    normal[0]=(-1.0)*f0[1];
    normal[1]=f0[0];
    normal/=normal.two_norm();
    return (normal);
}// end normal

/*
 * calculates the shortest distance between point x and the line spanned by a,b
 * and returns this as positive value if the normal points from the line towards x
 * and negative if the normal points away from x
 */
template<typename FV>
typename FV::value_type signedDistance(const FV &a, const FV &b, const FV &x){
    FV closestPoint=Dumux::closestPointOnLine(a,b,x);
    FV lineNormal=Dumux::normal(a,b);
    FV distanceVector=(x-closestPoint);
    typename FV::value_type distance=distanceVector.two_norm();
    //calculate the scalar product of the line normal and the distanceVector
    distanceVector/=distance;
    typename FV::value_type sign=distanceVector*lineNormal;
    if (Dumux::arePointsEqual(sign,-1.0)) distance*=-1.0;
    return ( distance );
}//end distance

//calculates the shortest distance between point x and the line spanned by a,b
template<typename FV>
typename FV::value_type absDistance(const FV &a, const FV &b, const FV &x){
    FV closestPoint=Dumux::closestPointOnLine(a,b,x);
    return ( (x-closestPoint).two_norm() );
}//end distance

//calculates the shortest distance between points a,x
template<typename FV>
typename FV::value_type absDistance(const FV &a, const FV &x){
    return ( (x-a).two_norm() );
}//end distance


//test if a scalar value "test" is larger or equal than a lowerLimit and smaller or equal than an upperLimit
//and returns true if both are true
//this test is "soft", i.e., the boundary is thickened by epsilon so that equality with upper or lower limit is included in between
template<typename Scalar>
bool isBetweenOrEqual(const Scalar &test, const Scalar &lowerLimit, const Scalar &upperLimit, const Scalar &epsilon=Dumux::equalityEpsilon<Scalar>() ){
    Scalar relEpsilon=epsilon;
    //old
//    Scalar min = std::min(std::abs(lowerLimit), std::abs(upperLimit) );
//    if ( min >Dumux::equalityEpsilon<Scalar>() ){
//        relEpsilon*=min;
//    }
    //new
    Scalar min = std::abs(lowerLimit-upperLimit);
    if ( min >1.0 ){
        relEpsilon*=min;
    }
//    std::cout << "relEpsilon: " << relEpsilon<<std::endl;
//    Dune::FloatCmpOps<Scalar> floatCmpOps(relEpsilon);
//    return ( floatCmpOps.ge(test,lowerLimit) && floatCmpOps.le(test,upperLimit));
    return (test>=lowerLimit-relEpsilon && test<=upperLimit+relEpsilon );
}//end isBetween

//test if a scalar value "test" is larger or equal than a lowerLimit and smaller or equal than an upperLimit
//and returns true if both are true
//this test is "hard", i.e., the boundary is thinned by epsilon so that equality with upper or lower limit is excluded
template<typename Scalar>
bool isBetween(const Scalar &test, const Scalar &lowerLimit, const Scalar &upperLimit ){
    Dune::FloatCmpOps<Scalar> floatCmpOps(Dumux::equalityEpsilon<Scalar>());
    return ( floatCmpOps.gt(test,lowerLimit) && floatCmpOps.lt(test,upperLimit));
//    return (test>=lowerLimit+Dumux::equalityEpsilon() && test<=upperLimit-Dumux::equalityEpsilon() );
}//end isBetween

/* Calculates the intersection between a line spanned by a,b and another line c,d.
 *
 * The first two points should be the corners of the element,
 * the second two points should be the fracture element coordinates.
 *
 * This becomes important for the isBetween check, because the fracture should not cut directly through a corner
 * therefore there could be used the function isBetween which excludes equality,
 * but instead this check is performed outside this bool function, i.e., this function returns true even if the fracture
 * cuts very close to a corner.
 *
 * A fracture node is allowed to be close to a coarse rock matrix grid cell so that here inBetweenOrEqual should be used.
 *
 * The denominator becomes zero if the lines are parallel.
 * If the lines are not only parallel but actually the same,
 * the center of the two points closest together is calculated
 * but still false is returned.
 */

template<typename FV>
bool intersectionOfLines(const FV &a, const FV &b, const FV &c, const FV &d, FV &intersectionPoint){
    assert(FV::dimension==2);
    const typename FV::value_type denominator=(b[1]-a[1])*(c[0]-d[0]) - (b[0]-a[0])*(c[1]-d[1]);
    bool isIntersection=false;
//    std::cout << "denominator: " << denominator << std::endl;
    if ( std::abs( denominator ) < Dumux::equalityEpsilon<typename FV::value_type>() ){//the parallel case
        //find the two points which are closest together
        FV tmpA(a);
        FV tmpB(c);
        if ( Dumux::absDistance(a,d)<Dumux::absDistance(a,c) ) tmpB=d;
        if ( Dumux::absDistance(b,tmpB)<Dumux::absDistance(a,tmpB) ) tmpA=b;
        tmpA+=tmpB;
        tmpA/=2.0;
        if ( Dumux::arePointsEqual(Dumux::absDistance(a,b,tmpA),0.0) ) {
            intersectionPoint=tmpA;
        }
    }
    else{//the not parallel case
        const typename FV::value_type lambda1=( (b[1]-d[1])*(c[0]-d[0]) - (b[0]-d[0])*(c[1]-d[1]) ) / denominator;
        const typename FV::value_type lambda2=( (b[1]-d[1])*(a[0]-b[0]) - (b[0]-d[0])*(a[1]-b[1]) ) / denominator;
        intersectionPoint*=0.0;
        intersectionPoint+=(a-b);
        intersectionPoint*=lambda1;
        intersectionPoint+=b;

        if (Dumux::isBetweenOrEqual(lambda1,0.0,1.0) && Dumux::isBetweenOrEqual(lambda2,0.0,1.0) ){
            isIntersection = true;
        }
    }
    return ( isIntersection );
}//end intersection

//tests if a test point x lies on a line between two given points a,b
template<typename FV>
bool pointOnLine(const FV &a, const FV &b, const FV &x, const typename FV::value_type &epsilon=Dumux::equalityEpsilon<typename FV::value_type>()) {
    typename FV::value_type relEpsilon=epsilon*std::max(std::max(a.two_norm(),b.two_norm()),1.0);
    if (Dumux::absDistance(a,b,x)>relEpsilon){
//        std::cout << "|d|= " << Dumux::absDistance(a,b,x) <<" \t relEpsilon: " << relEpsilon << std::endl;
        return (false);
    }
    typename FV::value_type lambda=Dumux::lambda(a,b,x);
//    std::cout << "lambda: " << lambda << "\t relEpsilon: "<< relEpsilon << std::endl;
    if (Dumux::isBetweenOrEqual(lambda,0.0,1.0,relEpsilon)){
        return (true);
    }
    else {
        return (false);
    }
}// end pointOnLine


/* This implentation is based on Lascha Lagidse's implementation of a "ray casting" algorithm.
 * http://alienryderflex.com/polygon/
 * The function will return TRUE if the point is inside the polygon, or
 * FALSE if it is not.  If the point is exactly on the edge of the polygon,
 * then the function returns also TRUE.
 * Note that division by zero is avoided because the division is protected
 * by the "if" clause which surrounds it.
 *
 * int    numberOfCorners            =  how many corners the polygon has
 * float  polygonCo
 * rnerVector[i][0]  =  horizontal coordinates of corners
 * float  polygonCornerVector[i][1]  =  vertical coordinates of corners
 * FV     point                      =  point to be tested
 */
template<typename PolygonCornerVector>
bool pointInPolygon(const PolygonCornerVector &polygonCornerVector, const typename PolygonCornerVector::value_type &point) {
    int numberOfPolygonCorners=polygonCornerVector.size();
  int j=numberOfPolygonCorners-1 ;
  bool oddNodes=false;
  //tests if the point is one corner
  for (unsigned i=0; i<numberOfPolygonCorners; i++) {
      if( Dumux::arePointsEqual(polygonCornerVector[i],point) ){
//              std::cout << " point is on corner!\n";
          return (true);
      }
  }
  //tests if the point is on the line between the corner and the next
  for (unsigned i=0; i<numberOfPolygonCorners; i++) {
      if (Dumux::pointOnLine(polygonCornerVector[i], polygonCornerVector[j],point)){
//              std::cout << " point is on line!\n";
          return (true);
      }
      j=i;
  }
  //start ray casting algorithm only if point is not on the edges of the polygon
  j=numberOfPolygonCorners-1 ;
  for (unsigned i=0; i<numberOfPolygonCorners; i++) {
      if ( ( (polygonCornerVector[i][1]< point[1] && polygonCornerVector[j][1]>=point[1])
              || (polygonCornerVector[j][1]< point[1] && polygonCornerVector[i][1]>=point[1]) )
              &&  ( polygonCornerVector[i][0]<=point[0] || polygonCornerVector[j][0]<=point[0]) ) {
          oddNodes^=(polygonCornerVector[i][0]+(point[1]-polygonCornerVector[i][1])/(polygonCornerVector[j][1]-polygonCornerVector[i][1])*(polygonCornerVector[j][0]-polygonCornerVector[i][0])<point[0]);
      }
      j=i;
  }
//      std::cout << " point is in polygon!\n";
  return (oddNodes);
}//end bool pointInPolygon

template<typename Element, typename FV>
bool isInside(const Element &element, const FV &point){//TODO maybe the DUNE function checkInside can be used instead?!
    typedef typename std::vector<FV> StdVectorOfFieldVectors;
    StdVectorOfFieldVectors polygonCornerVector;
    for (std::size_t i=0;i<element.geometry().corners();i++){
        polygonCornerVector.push_back(element.geometry().local(element.geometry().corner(i)));
    }
    FV pointLocal=element.geometry().local(point);
    return (Dumux::pointInPolygon(polygonCornerVector,pointLocal));
}

//checks if a geometrical point lies on the global boundary of the GV
//this is a specialization, where it is already known in which element of the GV the geometrical point lies
template<typename GV, typename Element, typename FV>
bool onGlobalBoundary(const GV &gv, const Element &element, const FV &a){
    assert(GV::dimension==2);//this function only works in 2D
     typedef typename GV::IntersectionIterator IntersectionIterator;
     IntersectionIterator isItEnd = gv.iend(*element);
     for (IntersectionIterator isIt = gv.ibegin(*element); isIt != isItEnd; ++isIt){
         if (isIt->boundary() ){
             if( Dumux::absDistance( element.geometry().local(isIt->geometry().corner(1)),
                     element.geometry().local(isIt->geometry().corner(0)),
                     element.geometry().localc(a)) < Dumux::equalityEpsilon<FV::value_type>() ){
                 return (true);
             }
         }
     }
     return (false);
 }

//checks if a geometrical point lies on the global boundary of the GV
//this is the more general case and computational more expensive, because it has to be iterated over every element of GV
template<typename GV, typename FV>
bool onGlobalBoundary(const GV &gv, const FV &a){
    assert(GV::dimension==2);//this function only works in 2D
    typedef typename GV::IntersectionIterator IntersectionIterator;
    typedef typename GV::template Codim<0>::Iterator ElementGridIterator;
    ElementGridIterator eendit = gv.template end<0>();
    for (ElementGridIterator matrixElementIterator = gv.template begin<0>(); matrixElementIterator != eendit; ++matrixElementIterator) {
        FV aLocal=matrixElementIterator->geometry().local(a);
        //only test if a is on a global boundary if aLocal is within this matrix element
        if( Dumux::isInside(*matrixElementIterator,a) ){
            IntersectionIterator isItEnd = gv.iend(*matrixElementIterator);
            for (IntersectionIterator isIt = gv.ibegin(*matrixElementIterator); isIt != isItEnd; ++isIt){
                if (isIt->boundary() ){
                    if( Dumux::absDistance( matrixElementIterator->geometry().local(isIt->geometry().corner(1)),
                            matrixElementIterator->geometry().local(isIt->geometry().corner(0)),
                            aLocal) < Dumux::equalityEpsilon<FV::value_type>() ){
                        return (true);
                    }
                }
            }
        }
    }
    return (false);
}

/*
 * tests if a test point (in reference element coordinates) lies on a corner
 * works only for 2d
 *
 */
template<typename FV>
bool equalToCorner(const FV &test){
    typedef typename FV::value_type Scalar;
    assert(test.dimension==2);
    const Dune::ReferenceElement<Scalar,FV::dimension>& genericCube = Dune::ReferenceElements<Scalar, FV::dimension>::cube();
    for (std::size_t i=0;i<genericCube.size(test.dimension);i++){
        if ( Dumux::arePointsEqual(test,genericCube.position( i, test.dimension ) ) ){
            return (true);
        }
    }
    return (false);
}

//comparison function
template<typename Intersection>
bool sortVectorX0 (Intersection vector1,Intersection vector2) { return (vector1[0] < vector2[0]); }

//sort function
template<typename StdVectorOfIntersections, typename EG>
StdVectorOfIntersections sortPoints (StdVectorOfIntersections &pointsNotOnCorners, EG &eg){

//    sortedPoints[0]=point1;
//    sortedPoints[1]=point2;
    typedef typename StdVectorOfIntersections::value_type Intersection;
    StdVectorOfIntersections sortedPoints;
    sortedPoints.clear();
    //fill sortedPoints vector with entry and exit point and all four corners. Coordinates are local on the reference element.
    for (unsigned i=0;i<4;i++){
        Intersection intersection;
        intersection.intersection=eg.geometry().local(eg.geometry().corner(i));
        sortedPoints.push_back(intersection);}
    typedef typename StdVectorOfIntersections::const_iterator stdVectorConstIterator;
    for (stdVectorConstIterator it=pointsNotOnCorners.begin(); it!=pointsNotOnCorners.end(); it++ )
    {
        sortedPoints.push_back(*it);
    }

    /*
     * project all points on x axis with the following rules:
     * if (x1==0.0) then do nothing
     * else if (x1==1.0) then x0= 3.0-x0
     * else if (x0==1.0) then x=1.0+x1
     * else if (x0==0.0) then x=4-x1
     */
    for (unsigned i=0;i<sortedPoints.size() ;i++){
        if( Dumux::arePointsEqual( (sortedPoints[i])[1], 0.0 ) ) {//do nothing
            }
        else if ( Dumux::arePointsEqual ( (sortedPoints[i])[1], 1.0 ) ){
            (sortedPoints[i])[0]=3.0 -(sortedPoints[i])[0];
        }
        else if ( Dumux::arePointsEqual( (sortedPoints[i])[0], 1.0 ) ){
            (sortedPoints[i])[0]=1.0 +(sortedPoints[i])[1];
        }
        else if ( Dumux::arePointsEqual( (sortedPoints[i])[0], 0.0 ) ){
            (sortedPoints[i])[0]=4.0 -(sortedPoints[i])[1];
        }
    }

    // using function as comparison
    std::sort (sortedPoints.begin(), sortedPoints.end(), Dumux::sortVectorX0<Intersection> );
    /*
     * transform the sorted points from the x-axis back to their original position with the following rules:
     * if if (x1==0.0) then do nothing
     * else if (x1==1.0) then x0= 3.0-x0
     * else if (1.0<x0<2.0) then x=1.0
     * else if (3.0<x0<4.0) then x=0.0
     */
    for (unsigned i=0;i<sortedPoints.size() ;i++){
        if( Dumux::arePointsEqual( (sortedPoints[i])[1], 0.0 ) ) {//do nothing
            }
        else if ( Dumux::arePointsEqual( (sortedPoints[i])[1], 1.0 ) ){
            (sortedPoints[i])[0]=3.0 -(sortedPoints[i])[0];
        }
        else if ( Dumux::isBetween( (sortedPoints[i])[0], 1.0, 2.0 ) ){
            (sortedPoints[i])[0]=1.0;
        }
        else if ( Dumux::isBetween( (sortedPoints[i])[0], 3.0, 4.0 ) ){
            (sortedPoints[i])[0]=0.0;
        }
    }

    return (sortedPoints);
}//end function sortPoints

//sort function
template<typename FV, typename StdVectorOfIntersections, typename EG>
std::vector<FV> sortPoints (StdVectorOfIntersections &pointsNotOnCorners, EG &eg){

//    sortedPoints[0]=point1;
//    sortedPoints[1]=point2;
    std::vector<FV> sortedPoints;
    //fill sortedPoints vector with entry and exit point and all four corners. Coordinates are local on the reference element.
    for (unsigned i=0;i<4;i++){
        sortedPoints.push_back(eg.geometry().local(eg.geometry().corner(i)));}
    typedef typename StdVectorOfIntersections::const_iterator stdVectorConstIterator;
    for (stdVectorConstIterator it=pointsNotOnCorners.begin(); it!=pointsNotOnCorners.end(); it++ )
    {
        sortedPoints.push_back(*it);
    }

    /*
     * project all points on x axis with the following rules:
     * if (x1==0.0) then do nothing
     * else if (x1==1.0) then x0= 3.0-x0
     * else if (x0==1.0) then x=1.0+x1
     * else if (x0==0.0) then x=4-x1
     */
    for (unsigned i=0;i<sortedPoints.size() ;i++){
        if( Dumux::arePointsEqual( (sortedPoints[i])[1], 0.0 ) ) {//do nothing
            }
        else if ( Dumux::arePointsEqual ( (sortedPoints[i])[1], 1.0 ) ){
            (sortedPoints[i])[0]=3.0 -(sortedPoints[i])[0];
        }
        else if ( Dumux::arePointsEqual( (sortedPoints[i])[0], 1.0 ) ){
            (sortedPoints[i])[0]=1.0 +(sortedPoints[i])[1];
        }
        else if ( Dumux::arePointsEqual( (sortedPoints[i])[0], 0.0 ) ){
            (sortedPoints[i])[0]=4.0 -(sortedPoints[i])[1];
        }
    }

    // using function as comparison
    std::sort (sortedPoints.begin(), sortedPoints.end(), Dumux::sortVectorX0<FV> );
    /*
     * transform the sorted points from the x-axis back to their original position with the following rules:
     * if if (x1==0.0) then do nothing
     * else if (x1==1.0) then x0= 3.0-x0
     * else if (1.0<x0<2.0) then x=1.0
     * else if (3.0<x0<4.0) then x=0.0
     */
    for (unsigned i=0;i<sortedPoints.size() ;i++){
        if( Dumux::arePointsEqual( (sortedPoints[i])[1], 0.0 ) ) {//do nothing
            }
        else if ( Dumux::arePointsEqual( (sortedPoints[i])[1], 1.0 ) ){
            (sortedPoints[i])[0]=3.0 -(sortedPoints[i])[0];
        }
        else if ( Dumux::isBetween( (sortedPoints[i])[0], 1.0, 2.0 ) ){
            (sortedPoints[i])[0]=1.0;
        }
        else if ( Dumux::isBetween( (sortedPoints[i])[0], 3.0, 4.0 ) ){
            (sortedPoints[i])[0]=0.0;
        }
    }

    return (sortedPoints);
}//end function sortPoints

/*
 * Calculates the absolut value of the included area of the given polygon.
 * Works for every simple (i.e., not self-intersecting polygon) - not only for convex polygons.
 */
template<typename PolygonCornerVector>
typename PolygonCornerVector::value_type::value_type polygonArea(const PolygonCornerVector &polygonCornerVector) {
  int numberOfPolygonCorners=polygonCornerVector.size();
  unsigned int j=numberOfPolygonCorners-1 ;
  typedef typename PolygonCornerVector::value_type::value_type Scalar;
  Scalar area=0.0;
  for (unsigned int i=0; i<numberOfPolygonCorners; i++) {
//      std::cout << "polygonCornerVector: " << polygonCornerVector[i] << "\t";
      area+=polygonCornerVector[j][0]*polygonCornerVector[i][1]-polygonCornerVector[i][0]*polygonCornerVector[j][1];
      j=i;
  }
  area*=0.5;
  area=std::abs(area);
//  std::cout << "area: " << area << "\t eps= " << Dumux::equalityEpsilon<Scalar>() <<std::endl;
//  assert(area>Dumux::equalityEpsilon<Scalar>());
  if (area<Dumux::equalityEpsilon<Scalar>()){
      area=0.0;
      std::cout << "WARNING -------------------- \n polygon has probably 0 area!" << std::endl;
  }
  return (area);
}//end Scalar polygonArea


/*
 * calculates the fracture vector which points towards the crossing from the angle
 * it operates on global coordinates
 * This is the "inverse" operation to orientationAngle.
 * This vector is a unit vector (it has the length one).
 * The input angle is between 0 and 2Pi. sin and cos of this angle
 * give the orientation of the vector with the correct sign!
 */
template<typename FV, typename RF>
FV fractureOrientationVector(const RF &angle) {
    FV fi(std::cos(angle));
    fi[1]=std::sin(angle);
    return (fi);
}// end orientationAngle




/*
 * Example and test program for testing whether a polygon is convex or concave,
 * modified after a contribuion by G. Adam Stanislav.
 *
 * Return whether a polygon in 2D is concave or convex.
 *
 * Returns true, if and only if the polygon is convex.
 * Returns false for concave, but also for incomputables eg: colinear points
 *
 * It is assumed that the polygon is simple
 * (does not intersect itself or have holes).
 */
template<typename Polygon>
bool isConvex(const Polygon &p)
{
    assert(p[0].size()==2);//only works in 2D
    int n=p.size();
    if (n < 3)//only works for polygons, not simple lines
        return(false);

    int i,j,k;
    /*
     * cross product
     *          u2*v3 - u3*v2
     * u x v =  u3*v1 - u1*v3
     *          u1*v2 - u2*v1
     */

    double z=0.0;
    int flag=0;

    for (i=0;i<n;i++) {
        j = (i + 1) % n;
        k = (i + 2) % n;
        z  = (p[j][0] - p[i][0]) * (p[k][1] - p[j][1]);//u1*v2
        z -= (p[j][1] - p[i][1]) * (p[k][0] - p[j][0]);//- u2*v1
//        std::cout << "z: " << z << std::endl;
        if (!Dumux::arePointsEqual(z,0.0)){//,1.0e-4
            if (z < 0)
                /*
                 * bitwise inclusive or, i.e., in this case it stays the same
                 * if flag is the same as the right hand side otherwise it is added
                 */
                flag |= 1;
            else if (z > 0)
                flag |= 2;
            if (flag == 3)
                return(false);
        }
  }
  if (flag != 0)
     return(true);
  else
     return(false);
}

}//end namespace Dumux
#endif /* HELPERFUNCTIONS_HH_ */
