#ifndef SOLUTION_HH_
#define SOLUTION_HH_

#include <dumux/io/exporttodgf.hh>
#include "matrixsolution.hh"
#include "fracturesolution.hh"
#include "gridcreator.hh"

namespace Dumux{

template<typename MixedGrid, typename MultiDomainGrid, typename IntersectionPointsMap, typename MultiGFS, typename EnrichedProblem, typename XM,
/*now fracture solution types*/ typename FractureGrid, typename FGFS, typename XF,typename OutputName>
class Solution
        : public MatrixSolution<MultiDomainGrid, IntersectionPointsMap, MultiGFS, EnrichedProblem, XM>,
          public FractureSolution<FractureGrid, FGFS, XF, MatrixSolution<MultiDomainGrid, IntersectionPointsMap, MultiGFS, EnrichedProblem, XM> >
{
    typedef MatrixSolution<MultiDomainGrid, IntersectionPointsMap, MultiGFS, EnrichedProblem, XM> MatrixSolutionType;
    typedef FractureSolution<FractureGrid, FGFS, XF, MatrixSolutionType> FractureSolutionType;
    typedef typename EnrichedProblem::Traits::TrialLocalFunctionSpace EPLFS;
    typedef typename EPLFS::template Child<0>::Type LBFS;
    typedef typename LBFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits FTraits;
    typedef typename FTraits::RangeFieldType Scalar;
    typedef typename MixedGrid::LeafGridView MGV;
    typedef typename MGV::IntersectionIterator IntersectionIterator;

    typedef CreateMixedGrid<MixedGrid, IntersectionPointsMap, Scalar> CreateMixedGridType;

public:

    Solution(MixedGrid &hostGrid, MultiDomainGrid& multiDomainGrid, IntersectionPointsMap& intersectionPointsMap,  MultiGFS& multigfs,EnrichedProblem& ep, XM& xm,
            const FractureGrid& fractureGrid, const FGFS& fgfs,const XF& xf, const OutputName &outputName) : //x FE solution
                MatrixSolutionType(multiDomainGrid,intersectionPointsMap,multigfs, ep,xm),
                matrixSolution_(*this),//slicing, i.e., this is not anymore of Solution type but of MatrixSolution type
                FractureSolutionType(fractureGrid,fgfs,xf,matrixSolution_ ),
                hostGrid_(hostGrid), fractureGrid_(fractureGrid),
                outputName_(outputName)

{
        /*
         * write new simplex-quadrilateral mixed grid for output, transport and coupling to DuMuX
         */
//        typedef typename MultiDomainGrid::HostGridType HostGrid;

        CreateMixedGridType createMixedGrid(hostGrid_,intersectionPointsMap);
        mixedgrid_= createMixedGrid.create();
        MGV mixedGridView=mixedgrid_->leafGridView();
//        faceMapper_(mixedGridView);
        pressureElementFaceVector_.resize(mixedGridView.size(/*codim*/1));

//        permeabilityVector_=createMixedGrid.getPermeabilityVector();

}

    template<typename Soil, typename VV>
    void evaluateMatrixVelocities(const Soil &soil, VV &velocitiesVector){
        MatrixSolutionType::evaluateVelocities(soil,velocitiesVector);
    }

    template<typename FV>
    Scalar evaluateMatrixPressure(const FV &global) {
//        std::cout << "now evaluate matrix pressure! "<< std::endl;
        return (MatrixSolutionType::evaluatePressureGlobal(global));
    }

    template<typename FV>
    Scalar evaluateFracturePressure(const FV &global) {
        return (FractureSolutionType::evaluatePressureGlobal(global));
    }

    template<typename FV>
    Scalar evaluatePressure(const FV &global) {
        Scalar pressure=0.0;
        //check if matrix or fracture
        typedef typename FractureGrid::LeafGridView FGV;
        FGV fgv=fractureGrid_.leafGridView();
        bool onFracture=false;
        typedef typename FGV::template Codim< 0>::Iterator FractureElementIterator;
        FractureElementIterator eEndIt = fgv.template end<0>();
        FractureElementIterator eStartIt = fgv.template begin<0>();
        for (FractureElementIterator eIt = eStartIt; eIt != eEndIt; ++eIt){
            auto local= eIt->geometry().local(global);
            typedef typename Dune::ReferenceElements<typename FractureGrid::ctype,FractureGrid::dimension> GRE;
            Dune::GeometryType gt = eIt->geometry().type();
            if (GRE::general(gt).checkInside(local)){
                /*
                 * for simplex fracture element the epsilon has to be large because the center of the simplex
                 * is not necessarily exactly on the fracture
                 * TODO The epsilon should be related to the fracture width
                 */
                if ( (eIt->geometry().global(local)-global).two_norm()< Dumux::equalityEpsilon<typename FractureGrid::ctype>() ){
                    onFracture=true;
                    break;
                }
            }
        }

        if (onFracture){
            /*
             * the global pressure evaluation
             * cannot easily be used on mixed grids (fracture + matrix)
             * because evaluating a gridfunction outside the actual grid
             * makes dune grid throw an error
             */
            //TODO to make use of the information in which element the global position lies implement a evaluateLocalFracturePressure!
            pressure=this->evaluateFracturePressure(global);

        }
        else{
            pressure=this->evaluateMatrixPressure(global);
        }

        return (pressure);
    }

    template<typename Soil, typename FV>
    Scalar evaluatePermeability(const Soil &soil,const FV &global) {
//        std::cout << "now evaluate permeability!"<<std::endl;
        Scalar permeability=1.0;

        //TRANSPORT HACK
        bool transportHack=true;
        double aperture=1.0e-4/2;
        double permeabilityF=1.0e4;
        if (transportHack){
//            if (global[0]>0.5-aperture && global[0] <0.5+aperture && global[1]>0.5-aperture && global[1] <0.5+aperture)
            if (global[0]>0.5-aperture && global[0] <0.5+aperture)
                return (permeabilityF);
            else if (global[1]>0.5-aperture && global[1] <0.5+aperture)
                return (permeabilityF);
            else if (global[0]>0.75-aperture && global[0] <0.75+aperture && global[1]>=0.5)
                return (permeabilityF);
            else if (global[1]>0.75-aperture && global[1] <0.75+aperture && global[0]>=0.5)
                return (permeabilityF);
            else if (global[0]>0.625-aperture && global[0] <0.625+aperture && global[1]>0.5 && global[1]<0.75)
                return (permeabilityF);
            else if (global[1]>0.625-aperture && global[1] <0.625+aperture && global[0]>0.5 && global[0]<0.75)
                return (permeabilityF);
        }
        else{

            //check if matrix or fracture
            typedef typename FractureGrid::LeafGridView FGV;
            FGV fgv=fractureGrid_.leafGridView();
            bool onFracture=false;
            typedef typename FGV::template Codim< 0>::Iterator FractureElementIterator;
            FractureElementIterator eEndIt = fgv.template end<0>();
            FractureElementIterator eStartIt = fgv.template begin<0>();
            for (FractureElementIterator eIt = eStartIt; eIt != eEndIt; ++eIt){
                auto local= eIt->geometry().local(global);
                typedef typename Dune::ReferenceElements<typename FractureGrid::ctype,FractureGrid::dimension> GRE;
                Dune::GeometryType gt = eIt->geometry().type();

                if (GRE::general(gt).checkInside(local)){
                    //                std::cout << "global(local(global)): " << eIt->geometry().global(local)  << "\t global: " << global <<
                    //                    "\t e= " <<(eIt->geometry().global(local)-global).two_norm() <<std::endl;
                    if ( (eIt->geometry().global(local)-global).two_norm()< Dumux::equalityEpsilon<typename FractureGrid::ctype>() ){
                        onFracture=true;
                        permeability=soil.kFT(*eIt);
                        //                    std::cout << "perm F: " << permeability<< std::endl;
                        break;
                    }
                }
            }
            if (!onFracture){
                permeability=1.0;
            }
        }


        return (permeability);
    }

/*
 * This is the cell centered interpolation.
 * It combines the matrix and fracture solution on a mixed,
 * equi-dimensional grid.
 * The pressure jump is not anymore clearly visible.
 */
    template<typename Soil>
    void interpolateGlobalCCSolution(const Soil &soil){
        MGV mixedGridView=mixedgrid_->leafGridView();
        MixedGridElementMapper mixedGridMapper(mixedGridView);

        pressureCellVector_.clear();
        pressureCellVector_.resize(mixedGridView.size(/*codim*/0));

        permeabilityVector_.clear();
        permeabilityVector_.resize(mixedGridView.size(/*codim*/0));

        typedef typename MGV::template Codim< 0>::Iterator ElementIterator;
        ElementIterator eEndIt = mixedGridView.template end<0>();
        ElementIterator eStartIt = mixedGridView.template begin<0>();
        for (ElementIterator eIt = eStartIt; eIt != eEndIt; ++eIt){
            auto global= eIt->geometry().center();
            auto pressure=this->evaluatePressure(global);
            auto permeability=this->evaluatePermeability(soil,global);
            //get index
            auto index=mixedGridMapper.map(*eIt);
            pressureCellVector_[index]=pressure;
            permeabilityVector_[index]=permeability;
        }
    }


    /*
     * This is the interpolation of pressures at the faces.
     * It combines the matrix and fracture solution on a mixed,
     * equidimensional grid. However, the fracture pressure is
     * only taken into account if the equidimensional fracture is
     * resolved with more than one element in normal direction.
     */
        void interpolateGlobalFaceSolution(){
            MGV mixedGridView=mixedgrid_->leafGridView();
            MixedGridElementMapper mixedGridMapper(mixedGridView);

            const int numberOfFaces=/*this is hard coded simplex!*/3;
            PressureFaceVector pressureFaceVector(numberOfFaces,0.0);

            /*
             * loop over all elements and then over all edges
             */
            typedef typename MGV::template Codim< 0>::Iterator ElementIterator;
            ElementIterator eEndIt = mixedGridView.template end<0>();
            ElementIterator eStartIt = mixedGridView.template begin<0>();
            for (ElementIterator eIt = eStartIt; eIt != eEndIt; ++eIt){

                // run through all intersections with neighbors and boundary
                IntersectionIterator isEndIt = mixedGridView.iend(*eIt);
                for (IntersectionIterator isIt = mixedGridView.ibegin(*eIt); isIt != isEndIt; ++isIt)
                {
                    int indexInInside = isIt->indexInInside();
                    auto global= isIt->geometry().center();
                    pressureFaceVector[indexInInside] = this->evaluatePressure(global);
                }

                //get index
                auto index=mixedGridMapper.map(*eIt);
                pressureElementFaceVector_[index]=pressureFaceVector;
            }
        }

    /*
     * This is the mapping of the xfem solution onto an FEM grid.
     * It writes the matrix solution on a mixed,
     * equi-dimensional grid. The fracture solution is not incorporated and the fracture elements
     * should be deleted!
     */
    void interpolateGlobalSolution(){
        MGV mixedGridView=mixedgrid_->leafGridView();
        MixedGridVertexMapper mixedGridMapper(mixedGridView);

        pressureVertexVector_.clear();
        pressureVertexVector_.resize(mixedGridView.size(/*codim*/MGV::dimension));

        typedef typename MGV::template Codim< MGV::dimension>::Iterator VertexIterator;
        VertexIterator eEndIt = mixedGridView.template end<MGV::dimension>();
        VertexIterator eStartIt = mixedGridView.template begin<MGV::dimension>();
        for (VertexIterator vIt = eStartIt; vIt != eEndIt; ++vIt){
            auto global= vIt->geometry().center();
            auto pressure=this->evaluatePressure(global);
//            std::cout << "pressure: " << pressure << std::endl;
            //get index
            auto index=mixedGridMapper.map(*vIt);
            pressureVertexVector_[index]=pressure;
        }

        /*
         * velocity evaluation is cell centered
         */

    }



    void writeCCVTK(){
        MGV mixedGridView=mixedgrid_->leafGridView();

        outputName_+="_mixedGrid";
        Dune::VTKWriter<MGV> vtkwriter(mixedGridView,Dune::VTK::conforming);
        vtkwriter.addCellData(pressureCellVector_,"pressure");
        vtkwriter.addCellData(permeabilityVector_,"permeability");
        vtkwriter.write(outputName_.c_str(),Dune::VTK::ascii);
    }

    void writeVTK(){
        MGV mixedGridView=mixedgrid_->leafGridView();

        outputName_+="_mixedGrid";
        Dune::VTKWriter<MGV> vtkwriter(mixedGridView,Dune::VTK::conforming);
        vtkwriter.addVertexData(pressureVertexVector_,"pressure");
        vtkwriter.write(outputName_.c_str(),Dune::VTK::ascii);
    }


    /*
     * write a dgf with pressure solution on a new,
     * possibly mixed simplex-quadrilateral, grid
     * for coupling to DuMuX and transport simulation within IMPET.
     */
    void writeDGF(const bool addFacePressures=false , const int refinementLevel=0){

        MGV mixedGridView=mixedgrid_->leafGridView();
        const int paramnumber = ( addFacePressures ? 5 : 2);
        std::string dataFileName = "dgfExport";
        const bool cellWise = true;

        typedef typename std::vector<Scalar> DataInCell;
        DataInCell dataInCell(paramnumber);
        std::vector<DataInCell> data(pressureCellVector_.size());

        MixedGridElementMapper mixedGridMapper(mixedGridView);

        typedef typename MGV::template Codim< 0>::Iterator ElementIterator;
        ElementIterator eEndIt = mixedGridView.template end<0>();
        ElementIterator eStartIt = mixedGridView.template begin<0>();
        for (ElementIterator eIt = eStartIt; eIt != eEndIt; ++eIt){

            //get index
            auto index=mixedGridMapper.map(*eIt);

            //at position 0 pressure
            dataInCell[0]=pressureCellVector_[index];
            //at position 1 permeability
            dataInCell[1]=permeabilityVector_[index];

            if (addFacePressures){
                for (int faceIdx=0;faceIdx<(pressureElementFaceVector_[index]).size();++faceIdx){
                    dataInCell[2+faceIdx]=(pressureElementFaceVector_[index])[faceIdx];
                }
            }

            data[index]=dataInCell;
        }


        Dumux::exportToDGF(mixedGridView, data, paramnumber, dataFileName,cellWise);//first only pressure TODO later add permeabilities also!
    }




private:
    const MatrixSolutionType &matrixSolution_;
    MixedGrid &hostGrid_;
    const FractureGrid &fractureGrid_;
    typedef typename Dune::GridPtr<MixedGrid> MixedGridPointer;
    MixedGridPointer mixedgrid_;
    std::vector<Scalar> pressureCellVector_;
    std::vector<Scalar> pressureVertexVector_;
    typedef typename std::vector<Scalar> Velocity;
    std::vector<Velocity> velocityCellVector_;
    typedef typename Dune::MultipleCodimMultipleGeomTypeMapper<MGV,Dune::MCMGElementLayout> MixedGridElementMapper;
    typedef typename Dune::MultipleCodimMultipleGeomTypeMapper<MGV,Dune::MCMGVertexLayout> MixedGridVertexMapper;
//    template<int dim>
//    struct FaceLayout
//    {
//        bool contains (Dune::GeometryType gt)
//        {
//            return gt.dim() == dim-1;
//        }
//    };
//    typedef Dune::MultipleCodimMultipleGeomTypeMapper<MGV,FaceLayout> MixedGridFaceMapper;
//    MixedGridFaceMapper faceMapper_;
    typedef typename std::vector<Scalar> PressureFaceVector;
    typename std::vector< PressureFaceVector >pressureElementFaceVector_;
    OutputName outputName_;
    std::vector<Scalar> permeabilityVector_;

};//end class Solution
}//end namespace Dumux
#endif /* SOLUTION_HH_ */
