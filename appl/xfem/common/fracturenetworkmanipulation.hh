#ifndef FRACTURENETWORKMANIPULATION_HH_
#define FRACTURENETWORKMANIPULATION_HH_

namespace Dumux{
//template<typename MonolithicMatrix, typename MonolithicResidual, typename MatrixManipulationMap>
//MonolithicMatrix fractureNetworkManipulation(MonolithicMatrix &gMonolithic, MonolithicResidual &rMonolithic,
//        MatrixManipulationMap &matrixManipulationMap, const int offset=0 )
//{
//    Dumux::manipulateMatrix(gMonolithic, matrixManipulationMap,offset);
//    Dumux::manipulateResidual(rMonolithic, matrixManipulationMap,offset);
//}

template<typename MonolithicMatrix, typename MatrixManipulationMap>
MonolithicMatrix manipulateMatrix(MonolithicMatrix &gMonolithic, MatrixManipulationMap &matrixManipulationMap, const int offset=0 )
{
    typedef typename MonolithicMatrix::field_type Scalar;
    typedef Dune::FieldMatrix<Scalar,1,1> M;
    typedef typename Dune::BCRSMatrix<M> ISTLM;
    std::size_t gMonolithicNumberOfRows=gMonolithic.N();
    std::size_t gMonolithicNumberOfColumns=gMonolithic.M();
    MonolithicMatrix manipulatedM(gMonolithicNumberOfRows,gMonolithicNumberOfColumns,ISTLM::random);
    typename MatrixManipulationMap::iterator it;

    // initially set row size for each row
    int modifier=-2;
    //set the rowsize:
    //for the matrix and matrix-coupling part simply copy the matrix
    for (std::size_t j=0;j<offset;j++){//loop over rows of matrix block
        manipulatedM.setrowsize(j,gMonolithic.getrowsize(j));
    }
    //here the manipulation begins
    for (int j=offset;j<gMonolithicNumberOfRows;j++){//loop over rows
        //j is here the index
        //modifier is
        // -1 if j is an unmodified row
        // -3/-4 if j is a sum row with a 3-/4-crossing TODO -3/-4 is not used
        // larger than zero to indicate the row to which this is added
        it=(matrixManipulationMap.find(j-offset));
        modifier=( it!= matrixManipulationMap.end() && (*it).second>=0 ) ? ((*it).second+offset)  : -1 ;
        if (modifier<0) {//this row is either not in the map or a basic row (to which something is added)
            manipulatedM.setrowsize(j,gMonolithic.getrowsize(j));//set row size equal to row size in gMonolithic
        }
        else {//this will be an "empty line"
            /*
             * the added-to line size is incremented by the number of entris in the "empty line"
             * which are not yet in the added-to line
             */
            for (int k=0;k<gMonolithicNumberOfColumns;k++){
                if (gMonolithic.exists(j,k) &&  !gMonolithic.exists(modifier,k) ) { manipulatedM.incrementrowsize(modifier,1); }
            }
            //here it is assumed that the entries in matrixManipulationMap are actually sorted, starting with the lowest
            //so that the size of the sum Row is set before it gets incremented
            manipulatedM.setrowsize(j,2);
        }
    }//end loop over rows
        // finalize row setup phase
    manipulatedM.endrowsizes();

    // add column entries to rows
    //for the matrix and matrix-coupling part simply copy the matrix
    for (std::size_t j=0;j<offset;j++){//loop over rows of matrix block
        for (int k=0;k<gMonolithicNumberOfColumns;k++){//loop over columns in row j
            if (gMonolithic.exists(j,k)) manipulatedM.addindex(j,k);
        }
    }
    //here the manipulation begins
    for (int j=offset;j<gMonolithicNumberOfRows;j++){//loop over rows
        int numberOfEntries=0;
        it=(matrixManipulationMap.find(j-offset));
        modifier=( it!= matrixManipulationMap.end() && (*it).second>=0 ) ? ((*it).second+offset)  : -1 ;
        if (modifier<0)
            for (int k=0;k<gMonolithicNumberOfColumns;k++){//loop over columns in row j
                if (gMonolithic.exists(j,k)) manipulatedM.addindex(j,k);//this row is the same as in gMonolithic or a sum row
            }
        else {//this is an "empty" row
            manipulatedM.addindex(j,modifier);
            manipulatedM.addindex(j,j);
            for (int k=0;k<gMonolithicNumberOfColumns;k++){
                if (gMonolithic.exists(j,k) && !manipulatedM.exists(modifier,k)) {
                    numberOfEntries++;
                    manipulatedM.addindex(modifier,k);
                }
            }
        }
    }//end loop over rows
    // finalize column setup phase
    manipulatedM.endindices();

    // set entries using the random access operator
    //for the matrix and matrix-coupling part simply copy the matrix
    for (std::size_t j=0;j<offset;j++){//loop over rows of matrix block
        for (int k=0;k<gMonolithicNumberOfColumns;k++){//loop over columns in row j
            if (manipulatedM.exists(j,k)){
                assert(gMonolithic.exists(j,k));
                manipulatedM[j][k]=(gMonolithic)[j][k];
            }
        }
    }
    //here the manipulation begins
    for (int j=offset;j<gMonolithicNumberOfRows;j++){//loop over rows
        it=(matrixManipulationMap.find(j-offset));
        modifier=( it!= matrixManipulationMap.end() && (*it).second>=0 ) ? ((*it).second+offset)  : -1 ;
        if (modifier<0){
            for (int k=0;k<gMonolithicNumberOfColumns;k++){//loop over columns in row j
                if (manipulatedM.exists(j,k)){
                    if (gMonolithic.exists(j,k) ) manipulatedM[j][k]=(gMonolithic)[j][k];
                    else manipulatedM[j][k]=0.0;
                }
            }
        }
        else {//this is an "empty" row
            manipulatedM[j][modifier]=-1.0;
            manipulatedM[j][j]=1.0;
            for (int k=0;k<gMonolithicNumberOfColumns;k++){//loop over columns in row j
                if (gMonolithic.exists(j,k)){
                    M temp=manipulatedM[modifier][k];
                    temp+=(gMonolithic)[j][k];
                    manipulatedM[modifier][k]=temp;
                }
            }
        }
    }//end loop over rows

    return(manipulatedM);
}


template<typename MonolithicResidual, typename MatrixManipulationMap>
void manipulateResidual(MonolithicResidual &rMonolithic, MatrixManipulationMap &matrixManipulationMap, const int offset=0 )
{
    typename MatrixManipulationMap::iterator it;
    int modifier=-2;
    for (int j=offset;j<rMonolithic.N();j++){//loop over entries of r which are in the fracture part
        it=(matrixManipulationMap.find(j-offset));
        modifier=( it!= matrixManipulationMap.end() && (*it).second>=0 ) ? ((*it).second+offset)  : -1 ;
        if (modifier>0){//this is an "empty" row
            rMonolithic[modifier]+=rMonolithic[j];
            rMonolithic[j]=0;
        }
    }
}
}//end namespace Dumux
#endif /* FRACTURENETWORKMANIPULATION_HH_ */
