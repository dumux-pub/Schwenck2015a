#include <dune/common/array.hh>

namespace Dumux{

template<typename FV>
bool onBottom(const FV &x) {
    return (x[1]<-1.0+Dumux::equalityEpsilon<typename FV::value_type>());
}
template<typename FV>
bool onLeft(const FV &x) {
    return (x[0]<Dumux::equalityEpsilon<typename FV::value_type>());
}
template<typename FV>
bool onRight(const FV &x) {
    return (x[0]>1.6-Dumux::equalityEpsilon<typename FV::value_type>());
}
/*
 * in this case onTop is not precise
 * but catches all boundary segments y>0.1 which are not left or right
 */
template<typename FV>
bool onTop(const FV &x) {
	if ( !Dumux::onLeft(x) && !Dumux::onRight(x)
	&& x[1]>0.1-Dumux::equalityEpsilon<typename FV::value_type>() ){
		return (true);
	}
	return (false);
}

template<typename RF, typename FV>
RF hydrostatic(const FV &x){
    if ( Dumux::onTop(x) ||Dumux::onLeft(x)||Dumux::onRight(x) || Dumux::onBottom(x)){
        return(1.0+x[1]);
//    	return (2.0);
    }
    /*
     * this should give the linear interpolation of the boundary values
     * whatever this means for complex domains with complex Dirichlet value distributions
     */
    else{
        return( -1.0);
    }
}//end standardBC

}//end namespace Dumux


struct BCType : public Dune::PDELab::DirichletConstraintsParameters
{

    template<typename I>
    bool isDirichlet(const I &ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> &x) const
    {
        auto xg = ig.geometry().global(x);
        // no bc
        if (!ig.boundary()){
            Dune::dinfo << "At intersection centre: "<< xg << "  no bc" << std::endl;
            return (false); // no bc on subdomain interface
        }
        // Dirichlet
//        else if ( Dumux::onTop(xg) ||  Dumux::onBottom(xg)  ||  Dumux::onLeft(xg)  ||  Dumux::onRight(xg)  ){
        else if ( Dumux::onTop(xg) ){
            Dune::dinfo << "At intersection centre: "<< xg << " Dirichlet bc" << std::endl;
            return (true);
        }
        // Neumann
        return (false);
    }
    template<typename Scalar, typename I>
    Scalar neumannFlux(const I & ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
    {
//        auto xg = ig.geometry().global(x);
        assert (this->isNeumann(ig,x));
        return ( Scalar(0.0) );
    }

    template<typename I>
    bool isNeumann(const I &ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> &x) const
    {
//        auto xg = ig.geometry().global(x);
        if (!ig.boundary()) return (false); // no bc
        //returns true if no Dirichlet
        return ( !(this->isDirichlet(ig,x)) );//this assumes there are either Dirichlet or Neumann boundaries
    }
};

template<typename GB>
struct FractureBCType : public Dune::PDELab::DirichletConstraintsParameters
{
public:
    FractureBCType(const GB &gb): gb_(gb){};
private:
    template<typename FV>
    bool onBoundary(const FV &xg) const{
        return (gb_.onGlobalBoundary(xg));
    }
public:
    template<typename I>
    bool isDirichlet(const I & ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
    {
        auto xg = ig.geometry().global(x);
        if (!ig.boundary()){
            Dune::dinfo << xg << "  no bc" << std::endl;
//          std::cout << xg << "  no bc" << std::endl;
            return (false); // no bc on subdomain interface
        }
        else if (!this->onBoundary(xg)) return (false);
//        else if ( Dumux::onTop(xg) ||  Dumux::onBottom(xg)  ||  Dumux::onLeft(xg)  ||  Dumux::onRight(xg)  ){
        else if ( Dumux::onTop(xg) ){
            return (true);
        }
        return (false);//Neumann boundary or internal node
    }

    template<typename I>
    bool isNeumann(const I & ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
    {
        auto xg = ig.geometry().global(x);
        if (!ig.boundary()) return (false); // no bc
        else if (!this->onBoundary(xg)) {
            return (false);
        }
        //returns true if no Dirichlet
        return ( !(this->isDirichlet(ig,x)) );//this assumes there are either Dirichlet or Neumann boundaries
    }
    template<typename Scalar, typename I>
    Scalar neumannFlux(const I & ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
    {
//        auto xg = ig.geometry().global(x);
        assert (this->isNeumann(ig,x));
        return ( Scalar(0.0) );
    }
    template<typename I>
    bool isEnding(const I & ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
    {
        auto xg = ig.geometry().global(x);
        auto local = ig.geometryInInside().global(x);//element local (transformation from intersection local to element local)
        bool tripleNode=Dumux::arePointsEqual(ig.inside()->geometry().integrationElement(local),0.0);
//        std::cout << "xg: " << xg << "\t is B: "<<this->onBoundary(xg)<<"\t is D: " << this->isDirichlet(ig,x)<< "\t is N: "  <<this->isNeumann(ig,x)<<std::endl;
        if (!ig.boundary()){
            return (false); // no bc on subdomain interface
        }
        else if (tripleNode){//catch here the case of duplicated nodes due to alberta limiations of crossing handling
            return (false); // no bc on subdomain interface
        }

        else if (this->onBoundary(xg)) {
            return (false);
        }
        else if (!(this->isDirichlet(ig,x)) && !(this->isNeumann(ig,x)) ){
//            std::cout << "xg: " << xg << "\t is E!"<<std::endl;
            return (true);
        }
        return ( false );//returns false for all inner boundaries or Dirichlet or Neumann boundaries
    }
private:
    const GB &gb_;
};

/*
 * this struct returns always false for Dirichlet bc
 * and in addition always true for Neumann boundaries, i.e.,
 * there is always no flow for the enriched element boundaries.
 */

struct EmptyBCType : public Dune::PDELab::DirichletConstraintsParameters
{
    template<typename I>
    bool isDirichlet(const I & ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
    {
        Dune::dinfo << ig.geometry().center() << "  no bc" << std::endl;
        return (false); // no bc on subdomain interface
    }

    template<typename I>
    bool isNeumann(const I & ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
    {
        auto xg = ig.geometry().global(x);
        if (!ig.boundary()) return (false); // no bc
        //returns true if no Dirichlet
        return ( !(this->isDirichlet(ig,x)) );//this assumes there are either Dirichlet or Neumann boundaries
    }
};

/** \brief A function that defines Dirichlet boundary conditions AND
    its extension to the interior */
template<typename GV, typename Reference, typename RF>
class BCDirichlet : public Dune::PDELab::GridFunctionBase<Dune::PDELab::
        GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, BCDirichlet<GV,Reference,RF> > {

public:
    typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

    //! construct from grid view
    BCDirichlet (const GV &gv, const Reference &reference) : gv_(gv),reference_(reference) {}

    //! evaluate extended function on element
    inline void evaluate (const typename Traits::ElementType& e,
            const typename Traits::DomainType& xlocal, typename Traits::RangeType& y) const
    {
        const int dimworld = Traits::GridViewType::Grid::dimensionworld;
        typedef typename Traits::GridViewType::Grid::ctype ctype;
        Dune::FieldVector<ctype,dimworld> xg = e.geometry().global(xlocal);
        //check global coordinates and set the Dirichlet value (y=...)
        //if vertex is not on a boundary with type Dirichlet the value can be set but will NOT be used
        y = Dumux::hydrostatic<RF>(xg);
        return;
    }
    //! get a reference to the grid view
    inline const GV& getGridView () {return (gv_);}
private:
    const GV &gv_;
    const Reference &reference_;
};

template<typename GV, typename Reference, typename RF>
class BCDirichletFracture : public Dune::PDELab::GridFunctionBase<Dune::PDELab::
        GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, BCDirichletFracture<GV,Reference,RF> >
    {
public:
    typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

    //! construct from grid view
    BCDirichletFracture (const GV &gv, const Reference &reference) : gv_(gv),reference_(reference) {}

    //! evaluate extended function on element
    inline void evaluate (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal,
            typename Traits::RangeType& y) const
    {
        const int dimworld = Traits::GridViewType::Grid::dimensionworld;
        typedef typename Traits::GridViewType::Grid::ctype ctype;
        Dune::FieldVector<ctype,dimworld> xg = e.geometry().global(xlocal);

        //check global coordinates and set the Dirichlet value (y=...)
        //if vertex is not on a boundary with type Dirichlet the value can be set but will NOT be used
        if ( Dumux::onTop(xg) || Dumux::onLeft(xg) || Dumux::onRight(xg) || Dumux::onBottom(xg)){
            y=Dumux::hydrostatic<RF>(xg);
		std::cout << "fracture D boundary: " << y << std::endl;
        }
        else y=3.0;

        return;
    }
    //! get a reference to the grid view
    inline const GV& getGridView () {return (gv_);}
private:
    const GV &gv_;
    const Reference &reference_;
};

      /** \brief A function that defines Dirichlet boundary conditions AND
      its extension to the interior */
template<typename GV, typename RF>
class BCDirichletEnriched : public Dune::PDELab::GridFunctionBase<Dune::PDELab::
        GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, BCDirichletEnriched<GV,RF> >
{
    const GV& gv;
public:
    typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

    //! construct from grid view
    BCDirichletEnriched (const GV& gv_) : gv(gv_) {}

    //! evaluate extended function on element
    inline void evaluate (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal,
            typename Traits::RangeType& y) const
    {
        const int dimworld = Traits::GridViewType::Grid::dimensionworld;
        typedef typename Traits::GridViewType::Grid::ctype ctype;
        Dune::FieldVector<ctype,dimworld> xg = e.geometry().global(xlocal);
        //check global coordinates and set the Dirichlet value (y=...)
        //if vertex is not on a boundary with type Dirichlet the value can be set but will NOT be used
        if (Dumux::onTop(xg) || Dumux::onBottom(xg) || Dumux::onLeft(xg) || Dumux::onRight(xg) ) {
            y=Dumux::hydrostatic<RF>(xg);
        }
        else{
            y=0.0;
        }
        return;
    }

    //! get a reference to the grid view
    inline const GV& getGridView () {return (gv);}
};
