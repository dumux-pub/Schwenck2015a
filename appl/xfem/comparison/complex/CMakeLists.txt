add_input_file_links()

add_dumux_test(monocomplex monocomplex monocomplex.cc
  ${CMAKE_CURRENT_BINARY_DIR}/monocomplex)
add_dune_alberta_flags(monocomplex GRIDDIM 1 WORLDDIM 2)

install(FILES
        boundaryconditions.hh
        complex.art
        cube.dgf
        fracture_network.ods
        Makefile.am
        monocomplex.cc
        parameter.input
        RFGN01.art
        rfgn01.ods
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/appl/xfem/comparison/complex)
