#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <dumux/common/quad.hh>
//#include <quadmath.h>
//#include <math.h>
#include <iostream>
#include <boost/format.hpp>
#include <vector>
#include <map>
#include <string>
#include <typeinfo>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/static_assert.hh>
#include <dune/common/timer.hh>
#include <dune/common/parametertreeparser.hh>
//===============================================================
// dune-grid
//===============================================================
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/grid/io/file/dgfparser.hh>
#if HAVE_ALBERTA
#include <dune/grid/albertagrid.hh>
#endif
#if HAVE_UG
#include <dune/grid/uggrid.hh>
#endif
#if HAVE_ALUGRID
#include <dune/grid/alugrid.hh>
#endif

#include <dune/grid/multidomaingrid.hh>
#include <dune/grid/common/gridinfo.hh>
#include <dune/grid/geometrygrid.hh>
#include <dune/grid/common/mcmgmapper.hh>
//===============================================================
// dune-istl
//===============================================================
#include <dune/istl/bvector.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/io.hh>
#include <dune/istl/superlu.hh>
//===============================================================
// dune-pdelab
//===============================================================
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/finiteelementmap/q1fem.hh>
#include <dune/pdelab/finiteelementmap/conformingconstraints.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/subspace.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include <dune/pdelab/gridfunctionspace/genericdatahandle.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/backend/istlvectorbackend.hh>
#include <dune/pdelab/backend/istlmatrixbackend.hh>
#include <dune/pdelab/backend/istlsolverbackend.hh>
//===============================================================
// dune-multidomain
//===============================================================
#include <dune/pdelab/multidomain/multidomaingridfunctionspace.hh>
#include <dune/pdelab/multidomain/subproblemlocalfunctionspace.hh>
#include <dune/pdelab/multidomain/constraints.hh>
#include <dune/pdelab/multidomain/interpolate.hh>
#include <dune/pdelab/multidomain/gridoperator.hh>
#include <dune/pdelab/multidomain/subproblem.hh>
#include <dune/pdelab/multidomain/coupling.hh>
#include <dune/pdelab/multidomain/vtk.hh>
//===============================================================
// helper functions
//===============================================================
#include "../common/helperfunctions.hh"
#include "../common/fracturenetworkmanipulation.hh"
#include "analyticalsolution.hh"
//===============================================================
// boundary conditions, basis functions, soil structure
//===============================================================
#include "../common/globalboundary.hh"
#include "../common/referenceproperties.hh"
#include "soilanalytical.hh"
#include "../common/modifiedbasisfunction.hh"
#include "boundaryconditionsanalytical.hh"
//===============================================================
// local operator
//===============================================================
#include "matrixlocaloperatoranalytical.hh"
#include "matrixlocaloperatorenrichedanalytical.hh"
#include "fracturelocaloperatoranalytical.hh"
#include "../mono/couplinglocaloperatordarcy.hh"
#include "../mono/sizeofcouplinglop.hh"
//===============================================================
// Main program with grid setup
//===============================================================
#include "../common/artreader2d.hh"
#include "../common/gridcreator.hh"
#include "../common/gridcoupling.hh"
#include "matrixerror.hh"
#include "fractureerror.hh"
#include "main.hh"
