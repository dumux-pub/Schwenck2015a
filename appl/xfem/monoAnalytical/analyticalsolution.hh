#ifndef ANALYTICALSOLUTION_HH_
#define ANALYTICALSOLUTION_HH_

#include<math.h>
#include<dune/pdelab/common/function.hh>

template<typename GV, typename Soil, typename RF, typename SolutionCase>
class AnalyticalPressureSolutionMatrix : public Dune::PDELab::AnalyticGridFunctionBase<Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1>,AnalyticalPressureSolutionMatrix<GV,Soil,RF,SolutionCase> >
{
public:
    typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1> Traits;
    typedef Dune::PDELab::AnalyticGridFunctionBase<Traits,AnalyticalPressureSolutionMatrix<GV,Soil,RF,SolutionCase> > B;

    AnalyticalPressureSolutionMatrix (const GV& gv, const Soil &soil, const SolutionCase &solutionCase) : B(gv), soil_(soil), solutionCase_(solutionCase) {}

    // xg is the global coordinate vector of the integration point.
    inline void evaluateGlobal (const typename Traits::DomainType& xg, typename Traits::RangeType& y) const
    {
        auto K=soil_.intrinsicPermeability();
        RF k_m=K[0][0];
        RF k_f=soil_.kFT();
        RF a_f=soil_.fractureWidth();
        //because the fracture is horizontal the ratio is between the tangential fracture permeability and the xx-entry in K
        RF k=k_f/k_m;
        RF sqrta=std::sqrt(K[0][0]/K[1][1]);
        assert(Dumux::arePointsEqual(sqrta,1.0));
        if (solutionCase_==0){
            y= (k*std::cos(xg[0])*std::cosh(xg[1]*sqrta)+(1.0-k)*std::cos(xg[0])*std::cosh(a_f/(2.0*sqrta)));
        }
        else y= ( std::cos(xg[0])*std::cosh(xg[1]*sqrta) );
    }
private:
    Soil soil_;
    const SolutionCase &solutionCase_;
};


template<typename GV, typename Soil, typename RF, typename SolutionCase>
class AnalyticalVelocitySolutionMatrix : public Dune::PDELab::AnalyticGridFunctionBase<Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,2>,AnalyticalVelocitySolutionMatrix<GV,Soil,RF,SolutionCase> >
{
public:
    typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,2> Traits;
    typedef Dune::PDELab::AnalyticGridFunctionBase<Traits,AnalyticalVelocitySolutionMatrix<GV,Soil,RF,SolutionCase> > B;

    AnalyticalVelocitySolutionMatrix (const GV& gv, const Soil &soil, const SolutionCase &solutionCase) : B(gv), soil_(soil), solutionCase_(solutionCase) {}

    // xg is the global coordinate vector of the integration point.
    inline void evaluateGlobal (const typename Traits::DomainType& xg, typename Traits::RangeType& y) const
    {
        auto K=soil_.intrinsicPermeability();
        RF k_m=K[0][0];
        RF k_f=soil_.kFT();
        RF a_f=soil_.fractureWidth();
        //because the fracture is horizontal the ratio is between the tangential fracture permeability and the xx-entry in K
        RF k=k_f/k_m;
        RF sqrta=std::sqrt(K[0][0]/K[1][1]);
        assert(Dumux::arePointsEqual(sqrta,1.0));
        if (solutionCase_==0){
            y[0]= -k_m*(k*(-1.0)*std::sin(xg[0])*std::cosh(xg[1]) + (1.0-k)*(-1.0)*std::sin(xg[0])*std::cosh(a_f/(2.0*sqrta)));
            y[1]= -k_m*(k*std::cos(xg[0])*std::sinh(xg[1]));
        }
        else{
            y[0]=-k_m*( (-1.0)*std::sin(xg[0])*std::cosh(xg[1]*sqrta));
            y[1]=-k_m*( std::cos(xg[0])*std::sinh(xg[1]*sqrta));
        }

    }
private:
    Soil soil_;
    const SolutionCase &solutionCase_;
};

template<typename GV, typename Soil, typename RF, typename SolutionCase>
class AnalyticalPressureSolutionFracture :
    public Dune::PDELab::AnalyticGridFunctionBase<Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1>,AnalyticalPressureSolutionFracture<GV,Soil,RF,SolutionCase> >
{
public:
    typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1> Traits;
    typedef Dune::PDELab::AnalyticGridFunctionBase<Traits,AnalyticalPressureSolutionFracture<GV,Soil,RF,SolutionCase> > B;


    AnalyticalPressureSolutionFracture (const GV& gv, const Soil &soil, const SolutionCase &solutionCase) : B(gv), soil_(soil), solutionCase_(solutionCase) {}

    // xg is the global coordinate vector of the integration point.
    inline void evaluateGlobal (const typename Dune::FieldVector<typename Traits::DomainFieldType,GV::dimensionworld> &xg, typename Traits::RangeType& y) const //const typename Traits::DomainType& xg
    {
        auto K=soil_.intrinsicPermeability();
        RF k_m=K[0][0];
        RF k_f=soil_.kFT();
        //because the fracture is horizontal the ratio is between the tangential fracture permeability and the xx-entry in K
        RF k=k_f/k_m;
        RF sqrta=std::sqrt(K[0][0]/K[1][1]);
        assert(Dumux::arePointsEqual(sqrta,1.0));
        if (solutionCase_==0){
            y= std::cos(xg[0])*std::cosh(xg[1]*sqrta);
    //        y=std::cos(xg[0])*2.0*std::sinh(a_f/2.0)/a_f;//y integrated and averaged pressure
        }
        else{
            auto a_f=soil_.fractureWidth();
            y=(1.0/k)*std::cos(xg[0])*std::cosh(xg[1]) + (1.0-(1.0/k))*std::cos(xg[0])*std::cosh(a_f/(2.0*sqrta));
        }

    }
private:
    Soil soil_;
    const SolutionCase &solutionCase_;
};

template<typename GV, typename Soil, typename RF,typename SolutionCase>
class AnalyticalVelocitySolutionFracture :
    public Dune::PDELab::AnalyticGridFunctionBase<Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1>,AnalyticalVelocitySolutionFracture<GV,Soil,RF,SolutionCase> >
{
public:
    typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1> Traits;
    typedef Dune::PDELab::AnalyticGridFunctionBase<Traits,AnalyticalVelocitySolutionFracture<GV,Soil,RF,SolutionCase> > B;


    AnalyticalVelocitySolutionFracture (const GV& gv, const Soil &soil, const SolutionCase &solutionCase) : B(gv), soil_(soil), solutionCase_(solutionCase) {}

    // xg is the global coordinate vector of the integration point.
    inline void evaluateGlobal (const typename Dune::FieldVector<typename Traits::DomainFieldType,GV::dimensionworld> &xg, typename Traits::RangeType& y) const //const typename Traits::DomainType& xg
    {
        auto K=soil_.intrinsicPermeability();
        RF k_m=K[0][0];
        RF k_f=soil_.kFT();
        RF a_f=soil_.fractureWidth();
        //because the fracture is horizontal the ratio is between the tangential fracture permeability and the xx-entry in K
        RF k=k_f/k_m;
        RF sqrta=std::sqrt(K[0][0]/K[1][1]);
        assert(Dumux::arePointsEqual(sqrta,1.0));
        if(solutionCase_==0){
            y= k_f*std::sin(xg[0])*std::cosh(xg[1]);
        }
        else{
            y = -k_f*((-1.0/k)*std::sin(xg[0])*std::cosh(xg[1]) + (1.0-(1.0/k))*(-1.0)*std::sin(xg[0])*std::cosh(a_f/(2.0*sqrta)));
        }

    }
private:
    Soil soil_;
    const SolutionCase &solutionCase_;
};

#endif /* ANALYTICALSOLUTION_HH_ */
