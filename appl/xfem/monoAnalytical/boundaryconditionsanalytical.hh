#include "../common/boundaryconditions.hh"

struct BCTypeAnalytical : BCType
    //public Dune::PDELab::DirichletConstraintsParameters
{
    template<typename I>
    bool isDirichlet(const I & ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
    {
        auto xg = ig.geometry().global(x);
        if (!ig.boundary()){        // no bc
            Dune::dinfo << "At intersection centre: "<< xg << "  no bc" << std::endl;
            return (false); // no bc on subdomain interface
        }
        else //always Dirichlet
        {// Dirichlet
            Dune::dinfo << "At intersection centre: "<< xg << " Dirichlet bc" << std::endl;
            return (true);
        }
    }
};

template<typename GB>
struct FractureBCTypeAnalytical : FractureBCType<GB>
{
    FractureBCTypeAnalytical(const GB &gb): FractureBCType<GB>(gb), gb_(gb){};

    template<typename I>
    bool isDirichlet(const I & ig, const Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
    {
        auto xg = ig.geometry().global(x);
        if (!ig.boundary()){
            Dune::dinfo << xg << "  no bc" << std::endl;
//          std::cout << xg << "  no bc" << std::endl;
            return (false); // no bc on subdomain interface
        }
        else return (true);
    }
private:
    const GB &gb_;
};

/** \brief A function that defines Dirichlet boundary conditions AND
    its extension to the interior */
template<typename GV, typename RF, typename AnalyticalSolution>
class BCDirichletAnalytical :
    public Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, BCDirichletAnalytical<GV,RF,AnalyticalSolution> >

{

public:
    typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

    //! construct from grid view
    BCDirichletAnalytical (const GV &gv, const AnalyticalSolution &analyticalSolution) : gv_(gv),analyticalSolution_(analyticalSolution) {
    }

    //! evaluate extended function on element
    inline void evaluate (const typename Traits::ElementType& e,
            const typename Traits::DomainType& xlocal, typename Traits::RangeType& y) const
    {
        const int dimworld = Traits::GridViewType::Grid::dimensionworld;
        typedef typename Traits::GridViewType::Grid::ctype ctype;
        typedef typename Dune::FieldVector<ctype,dimworld> FV;
        FV xg = e.geometry().global(xlocal);
        /*
         * analytical solution as bc
         * k cos(x) cosh(y sqrt(a) ) + (1-k) cos(x) cosh( a_f / 2 sqrt(a) )
         *
         * It is required that the anisotropy ration is the same for matrix and fracture!
         * with a=1
         * k cos(x) cosh(y) + (1-k) cos(x) cosh(a_f /2)
         * with a_f the aperture of the fracture
         * k the heterogeneity ratio k_f/k_m
         * the domain here is [-1 1] x [-1 1] with the horizontal fracture crossing (0,0)
         */
        ctype	yMinMax=0.1;
        if ( (xg[0]<-1.0+1.0e-4) ||  (xg[0]>1.0-1.0e-4) || (xg[1]<-yMinMax+1.0e-4) || (xg[1]>yMinMax-1.0e-4) ){
            analyticalSolution_.evaluateGlobal(xg,y);
        }
        else{
            y=1.0;
//            //this is a "linear interpolation between all four Dirichlet boundaries
////            y= 0.25*( this->analyticalSolution(left) +this->analyticalSolution(right)
////                    +this->analyticalSolution(top)+this->analyticalSolution(bottom) );
        }
        return;
    }
    //! get a reference to the grid view
    inline const GV& getGridView () {return (gv_);}
private:
//    template<typename FV>
//    RF analyticalSolution(const FV &xglobal) const{
//        //        if (Dumux::arePointsEqual(xglobal[1],0.0))
//        //            return std::cos(xglobal[0])*std::cosh(a_f_/(2.0*sqrta_));//this should never be used because there will be no node at y=0.0
//        //        else
//        if (solutionCase_==0){
//            return (k_*std::cos(xglobal[0])*std::cosh(xglobal[1]*sqrta_)+(1-k_)*std::cos(xglobal[0])*std::cosh(a_f_/(2.0*sqrta_)));
//        }
//        return ( std::cos(xglobal[0])*std::cosh(xglobal[1]*sqrta_) );
//    }
//
//    RF analyticalSolution(const RF &xglobal, const RF &yglobal) const{
//        if (solutionCase_==0){
//            return (k_*std::cos(xglobal)*std::cosh(yglobal*sqrta_)+(1-k_)*std::cos(xglobal)*std::cosh(a_f_/(2.0*sqrta_)));
//        }
//        return ( std::cos(xglobal)*std::cosh(yglobal*sqrta_));
//    }

    const GV &gv_;
    const AnalyticalSolution &analyticalSolution_;

};

template<typename GV, typename RF, typename AnalyticalSolution>
class BCDirichletFractureAnalytical :
    public Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, BCDirichletFractureAnalytical<GV,RF,AnalyticalSolution> >
    {
public:
    typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

    //! construct from grid view
    BCDirichletFractureAnalytical (const GV &gv, const AnalyticalSolution &analyticalSolution) : gv_(gv),analyticalSolution_(analyticalSolution) {
    }

    //! evaluate extended function on element
    inline void evaluate (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal,
            typename Traits::RangeType& y) const
    {
        const int dimworld = Traits::GridViewType::Grid::dimensionworld;
        typedef typename Traits::GridViewType::Grid::ctype ctype;
        Dune::FieldVector<ctype,dimworld> xg = e.geometry().global(xlocal);

        /*
         * analytical solution as bc
         * cos(x) cosh( sqrt(a) y )
         * It is required that the anisotropy ration is the same for matrix and fracture!
         * with a=1
         * cos(x) cosh(y)
         * with a_f the aperture of the fracture
         * the domain here is [-1 1] x [-1 1] with the horizontal fracture crossing (0,0)
         */

//        if (Dumux::arePointsEqual(xg[0],-1.0) || Dumux::arePointsEqual(xg[0],1.0)){
            analyticalSolution_.evaluateGlobal(xg,y);
//        }
//        else {
////            y=std::cos(-1.0);//linear interpolation between the same values always gives a constant
////            y=std::cos(xg[0]);//correct solution to test the iterative solver
//            y=1.0;
//        }

        return;
    }

    //! get a reference to the grid view
    inline const GV& getGridView () {return (gv_);}
private:
    const GV &gv_;
    const AnalyticalSolution &analyticalSolution_;
};

      /** \brief A function that defines Dirichlet boundary conditions AND
      its extension to the interior */
template<typename GV, typename Soil, typename RF, typename AnalyticalSolution>
class BCDirichletEnrichedAnalytical :
public Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, BCDirichletEnrichedAnalytical<GV,Soil,RF,AnalyticalSolution> >
{
public:
    typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

    //! construct from grid view
    BCDirichletEnrichedAnalytical (const GV &gv, const Soil &soil, const AnalyticalSolution &analyticalSolution) :
        gv_(gv),soil_(soil),analyticalSolution_(analyticalSolution) {
        a_f_=soil_.fractureWidth();
    }

    //! evaluate extended function on element
    inline void evaluate (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal,//xlocal is the corner coordinate
            typename Traits::RangeType& y) const
    {
        const int dimworld = Traits::GridViewType::Grid::dimensionworld;
        typedef typename Traits::GridViewType::Grid::ctype ctype;
        typedef typename Dune::FieldVector<ctype,dimworld> FV;
        FV xg = e.geometry().global(xlocal);

        /*
         * analytical solution as bc
         * k cos(x) cosh(y sqrt(a) ) + (1-k) cos(x) cosh( a_f / 2 sqrt(a) )
         *
         * It is required that the anisotropy ratio is the same for matrix and fracture!
         * with a=1
         * k cos(x) cosh(y) + (1-k) cos(x) cosh(a_f /2)
         * with a_f the aperture of the fracture
         * k the heterogeneity ratio k_f/k_m
         * the domain here is [-1 1] x [-1 1] with the horizontal fracture crossing (0,0)
         */

        FV yPoint(xg);
        yPoint[1]=a_f_/2.0;
        typename Traits::RangeType p_y0(0.0);
        analyticalSolution_.evaluateGlobal(yPoint,p_y0);
        typename Traits::RangeType p_1(0.0);
        analyticalSolution_.evaluateGlobal(xg,p_1);
        RF hm=2.0*std::abs(xg[1]);
        RF pf=std::cos(xg[0]);//*std::cosh(0.0)==1.0
//        if (xg[0]<-1.0+1.0e-6 || xg[0]>1.0-1.0e-6) {
//            analyticalSolution_.evaluateGlobal(xg,y);
//            y*=-1.0;
//            y+=2.0*pf;
            y=(p_y0 -(0.5+ (a_f_/(2.0*hm) ))*p_1)/(0.5- (a_f_/(2.0*hm) ));

//        }
//        else{
//            y=0.0;
//        }

        return;
    }

    //! get a reference to the grid view
    inline const GV& getGridView () {return (gv_);}
private:

    const GV &gv_;
    const Soil &soil_;
    const AnalyticalSolution &analyticalSolution_;

    //soil parameter
    RF a_f_;
};
