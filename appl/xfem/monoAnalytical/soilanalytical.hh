#ifndef SOILANALYTICAL_HH_
#define SOILANALYTICAL_HH_

namespace Dumux{
template<typename ParameterTree, typename ReferenceProperties, typename GridPtrFracture, typename GridPtrMatrix>
class SoilAnalytical{
private:
    static const int dimWorld_=GridPtrFracture::value_type::dimensionworld;
    typedef typename ReferenceProperties::RF RF;
    typedef typename GridPtrMatrix::value_type::ctype ctype;
    typedef Dune::FieldVector<ctype,dimWorld_> FV;
    typedef Dune::FieldMatrix<RF,dimWorld_,dimWorld_> FM;
    //TODO: HACK to get sizeType
    typedef int sizeType;
    //Hack end
public:
SoilAnalytical(const ParameterTree &inputParameters, const ReferenceProperties &reference, GridPtrFracture *gridPtrFracture, GridPtrMatrix *gridPtrMatrix)
: gridPtrFracture_(gridPtrFracture), gridPtrMatrix_(gridPtrMatrix), K12_(0.0)
{
    /*
     * set soil and fracture default parameters
     * They are usually overwritten in the respective functions by the value of the dgf file parameters.
     * fracture width and permeability: alpha_f=2*K_f,n / d
     */
    //
    KFT_= (inputParameters.template get<RF>("Soil.KFN") ) /reference.permeability();
    KFN_= (inputParameters.template get<RF>("Soil.KFT") ) /reference.permeability();
    d_= (inputParameters.template get<RF>("Soil.aperture") ) /reference.length();
    xi_=inputParameters.template get<RF>("Soil.averageXi");//fumble parameter 0.5 < xi <= 1.0

    //    RF k=1.0e0;//k=kf/km=kf_t/km[0][0]=kf_n/km[1][1]
    //    RF anisotropyRatio=KFT_/KFN_;
    //************************************************************************************************************
    // K: dimensionless matrix, normally diagonal. Always symmetric? K= k_intrinsic/mu * rho*u_infty/L; with rhs dimensionful
    // K is elementwise defined and does not change for each quadrature point as long as rock is the same on every side of the fracture.
    RF k= (inputParameters.template get<RF>("Soil.KM") ) /reference.permeability();
    for (sizeType i=0; i<dimWorld_; i++){
        for (sizeType j=0; j<dimWorld_; j++){
            K_[i][j]= (i==j) ? (k) : 0.0;
        }
    }

    std::cout << "KFT: " << this->kFT() << " KFN: " << this->kFN() <<  " a: "
            << this->fractureWidth() <<  " alphaF: " << this->alphaF() <<  std::endl;
    std::cout << " KM: " << this->intrinsicPermeability() << std::endl;
//    K_[1][1]=KFN_/k;
//  K_[1][1]=1.0;
// end filling K ********************************************************************************************

}//end constructor

FM intrinsicPermeability() const {
    return (K_);
}//end intrinsicPermeability function


//TODO in general improve the default output value. at the moment it does not
//always return the initialization value but the value of the last tested entity

/*
 * Tensor permeability function for the matrix.
 * Depending on the number of given values in the dgf file
 * the corresponding default values are overwritten.
 */
template<typename Entity>
FM intrinsicPermeability(const Entity &entity) const {
    return (K_);
}//end intrinsicPermeability function

/*
 * Tensor permeability function for the crossing.
 * Depending on the number of given values in the dgf file
 * the corresponding default values are overwritten.
 */
template<typename Entity>
FM intrinsicPermeabilityCrossing(const Entity &entity) const {
    return (K_);
}//end intrinsicPermeabilityCrossing function


RF xi() const {
    return (xi_);
}//end xi function

template<typename Entity>
RF alphaF(const Entity &entity) const {
    //a large alphaF means high permeability normal through the fracture and small fracture width
    RF alphaF_=2.0*this->kFN()/this->fractureWidth();
    return (alphaF_);
}//end alphaF function

RF alphaF() const {
    //a large alphaF means high permeability normal through the fracture and small fracture width
    RF alphaF_=2.0*this->kFN()/this->fractureWidth();
    return (alphaF_);
}//end alphaF function

template<typename Entity>
RF kFT(const Entity &entity) const {
    return (KFT_);
}//end kFT function

RF kFT() const {
    return (KFT_);
}//end kFT function

template<typename Entity>
RF kFN(const Entity &entity) const {
    return (KFN_);
}//end kfN function

RF kFN() const {
    return (KFN_);
}//end kfN function

template<typename Entity>
RF fractureWidth(const Entity &entity) const {
    return (d_);
}//end fractureWidth function

RF fractureWidth() const {
    return (d_);
}//end fractureWidth function


    template<typename Entity>
    RF angle(const Entity &entity) const {
        typedef typename GridPtrFracture::value_type::Traits::template Codim<0>::EntityPointer ElementPointer;
        ElementPointer entityPtr(entity);
        while (entityPtr->hasFather()){
            entityPtr=entityPtr->father();
        }
        assert( (*gridPtrFracture_).nofParameters(*entityPtr) >=8);
        return ((*gridPtrFracture_).parameters(*entityPtr)[7]);
    }//end angle function

    template<typename Entity>
    FV ti(const Entity &entity) const {
        typedef typename GridPtrFracture::value_type::Traits::template Codim<0>::EntityPointer ElementPointer;
        ElementPointer entityPtr(entity);
        while (entityPtr->hasFather()){
            entityPtr=entityPtr->father();
        }
        FV ti(0.0);
        assert ( (*gridPtrFracture_).nofParameters(*entityPtr) >=7);
        ti[0]=(*gridPtrFracture_).parameters(*entityPtr)[5];
        ti[1]=(*gridPtrFracture_).parameters(*entityPtr)[6];
        return (ti);
    }//end ti function

    template<typename Entity>
    RF li(const Entity &entity) const {
        typedef typename GridPtrFracture::value_type::Traits::template Codim<0>::EntityPointer ElementPointer;
        ElementPointer entityPtr(entity);
        while (entityPtr->hasFather()){
            entityPtr=entityPtr->father();
        }
        assert ( (*gridPtrFracture_).nofParameters(*entityPtr) >=5);
        return ((*gridPtrFracture_).parameters(*entityPtr)[4]);
    }//end li function


private:
    GridPtrFracture *gridPtrFracture_;
    GridPtrMatrix *gridPtrMatrix_;
    RF KFN_;
    RF KFT_;
    RF K12_;
    RF d_;
    FM K_;
    RF xi_;//fumble parameter 0.5 < xi <= 1.0

};//end class
}//end namespace dumux

#endif /* SOILANALYTICAL_HH_ */
