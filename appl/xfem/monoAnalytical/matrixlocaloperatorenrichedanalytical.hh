#ifndef MATRIXLOCALOPERATORENRICHEDANALYTICAL_HH_
#define MATRIXLOCALOPERATORENRICHEDANALYTICAL_HH_

/*
 * this is the basic local operator which defines the integration pattern.
 * it is used for every xfem element.
 * it should only access to the basic gfs.
 */

template <typename B, typename MultiDomainGrid, typename FractureGrid, typename IntersectionPointsMap, typename CouplingMap, typename Soil, typename SolutionCase>
class MatrixLocalOperatorEnrichedAnalytical :
  public Dune::PDELab::NumericalJacobianApplyVolume<MatrixLocalOperatorEnrichedAnalytical<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil,SolutionCase> >,
  public Dune::PDELab::NumericalJacobianVolume<MatrixLocalOperatorEnrichedAnalytical<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil,SolutionCase> >,
  public Dune::PDELab::NumericalJacobianApplyBoundary<MatrixLocalOperatorEnrichedAnalytical<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil,SolutionCase> >,
  public Dune::PDELab::NumericalJacobianBoundary<MatrixLocalOperatorEnrichedAnalytical<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil,SolutionCase> >,
//  public Dune::PDELab::JacobianBasedAlphaVolume<MatrixLocalOperatorEnrichedAnalytical<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil,SolutionCase> >,
//  public Dune::PDELab::JacobianBasedAlphaBoundary<MatrixLocalOperatorEnrichedAnalytical<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil,SolutionCase> >,
  public Dune::PDELab::FullVolumePattern,
  public Dune::PDELab::LocalOperatorDefaultFlags

  {
    typedef typename MultiDomainGrid::ctype ctype;
public:
  // pattern assembly flags
  enum { doPatternVolume = true };
//  enum { doPatternBoundary = true };//TODO does not work

  // residual assembly flags
  enum { doAlphaVolume = true };
  enum { doAlphaBoundary  = false };//TODO only homogeneous Neumann bc are possible!

  MatrixLocalOperatorEnrichedAnalytical (const B &b,const MultiDomainGrid &multiDomainGrid, const FractureGrid &fractureGrid, IntersectionPointsMap &intersectionPointsMap,
          CouplingMap &couplingMap, const Soil &soil, const SolutionCase &solutionCase, unsigned int intorder=3)
    :
        Dune::PDELab::NumericalJacobianApplyVolume<MatrixLocalOperatorEnrichedAnalytical<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil,SolutionCase> >(1e-1),// takes as argument the epsilon for the evaluation of numerical Jacobian
        Dune::PDELab::NumericalJacobianVolume<MatrixLocalOperatorEnrichedAnalytical<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil,SolutionCase> >(1e-1),
        Dune::PDELab::NumericalJacobianApplyBoundary<MatrixLocalOperatorEnrichedAnalytical<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil,SolutionCase> >(1e-1),
        Dune::PDELab::NumericalJacobianBoundary<MatrixLocalOperatorEnrichedAnalytical<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil,SolutionCase> >(1e-1),
        b_(b),
        multiDomainGrid_(multiDomainGrid),
        fractureGrid_(fractureGrid),
        intersectionPointsMapLocal_(intersectionPointsMap),
        couplingMap_(couplingMap),
        soil_(soil),
        xi_(soil_.xi()),
        solutionCase_(solutionCase),
        intorder_(intorder)
  {}

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {
      /*
       * here it is assumed that the LFSU is a powergridfunctionspace,
       * i.e., all individual function spaces are of the same order!
       * domain and range field type (assume both components have same Scalar)
       */
      typedef typename LFSU::template Child<0>::Type LGFSChild; //always child 0 is used for type extraction even if child zero is not "present" at this element
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::DomainType FV;
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::RangeFieldType Scalar;
      typedef typename LGFSChild::Traits::SizeType sizeType;


      typedef typename LFSU::template Child<0>::Type LGFSChild0;
      typedef typename LFSU::template Child<1>::Type LGFSChild1;
      typedef typename LFSU::template Child<2>::Type LGFSChild2;
      typedef typename LFSU::template Child<3>::Type LGFSChild3;
      typedef typename LFSU::template Child<4>::Type LGFSChild4;
      typedef typename LFSU::template Child<5>::Type LGFSChild5;
      const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();
      const LGFSChild1 &lgfsChild1 = lfsu.template child<1>();
      const LGFSChild2 &lgfsChild2 = lfsu.template child<2>();
      const LGFSChild3 &lgfsChild3 = lfsu.template child<3>();
      const LGFSChild4 &lgfsChild4 = lfsu.template child<4>();
      const LGFSChild5 &lgfsChild5 = lfsu.template child<5>();

    //get element Id
    typedef typename MultiDomainGrid::Traits::GlobalIdSet::IdType IdType;
    const IdType elementId =multiDomainGrid_.globalIdSet().id(eg.entity());

    typedef typename IntersectionPointsMap::mapped_type IntersectionPoints;
    IntersectionPoints intersectionPoints=(((*intersectionPointsMapLocal_.find(elementId)).second));
    sizeType children=(lgfsChild0.size()+lgfsChild1.size()+lgfsChild2.size()+lgfsChild3.size()+lgfsChild4.size()+lgfsChild5.size())/4;
    assert(children<6);
    if (children>2) --children;

    typedef typename std::vector<FV> PointsNotOnCorners;
    PointsNotOnCorners pointsNotOnCorners(children);//TODO check if this is true for cuts through a corner!
    std::vector<FV> xfMiddle;
    xfMiddle.clear();
    FV check(-1000.0);//check means no entry
    xfMiddle.push_back(intersectionPoints[4].intersection);

    /*
     * lmax is 1 for ending fractures, 2 for single fractures, 3 for 3-crossings and 4 for 4-crossings.
     * Two not-crossing fractures have an lmax of 2.
     */
//    int lmax=pointsNotOnCorners.size();
    for (int i=0;i<pointsNotOnCorners.size();i++){
        pointsNotOnCorners[i]=(intersectionPoints[i].intersection);
    }
    if (Dumux::arePointsEqual(intersectionPoints[1].intersection,check) ){//this indicates an ending fracture
        pointsNotOnCorners[1]=(intersectionPoints[5].intersection);
//        lmax=1;
    }
    else if( !Dumux::arePointsEqual(intersectionPoints[5].intersection,check) ){//this indicates two not-crossing fractures
        xfMiddle.push_back(intersectionPoints[5].intersection);
        std::cout << "xfMiddle.size(): " << xfMiddle.size() <<std::endl;
        /*
         * there are 4 points on the boundary of the element,
         * but there are also two centre points, so that the for each xfMiddle there are only two pointsNotOnCorners.
         */
//        lmax=2;
    }

    //create object which modifies the basis and test functions correctly
    typedef Dumux::modifiedBasisFunction<MultiDomainGrid, typename EG::Entity, IntersectionPointsMap, LFSU> MBF;
    const MBF mbf(multiDomainGrid_, eg.entity(), intersectionPointsMapLocal_, lfsu);

    typedef typename std::vector<FV> SortedPoints;
    /*
     * for ending fractures the sub-triangulation is the same as for one single not bending fracture through the element
     * the fracture is extrapolated to the end of the element and a virtual exit point is determined
     * this happens in the gridCoupling and in line 62,
     * so that the vector pointsNotOnCorners does look the same as in the single not bending fracture case
     */
    SortedPoints sortedPoints;
    sortedPoints=Dumux::sortPoints<FV>(pointsNotOnCorners, eg);
    typedef Dune::MultiLinearGeometry<ctype,dim_,dimw_> ElementGeometry;
    typedef Dune::GeometryType::BasicType BasicType;
    const BasicType simplex = Dune::GeometryType::simplex;
    const GT subSimplex(simplex,dimw_);
    std::vector<FV> listOfCornerCoordinates(3);//this is a vector which stores all corners of the used subtriangle
    /*
     * this scheme is based on the DUNE node ordering as defined for the generic reference element cube
     * create subtriangles depending on the fracture geoemtry
     * for straight, bending and ending fractures always 6 subtriangles are generated
     * for 3-crossings 7 subtriangles are generated
     * for 4-crossings 8 subtriangles are generated
     * and for two not-crossing fractures 10 subtriangles are generated
     */
    if (xfMiddle.size()==1){
        listOfCornerCoordinates[0]=eg.geometry().global(xfMiddle[0]);
        for (int i=0;i<4+children;i++){
            listOfCornerCoordinates[1]=eg.geometry().global(sortedPoints[i]);
            listOfCornerCoordinates[2]= (i<3+children ? eg.geometry().global(sortedPoints[i+1]) : eg.geometry().global(sortedPoints[0]) );
            ElementGeometry subElement(subSimplex,listOfCornerCoordinates);
//            std::cout << "subelement: " << listOfCornerCoordinates[0] <<"\t" << listOfCornerCoordinates[1] <<"\t" << listOfCornerCoordinates[2]<<std::endl;
            //accumulate the residuals for the matrix part of the element (without coupling terms)
            evaluateMatrixResidual(eg, lfsu, mbf, x, r, subElement);
            /*
             * accumulate the additional source/sink term for the analytical solution
             * only on the real matrix area therefore
             * change entries 1,2 in listOfCornerCoordinates
             */
            //            std::cout << "old: " << listOfCornerCoordinates[0] <<"\t"<<
            //            		listOfCornerCoordinates[1] << "\t"<<
            //            		listOfCornerCoordinates[2] << std::endl;
            Scalar a_f=soil_.fractureWidth();
            bool sourceInThisElement=true;
            for (int index=1;index<=2;++index){
                const int indexPlusOne= (index==1  ? 2 : 1);
                /*
                 * For the analytical case the element is always divided into 6 simplex subelements.
                 * For the subelements which share an edge with the fracture the center node and the fracture node
                 * have to be moved by aperture/2 towards the corner node.
                 * The subelements which have to corner nodes get only the center node moved towards the corner nodes.
                 */
                if (!Dumux::equalToCorner( eg.geometry().global(listOfCornerCoordinates[index]) )){
                    if (Dumux::absDistance(listOfCornerCoordinates[index],
                            listOfCornerCoordinates[indexPlusOne])<a_f/2.0){
                        std::cout << "WARNING !!!!!!!!! \n aperture > h_matrix!!!"<<std::endl;
                        sourceInThisElement=false;
                        continue;
                    }
                    auto tempMiddleCornerPlus=(listOfCornerCoordinates[index]);
                    auto tempMiddleCornerMinus=(listOfCornerCoordinates[index]);
                    tempMiddleCornerPlus[1]+=a_f/2.0;
                    tempMiddleCornerMinus[1]-=a_f/2.0;
                    auto tempCenterPlus=(listOfCornerCoordinates[0]);
                    auto tempCenterMinus=(listOfCornerCoordinates[0]);
                    tempCenterPlus[1]+=a_f/2.0;
                    tempCenterMinus[1]-=a_f/2.0;
                    if (Dumux::absDistance(tempMiddleCornerPlus,(listOfCornerCoordinates[indexPlusOne]))<
                            Dumux::absDistance(tempMiddleCornerMinus,(listOfCornerCoordinates[indexPlusOne])) ){
                        listOfCornerCoordinates[index]=tempMiddleCornerPlus;
                        listOfCornerCoordinates[0]=tempCenterPlus;
                    }
                    else{
                        listOfCornerCoordinates[index]=tempMiddleCornerMinus;
                        listOfCornerCoordinates[0]=tempCenterMinus;
                    }
                }//end fracture element
                //center element
                else if(Dumux::equalToCorner( eg.geometry().global(listOfCornerCoordinates[index]) ) ||
                        Dumux::equalToCorner( eg.geometry().global(listOfCornerCoordinates[indexPlusOne]) ) )
                {
                    auto tempCenterPlus=(listOfCornerCoordinates[0]);
                    auto tempCenterMinus=(listOfCornerCoordinates[0]);
                    tempCenterPlus[1]+=a_f/2.0;
                    tempCenterMinus[1]-=a_f/2.0;
                    auto tempCornerMiddle(listOfCornerCoordinates[indexPlusOne]);
                    tempCornerMiddle+=listOfCornerCoordinates[index];
                    tempCornerMiddle/=2.0;
                    if ( Dumux::absDistance(tempCenterPlus, tempCornerMiddle) <
                            Dumux::absDistance(tempCenterMinus,tempCornerMiddle) ){
                        listOfCornerCoordinates[0]=tempCenterPlus;
                    }
                    else{
                        listOfCornerCoordinates[0]=tempCenterMinus;
                    }
                }//end center element
            }//end loop over index
//            std::cout << "new: " << listOfCornerCoordinates[0] <<"\t"<<
//            		listOfCornerCoordinates[1] << "\t"<<
//            		listOfCornerCoordinates[2] << std::endl;
            if (sourceInThisElement){
                ElementGeometry subElementSource(subSimplex,listOfCornerCoordinates);
                evaluateMatrixResidualSource(eg, lfsu, mbf, x, r, subElementSource);
            }
            //reset the center to the original center
            listOfCornerCoordinates[0]=eg.geometry().global(xfMiddle[0]);

        }//end loop over all subelements
    }//end if for all cases except two-not crossing fractures

    else if (xfMiddle.size()==2){//handles the special case of two not-crossing fractures
        assert(xfMiddle.size()!=2);//TODO adapt the algorithm for two parallel fractures: there are two xfMiddle now!
        listOfCornerCoordinates[0]=eg.geometry().global(xfMiddle[0]);
        for (int i=0;i<4+children;i++){
            listOfCornerCoordinates[1]=eg.geometry().global(sortedPoints[i]);
            listOfCornerCoordinates[2]= (i<3+children ? eg.geometry().global(sortedPoints[i+1]) : eg.geometry().global(sortedPoints[0]) );
            ElementGeometry subElement(subSimplex,listOfCornerCoordinates);

            //accumulate the residuals for the matrix part of the element (without coupling terms)
            evaluateMatrixResidual(eg, lfsu, mbf, x, r, subElement);
	    /*
	     * accumulate the additional source/sink term for the analytical solution
	     * only on the real matrix area
	     */

            evaluateMatrixResidualSource(eg, lfsu, mbf, x, r, subElement);

        }//end loop over all subelements
    }//end if for the special case of two-not crossing fractures
    assert(xfMiddle.size()<=2);

//create subelements of codimension one depending on the fracture geometry
    typedef typename CouplingMap::iterator CouplingMapIterator;
    CouplingMapIterator cmit=couplingMap_.find(elementId);
    assert(cmit != couplingMap_.end());

    typedef typename FractureGrid::Traits::template Codim<0>::EntitySeed FGES;
    typedef std::vector<FGES> VectorOfEntitySeeds;
    typedef typename VectorOfEntitySeeds::iterator VectorOfEntitySeedsIterator;

    typedef typename FractureGrid::LeafGridView FGV;
    typedef typename FGV::IntersectionIterator FractureIntersectionIterator;
    FGV fgv=fractureGrid_.leafView();
    typedef typename MultiDomainGrid::ctype ctype;
    const int worlddim=MultiDomainGrid::dimensionworld;
    typedef typename Dune::ReferenceElements<ctype,worlddim> GRE;
    typedef Dune::GeometryType GT;
    GT matrixElementGeometryType =eg.geometry().type();

typedef Dune::MultiLinearGeometry<ctype,dimw_-1,dimw_> ElementGeometryFracture;
const GT subSimplexFracture(simplex,dimw_-1);
std::vector<FV> listOfCornerCoordinatesFracture(2);
//iterate over all fracture subelements
VectorOfEntitySeeds vectorOfEntitySeeds=(((*cmit).second));//FractureNodeIndexType
for (VectorOfEntitySeedsIterator esIt=vectorOfEntitySeeds.begin(); esIt!=vectorOfEntitySeeds.end();esIt++ ){
    const Scalar alphaF=this->template alphaF<Scalar>(*(fractureGrid_.entityPointer(*esIt)));
    FractureIntersectionIterator isItEnd = fgv.iend( *(fractureGrid_.entityPointer(*esIt)) );
    int i=0;
    for (FractureIntersectionIterator isIt = fgv.ibegin( *(fractureGrid_.entityPointer(*esIt)) ); isIt != isItEnd; ++isIt){
        listOfCornerCoordinatesFracture[i]= isIt->geometry().center();
        i++;
    }//end loop over fracture nodes

    /*
     * if the fracture element node is within this matrix element, then use it as node for the integration subelement.
     * Otherwise use the intersection point with the element boundary.
     */
    for (int i=0;i<2;i++){
        if ( !GRE::general(matrixElementGeometryType).checkInside( eg.geometry().local( listOfCornerCoordinatesFracture[i] ) ) ){
            Scalar distanceOnLine=1.0e6;
            FV closestPoint(check);
            for (int j=0;j<4;j++){//index 4 and 5 only store the midpoints/virtual exit points
                /*
                 * if there is an empty entry there are no more relevant entries after that
                 * for ending fractures the projected exit is not to be used!
                 */
                if (Dumux::arePointsEqual(intersectionPoints[j].intersection,check)){
                    break;
                }
                else if( Dumux::pointOnLine(eg.geometry().local(listOfCornerCoordinatesFracture[0]),
                        eg.geometry().local(listOfCornerCoordinatesFracture[1]),
                        intersectionPoints[j].intersection) //make the comparison in local coordinates
                ) {
                    /*
                     * make sure that always the intersectionPoint closest to the old fracture node is chosen
                     */
                    if (Dumux::arePointsEqual(closestPoint,check) ) {
                        closestPoint=eg.geometry().global(intersectionPoints[j].intersection);//but store the global coordinates
                        //clean up closest Point zeros
                        for (int indexK=0;indexK<2;indexK++){
                            if (Dumux::arePointsEqual(closestPoint[indexK],0.0)) closestPoint[indexK]=0.0;
                        }
                        distanceOnLine=Dumux::absDistance(listOfCornerCoordinatesFracture[i],closestPoint);
                    }
                    else if (Dumux::absDistance(listOfCornerCoordinatesFracture[i],eg.geometry().global(intersectionPoints[j].intersection)) < distanceOnLine){
                        //if the distance to the second intersectionPoint is smaller than to the first, replace the closestPoint by this intersectionPoint
                        closestPoint=eg.geometry().global(intersectionPoints[j].intersection);
                    }
                }
            }//end loop over intersectionPoints
            assert(!Dumux::arePointsEqual(closestPoint,check));
            listOfCornerCoordinatesFracture[i] = closestPoint;
        }//end if one fracture node does not lie within this matrix element
    }//end loop over fracture nodes
    //every subelement is defined by its corners
    if (Dumux::arePointsEqual(listOfCornerCoordinatesFracture[0],listOfCornerCoordinatesFracture[1])) {continue;}
    ElementGeometryFracture subElementFracture(subSimplexFracture,listOfCornerCoordinatesFracture);
    evaluateMatrixResidualCouplingTerm(eg, lfsu, mbf, x, alphaF, r, subElementFracture);
}//end loop over fracture entities connected to this matrix element

  }//end alpha volume


  template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
  void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv,
                        M& m) const
  {

      /*
       * here it is assumed that the LFSU is a powergridfunctionspace,
       * i.e., all individual function spaces are of the same order!
       * domain and range field type (assume both components have same Scalar)
       */
      typedef typename LFSU::template Child<0>::Type LGFSChild; //always child 0 is used for type extraction even if child zero is not "present" at this element
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::DomainType FV;
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::RangeFieldType Scalar;
      typedef typename LGFSChild::Traits::SizeType sizeType;



      typedef typename LFSU::template Child<0>::Type LGFSChild0;
      typedef typename LFSU::template Child<1>::Type LGFSChild1;
      typedef typename LFSU::template Child<2>::Type LGFSChild2;
      typedef typename LFSU::template Child<3>::Type LGFSChild3;
      typedef typename LFSU::template Child<4>::Type LGFSChild4;
      typedef typename LFSU::template Child<5>::Type LGFSChild5;
      const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();
      const LGFSChild1 &lgfsChild1 = lfsu.template child<1>();
      const LGFSChild2 &lgfsChild2 = lfsu.template child<2>();
      const LGFSChild3 &lgfsChild3 = lfsu.template child<3>();
      const LGFSChild4 &lgfsChild4 = lfsu.template child<4>();
      const LGFSChild5 &lgfsChild5 = lfsu.template child<5>();

    //get element Id
    typedef typename MultiDomainGrid::Traits::GlobalIdSet::IdType IdType;
    const IdType elementId =multiDomainGrid_.globalIdSet().id(eg.entity());

    typedef typename IntersectionPointsMap::mapped_type IntersectionPoints;
    IntersectionPoints intersectionPoints=(((*intersectionPointsMapLocal_.find(elementId)).second));
    sizeType children=(lgfsChild0.size()+lgfsChild1.size()+lgfsChild2.size()+lgfsChild3.size()+lgfsChild4.size()+lgfsChild5.size())/4;
    assert(children<6);
    if (children>2) --children;

    typedef typename std::vector<FV> PointsNotOnCorners;
    PointsNotOnCorners pointsNotOnCorners(children);//TODO check if this is true for cuts through a corner!
    std::vector<FV> xfMiddle;
    xfMiddle.clear();
    FV check(-1000.0);//check means no entry
    xfMiddle.push_back(intersectionPoints[4].intersection);

    /*
     * lmax is 1 for ending fractures, 2 for single fractures, 3 for 3-crossings and 4 for 4-crossings.
     * Two not-crossing fractures have an lmax of 2.
     */
//    int lmax=pointsNotOnCorners.size();
    for (int i=0;i<pointsNotOnCorners.size();i++){
        pointsNotOnCorners[i]=(intersectionPoints[i].intersection);
    }
    if (Dumux::arePointsEqual(intersectionPoints[1].intersection,check) ){//this indicates an ending fracture
        pointsNotOnCorners[1]=(intersectionPoints[5].intersection);
//        lmax=1;
    }
    else if( !Dumux::arePointsEqual(intersectionPoints[5].intersection,check) ){//this indicates two not-crossing fractures
        xfMiddle.push_back(intersectionPoints[5].intersection);
        std::cout << "xfMiddle.size(): " << xfMiddle.size() <<std::endl;
        /*
         * there are 4 points on the boundary of the element,
         * but there are also two centre points, so that the for each xfMiddle there are only two pointsNotOnCorners.
         */
//        lmax=2;
    }

    //create object which modifies the basis and test functions correctly
    typedef Dumux::modifiedBasisFunction<MultiDomainGrid, typename EG::Entity, IntersectionPointsMap, LFSU> MBF;
    const MBF mbf(multiDomainGrid_, eg.entity(), intersectionPointsMapLocal_, lfsu);

    typedef typename std::vector<FV> SortedPoints;
    /*
     * for ending fractures the sub-triangulation is the same as for one single not bending fracture through the element
     * the fracture is extrapolated to the end of the element and a virtual exit point is determined
     * this happens in the gridCoupling and in line 62,
     * so that the vector pointsNotOnCorners does look the same as in the single not bending fracture case
     */
    SortedPoints sortedPoints;
    sortedPoints=Dumux::sortPoints<FV>(pointsNotOnCorners, eg);
    typedef Dune::MultiLinearGeometry<ctype,dim_,dimw_> ElementGeometry;
    typedef Dune::GeometryType::BasicType BasicType;
    const BasicType simplex = Dune::GeometryType::simplex;
    const GT subSimplex(simplex,dimw_);
    std::vector<FV> listOfCornerCoordinates(3);//this is a vector which stores all corners of the used subtriangle
    /*
     * this scheme is based on the DUNE node ordering as defined for the generic reference element cube
     * create subtriangles depending on the fracture geoemtry
     * for straight, bending and ending fractures always 6 subtriangles are generated
     * for 3-crossings 7 subtriangles are generated
     * for 4-crossings 8 subtriangles are generated
     * and for two not-crossing fractures 10 subtriangles are generated
     */
    if (xfMiddle.size()==1){
        listOfCornerCoordinates[0]=eg.geometry().global(xfMiddle[0]);
        for (int i=0;i<4+children;i++){
            listOfCornerCoordinates[1]=eg.geometry().global(sortedPoints[i]);
            listOfCornerCoordinates[2]= (i<3+children ? eg.geometry().global(sortedPoints[i+1]) : eg.geometry().global(sortedPoints[0]) );
            ElementGeometry subElement(subSimplex,listOfCornerCoordinates);
            //accumulate the residuals for the matrix part of the element (without coupling terms)
            evaluateMatrixJacobian(eg, lfsu, mbf, x, m, subElement);
        }//end loop over all subelements
    }//end if for all cases except two-not crossing fractures

    else if (xfMiddle.size()==2){//handles the special case of two not-crossing fractures
        assert(xfMiddle.size()!=2);//TODO adapt the algorithm for two parallel fractures: there are two xfMiddle now!
        listOfCornerCoordinates[0]=eg.geometry().global(xfMiddle[0]);
        for (int i=0;i<4+children;i++){
            listOfCornerCoordinates[1]=eg.geometry().global(sortedPoints[i]);
            listOfCornerCoordinates[2]= (i<3+children ? eg.geometry().global(sortedPoints[i+1]) : eg.geometry().global(sortedPoints[0]) );
            ElementGeometry subElement(subSimplex,listOfCornerCoordinates);

            //accumulate the residuals for the matrix part of the element (without coupling terms)
            evaluateMatrixJacobian(eg, lfsu, mbf, x, m, subElement);
        }//end loop over all subelements
    }//end if for the special case of two-not crossing fractures
    assert(xfMiddle.size()<=2);

//create subelements of codimension one depending on the fracture geometry
    typedef typename CouplingMap::iterator CouplingMapIterator;
    CouplingMapIterator cmit=couplingMap_.find(elementId);
    assert(cmit != couplingMap_.end());

    typedef typename FractureGrid::Traits::template Codim<0>::EntitySeed FGES;
    typedef std::vector<FGES> VectorOfEntitySeeds;
    typedef typename VectorOfEntitySeeds::iterator VectorOfEntitySeedsIterator;

    typedef typename FractureGrid::LeafGridView FGV;
    typedef typename FGV::IntersectionIterator FractureIntersectionIterator;
    FGV fgv=fractureGrid_.leafView();
    const int worlddim=MultiDomainGrid::dimensionworld;
    typedef typename Dune::ReferenceElements<ctype,worlddim> GRE;
    typedef Dune::GeometryType GT;
    GT matrixElementGeometryType =eg.geometry().type();

typedef Dune::MultiLinearGeometry<ctype,dimw_-1,dimw_> ElementGeometryFracture;
const GT subSimplexFracture(simplex,dimw_-1);
std::vector<FV> listOfCornerCoordinatesFracture(2);
//iterate over all fracture subelements
VectorOfEntitySeeds vectorOfEntitySeeds=(((*cmit).second));//FractureNodeIndexType
for (VectorOfEntitySeedsIterator esIt=vectorOfEntitySeeds.begin(); esIt!=vectorOfEntitySeeds.end();esIt++ ){
    const Scalar alphaF=this->template alphaF<Scalar>(*(fractureGrid_.entityPointer(*esIt)));
    FractureIntersectionIterator isItEnd = fgv.iend( *(fractureGrid_.entityPointer(*esIt)) );
    int i=0;
    for (FractureIntersectionIterator isIt = fgv.ibegin( *(fractureGrid_.entityPointer(*esIt)) ); isIt != isItEnd; ++isIt){
        listOfCornerCoordinatesFracture[i]= isIt->geometry().center();
        i++;
    }//end loop over fracture nodes

    /*
     * if the fracture element node is within this matrix element, then use it as node for the integration subelement.
     * Otherwise use the intersection point with the element boundary.
     */
    for (int i=0;i<2;i++){
        if ( !GRE::general(matrixElementGeometryType).checkInside( eg.geometry().local( listOfCornerCoordinatesFracture[i] ) ) ){
            Scalar distanceOnLine=1.0e6;
            FV closestPoint(check);
            for (int j=0;j<4;j++){//index 4 and 5 only store the midpoints/virtual exit points
                /*
                 * if there is an empty entry there are no more relevant entries after that
                 * for ending fractures the projected exit is not to be used!
                 */
                if (Dumux::arePointsEqual(intersectionPoints[j].intersection,check)){
                    break;
                }
                else if( Dumux::pointOnLine(eg.geometry().local(listOfCornerCoordinatesFracture[0]),
                        eg.geometry().local(listOfCornerCoordinatesFracture[1]),
                        intersectionPoints[j].intersection) //make the comparison in local coordinates
                ) {
                    /*
                     * make sure that always the intersectionPoint closest to the old fracture node is chosen
                     */
                    if (Dumux::arePointsEqual(closestPoint,check) ) {
                        closestPoint=eg.geometry().global(intersectionPoints[j].intersection);//but store the global coordinates
                        //clean up closest Point zeros
                        for (int indexK=0;indexK<2;indexK++){
                            if (Dumux::arePointsEqual(closestPoint[indexK],0.0)) closestPoint[indexK]=0.0;
                        }
                        distanceOnLine=Dumux::absDistance(listOfCornerCoordinatesFracture[i],closestPoint);
                    }
                    else if (Dumux::absDistance(listOfCornerCoordinatesFracture[i],eg.geometry().global(intersectionPoints[j].intersection)) < distanceOnLine){
                        //if the distance to the second intersectionPoint is smaller than to the first, replace the closestPoint by this intersectionPoint
                        closestPoint=eg.geometry().global(intersectionPoints[j].intersection);
                    }
                }
            }//end loop over intersectionPoints
            assert(!Dumux::arePointsEqual(closestPoint,check));
            listOfCornerCoordinatesFracture[i] = closestPoint;
        }//end if one fracture node does not lie within this matrix element
    }//end loop over fracture nodes
    if (Dumux::arePointsEqual(listOfCornerCoordinatesFracture[0],listOfCornerCoordinatesFracture[1])) {continue;}
    //every subelement is defined by its corners
    ElementGeometryFracture subElementFracture(subSimplexFracture,listOfCornerCoordinatesFracture);
    evaluateMatrixJacobianCouplingTerm(eg, lfsu, mbf, x, alphaF, m, subElementFracture);
}//end loop over fracture entities connected to this matrix element


  }//end jacobian_volume

protected:

  template<typename EG, typename LFSU, typename MBF, typename X, typename AlphaF, typename R, typename SubEG>
  void evaluateMatrixResidualCouplingTerm(const EG &eg, const LFSU &lfsu, const MBF &mbf, const X &x,
          const AlphaF &alphaF, R &r, const SubEG &subElementFracture) const
  {
      typedef typename LFSU::template Child<0>::Type LGFSChild;       //HACK: Assuming that every type is the same
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::RangeFieldType Scalar;
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::DomainType FV;

      GT gtf=subElementFracture.type();//subSimplex;
        // select quadrature rule for the subelement
        const Dune::QuadratureRule<ctype,dimw_-1>& ruleFace = Dune::QuadratureRules<ctype,dimw_-1>::rule(gtf,intorder_); //integration on type, integration order
        //start loop for integration over fracture subelement
        for (typename Dune::QuadratureRule<ctype,dimw_-1>::const_iterator it=ruleFace.begin(); it!=ruleFace.end(); ++it)
        {
            // position of quadrature point in local coordinates of element
            Dune::FieldVector<ctype,dimw_-1> const localOnFracture=it->position();
            //transforms coordinates of integration point on the lower dimensional fracture into the dimw_orld dimensional global coordinates
            FV const globalPos = subElementFracture.global( localOnFracture );
            //transforms global coordinates of integration point into local rock matrix element coordinates
            FV const xQP = eg.geometry().local( globalPos );
            Scalar factor = it->weight()*subElementFracture.integrationElement(localOnFracture);//global fracture length within this element
            Scalar uJump=0.0;
            Scalar uAverage=0.0;
            evaluateU(lfsu, mbf, x, xQP, uJump, uAverage);
            accumulateMatrixResidualCouplingTerm(lfsu, mbf, x, xQP, factor, alphaF, uJump, uAverage, r);
        }
  }//end evaluateMatrixResidualCouplingTerm

  template<typename EG, typename LFSU, typename MBF, typename X, typename AlphaF, typename M, typename SubEG>
  void evaluateMatrixJacobianCouplingTerm(const EG &eg, const LFSU &lfsu, const MBF &mbf, const X &x,
          const AlphaF &alphaF, M &m, const SubEG &subElementFracture) const
  {
      typedef typename LFSU::template Child<0>::Type LGFSChild;       //HACK: Assuming that every type is the same
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::RangeFieldType Scalar;
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::DomainType FV;

      GT gtf=subElementFracture.type();//subSimplex;
        // select quadrature rule for the subelement
        const Dune::QuadratureRule<ctype,dimw_-1>& ruleFace = Dune::QuadratureRules<ctype,dimw_-1>::rule(gtf,2); //integration on type, integration order
        //start loop for integration over fracture subelement
        for (typename Dune::QuadratureRule<ctype,dimw_-1>::const_iterator it=ruleFace.begin(); it!=ruleFace.end(); ++it)
        {
            // position of quadrature point in local coordinates of element
            Dune::FieldVector<ctype,dimw_-1> const localOnFracture=it->position();
            //transforms coordinates of integration point on the lower dimensional fracture into the dimw_orld dimensional global coordinates
            FV const globalPos = subElementFracture.global( localOnFracture );
            //transforms global coordinates of integration point into local rock matrix element coordinates
            FV const xQP = eg.geometry().local( globalPos );
            Scalar factor = it->weight()*subElementFracture.integrationElement(localOnFracture);//global fracture length within this element
            accumulateMatrixJacobianCouplingTerm(lfsu, mbf, x, xQP, factor, alphaF, m);
        }
  }//end evaluateMatrixJacobianCouplingTerm

  template<typename EG, typename LFSU, typename MBF, typename X, typename R, typename SubEG>
  void evaluateMatrixResidual(const EG &eg, const LFSU &lfsu, const MBF &mbf, const X &x, R &r, const SubEG &subElement) const{
      typedef typename LFSU::template Child<0>::Type LGFSChild;       //HACK: Assuming that every type is the same
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::RangeFieldType Scalar;
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::DomainType FV;

      GT gt=subElement.type();//subSimplex;
      const Dune::QuadratureRule<ctype,dimw_>& rule = Dune::QuadratureRules<ctype,dim_>::rule(gt,intorder_);
      for (typename Dune::QuadratureRule<ctype,dimw_>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
      {
          /*
           * it->position is on the local subelement
           * subElement.global makes it world global
           * eg.geometry().local transformes it into the local matrix element
           */
          FV xQP = eg.geometry().local( subElement.global( it->position() ) );
          /*
           * factor consists of the weight and the integrationElement, which is for triangles DOUBLE the element area.
           * and for cubes just the element area.
           * here integrationElement of the subelement is called with the local position of the quadrature point within this subelement
           */
          Scalar factor = it->weight()*subElement.integrationElement(it->position());

          typedef typename Dune::FieldVector<Scalar,dimw_> GradU;
          const GradU gradu=evaluateGradU< GradU > (eg, lfsu, mbf, x, xQP);
          const Dune::FieldMatrix<Scalar,dimw_,dimw_> K=soil_.intrinsicPermeability();
          GradU vector1(0.0);
          K.umv(gradu,vector1);
          accumulateMatrixResidual(eg, lfsu, mbf, x, xQP, factor, vector1, r);
      }
  }//end evaluateMatrixResidual

  template<typename EG, typename LFSU, typename MBF, typename X, typename R, typename SubEG>
  void evaluateMatrixResidualSource(const EG &eg, const LFSU &lfsu, const MBF &mbf, const X &x, R &r, const SubEG &subElement) const{
      typedef typename LFSU::template Child<0>::Type LGFSChild;       //HACK: Assuming that every type is the same
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::RangeFieldType Scalar;
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::DomainType FV;

      GT gt=subElement.type();//subSimplex;
      const Dune::QuadratureRule<ctype,dimw_>& rule = Dune::QuadratureRules<ctype,dim_>::rule(gt,intorder_);
      for (typename Dune::QuadratureRule<ctype,dimw_>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
      {
          /*
           * it->position is on the local subelement
           * subElement.global makes it world global
           * eg.geometry().local transformes it into the local matrix element
           */
          FV xQP = eg.geometry().local( subElement.global( it->position() ) );
          /*
           * factor consists of the weight and the integrationElement, which is for triangles DOUBLE the element area.
           * and for cubes just the element area.
           * here integrationElement of the subelement is called with the local position of the quadrature point within this subelement
           */
          Scalar factor = it->weight()*subElement.integrationElement(it->position());

          typedef typename Dune::FieldVector<Scalar,dimw_> GradU;
          const GradU gradu=evaluateGradU< GradU > (eg, lfsu, mbf, x, xQP);
          const Dune::FieldMatrix<Scalar,dimw_,dimw_> K=soil_.intrinsicPermeability();
          GradU vector1(0.0);
          K.umv(gradu,vector1);
          accumulateMatrixResidualSource(eg, lfsu, mbf, x, xQP, factor, vector1, r);
      }
  }//end evaluateMatrixResidualSource

  template<typename EG, typename LFSU, typename MBF, typename X, typename M, typename SubEG>
  void evaluateMatrixJacobian(const EG &eg, const LFSU &lfsu, const MBF &mbf, const X &x, M &m, const SubEG &subElement) const{
      typedef typename LFSU::template Child<0>::Type LGFSChild;       //HACK: Assuming that every type is the same
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::RangeFieldType Scalar;
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::DomainType FV;

      GT gt=subElement.type();//subSimplex;
      const Dune::QuadratureRule<ctype,dimw_>& rule = Dune::QuadratureRules<ctype,dim_>::rule(gt,2);
      for (typename Dune::QuadratureRule<ctype,dimw_>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
      {
          /*
           * it->position is on the local subelement
           * subElement.global makes it world global
           * eg.geometry().local transformes it into the local matrix element
           */
          FV xQP = eg.geometry().local( subElement.global( it->position() ) );
          /*
           * factor consists of the weight and the integrationElement, which is for triangles DOUBLE the element area.
           * and for cubes just the element area.
           * here integrationElement of the subelement is called with the local position of the quadrature point within this subelement
           */
          Scalar factor = it->weight()*subElement.integrationElement(it->position());
          accumulateMatrixJacobian(eg, lfsu, mbf, x, xQP, factor, m);
      }
  }//end evaluateMatrixJacobian

  //evaluateGradPhi needs to get the child function space!!! and the position of it
  template<typename GradPhi, typename EG, typename LGFSChild, typename MBF, typename FV >
  GradPhi evaluateGradPhi(const EG &eg, const LGFSChild &lgfsChild, const MBF &mbf, const FV &xQP, const int &spaceNumber) const{
//    typedef typename LGFSChild::Traits::FiniteElementType::
//            Traits::LocalBasisType::Traits::RangeFieldType Scalar;
    typedef typename LGFSChild::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::JacobianType JacobianType;
    typedef typename LGFSChild::Traits::SizeType sizeType;
    // evaluate gradient of basis functions on reference element
    std::vector<JacobianType> js(lgfsChild.size());
    lgfsChild.finiteElement().localBasis().evaluateJacobian(xQP,js);
    // transform gradients from reference element to real element
    const Dune::FieldMatrix<ctype,dimw_,dim_> jac = eg.geometry().jacobianInverseTransposed(xQP);
    GradPhi gradphi(lgfsChild.size());
    for (sizeType i=0; i<lgfsChild.size(); i++){
        jac.mv(js[i][0],gradphi[i]);
    }
    //modify gradients of basis functions
    GradPhi gradphiModified=mbf.modify(gradphi,xQP,spaceNumber);
    return (gradphiModified);
}

template<typename GradU, typename EG, typename LGFS, typename MBF, typename X, typename FV >
GradU evaluateGradU(const EG &eg, const LGFS &lgfs, const MBF &mbf, const X& x, const FV &xQP) const{
    typedef typename LGFS::template Child<0>::Type LGFSChild0;
    typedef typename LGFSChild0::Traits::SizeType sizeType0;
    typedef typename LGFS::template Child<1>::Type LGFSChild1;
    typedef typename LGFSChild1::Traits::SizeType sizeType1;
    typedef typename LGFS::template Child<2>::Type LGFSChild2;
    typedef typename LGFSChild2::Traits::SizeType sizeType2;
    typedef typename LGFS::template Child<3>::Type LGFSChild3;
    typedef typename LGFSChild3::Traits::SizeType sizeType3;
    typedef typename LGFS::template Child<4>::Type LGFSChild4;
    typedef typename LGFSChild4::Traits::SizeType sizeType4;
    typedef typename LGFS::template Child<5>::Type LGFSChild5;
    typedef typename LGFSChild5::Traits::SizeType sizeType5;


    typedef typename std::vector<GradU> GradPhi;
    GradU gradu(0.0);

    const LGFSChild0 &lgfsChild0 = lgfs.template child<0>();
    GradPhi gradPhi=evaluateGradPhi< GradPhi > (eg, lgfsChild0, mbf, xQP, 0);
    for (sizeType0 i=0; i<lgfsChild0.size(); i++){
        gradu.axpy(x(lgfsChild0,i),gradPhi[i]);
    }
    const LGFSChild1 &lgfsChild1 = lgfs.template child<1>();
    if (lgfsChild1.size()!=0){
        GradPhi gradPhi=evaluateGradPhi< GradPhi > (eg, lgfsChild1, mbf, xQP, 1);
        for (sizeType1 i=0; i<lgfsChild1.size(); i++){
            gradu.axpy(x(lgfsChild1,i),gradPhi[i]);
        }
    }
    const LGFSChild2 &lgfsChild2 = lgfs.template child<2>();
    if (lgfsChild2.size()!=0){
        GradPhi gradPhi=evaluateGradPhi< GradPhi > (eg, lgfsChild2, mbf, xQP, 2);
        for (sizeType2 i=0; i<lgfsChild2.size(); i++){
            gradu.axpy(x(lgfsChild2,i),gradPhi[i]);
        }
    }
    const LGFSChild3 &lgfsChild3 = lgfs.template child<3>();
    if (lgfsChild3.size()!=0){
        GradPhi gradPhi=evaluateGradPhi< GradPhi > (eg, lgfsChild3, mbf, xQP, 3);
        for (sizeType3 i=0; i<lgfsChild3.size(); i++){
            gradu.axpy(x(lgfsChild3,i),gradPhi[i]);
        }
    }
    const LGFSChild4 &lgfsChild4 = lgfs.template child<4>();
    if (lgfsChild4.size()!=0){
        GradPhi gradPhi=evaluateGradPhi< GradPhi > (eg, lgfsChild4, mbf, xQP, 4);
        for (sizeType4 i=0; i<lgfsChild4.size(); i++){
            gradu.axpy(x(lgfsChild4,i),gradPhi[i]);
        }
    }
    const LGFSChild5 &lgfsChild5 = lgfs.template child<5>();
    if (lgfsChild5.size()!=0){
        GradPhi gradPhi=evaluateGradPhi< GradPhi > (eg, lgfsChild5, mbf, xQP, 5);
        for (sizeType5 i=0; i<lgfsChild5.size(); i++){
            gradu.axpy(x(lgfsChild5,i),gradPhi[i]);
        }
    }

    return (gradu);
}

template<typename EG, typename LFSU, typename MBF, typename X, typename FV, typename Factor,typename U, typename R>
void accumulateMatrixResidual(const EG &eg, const LFSU &lfsu, const MBF &mbf,
        const X& x, const FV &xQP, const Factor &factor, const U &u, R &r) const{
    typedef typename LFSU::template Child<0>::Type LGFSChild0;
    typedef typename LGFSChild0::Traits::SizeType sizeType0;
    typedef typename LFSU::template Child<1>::Type LGFSChild1;
    typedef typename LGFSChild1::Traits::SizeType sizeType1;
    typedef typename LFSU::template Child<2>::Type LGFSChild2;
    typedef typename LGFSChild2::Traits::SizeType sizeType2;
    typedef typename LFSU::template Child<3>::Type LGFSChild3;
    typedef typename LGFSChild3::Traits::SizeType sizeType3;
    typedef typename LFSU::template Child<4>::Type LGFSChild4;
    typedef typename LGFSChild4::Traits::SizeType sizeType4;
    typedef typename LFSU::template Child<5>::Type LGFSChild5;
    typedef typename LGFSChild5::Traits::SizeType sizeType5;
    typedef typename LGFSChild0::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::RangeFieldType Scalar;

    typedef typename std::vector<U> GradPhi;

    const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();
    const GradPhi gradPhi= evaluateGradPhi< GradPhi >(eg, lgfsChild0, mbf, xQP,0);
    for (sizeType0 i=0; i<lgfsChild0.size(); i++){
        r.accumulate(lgfsChild0,i,( u * gradPhi[i] )*factor);
    }

    const LGFSChild1 &lgfsChild1 = lfsu.template child<1>();
    if (lgfsChild1.size()!=0){
        const GradPhi gradPhi= evaluateGradPhi< GradPhi >(eg, lgfsChild1, mbf, xQP,1);
        for (sizeType1 i=0; i<lgfsChild1.size(); i++){
            r.accumulate(lgfsChild1,i,( u * gradPhi[i] )*factor);
        }
    }
    const LGFSChild2 &lgfsChild2 = lfsu.template child<2>();
    if (lgfsChild2.size()!=0){
        const GradPhi gradPhi= evaluateGradPhi< GradPhi >(eg, lgfsChild2, mbf, xQP,2);
        for (sizeType2 i=0; i<lgfsChild2.size(); i++){
            r.accumulate(lgfsChild2,i,( u * gradPhi[i] )*factor);
        }
    }
    const LGFSChild3 &lgfsChild3 = lfsu.template child<3>();
    if (lgfsChild3.size()!=0){
        const GradPhi gradPhi= evaluateGradPhi< GradPhi >(eg, lgfsChild3, mbf, xQP,3);
        for (sizeType3 i=0; i<lgfsChild3.size(); i++){
            r.accumulate(lgfsChild3,i,( u * gradPhi[i] )*factor);
        }
    }
    const LGFSChild4 &lgfsChild4 = lfsu.template child<4>();
    if (lgfsChild4.size()!=0){
        const GradPhi gradPhi= evaluateGradPhi< GradPhi >(eg, lgfsChild4, mbf, xQP,4);
        for (sizeType4 i=0; i<lgfsChild4.size(); i++){
            r.accumulate(lgfsChild4,i,( u * gradPhi[i] )*factor);
        }
    }
    const LGFSChild5 &lgfsChild5 = lfsu.template child<5>();
    if (lgfsChild5.size()!=0){
        const GradPhi gradPhi= evaluateGradPhi< GradPhi >(eg, lgfsChild5, mbf, xQP,5);
        for (sizeType5 i=0; i<lgfsChild5.size(); i++){
            r.accumulate(lgfsChild5,i,( u * gradPhi[i] )*factor);
        }
    }
//    /*
//     * only for analytical solution 0 additional source sink terms in the matrix
//     * have to be added
//     */
//    if (solutionCase_==0){
//    	std::cout << "old:"<<std::endl;
////        typedef typename LGFSChild0::Traits::FiniteElementType::
////                Traits::LocalBasisType::Traits::DomainFieldType ctype;
//        typedef typename LGFSChild0::Traits::FiniteElementType::
//                Traits::LocalBasisType::Traits::RangeType RangeType;
////        typedef typename LGFSChild0::Traits::FiniteElementType::
////                Traits::LocalBasisType::Traits::DomainType FV;
//        typedef typename LGFSChild0::Traits::SizeType sizeType;
//        const int dim = EG::Geometry::dimension;
//
//        Dune::FieldMatrix<Scalar,dim,dim> K=soil_.intrinsicPermeability();
//        Scalar k_m=K[0][0];
//        Scalar k_f=soil_.kFT();
//        Scalar a_f=soil_.fractureWidth();
//        Scalar k=k_f/k_m;
//
//        if (!Dumux::arePointsEqual(k,1.0)){
//            Scalar sqrta=std::sqrt(K[0][0]/K[1][1]);
//            assert(sqrta==1.0);
//            FV global= eg.geometry().global(xQP);
//            Scalar factor2=factor;
//            factor2*=(-1.0)* (1.0-k)*std::cos(global[0])*std::cosh(a_f/(2.0*sqrta));
//            //        std::cout << "factor: " << factor << "\t factor2: " << factor2 << std::endl;
//            //    assert(Dumux::arePointsEqual(std::cosh(a_f/(2.0*sqrta)),1.0));
//            //    factor2*=(-1.0)* (1-k)*std::cos(global[0]);
//            //    factor2*=0.0;
//            {
//
//
//                /*
//                 * only for analytical solution comparison test case needed source/sink term:
//                 * (1-k) cos(x) cosh(a_f/(2 sqrt(a) )
//                 * with k=k_f /k_m and a_f the fracture aperture
//                 */
//
//                const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();
//
//                // evaluate basis functions on reference element
//                std::vector<RangeType> phi(lgfsChild0.size());
//                lgfsChild0.finiteElement().localBasis().evaluateFunction(xQP,phi);
//                std::vector<RangeType> phiModified(lgfsChild0.size());
//                phiModified=mbf.modify(phi,xQP,0);
//
//                for (sizeType i=0; i<lgfsChild0.size(); i++){
//                    r.accumulate(lgfsChild0,i,phiModified[i]*factor2);
//                    std::cout << "phi( " << i << "): " << phiModified[i] << "\t"<<factor2 << std::endl;
//                }
//            }
//            {
//                typedef typename LFSU::template Child<1>::Type LGFSChild1;
//
//                /*
//                 * only for analytical solution comparison test case needed source/sink term:
//                 * (1-k) cos(x) cosh(a_f/(2 sqrt(a) )
//                 * with k=k_f /k_m and a_f the fracture aperture
//                 */
//
//                const LGFSChild1 &lgfsChild1 = lfsu.template child<1>();
//
//                // evaluate basis functions on reference element
//                std::vector<RangeType> phi(lgfsChild1.size());
//                lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi);
//                std::vector<RangeType> phiModified(lgfsChild1.size());
//                phiModified=mbf.modify(phi,xQP,1);
//
//                for (sizeType i=0; i<lgfsChild1.size(); i++){
//                    r.accumulate(lgfsChild1,i,phiModified[i]*factor2);
//                    std::cout << "phi( " << i << "): " << phiModified[i] << "\t"<<factor2 << std::endl;
//                }
//            }
//        }//end if k!=1.0
//    }//end solutionCase==0

}//end accumulateMatrixResidual

template<typename EG, typename LFSU, typename MBF, typename X, typename FV, typename Factor,typename U, typename R>
void accumulateMatrixResidualSource(const EG &eg, const LFSU &lfsu, const MBF &mbf,
        const X& x, const FV &xQP, const Factor &factor, const U &u, R &r) const{
    typedef typename LFSU::template Child<0>::Type LGFSChild0;
    typedef typename LGFSChild0::Traits::SizeType sizeType0;
    typedef typename LFSU::template Child<1>::Type LGFSChild1;
    typedef typename LGFSChild1::Traits::SizeType sizeType1;
    typedef typename LFSU::template Child<2>::Type LGFSChild2;
    typedef typename LGFSChild2::Traits::SizeType sizeType2;
    typedef typename LFSU::template Child<3>::Type LGFSChild3;
    typedef typename LGFSChild3::Traits::SizeType sizeType3;
    typedef typename LFSU::template Child<4>::Type LGFSChild4;
    typedef typename LGFSChild4::Traits::SizeType sizeType4;
    typedef typename LFSU::template Child<5>::Type LGFSChild5;
    typedef typename LGFSChild5::Traits::SizeType sizeType5;
    typedef typename LGFSChild0::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::RangeFieldType Scalar;

    /*
     * only for analytical solution 0 additional source sink terms in the matrix
     * have to be added
     */
    if (solutionCase_==0){
//    	std::cout << "new!"<<std::endl;
//        typedef typename LGFSChild0::Traits::FiniteElementType::
//                Traits::LocalBasisType::Traits::DomainFieldType ctype;
        typedef typename LGFSChild0::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::RangeType RangeType;
//        typedef typename LGFSChild0::Traits::FiniteElementType::
//                Traits::LocalBasisType::Traits::DomainType FV;
        typedef typename LGFSChild0::Traits::SizeType sizeType;
        const int dim = EG::Geometry::dimension;

        Dune::FieldMatrix<Scalar,dim,dim> K=soil_.intrinsicPermeability();
        Scalar k_m=K[0][0];
        Scalar k_f=soil_.kFT();
        Scalar a_f=soil_.fractureWidth();
        Scalar k=k_f/k_m;

        if (!Dumux::arePointsEqual(k,1.0)){
            Scalar sqrta=std::sqrt(K[0][0]/K[1][1]);
            assert(sqrta==1.0);
            FV global= eg.geometry().global(xQP);
            Scalar factor2=factor;
            factor2*=(-1.0)* (1.0-k)*std::cos(global[0])*std::cosh(a_f/(2.0*sqrta));
            //        std::cout << "factor: " << factor << "\t factor2: " << factor2 << std::endl;
            //    assert(Dumux::arePointsEqual(std::cosh(a_f/(2.0*sqrta)),1.0));
            //    factor2*=(-1.0)* (1-k)*std::cos(global[0]);
            //    factor2*=0.0;
            {


                /*
                 * only for analytical solution comparison test case needed source/sink term:
                 * (1-k) cos(x) cosh(a_f/(2 sqrt(a) )
                 * with k=k_f /k_m and a_f the fracture aperture
                 */

                const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();

                // evaluate basis functions on reference element
                std::vector<RangeType> phi(lgfsChild0.size());
                lgfsChild0.finiteElement().localBasis().evaluateFunction(xQP,phi);
                std::vector<RangeType> phiModified(lgfsChild0.size());
                phiModified=mbf.modify(phi,xQP,0);

                for (sizeType i=0; i<lgfsChild0.size(); i++){
                    r.accumulate(lgfsChild0,i,phiModified[i]*factor2);
//                	std::cout << "phi( " << i << "): " << phiModified[i] << "\t"<<factor2 << std::endl;
                }
            }
            {
                typedef typename LFSU::template Child<1>::Type LGFSChild1;

                /*
                 * only for analytical solution comparison test case needed source/sink term:
                 * (1-k) cos(x) cosh(a_f/(2 sqrt(a) )
                 * with k=k_f /k_m and a_f the fracture aperture
                 */

                const LGFSChild1 &lgfsChild1 = lfsu.template child<1>();

                // evaluate basis functions on reference element
                std::vector<RangeType> phi(lgfsChild1.size());
                lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi);
                std::vector<RangeType> phiModified(lgfsChild1.size());
                phiModified=mbf.modify(phi,xQP,1);

                for (sizeType i=0; i<lgfsChild1.size(); i++){
                    r.accumulate(lgfsChild1,i,phiModified[i]*factor2);
//                    std::cout << "phi( " << i << "): " << phiModified[i] << "\t"<<factor2 << std::endl;
                }
            }
        }//end if k!=1.0
    }//end solutionCase==0

}//end accumulateMatrixResidualSource

template<typename EG, typename LFSU, typename MBF, typename X, typename FV, typename Factor, typename M>
void accumulateMatrixJacobian(const EG &eg, const LFSU &lfsu, const MBF &mbf,
        const X& x, const FV &xQP, const Factor &factor, M &m) const{
    typedef typename LFSU::template Child<0>::Type LGFSChild0;
    typedef typename LGFSChild0::Traits::SizeType sizeType0;
    typedef typename LFSU::template Child<1>::Type LGFSChild1;
    typedef typename LGFSChild1::Traits::SizeType sizeType1;
    typedef typename LFSU::template Child<2>::Type LGFSChild2;
    typedef typename LGFSChild2::Traits::SizeType sizeType2;
    typedef typename LFSU::template Child<3>::Type LGFSChild3;
    typedef typename LGFSChild3::Traits::SizeType sizeType3;
    typedef typename LFSU::template Child<4>::Type LGFSChild4;
    typedef typename LGFSChild4::Traits::SizeType sizeType4;
    typedef typename LFSU::template Child<5>::Type LGFSChild5;
    typedef typename LGFSChild5::Traits::SizeType sizeType5;

    typedef typename LGFSChild0::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::RangeFieldType Scalar;
    typedef typename Dune::FieldVector<Scalar,dimw_> GradU;
    typedef typename std::vector<GradU> GradPhi;

    const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();
    const LGFSChild1 &lgfsChild1 = lfsu.template child<1>();
    const LGFSChild2 &lgfsChild2 = lfsu.template child<2>();
    const LGFSChild3 &lgfsChild3 = lfsu.template child<3>();
    const LGFSChild4 &lgfsChild4 = lfsu.template child<4>();
    const LGFSChild5 &lgfsChild5 = lfsu.template child<5>();

    const Dune::FieldMatrix<Scalar,dimw_,dimw_> K=soil_.intrinsicPermeability();

    const GradPhi gradPhi0= evaluateGradPhi< GradPhi >(eg, lgfsChild0, mbf, xQP,0);
    GradPhi kGradPhi0(lgfsChild0.size());
    for (sizeType0 i=0; i<lgfsChild0.size(); i++){
        K.mv(gradPhi0[i],kGradPhi0[i]);
    }

    for (sizeType0 rowIdx=0; rowIdx<lgfsChild0.size(); rowIdx++){
        for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
            m.accumulate(lgfsChild0,rowIdx,lgfsChild0,colIdx,
                    gradPhi0[colIdx]*kGradPhi0[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
        }
        if (lgfsChild1.size()!=0){
            const GradPhi gradPhi1= evaluateGradPhi< GradPhi >(eg, lgfsChild1, mbf, xQP,1);
            for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild1,colIdx
                                                  ,gradPhi1[colIdx]*kGradPhi0[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
        }
        if (lgfsChild2.size()!=0){
            const GradPhi gradPhi2= evaluateGradPhi< GradPhi >(eg, lgfsChild2, mbf, xQP,2);
            for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild2,colIdx
                                                  ,gradPhi2[colIdx]*kGradPhi0[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
        }
        if (lgfsChild3.size()!=0){
            const GradPhi gradPhi3= evaluateGradPhi< GradPhi >(eg, lgfsChild3, mbf, xQP,3);
            for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild3,colIdx
                                                  ,gradPhi3[colIdx]*kGradPhi0[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
        }
        if (lgfsChild4.size()!=0){
            const GradPhi gradPhi4= evaluateGradPhi< GradPhi >(eg, lgfsChild4, mbf, xQP,4);
            for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild4,colIdx
                                                  ,gradPhi4[colIdx]*kGradPhi0[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
        }
        if (lgfsChild5.size()!=0){
            const GradPhi gradPhi5= evaluateGradPhi< GradPhi >(eg, lgfsChild5, mbf, xQP,5);
            for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild5,colIdx
                                                  ,gradPhi5[colIdx]*kGradPhi0[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
        }
    }//end loop over test function lines associated to child space 0

    if (lgfsChild1.size()!=0){
        const GradPhi gradPhi1= evaluateGradPhi< GradPhi >(eg, lgfsChild1, mbf, xQP,1);
        GradPhi kGradPhi1(lgfsChild1.size());
        for (sizeType1 i=0; i<lgfsChild1.size(); i++){
            K.mv(gradPhi1[i],kGradPhi1[i]);
        }
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild1.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild1,rowIdx,lgfsChild0,colIdx
                                                  ,gradPhi0[colIdx]*kGradPhi1[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
            if (lgfsChild1.size()!=0){
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild1,colIdx
                                                      ,gradPhi1[colIdx]*kGradPhi1[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild2.size()!=0){
                const GradPhi gradPhi2= evaluateGradPhi< GradPhi >(eg, lgfsChild2, mbf, xQP,2);
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild2,colIdx
                                                      ,gradPhi2[colIdx]*kGradPhi1[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild3.size()!=0){
                const GradPhi gradPhi3= evaluateGradPhi< GradPhi >(eg, lgfsChild3, mbf, xQP,3);
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild3,colIdx
                                                      ,gradPhi3[colIdx]*kGradPhi1[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild4.size()!=0){
                const GradPhi gradPhi4= evaluateGradPhi< GradPhi >(eg, lgfsChild4, mbf, xQP,4);
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild4,colIdx
                                                      ,gradPhi4[colIdx]*kGradPhi1[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild5.size()!=0){
                const GradPhi gradPhi5= evaluateGradPhi< GradPhi >(eg, lgfsChild5, mbf, xQP,5);
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild5,colIdx
                                                      ,gradPhi5[colIdx]*kGradPhi1[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
        }//end loop over test function lines associated to child space 1
    }//end if lgfsChild!=0
    if (lgfsChild2.size()!=0){
        const GradPhi gradPhi2= evaluateGradPhi< GradPhi >(eg, lgfsChild2, mbf, xQP,2);
        GradPhi kGradPhi2(lgfsChild2.size());
        for (sizeType2 i=0; i<lgfsChild2.size(); i++){
            K.mv(gradPhi2[i],kGradPhi2[i]);
        }
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild2.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild2,rowIdx,lgfsChild0,colIdx
                                                  ,gradPhi0[colIdx]*kGradPhi2[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
            if (lgfsChild1.size()!=0){
                const GradPhi gradPhi1= evaluateGradPhi< GradPhi >(eg, lgfsChild1, mbf, xQP,1);
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild1,colIdx
                                                      ,gradPhi1[colIdx]*kGradPhi2[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild2.size()!=0){
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild2,colIdx
                                                      ,gradPhi2[colIdx]*kGradPhi2[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild3.size()!=0){
                const GradPhi gradPhi3= evaluateGradPhi< GradPhi >(eg, lgfsChild3, mbf, xQP,3);
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild3,colIdx
                                                      ,gradPhi3[colIdx]*kGradPhi2[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild4.size()!=0){
                const GradPhi gradPhi4= evaluateGradPhi< GradPhi >(eg, lgfsChild4, mbf, xQP,4);
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild4,colIdx
                                                      ,gradPhi4[colIdx]*kGradPhi2[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild5.size()!=0){
                const GradPhi gradPhi5= evaluateGradPhi< GradPhi >(eg, lgfsChild5, mbf, xQP,5);
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild5,colIdx
                                                      ,gradPhi5[colIdx]*kGradPhi2[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
        }//end loop over test function lines associated to child space 2
    }//end if lgfsChild!=0
    if (lgfsChild3.size()!=0){
        const GradPhi gradPhi3= evaluateGradPhi< GradPhi >(eg, lgfsChild3, mbf, xQP,3);
        GradPhi kGradPhi3(lgfsChild3.size());
        for (sizeType3 i=0; i<lgfsChild3.size(); i++){
            K.mv(gradPhi3[i],kGradPhi3[i]);
        }
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild3.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild3,rowIdx,lgfsChild0,colIdx
                                                  ,gradPhi0[colIdx]*kGradPhi3[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
            if (lgfsChild1.size()!=0){
                const GradPhi gradPhi1= evaluateGradPhi< GradPhi >(eg, lgfsChild1, mbf, xQP,1);
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild1,colIdx
                                                      ,gradPhi1[colIdx]*kGradPhi3[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild2.size()!=0){
                const GradPhi gradPhi2= evaluateGradPhi< GradPhi >(eg, lgfsChild2, mbf, xQP,2);
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild2,colIdx
                                                      ,gradPhi2[colIdx]*kGradPhi3[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild3.size()!=0){
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild3,colIdx
                                                      ,gradPhi3[colIdx]*kGradPhi3[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild4.size()!=0){
                const GradPhi gradPhi4= evaluateGradPhi< GradPhi >(eg, lgfsChild4, mbf, xQP,4);
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild4,colIdx
                                                      ,gradPhi4[colIdx]*kGradPhi3[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild5.size()!=0){
                const GradPhi gradPhi5= evaluateGradPhi< GradPhi >(eg, lgfsChild5, mbf, xQP,5);
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild5,colIdx
                                                      ,gradPhi5[colIdx]*kGradPhi3[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
        }//end loop over test function lines associated to child space 3
    }//end if lgfsChild!=0
    if (lgfsChild4.size()!=0){
        const GradPhi gradPhi4= evaluateGradPhi< GradPhi >(eg, lgfsChild4, mbf, xQP,4);
        GradPhi kGradPhi4(lgfsChild4.size());
        for (sizeType4 i=0; i<lgfsChild4.size(); i++){
            K.mv(gradPhi4[i],kGradPhi4[i]);
        }
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild4.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild4,rowIdx,lgfsChild0,colIdx
                                                  ,gradPhi0[colIdx]*kGradPhi4[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
            if (lgfsChild1.size()!=0){
                const GradPhi gradPhi1= evaluateGradPhi< GradPhi >(eg, lgfsChild1, mbf, xQP,1);
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild1,colIdx
                                                      ,gradPhi1[colIdx]*kGradPhi4[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild2.size()!=0){
                const GradPhi gradPhi2= evaluateGradPhi< GradPhi >(eg, lgfsChild2, mbf, xQP,2);
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild2,colIdx
                                                      ,gradPhi2[colIdx]*kGradPhi4[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild3.size()!=0){
                const GradPhi gradPhi3= evaluateGradPhi< GradPhi >(eg, lgfsChild3, mbf, xQP,3);
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild3,colIdx
                                                      ,gradPhi3[colIdx]*kGradPhi4[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild4.size()!=0){
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild4,colIdx
                                                      ,gradPhi4[colIdx]*kGradPhi4[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild5.size()!=0){
                const GradPhi gradPhi5= evaluateGradPhi< GradPhi >(eg, lgfsChild5, mbf, xQP,5);
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild5,colIdx
                                                      ,gradPhi5[colIdx]*kGradPhi4[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
        }//end loop over test function lines associated to child space 4
    }//end if lgfsChild!=0
    if (lgfsChild5.size()!=0){
        const GradPhi gradPhi5= evaluateGradPhi< GradPhi >(eg, lgfsChild5, mbf, xQP,5);
        GradPhi kGradPhi5(lgfsChild5.size());
        for (sizeType5 i=0; i<lgfsChild5.size(); i++){
            K.mv(gradPhi5[i],kGradPhi5[i]);
        }
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild5.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild5,rowIdx,lgfsChild0,colIdx
                                                  ,gradPhi0[colIdx]*kGradPhi5[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
            if (lgfsChild1.size()!=0){
                const GradPhi gradPhi1= evaluateGradPhi< GradPhi >(eg, lgfsChild1, mbf, xQP,1);
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild1,colIdx
                                                      ,gradPhi1[colIdx]*kGradPhi5[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild2.size()!=0){
                const GradPhi gradPhi2= evaluateGradPhi< GradPhi >(eg, lgfsChild2, mbf, xQP,2);
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild2,colIdx
                                                      ,gradPhi2[colIdx]*kGradPhi5[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild3.size()!=0){
                const GradPhi gradPhi3= evaluateGradPhi< GradPhi >(eg, lgfsChild3, mbf, xQP,3);
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild3,colIdx
                                                      ,gradPhi3[colIdx]*kGradPhi5[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild4.size()!=0){
                const GradPhi gradPhi4= evaluateGradPhi< GradPhi >(eg, lgfsChild4, mbf, xQP,4);
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild4,colIdx
                                                      ,gradPhi4[colIdx]*kGradPhi5[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild5.size()!=0){
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild5,colIdx
                                                      ,gradPhi5[colIdx]*kGradPhi5[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
        }//end loop over test function lines associated to child space 5
    }//end if lgfsChild!=0


}//end accumulateMatrixJacobian

template<typename U, typename LGFS, typename MBF, typename X, typename FV >
void evaluateU(const LGFS &lgfs, const MBF &mbf, const X& x, const FV &xQP, U &uJump, U &uAverage) const{
    typedef typename LGFS::template Child<0>::Type LGFSChild0;
    typedef typename LGFSChild0::Traits::SizeType sizeType0;
    typedef typename LGFS::template Child<1>::Type LGFSChild1;
    typedef typename LGFSChild1::Traits::SizeType sizeType1;
    typedef typename LGFS::template Child<2>::Type LGFSChild2;
    typedef typename LGFSChild2::Traits::SizeType sizeType2;
    typedef typename LGFS::template Child<3>::Type LGFSChild3;
    typedef typename LGFSChild3::Traits::SizeType sizeType3;
    typedef typename LGFS::template Child<4>::Type LGFSChild4;
    typedef typename LGFSChild4::Traits::SizeType sizeType4;
    typedef typename LGFS::template Child<5>::Type LGFSChild5;
    typedef typename LGFSChild5::Traits::SizeType sizeType5;

//    typedef typename LGFSChild0::Traits::FiniteElementType::
//            Traits::LocalBasisType::Traits::RangeFieldType Scalar;
    typedef typename LGFSChild0::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::RangeType RangeType;

    const LGFSChild0 &lgfsChild0 = lgfs.template child<0>();
    // evaluate basis functions on reference element
    std::vector<RangeType> phi(lgfsChild0.size());
    lgfsChild0.finiteElement().localBasis().evaluateFunction(xQP,phi);
    //evaluate the jump
    std::vector<RangeType> phiJump(lgfsChild0.size());
    phiJump=mbf.jump(phi,xQP,0);
    for (sizeType0 i=0; i<lgfsChild0.size(); i++){
        uJump += x(lgfsChild0,i)*phiJump[i];
    }
    //evaluate the average
    std::vector<RangeType> phiAverage(lgfsChild0.size());
    phiAverage=mbf.average(phi,xQP,0);
    for (sizeType0 i=0; i<lgfsChild0.size(); i++){
        uAverage += x(lgfsChild0,i)*phiAverage[i];
    }
    const LGFSChild1 &lgfsChild1 = lgfs.template child<1>();
    if (lgfsChild1.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild1.size());
        lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi);
        //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild1.size());
        phiJump=mbf.jump(phi,xQP,1);
        for (sizeType1 i=0; i<lgfsChild1.size(); i++){
            uJump += x(lgfsChild1,i)*phiJump[i];
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild1.size());
        phiAverage=mbf.average(phi,xQP,1);
        for (sizeType1 i=0; i<lgfsChild1.size(); i++){
            uAverage += x(lgfsChild1,i)*phiAverage[i];
        }
    }
    const LGFSChild2 &lgfsChild2 = lgfs.template child<2>();
    if (lgfsChild2.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild2.size());
        lgfsChild2.finiteElement().localBasis().evaluateFunction(xQP,phi);
        //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild2.size());
        phiJump=mbf.jump(phi,xQP,2);
        for (sizeType2 i=0; i<lgfsChild2.size(); i++){
            uJump += x(lgfsChild2,i)*phiJump[i];
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild2.size());
        phiAverage=mbf.average(phi,xQP,2);
        for (sizeType2 i=0; i<lgfsChild2.size(); i++){
            uAverage += x(lgfsChild2,i)*phiAverage[i];
        }
    }
    const LGFSChild3 &lgfsChild3 = lgfs.template child<3>();
    if (lgfsChild3.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild3.size());
        lgfsChild3.finiteElement().localBasis().evaluateFunction(xQP,phi);
        //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild3.size());
        phiJump=mbf.jump(phi,xQP,3);
        for (sizeType3 i=0; i<lgfsChild3.size(); i++){
            uJump += x(lgfsChild3,i)*phiJump[i];
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild3.size());
        phiAverage=mbf.average(phi,xQP,3);
        for (sizeType3 i=0; i<lgfsChild3.size(); i++){
            uAverage += x(lgfsChild3,i)*phiAverage[i];
        }
    }
    const LGFSChild4 &lgfsChild4 = lgfs.template child<4>();
    if (lgfsChild4.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild4.size());
        lgfsChild4.finiteElement().localBasis().evaluateFunction(xQP,phi);
        //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild4.size());
        phiJump=mbf.jump(phi,xQP,4);
        for (sizeType4 i=0; i<lgfsChild4.size(); i++){
            uJump += x(lgfsChild4,i)*phiJump[i];
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild4.size());
        phiAverage=mbf.average(phi,xQP,4);
        for (sizeType4 i=0; i<lgfsChild4.size(); i++){
            uAverage += x(lgfsChild4,i)*phiAverage[i];
        }
    }
    const LGFSChild5 &lgfsChild5 = lgfs.template child<5>();
    if (lgfsChild5.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild5.size());
        lgfsChild5.finiteElement().localBasis().evaluateFunction(xQP,phi);
        //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild5.size());
        phiJump=mbf.jump(phi,xQP,5);
        for (sizeType5 i=0; i<lgfsChild5.size(); i++){
            uJump += x(lgfsChild5,i)*phiJump[i];
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild5.size());
        phiAverage=mbf.average(phi,xQP,5);
        for (sizeType5 i=0; i<lgfsChild5.size(); i++){
            uAverage += x(lgfsChild5,i)*phiAverage[i];
        }
    }

}//end evaluateU

template<typename LFSU, typename MBF, typename X, typename FV, typename U, typename Factor, typename AlphaF, typename R>
void accumulateMatrixResidualCouplingTerm(const LFSU &lfsu, const MBF &mbf,
        const X& x, const FV &xQP, const Factor &factor, const AlphaF &alphaF, const U &uJump, const U &uAverage, R &r) const{
    typedef typename LFSU::template Child<0>::Type LGFSChild0;
    typedef typename LGFSChild0::Traits::SizeType sizeType0;
    typedef typename LFSU::template Child<1>::Type LGFSChild1;
    typedef typename LGFSChild1::Traits::SizeType sizeType1;
    typedef typename LFSU::template Child<2>::Type LGFSChild2;
    typedef typename LGFSChild2::Traits::SizeType sizeType2;
    typedef typename LFSU::template Child<3>::Type LGFSChild3;
    typedef typename LGFSChild3::Traits::SizeType sizeType3;
    typedef typename LFSU::template Child<4>::Type LGFSChild4;
    typedef typename LGFSChild4::Traits::SizeType sizeType4;
    typedef typename LFSU::template Child<5>::Type LGFSChild5;
    typedef typename LGFSChild5::Traits::SizeType sizeType5;

//    typedef typename LGFSChild0::Traits::FiniteElementType::
//            Traits::LocalBasisType::Traits::RangeFieldType Scalar;
    typedef typename LGFSChild0::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::RangeType RangeType;

    const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();
    // evaluate basis functions on reference element
    std::vector<RangeType> phi(lgfsChild0.size());
    lgfsChild0.finiteElement().localBasis().evaluateFunction(xQP,phi);
    //evaluate the jump
    std::vector<RangeType> phiJump(lgfsChild0.size());
    phiJump=mbf.jump(phi,xQP,0);
    //accumulate the jump
    for (sizeType0 i=0; i<lgfsChild0.size(); i++)
    {r.accumulate(lgfsChild0,i,(alphaF*0.5)*uJump*phiJump[i]*factor);
    }
    //evaluate the average
    std::vector<RangeType> phiAverage(lgfsChild0.size());
    phiAverage=mbf.average(phi,xQP,0);
    //accumulate the average
    for (sizeType0 i=0; i<lgfsChild0.size(); i++)
    {r.accumulate(lgfsChild0,i,(alphaF/(xi_-0.5))*uAverage*phiAverage[i]*factor);
    }
    const LGFSChild1 &lgfsChild1 = lfsu.template child<1>();
    if (lgfsChild1.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild1.size());
        lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi);
       //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild1.size());
        phiJump=mbf.jump(phi,xQP,1);
        //accumulate the jump
        for (sizeType1 i=0; i<lgfsChild1.size(); i++)
        {r.accumulate(lgfsChild1,i,(alphaF*0.5)*uJump*phiJump[i]*factor);
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild1.size());
        phiAverage=mbf.average(phi,xQP,1);
        //accumulate the average
        for (sizeType1 i=0; i<lgfsChild1.size(); i++)
        {r.accumulate(lgfsChild1,i,(alphaF/(xi_-0.5))*uAverage*phiAverage[i]*factor);
        }
    }
    const LGFSChild2 &lgfsChild2 = lfsu.template child<2>();
    if (lgfsChild2.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild2.size());
        lgfsChild2.finiteElement().localBasis().evaluateFunction(xQP,phi);
       //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild2.size());
        phiJump=mbf.jump(phi,xQP,2);
        //accumulate the jump
        for (sizeType2 i=0; i<lgfsChild2.size(); i++)
        {r.accumulate(lgfsChild2,i,(alphaF*0.5)*uJump*phiJump[i]*factor);
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild2.size());
        phiAverage=mbf.average(phi,xQP,2);
        //accumulate the average
        for (sizeType2 i=0; i<lgfsChild2.size(); i++)
        {r.accumulate(lgfsChild2,i,(alphaF/(xi_-0.5))*uAverage*phiAverage[i]*factor);
        }
    }
    const LGFSChild3 &lgfsChild3 = lfsu.template child<3>();
    if (lgfsChild3.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild3.size());
        lgfsChild3.finiteElement().localBasis().evaluateFunction(xQP,phi);
       //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild3.size());
        phiJump=mbf.jump(phi,xQP,3);
        //accumulate the jump
        for (sizeType3 i=0; i<lgfsChild3.size(); i++)
        {r.accumulate(lgfsChild3,i,(alphaF*0.5)*uJump*phiJump[i]*factor);
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild3.size());
        phiAverage=mbf.average(phi,xQP,3);
        //accumulate the average
        for (sizeType3 i=0; i<lgfsChild3.size(); i++)
        {r.accumulate(lgfsChild3,i,(alphaF/(xi_-0.5))*uAverage*phiAverage[i]*factor);
        }
    }
    const LGFSChild4 &lgfsChild4 = lfsu.template child<4>();
    if (lgfsChild4.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild4.size());
        lgfsChild4.finiteElement().localBasis().evaluateFunction(xQP,phi);
       //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild4.size());
        phiJump=mbf.jump(phi,xQP,4);
        //accumulate the jump
        for (sizeType4 i=0; i<lgfsChild4.size(); i++)
        {r.accumulate(lgfsChild4,i,(alphaF*0.5)*uJump*phiJump[i]*factor);
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild4.size());
        phiAverage=mbf.average(phi,xQP,4);
        //accumulate the average
        for (sizeType4 i=0; i<lgfsChild4.size(); i++)
        {r.accumulate(lgfsChild4,i,(alphaF/(xi_-0.5))*uAverage*phiAverage[i]*factor);
        }
    }
    const LGFSChild5 &lgfsChild5 = lfsu.template child<5>();
    if (lgfsChild5.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild5.size());
        lgfsChild5.finiteElement().localBasis().evaluateFunction(xQP,phi);
       //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild5.size());
        phiJump=mbf.jump(phi,xQP,5);
        //accumulate the jump
        for (sizeType5 i=0; i<lgfsChild5.size(); i++)
        {r.accumulate(lgfsChild5,i,(alphaF*0.5)*uJump*phiJump[i]*factor);
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild5.size());
        phiAverage=mbf.average(phi,xQP,5);
        //accumulate the average
        for (sizeType5 i=0; i<lgfsChild5.size(); i++)
        {r.accumulate(lgfsChild5,i,(alphaF/(xi_-0.5))*uAverage*phiAverage[i]*factor);
        }
    }

}//end accumulateMatrixResidualCouplingTerm

template<typename LFSU, typename MBF, typename X, typename FV, typename Factor, typename AlphaF, typename M>
void accumulateMatrixJacobianCouplingTerm(const LFSU &lfsu, const MBF &mbf,
        const X& x, const FV &xQP, const Factor &factor, const AlphaF &alphaF, M &m) const{
    typedef typename LFSU::template Child<0>::Type LGFSChild0;
    typedef typename LGFSChild0::Traits::SizeType sizeType0;
    typedef typename LFSU::template Child<1>::Type LGFSChild1;
    typedef typename LGFSChild1::Traits::SizeType sizeType1;
    typedef typename LFSU::template Child<2>::Type LGFSChild2;
    typedef typename LGFSChild2::Traits::SizeType sizeType2;
    typedef typename LFSU::template Child<3>::Type LGFSChild3;
    typedef typename LGFSChild3::Traits::SizeType sizeType3;
    typedef typename LFSU::template Child<4>::Type LGFSChild4;
    typedef typename LGFSChild4::Traits::SizeType sizeType4;
    typedef typename LFSU::template Child<5>::Type LGFSChild5;
    typedef typename LGFSChild5::Traits::SizeType sizeType5;
    typedef typename LGFSChild0::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::RangeType RangeType;

    const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();
    const LGFSChild1 &lgfsChild1 = lfsu.template child<1>();
    const LGFSChild2 &lgfsChild2 = lfsu.template child<2>();
    const LGFSChild3 &lgfsChild3 = lfsu.template child<3>();
    const LGFSChild4 &lgfsChild4 = lfsu.template child<4>();
    const LGFSChild5 &lgfsChild5 = lfsu.template child<5>();

    RangeType jumpFactor=(alphaF*0.5);
    RangeType averageFactor=(alphaF/(xi_-0.5));

    // evaluate basis functions on reference element
    std::vector<RangeType> phi0(lgfsChild0.size());
    lgfsChild0.finiteElement().localBasis().evaluateFunction(xQP,phi0);
    //evaluate the jump
    std::vector<RangeType> phiJump0(lgfsChild0.size());
    phiJump0=mbf.jump(phi0,xQP,0);
    //evaluate the average
    std::vector<RangeType> phiAverage0(lgfsChild0.size());
    phiAverage0=mbf.average(phi0,xQP,0);

    for (sizeType0 rowIdx=0; rowIdx<lgfsChild0.size(); rowIdx++){
        for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
            m.accumulate(lgfsChild0,rowIdx,lgfsChild0,colIdx
                                              ,phiAverage0[colIdx]*phiAverage0[rowIdx]*factor*averageFactor
                                              + phiJump0[colIdx]*phiJump0[rowIdx]*factor*jumpFactor);
        }
        if (lgfsChild1.size()!=0){
            // evaluate basis functions on reference element
            std::vector<RangeType> phi1(lgfsChild1.size());
            lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi1);
            //evaluate the jump
            std::vector<RangeType> phiJump1(lgfsChild1.size());
            phiJump1=mbf.jump(phi1,xQP,1);
            //evaluate the average
            std::vector<RangeType> phiAverage1(lgfsChild1.size());
            phiAverage1=mbf.average(phi1,xQP,1);
            for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild1,colIdx
                                                  ,phiAverage1[colIdx]*phiAverage0[rowIdx]*factor*averageFactor
                                                  + phiJump1[colIdx]*phiJump0[rowIdx]*factor*jumpFactor);
            }
        }
        if (lgfsChild2.size()!=0){
            // evaluate basis functions on reference element
            std::vector<RangeType> phi2(lgfsChild2.size());
            lgfsChild2.finiteElement().localBasis().evaluateFunction(xQP,phi2);
            //evaluate the jump
            std::vector<RangeType> phiJump2(lgfsChild2.size());
            phiJump2=mbf.jump(phi2,xQP,2);
            //evaluate the average
            std::vector<RangeType> phiAverage2(lgfsChild2.size());
            phiAverage2=mbf.average(phi2,xQP,2);
            for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild2,colIdx
                                                  ,phiAverage2[colIdx]*phiAverage0[rowIdx]*factor*averageFactor
                                                  + phiJump2[colIdx]*phiJump0[rowIdx]*factor*jumpFactor);
            }
        }
        if (lgfsChild3.size()!=0){
            // evaluate basis functions on reference element
            std::vector<RangeType> phi3(lgfsChild3.size());
            lgfsChild3.finiteElement().localBasis().evaluateFunction(xQP,phi3);
            //evaluate the jump
            std::vector<RangeType> phiJump3(lgfsChild3.size());
            phiJump3=mbf.jump(phi3,xQP,3);
            //evaluate the average
            std::vector<RangeType> phiAverage3(lgfsChild3.size());
            phiAverage3=mbf.average(phi3,xQP,3);
            for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild3,colIdx
                                                  ,phiAverage3[colIdx]*phiAverage0[rowIdx]*factor*averageFactor
                                                  + phiJump3[colIdx]*phiJump0[rowIdx]*factor*jumpFactor);
            }
        }
        if (lgfsChild4.size()!=0){
            // evaluate basis functions on reference element
            std::vector<RangeType> phi4(lgfsChild4.size());
            lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi4);
            //evaluate the jump
            std::vector<RangeType> phiJump4(lgfsChild4.size());
            phiJump4=mbf.jump(phi4,xQP,4);
            //evaluate the average
            std::vector<RangeType> phiAverage4(lgfsChild4.size());
            phiAverage4=mbf.average(phi4,xQP,4);
            for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild4,colIdx
                                                  ,phiAverage4[colIdx]*phiAverage0[rowIdx]*factor*averageFactor
                                                  + phiJump4[colIdx]*phiJump0[rowIdx]*factor*jumpFactor);
            }
        }
        if (lgfsChild5.size()!=0){
            // evaluate basis functions on reference element
            std::vector<RangeType> phi5(lgfsChild5.size());
            lgfsChild5.finiteElement().localBasis().evaluateFunction(xQP,phi5);
            //evaluate the jump
            std::vector<RangeType> phiJump5(lgfsChild5.size());
            phiJump5=mbf.jump(phi5,xQP,5);
            //evaluate the average
            std::vector<RangeType> phiAverage5(lgfsChild5.size());
            phiAverage5=mbf.average(phi5,xQP,5);
            for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild5,colIdx
                                                  ,phiAverage5[colIdx]*phiAverage0[rowIdx]*factor*averageFactor
                                                  + phiJump5[colIdx]*phiJump0[rowIdx]*factor*jumpFactor);
            }
        }
    }//end loop over test function lines associated to child space 0

    if (lgfsChild1.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi1(lgfsChild1.size());
        lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi1);
        //evaluate the jump
        std::vector<RangeType> phiJump1(lgfsChild1.size());
        phiJump1=mbf.jump(phi1,xQP,1);
        //evaluate the average
        std::vector<RangeType> phiAverage1(lgfsChild1.size());
        phiAverage1=mbf.average(phi1,xQP,1);
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild1.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild1,rowIdx,lgfsChild0,colIdx
                                                  ,phiAverage0[colIdx]*phiAverage1[rowIdx]*factor*averageFactor
                                                  + phiJump0[colIdx]*phiJump1[rowIdx]*factor*jumpFactor);
            }
            if (lgfsChild1.size()!=0){
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild1,colIdx
                                                      ,phiAverage1[colIdx]*phiAverage1[rowIdx]*factor*averageFactor
                                                      + phiJump1[colIdx]*phiJump1[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild2.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi2(lgfsChild2.size());
                lgfsChild2.finiteElement().localBasis().evaluateFunction(xQP,phi2);
                //evaluate the jump
                std::vector<RangeType> phiJump2(lgfsChild2.size());
                phiJump2=mbf.jump(phi2,xQP,2);
                //evaluate the average
                std::vector<RangeType> phiAverage2(lgfsChild2.size());
                phiAverage2=mbf.average(phi2,xQP,2);
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild2,colIdx
                                                      ,phiAverage2[colIdx]*phiAverage1[rowIdx]*factor*averageFactor
                                                      + phiJump2[colIdx]*phiJump1[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild3.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi3(lgfsChild3.size());
                lgfsChild3.finiteElement().localBasis().evaluateFunction(xQP,phi3);
                //evaluate the jump
                std::vector<RangeType> phiJump3(lgfsChild3.size());
                phiJump3=mbf.jump(phi3,xQP,3);
                //evaluate the average
                std::vector<RangeType> phiAverage3(lgfsChild3.size());
                phiAverage3=mbf.average(phi3,xQP,3);
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild3,colIdx
                                                      ,phiAverage3[colIdx]*phiAverage1[rowIdx]*factor*averageFactor
                                                      + phiJump3[colIdx]*phiJump1[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild4.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi4(lgfsChild4.size());
                lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi4);
                //evaluate the jump
                std::vector<RangeType> phiJump4(lgfsChild4.size());
                phiJump4=mbf.jump(phi4,xQP,4);
                //evaluate the average
                std::vector<RangeType> phiAverage4(lgfsChild4.size());
                phiAverage4=mbf.average(phi4,xQP,4);
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild4,colIdx
                                                      ,phiAverage4[colIdx]*phiAverage1[rowIdx]*factor*averageFactor
                                                      + phiJump4[colIdx]*phiJump1[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild5.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi5(lgfsChild5.size());
                lgfsChild5.finiteElement().localBasis().evaluateFunction(xQP,phi5);
                //evaluate the jump
                std::vector<RangeType> phiJump5(lgfsChild5.size());
                phiJump5=mbf.jump(phi5,xQP,5);
                //evaluate the average
                std::vector<RangeType> phiAverage5(lgfsChild5.size());
                phiAverage5=mbf.average(phi5,xQP,5);
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild5,colIdx
                                                      ,phiAverage5[colIdx]*phiAverage1[rowIdx]*factor*averageFactor
                                                      + phiJump5[colIdx]*phiJump1[rowIdx]*factor*jumpFactor);
                }
            }
        }//end loop over test function lines associated to child space 1
    }//end if lgfsChild!=0
    if (lgfsChild2.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi2(lgfsChild2.size());
        lgfsChild2.finiteElement().localBasis().evaluateFunction(xQP,phi2);
        //evaluate the jump
        std::vector<RangeType> phiJump2(lgfsChild2.size());
        phiJump2=mbf.jump(phi2,xQP,2);
        //evaluate the average
        std::vector<RangeType> phiAverage2(lgfsChild2.size());
        phiAverage2=mbf.average(phi2,xQP,2);
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild2.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild2,rowIdx,lgfsChild0,colIdx
                                                  ,phiAverage0[colIdx]*phiAverage2[rowIdx]*factor*averageFactor
                                                  + phiJump0[colIdx]*phiJump2[rowIdx]*factor*jumpFactor);
            }
            if (lgfsChild1.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi1(lgfsChild1.size());
                lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi1);
                //evaluate the jump
                std::vector<RangeType> phiJump1(lgfsChild1.size());
                phiJump1=mbf.jump(phi1,xQP,1);
                //evaluate the average
                std::vector<RangeType> phiAverage1(lgfsChild1.size());
                phiAverage1=mbf.average(phi1,xQP,1);
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild1,colIdx
                                                      ,phiAverage1[colIdx]*phiAverage2[rowIdx]*factor*averageFactor
                                                      + phiJump1[colIdx]*phiJump2[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild2.size()!=0){
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild2,colIdx
                                                      ,phiAverage2[colIdx]*phiAverage2[rowIdx]*factor*averageFactor
                                                      + phiJump2[colIdx]*phiJump2[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild3.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi3(lgfsChild3.size());
                lgfsChild3.finiteElement().localBasis().evaluateFunction(xQP,phi3);
                //evaluate the jump
                std::vector<RangeType> phiJump3(lgfsChild3.size());
                phiJump3=mbf.jump(phi3,xQP,3);
                //evaluate the average
                std::vector<RangeType> phiAverage3(lgfsChild3.size());
                phiAverage3=mbf.average(phi3,xQP,3);
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild3,colIdx
                                                      ,phiAverage3[colIdx]*phiAverage2[rowIdx]*factor*averageFactor
                                                      + phiJump3[colIdx]*phiJump2[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild4.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi4(lgfsChild4.size());
                lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi4);
                //evaluate the jump
                std::vector<RangeType> phiJump4(lgfsChild4.size());
                phiJump4=mbf.jump(phi4,xQP,4);
                //evaluate the average
                std::vector<RangeType> phiAverage4(lgfsChild4.size());
                phiAverage4=mbf.average(phi4,xQP,4);
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild4,colIdx
                                                      ,phiAverage4[colIdx]*phiAverage2[rowIdx]*factor*averageFactor
                                                      + phiJump4[colIdx]*phiJump2[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild5.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi5(lgfsChild5.size());
                lgfsChild5.finiteElement().localBasis().evaluateFunction(xQP,phi5);
                //evaluate the jump
                std::vector<RangeType> phiJump5(lgfsChild5.size());
                phiJump5=mbf.jump(phi5,xQP,5);
                //evaluate the average
                std::vector<RangeType> phiAverage5(lgfsChild5.size());
                phiAverage5=mbf.average(phi5,xQP,5);
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild5,colIdx
                                                      ,phiAverage5[colIdx]*phiAverage2[rowIdx]*factor*averageFactor
                                                      + phiJump5[colIdx]*phiJump2[rowIdx]*factor*jumpFactor);
                }
            }
        }//end loop over test function lines associated to child space 2
    }//end if lgfsChild!=0
    if (lgfsChild3.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi3(lgfsChild3.size());
        lgfsChild3.finiteElement().localBasis().evaluateFunction(xQP,phi3);
        //evaluate the jump
        std::vector<RangeType> phiJump3(lgfsChild3.size());
        phiJump3=mbf.jump(phi3,xQP,3);
        //evaluate the average
        std::vector<RangeType> phiAverage3(lgfsChild3.size());
        phiAverage3=mbf.average(phi3,xQP,3);
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild3.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild3,rowIdx,lgfsChild0,colIdx
                                                  ,phiAverage0[colIdx]*phiAverage3[rowIdx]*factor*averageFactor
                                                  + phiJump0[colIdx]*phiJump3[rowIdx]*factor*jumpFactor);
            }
            if (lgfsChild1.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi1(lgfsChild1.size());
                lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi1);
                //evaluate the jump
                std::vector<RangeType> phiJump1(lgfsChild1.size());
                phiJump1=mbf.jump(phi1,xQP,1);
                //evaluate the average
                std::vector<RangeType> phiAverage1(lgfsChild1.size());
                phiAverage1=mbf.average(phi1,xQP,1);
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild1,colIdx
                                                      ,phiAverage1[colIdx]*phiAverage3[rowIdx]*factor*averageFactor
                                                      + phiJump1[colIdx]*phiJump3[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild2.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi2(lgfsChild2.size());
                lgfsChild2.finiteElement().localBasis().evaluateFunction(xQP,phi2);
                //evaluate the jump
                std::vector<RangeType> phiJump2(lgfsChild2.size());
                phiJump2=mbf.jump(phi2,xQP,2);
                //evaluate the average
                std::vector<RangeType> phiAverage2(lgfsChild2.size());
                phiAverage2=mbf.average(phi2,xQP,2);
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild2,colIdx
                                                      ,phiAverage2[colIdx]*phiAverage3[rowIdx]*factor*averageFactor
                                                      + phiJump2[colIdx]*phiJump3[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild3.size()!=0){
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild3,colIdx
                                                      ,phiAverage3[colIdx]*phiAverage3[rowIdx]*factor*averageFactor
                                                      + phiJump3[colIdx]*phiJump3[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild4.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi4(lgfsChild4.size());
                lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi4);
                //evaluate the jump
                std::vector<RangeType> phiJump4(lgfsChild4.size());
                phiJump4=mbf.jump(phi4,xQP,4);
                //evaluate the average
                std::vector<RangeType> phiAverage4(lgfsChild4.size());
                phiAverage4=mbf.average(phi4,xQP,4);
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild4,colIdx
                                                      ,phiAverage4[colIdx]*phiAverage3[rowIdx]*factor*averageFactor
                                                      + phiJump4[colIdx]*phiJump3[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild5.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi5(lgfsChild5.size());
                lgfsChild5.finiteElement().localBasis().evaluateFunction(xQP,phi5);
                //evaluate the jump
                std::vector<RangeType> phiJump5(lgfsChild5.size());
                phiJump5=mbf.jump(phi5,xQP,5);
                //evaluate the average
                std::vector<RangeType> phiAverage5(lgfsChild5.size());
                phiAverage5=mbf.average(phi5,xQP,5);
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild5,colIdx
                                                      ,phiAverage5[colIdx]*phiAverage3[rowIdx]*factor*averageFactor
                                                      + phiJump5[colIdx]*phiJump3[rowIdx]*factor*jumpFactor);
                }
            }
        }//end loop over test function lines associated to child space 3
    }//end if lgfsChild!=0
    if (lgfsChild4.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi4(lgfsChild4.size());
        lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi4);
        //evaluate the jump
        std::vector<RangeType> phiJump4(lgfsChild4.size());
        phiJump4=mbf.jump(phi4,xQP,4);
        //evaluate the average
        std::vector<RangeType> phiAverage4(lgfsChild4.size());
        phiAverage4=mbf.average(phi4,xQP,4);
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild4.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild4,rowIdx,lgfsChild0,colIdx
                                                  ,phiAverage0[colIdx]*phiAverage4[rowIdx]*factor*averageFactor
                                                  + phiJump0[colIdx]*phiJump4[rowIdx]*factor*jumpFactor);
            }
            if (lgfsChild1.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi1(lgfsChild1.size());
                lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi1);
                //evaluate the jump
                std::vector<RangeType> phiJump1(lgfsChild1.size());
                phiJump1=mbf.jump(phi1,xQP,1);
                //evaluate the average
                std::vector<RangeType> phiAverage1(lgfsChild1.size());
                phiAverage1=mbf.average(phi1,xQP,1);
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild1,colIdx
                                                      ,phiAverage1[colIdx]*phiAverage4[rowIdx]*factor*averageFactor
                                                      + phiJump1[colIdx]*phiJump4[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild2.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi2(lgfsChild2.size());
                lgfsChild2.finiteElement().localBasis().evaluateFunction(xQP,phi2);
                //evaluate the jump
                std::vector<RangeType> phiJump2(lgfsChild2.size());
                phiJump2=mbf.jump(phi2,xQP,2);
                //evaluate the average
                std::vector<RangeType> phiAverage2(lgfsChild2.size());
                phiAverage2=mbf.average(phi2,xQP,2);
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild2,colIdx
                                                      ,phiAverage2[colIdx]*phiAverage4[rowIdx]*factor*averageFactor
                                                      + phiJump2[colIdx]*phiJump4[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild3.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi3(lgfsChild3.size());
                lgfsChild3.finiteElement().localBasis().evaluateFunction(xQP,phi3);
                //evaluate the jump
                std::vector<RangeType> phiJump3(lgfsChild3.size());
                phiJump3=mbf.jump(phi3,xQP,3);
                //evaluate the average
                std::vector<RangeType> phiAverage3(lgfsChild3.size());
                phiAverage3=mbf.average(phi3,xQP,3);
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild3,colIdx
                                                      ,phiAverage3[colIdx]*phiAverage4[rowIdx]*factor*averageFactor
                                                      + phiJump3[colIdx]*phiJump4[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild4.size()!=0){
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild4,colIdx
                                                      ,phiAverage4[colIdx]*phiAverage4[rowIdx]*factor*averageFactor
                                                      + phiJump4[colIdx]*phiJump4[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild5.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi5(lgfsChild5.size());
                lgfsChild5.finiteElement().localBasis().evaluateFunction(xQP,phi5);
                //evaluate the jump
                std::vector<RangeType> phiJump5(lgfsChild5.size());
                phiJump5=mbf.jump(phi5,xQP,5);
                //evaluate the average
                std::vector<RangeType> phiAverage5(lgfsChild5.size());
                phiAverage5=mbf.average(phi5,xQP,5);
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild5,colIdx
                                                      ,phiAverage5[colIdx]*phiAverage4[rowIdx]*factor*averageFactor
                                                      + phiJump5[colIdx]*phiJump4[rowIdx]*factor*jumpFactor);
                }
            }
        }//end loop over test function lines associated to child space 4
    }//end if lgfsChild!=0
    if (lgfsChild5.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi5(lgfsChild5.size());
        lgfsChild5.finiteElement().localBasis().evaluateFunction(xQP,phi5);
        //evaluate the jump
        std::vector<RangeType> phiJump5(lgfsChild5.size());
        phiJump5=mbf.jump(phi5,xQP,5);
        //evaluate the average
        std::vector<RangeType> phiAverage5(lgfsChild5.size());
        phiAverage5=mbf.average(phi5,xQP,5);
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild5.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild5,rowIdx,lgfsChild0,colIdx
                                                  ,phiAverage0[colIdx]*phiAverage5[rowIdx]*factor*averageFactor
                                                  + phiJump0[colIdx]*phiJump5[rowIdx]*factor*jumpFactor);
            }
            if (lgfsChild1.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi1(lgfsChild1.size());
                lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi1);
                //evaluate the jump
                std::vector<RangeType> phiJump1(lgfsChild1.size());
                phiJump1=mbf.jump(phi1,xQP,1);
                //evaluate the average
                std::vector<RangeType> phiAverage1(lgfsChild1.size());
                phiAverage1=mbf.average(phi1,xQP,1);
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild1,colIdx
                                                      ,phiAverage1[colIdx]*phiAverage5[rowIdx]*factor*averageFactor
                                                      + phiJump1[colIdx]*phiJump5[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild2.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi2(lgfsChild2.size());
                lgfsChild2.finiteElement().localBasis().evaluateFunction(xQP,phi2);
                //evaluate the jump
                std::vector<RangeType> phiJump2(lgfsChild2.size());
                phiJump2=mbf.jump(phi2,xQP,2);
                //evaluate the average
                std::vector<RangeType> phiAverage2(lgfsChild2.size());
                phiAverage2=mbf.average(phi2,xQP,2);
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild2,colIdx
                                                      ,phiAverage2[colIdx]*phiAverage5[rowIdx]*factor*averageFactor
                                                      + phiJump2[colIdx]*phiJump5[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild3.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi3(lgfsChild3.size());
                lgfsChild3.finiteElement().localBasis().evaluateFunction(xQP,phi3);
                //evaluate the jump
                std::vector<RangeType> phiJump3(lgfsChild3.size());
                phiJump3=mbf.jump(phi3,xQP,3);
                //evaluate the average
                std::vector<RangeType> phiAverage3(lgfsChild3.size());
                phiAverage3=mbf.average(phi3,xQP,3);
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild3,colIdx
                                                      ,phiAverage3[colIdx]*phiAverage5[rowIdx]*factor*averageFactor
                                                      + phiJump3[colIdx]*phiJump5[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild4.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi4(lgfsChild4.size());
                lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi4);
                //evaluate the jump
                std::vector<RangeType> phiJump4(lgfsChild4.size());
                phiJump4=mbf.jump(phi4,xQP,4);
                //evaluate the average
                std::vector<RangeType> phiAverage4(lgfsChild4.size());
                phiAverage4=mbf.average(phi4,xQP,4);
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild4,colIdx
                                                      ,phiAverage4[colIdx]*phiAverage5[rowIdx]*factor*averageFactor
                                                      + phiJump4[colIdx]*phiJump5[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild5.size()!=0){
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild5,colIdx
                                                      ,phiAverage5[colIdx]*phiAverage5[rowIdx]*factor*averageFactor
                                                      + phiJump5[colIdx]*phiJump5[rowIdx]*factor*jumpFactor);
                }
            }
        }//end loop over test function lines associated to child space 5
    }//end if lgfsChild!=0




}//end accumulateMatrixJacobianCouplingTerm

//template<typename LFSU, typename LGFSChild>
//LGFSChild extractLgfsChild(const LFSU &lfsu) const {
//    const LGFSChild &lgfsChild = lfsu.template child<enrichmentLevel_>();
//    return (lgfsChild);
//}
//template<typename Scalar, typename X, typename LGFS, typename FV, typename MBF>
//Scalar evaluate_phi(const X& x, const LGFS &lgfs, const FV &xQP, const MBF &mbf, const int &spaceNumber) const{
//
//    // evaluate basis functions on reference element
//    typedef typename LGFS::Traits::FiniteElementType::
//            Traits::LocalBasisType::Traits::RangeType RangeType;
//    typedef typename LGFS::Traits::SizeType sizeType;
//    std::vector<RangeType> phi(lgfs.size());
//    lgfs.finiteElement().localBasis().evaluateFunction(xQP,phi);
//    //modify basis functions
//    std::vector<RangeType> phiModified=mbf.modify(phi,xQP,spaceNumber);
//    // compute u_base, u_enriched at integration point
//    return (phi);
//  }
//template<typename Scalar, typename X, typename LGFS, typename Phi>
//Scalar evaluate_u(const X& x, const LGFS &lgfs, const Phi &phi) const{
//    typedef typename LGFS::Traits::SizeType sizeType;
//    // compute u_base, u_enriched at integration point
//    Scalar u=0.0;
//    for (sizeType i=0; i<lgfs.size(); i++){
//        u += x(lgfs,i)*phi[i];// localIndex() maps dof within leaf space to all dofs within given element
//    }
//    return (u);
//  }

//// boundary integral
//// for Neumann b.c.
//template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
//void alpha_boundary (const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s) const
//
//{
//  // select the two components (assume Galerkin scheme U=V)
//  typedef typename LFSU::template Child<enrichmentLevel_>::Type LBGFS;       // LBGFS is the base GFS
//  const LBGFS& lbgfs_s = lfsu_s.template child<enrichmentLevel_>();
//    typedef typename LFSU::template Child<1>::Type LEGFS0;       // LEGFS0 is the enriched GFS
//    const LEGFS0& legfs0_s = lfsu_s.template child<1>();
//
//  typedef typename B::template Child<enrichmentLevel_>::Type BCTypeNormal;
//  const BCTypeNormal& bctypeNormal= b_.template child<enrichmentLevel_>();
//  typedef typename B::template Child<1>::Type BCTypeEnriched;
//  const BCTypeEnriched& bctypeEnriched= b_.template child<1>();
//
//  // domain and range field type (assume both components have same Scalar)
//  typedef typename LBGFS::Traits::FiniteElementType::
//          Traits::LocalBasisType::Traits::DomainType FV;
//  typedef typename LBGFS::Traits::FiniteElementType::
//          Traits::LocalBasisType::Traits::RangeFieldType Scalar;
//  typedef typename LBGFS::Traits::FiniteElementType::
//          Traits::LocalBasisType::Traits::JacobianType JacobianType;
//  typedef typename LBGFS::Traits::FiniteElementType::
//          Traits::LocalBasisType::Traits::RangeType RangeType;
//  typedef typename LFSU::Traits::SizeType sizeType;
//
//  // dimensions
//  const int dim_ = IG::dimension;
//
////    typedef Dumux::modifiedBasisFunction<MultiDomainGrid, IG, IntersectionPointsMap, LFSU> MBF;
////    MBF mbf(multiDomainGrid_, ig, intersectionPointsMapLocal_, lfsu_s, level); TODO this does not work because MBF needs the element and not the intersection.
//
//  // select quadrature rule for face
//  Dune::GeometryType gtface = ig.geometryInInside().type();
//  const Dune::QuadratureRule<ctype,dim_-1>&
//  rule = Dune::QuadratureRules<ctype,dim_-1>::rule(gtface,intorder_);
//
//  // loop over quadrature points and integrate normal flux
//  for (typename Dune::QuadratureRule<ctype,dim_-1>::const_iterator it=rule.begin();it!=rule.end(); ++it)
//  {
//      // position of quadrature point in local coordinates of element
//      Dune::FieldVector<ctype,dim_> local = ig.geometryInInside().global(it->position());
//      // skip rest if we are on Dirichlet boundary
//      //TODO: here it is assumed, that if there is a Neumann boundary for the standard dofs, there is also a boundary for the enriched.
//      //if this is not true, you have to use bctypeEnriched to check!
//      if (bctypeNormal.isNeumann(ig,it->position())) continue;
//        // evaluate basis functions at integration point
//        std::vector<RangeType> phiBase(lbgfs_s.size());
//        lbgfs_s.finiteElement().localBasis().evaluateFunction(local,phiBase);
//        std::vector<RangeType> phiEnriched(legfs0_s.size());
//        legfs0_s.finiteElement().localBasis().evaluateFunction(local,phiEnriched);
//
//      phiBase=mbf.phiModified(phiBase);
//      phiEnriched=mbf.phiEnrichedModified(phiEnriched);
//
//        // evaluate flux boundary condition
//        Dune::FieldVector<ctype,dim_>
//        globalpos = ig.geometry().global(it->position());
//        Scalar j;
//        //if (globalpos[1]<0.5) j = 1.0; else j = -1.0; // some outflow
//        j=0.0; //homogeneous Neumann
//        // integrate j
//        Scalar factor = it->weight()*ig.geometry().integrationElement(it->position());
//        for (sizeType i=0; i<lbgfs_s.size(); i++)
//              r_s.accumulate(lbgfs_s,i, j*phiBase[i]*factor);
//        for (sizeType i=0; i<legfs0_s.size(); i++)
//              r_s.accumulate(legfs0_s,i, j*phiEnriched[i]*factor);
//  }//end quadrature point loop
//}//end alpha boundary


/*
* extract soil parameters
* alphaF depends on the fracture element
*/
template<typename Scalar, typename Entity>
const Scalar alphaF(const Entity &entity) const{
    return (soil_.alphaF(entity));//a large alphaF means high permeability normal through the fracture and small fracture width
}

typedef Dune::GeometryType GT;
typedef typename MultiDomainGrid::Traits::GlobalIdSet::IdType IdType;
//here it is assumed that the grid dimension is unique, i.e., elements of different dimensions are not allowed
static const int dim_=MultiDomainGrid::dimension;
static const int dimw_=MultiDomainGrid::dimensionworld;

private:
  const B &b_;
  const MultiDomainGrid &multiDomainGrid_;
  const FractureGrid &fractureGrid_;
  IntersectionPointsMap &intersectionPointsMapLocal_;
  CouplingMap &couplingMap_;
protected:
  const Soil &soil_;
  const typename MultiDomainGrid::ctype xi_;
private:
  const SolutionCase &solutionCase_;
  const unsigned int intorder_;
};

#endif /* MATRIXLOCALOPERATORENRICHEDANALYTICAL_HH_ */
