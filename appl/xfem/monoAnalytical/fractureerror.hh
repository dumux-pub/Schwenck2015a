#ifndef FRACTUREERROR_HH_
#define FRACTUREERROR_HH_

namespace Dumux{

template<typename FractureGrid, typename FGFS, typename X, typename AnalyticalVelocitySolution>
class FractureError{

    typedef typename FractureGrid::LeafGridView GV;
    typedef typename FractureGrid::ctype ctype;
    typedef typename GV::template Codim<0>::Iterator FractureGridIterator;
    typedef typename Dune::PDELab::LocalFunctionSpace<FGFS> LFSU;

    typedef typename LFSU::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::DomainType FVLocal;
    typedef typename LFSU::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::RangeFieldType Scalar;
    typedef typename LFSU::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::RangeType R;
    typedef typename LFSU::Traits::SizeType sizeType;
    typedef typename LFSU::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::JacobianType JacobianType;

    typedef Dune::PDELab::LFSIndexCache<LFSU> FLFSCache;
    typedef typename X::template LocalView<FLFSCache> XView;
    typedef typename X::template ConstLocalView<FLFSCache> ConstXView;

public:
FractureError(const FractureGrid& fractureGrid, const FGFS& fgfs,const X& x, const AnalyticalVelocitySolution &analyticalVelocitySolution) //x FE solution
    : fractureGrid_(fractureGrid), fgfs_(fgfs), x_(x), analyticalVelocitySolution_(analyticalVelocitySolution)
    , lfsu_(fgfs)
{}

template<typename Soil>
Scalar evaluateVelocity(const Soil &soil) {

    typedef Dune::FieldVector<ctype,dimw_> FVGlobal;
//    typedef Dune::FieldVector<R,dim_> GradPhi;
    typedef typename Dune::FieldVector<Scalar,dim_> GradP;
    typedef std::vector<FVLocal> GradPhi;
    R error(0.0);
    R denominator(0.0);

    FLFSCache lfs_cache_(lfsu_);
    ConstXView x_view_(x_);

FractureGridIterator eendit = fractureGrid_.template leafend<0>();
for (FractureGridIterator elementIterator = fractureGrid_.template leafbegin<0>(); elementIterator != eendit; ++elementIterator) {



    R u_fe(0.0);//TODO check what happens if u_fe is returned without a proper value (u_fe=0.0)?

    lfsu_.bind(*elementIterator);

    std::vector<R> xl(lfsu_.size());        // local coefficients
    lfs_cache_.update();
    x_view_.bind(lfs_cache_);
    x_view_.read(xl);// get local coefficients of the solution

        FVGlobal globalElementCenter = elementIterator->geometry().center();
        FVLocal localElementCenter=elementIterator->geometry().local(globalElementCenter);

        // evaluate gradient of basis functions on reference element
        std::vector<JacobianType> js(lfsu_.size());
        lfsu_.finiteElement().localBasis().evaluateJacobian(localElementCenter,js);

        // transform gradients from reference element to real element, but in fracture coordinates (i.e. adjusting only the length)
        R jac= elementIterator->geometry().integrationElement(localElementCenter);//TODO integrationElement is constant at every integration point. so it could be placed outside this loop
//        const Dune::FieldMatrix<DF,dimw_,dim_> jac2 = elementIterator->geometry().jacobianInverseTransposed(localElementCenter);
        GradPhi gradphif(lfsu_.size());//lfsu.size()=2 for 1D line elements here
        for (sizeType i=0; i<lfsu_.size(); i++)
        {
            gradphif[i]=js[i][0]/jac;//TODO: Only 1D, re-think for planes in 3D environment
        }

//            // transform gradients from reference element to real element
//            std::vector<Dune::FieldVector<Scalar,dimw_> > gradphiWorld(lfsu_.size());
//            for (sizeType i=0; i<lfsu_.size(); i++)
//            {jac2.mv(js[i][0],gradphiWorld[i]);
//            std::cout << "gradPhiWorld: " <<gradphiWorld[i]<<std::endl; }

        // compute gradient of pf
        GradP gradP(0.0);
        for (sizeType i=0; i<lfsu_.size(); i++){
            gradP.axpy(xl[lfsu_.localIndex(i)],gradphif[i]);
        }
        const Scalar KFT=soil.kFT(*elementIterator);
        const Scalar KFN=soil.kFN(*elementIterator  );
        const Scalar d=soil.fractureWidth(*elementIterator);
        R u(0.0);
        u[0]=-1.0*KFT*gradP;//tangential part of the velocity in the fracture
        typename AnalyticalVelocitySolution::Traits::RangeType analSol(0.0);
        analyticalVelocitySolution_.evaluateGlobal(globalElementCenter,analSol);
//        std::cout << "analSol: " << analSol << "\t u: " << u<< std::endl;
        denominator+= analSol.two_norm2()*elementIterator->geometry().integrationElement(localElementCenter)*d;

        analSol-=u;
        error+= analSol.two_norm2()* elementIterator->geometry().integrationElement(localElementCenter)*d;

//        std::cout << "error: "<< error << "\t den: "<<  denominator <<std::endl;
}//end loop over all elements

return (std::sqrt(error/denominator));

}//end evaluate velocities function

private:
    const FractureGrid &fractureGrid_;
    const FGFS &fgfs_;
    const X &x_;
    const AnalyticalVelocitySolution &analyticalVelocitySolution_;
    LFSU lfsu_;

    static const int dimw_=FractureGrid::dimensionworld;
    static const int dim_=FractureGrid::dimension;


};//end class FractureError
}//end namespace Dumux
#endif /* FRACTUREERROR_HH_ */
