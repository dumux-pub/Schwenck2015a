int main(int argc, char** argv)
{
#ifdef NDEBUG
  try{
#endif
    //Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
    if(Dune::MPIHelper::isFake)
      std::cout<< "This is a sequential program." << std::endl;
    else
	  {
		if(helper.rank()==0)
		  std::cout << "parallel run on " << helper.size() << " process(es)" << std::endl;
	  }
	if (argc!=1)
	  {
		if(helper.rank()==0)
		  std::cout << "usage: ./FNcoupled" << std::endl;
		return (1);
	  }
    // sequential version
    if (helper.size()==1)
    {
        typedef double Scalar;
//        typedef quad Scalar;
        Dune::Timer timer;
        Dune::Timer totalTimer;
        const int worlddim=2;
        const int fracdim=worlddim-1;
        timer.reset();

        //set up parameter tree
        std::string parameterInputFileName="parameter.input";
        typedef typename Dune::ParameterTree ParameterTree;
        ParameterTree inputParameters_;
        Dune::ParameterTreeParser::readINITree(parameterInputFileName, inputParameters_, false);
        inputParameters_.report();
//        char simulationName =inputParameters_.get<char>("Grid.simulationName");

        //Create the host grid
//        typedef Dune::YaspGrid<worlddim> HostGrid;
        typedef Dune::UGGrid<worlddim> HostGrid;
        typedef Dune::GridPtr<HostGrid> HostGridPointer;
        HostGridPointer hostgrid;

        bool dgfGrid_=false;
        if ( inputParameters_.hasKey("Grid.dgfFileName") ){
            dgfGrid_ =true;
            std::cout << "Reading from DGF file..." << std::endl;
        }
        if (dgfGrid_) {
            std::string dgfFileName_ = inputParameters_.get< std::string> ("Grid.dgfFileName");
            hostgrid = HostGridPointer(dgfFileName_.c_str());
            int elementsMatrix = inputParameters_.get<int>("Grid.elementsMatrix");
            hostgrid->globalRefine(elementsMatrix);
        }//end if create grid from dgf file
        else {//do not use a dgf file but instead create a unit cube grid
            std::cout << "Creating the unit cube background grid..." << std::endl;
            Dune::FieldVector<Scalar,worlddim> upperRight;
            Dune::FieldVector<int,worlddim> res; // cell resolution

            //set unit square/cube
            int elementsMatrix = inputParameters_.get<int>("Grid.elementsMatrix");
//            int elementsMatrix = inputParameters_.get<int>("Grid.elementsFracture");
//            --elementsMatrix;
            elementsMatrix=std::pow(2,elementsMatrix);
            ++elementsMatrix;
            for (int i=0; i<worlddim;i++)
            {upperRight[i] = 1.0;
            res[i]        = elementsMatrix;
            }
		upperRight[1]=0.1;
		res[1]=res[1]/10;
		if (!(res[1] &1))
		res[1]+=1;
            CreateGrid<HostGrid, Scalar> createGrid;
            hostgrid= createGrid.create(upperRight, res);
        }//end create unit cube grid

        typedef int SolutionCase;
        SolutionCase solutionCase=0;
        if ( inputParameters_.hasKey("solutionCase") ){
            solutionCase = inputParameters_.get<SolutionCase>("solutionCase");
        };

        //create the global boundary
        typedef typename Dumux::globalBoundary<HostGrid> GB;
        GB globalBoundary(*hostgrid);

       //create codimension one fracture grid
       typedef Dune::AlbertaGrid< fracdim,worlddim > FractureGrid;
       std::string artFileName_ = inputParameters_.get< std::string> ("Grid.artFileName");
       Dumux::artReader<FractureGrid> myARTgeometry(artFileName_.c_str());
       timer.reset();
       myARTgeometry.edgeColoring();
       Scalar timing = timer.elapsed();
       std::cout << "=== edge coloring: " << timing << " s" << std::endl;
       timer.reset();
       typedef Dune::GridPtr<FractureGrid> GridPointerFracture;
       std::string simulationNameString = inputParameters_.get<std::string>("Grid.simulationName");
       GridPointerFracture fractureGridPointer= myARTgeometry.createGrid(simulationNameString);
       FractureGrid &fractureGrid= *fractureGridPointer;
       typedef FractureGrid::LeafGridView FGV;
       typedef typename FGV::IndexSet::IndexType FractureGridIdxType;

       typedef typename FractureGrid::Traits::GlobalIdSet::IdType FractureGridIdType;
       typedef typename std::map<FractureGridIdType,int> FractureNetworkCouplingMap;
       FractureNetworkCouplingMap fractureNetworkCouplingMap=myARTgeometry.fractureNetworkCouplingMap(&fractureGridPointer,globalBoundary);

       /*
        * Grid Coupling:
        * computes the intersections between refined host grid and not refined fracture grid
        */
       typedef typename Dumux::gridCoupling<HostGrid,GridPointerFracture> GridCoupling;
       typedef typename GridCoupling::MultiDomainGrid MultiDomainGrid;
       GridCoupling gridCoupling(*hostgrid,&fractureGridPointer);
       GridCoupling::MultiDomainGrid multiDomainGrid(*hostgrid);
       gridCoupling.createMultiDomainGrid(multiDomainGrid,fractureNetworkCouplingMap);
       timing = timer.elapsed();
       std::cout << "=== grid creation: " << timing << " s" << std::endl;
       timer.reset();
       typedef typename GridCoupling::IntersectionPointsMap IntersectionPointsMap;
       IntersectionPointsMap intersectionPointsMap=gridCoupling.getIntersectionPointsMap();
       typedef typename GridCoupling::CouplingMap CouplingMap;
       typedef Dumux::ReferenceProperties<ParameterTree,Scalar> ReferenceProperties;
       ReferenceProperties reference(inputParameters_);
       typedef Dumux::SoilAnalytical<ParameterTree, ReferenceProperties,GridPointerFracture,HostGridPointer> Soil;
       Soil soil(inputParameters_, reference,&fractureGridPointer,&hostgrid);

       /*
        * refine the fracture grid (globally, but skip virtual elements)
		* even though there are no virtual elements in the analytical case.
        */
       int levelFracture = inputParameters_.get<int>("Grid.elementsFracture");
       FGV fractureGridView = fractureGrid.leafView();
       typedef typename FGV::Codim<0>::Iterator ElementLeafIterator;
       for ( ElementLeafIterator it = fractureGridView.begin<0>();
       it != fractureGridView.end<0>(); ++it ){
           if (soil.angle(*it)<-0.5) fractureGrid.mark(levelFracture,*it);
       }
       fractureGrid.preAdapt();
       fractureGrid.adapt();
       fractureGrid.postAdapt();

       gridCoupling.createFractureMatrixCoupling(multiDomainGrid);
       CouplingMap couplingMap=gridCoupling.getCouplingMap();

       typedef typename std::map<FractureGridIdxType,int> DuneIdxMap;
       DuneIdxMap matrixManipulationMap=myARTgeometry.gridCouplingMapToIdx(fractureGrid,fractureNetworkCouplingMap);

       timing = timer.elapsed();
       std::cout << "=== grid coupling " << timing << " s" << std::endl;
       timer.reset();
       typedef typename MultiDomainGrid::LeafGridView HGV;
       HGV hostGridView=multiDomainGrid.leafView();
       typedef typename MultiDomainGrid::ctype ctype;

       typedef typename MultiDomainGrid::SubDomainGrid SubDomainGrid;
       typedef typename SubDomainGrid::LeafGridView EGV;
       SubDomainGrid& sdg0 = multiDomainGrid.subDomain(0);
       SubDomainGrid& sdg1 = multiDomainGrid.subDomain(1);
       SubDomainGrid& sdg2 = multiDomainGrid.subDomain(2);
       SubDomainGrid& sdg3 = multiDomainGrid.subDomain(3);
       SubDomainGrid& sdg4 = multiDomainGrid.subDomain(4);

       EGV enrichedGridView0 = sdg0.leafView();
       EGV enrichedGridView1 = sdg1.leafView();
       EGV enrichedGridView2 = sdg2.leafView();
       EGV enrichedGridView3 = sdg3.leafView();
       EGV enrichedGridView4 = sdg4.leafView();

       std::cout << "number of elements in enriched space 0: " << enrichedGridView0.size(0) << std::endl;
       std::cout << "number of elements in enriched space 1: " << enrichedGridView1.size(0) << std::endl;
       std::cout << "number of elements in enriched space 2: " << enrichedGridView2.size(0) << std::endl;
       std::cout << "number of elements in enriched space 3: " << enrichedGridView3.size(0) << std::endl;
       std::cout << "number of elements in enriched space 4: " << enrichedGridView4.size(0) << std::endl;
       //TODO optimize the code for the cases where the size of some of the enriched spaces is zero!

       typedef typename FractureGrid::ctype fctype;

       typedef Dune::PDELab::Q1LocalFiniteElementMap<ctype,Scalar,worlddim> FEM;
       FEM fem;
       typedef Dune::PDELab::Q1LocalFiniteElementMap<fctype,Scalar,fracdim> FFEM;
       FFEM ffem;

       typedef Dune::PDELab::ConformingDirichletConstraints CON;     // constraints class
       CON con;

       typedef Dune::PDELab::ISTLVectorBackend<> VBE;

       //host- and enriched matrix grid functions spaces
       typedef Dune::PDELab::GridFunctionSpace<HGV,FEM,CON,VBE> HGFS;
       HGFS hgfs(hostGridView,fem,con);
       typedef Dune::PDELab::GridFunctionSpace<EGV,FEM,CON,VBE> EGFS;
       EGFS egfs0(enrichedGridView0,fem,con);
       EGFS egfs1(enrichedGridView1,fem,con);
       EGFS egfs2(enrichedGridView2,fem,con);
       EGFS egfs3(enrichedGridView3,fem,con);
       EGFS egfs4(enrichedGridView4,fem,con);

       typedef Dune::PDELab::GridFunctionSpace<FGV,FFEM,CON,VBE> FGFS;
       FGFS fgfs(fractureGridView,ffem,con);

       typedef Dune::PDELab::MultiDomain::MultiDomainGridFunctionSpace<
               MultiDomainGrid,VBE,Dune::PDELab::LexicographicOrderingTag,HGFS,EGFS,EGFS,EGFS,EGFS,EGFS> MultiGFS;//EntityBlockedOrderingTag
       MultiGFS multigfs(multiDomainGrid,hgfs,egfs0,egfs1,egfs2,egfs3,egfs4); //normal, normal dofs on enriched elements, enriched dofs
       multigfs.name("string");//TODO
       multigfs.ordering();
       typedef BCTypeAnalytical  FixDBCType;
       typedef EmptyBCType FreeDBCType;
       FixDBCType fixDBC;
       FreeDBCType freeDBC;
       // next two lines make sure that the the enriched dofs at the boundary are not fixed
//       typedef Dune::PDELab::CompositeConstraintsParameters<FixDBCType,FreeDBCType,FreeDBCType,FreeDBCType,FreeDBCType,FreeDBCType> CompositeBCType;
//       CompositeBCType compositeBC(fixDBC,freeDBC,freeDBC,freeDBC,freeDBC,freeDBC);
       //fix the enriched dofs next 2 lines
       typedef Dune::PDELab::CompositeConstraintsParameters<FixDBCType,FixDBCType,FixDBCType,FixDBCType,FixDBCType,FixDBCType> CompositeBCType;
       CompositeBCType compositeBC(fixDBC,fixDBC,fixDBC,fixDBC,fixDBC,fixDBC);

       typedef Dune::PDELab::ISTLMatrixBackend MBE;


       typedef AnalyticalVelocitySolutionMatrix<HGV,Soil,Scalar,SolutionCase> AnalVSolFunctionMatrix;
       AnalVSolFunctionMatrix analVSolFunctionMatrix(hostGridView,soil,solutionCase);
       typedef AnalyticalPressureSolutionMatrix<HGV,Soil,Scalar,SolutionCase> AnalPSolFunctionMatrix;
       AnalPSolFunctionMatrix analPSolFunctionMatrix(hostGridView,soil,solutionCase);
       typedef AnalyticalPressureSolutionFracture<FGV,Soil,Scalar,SolutionCase> AnalPSolFunctionFracture;
       AnalPSolFunctionFracture analPSolFunctionFracture(fractureGridView,soil,solutionCase);
       typedef AnalyticalVelocitySolutionFracture<FGV,Soil,Scalar,SolutionCase> AnalVSolFunctionFracture;
       AnalVSolFunctionFracture analVSolFunctionFracture(fractureGridView,soil,solutionCase);

       typedef FractureBCTypeAnalytical<GB> FBType;
       typedef typename FGFS::ConstraintsContainer<Scalar>::Type FCC;
       typedef BCDirichletFractureAnalytical<FGV,Scalar,AnalPSolFunctionFracture> FBCDirichlet;

       typedef MatrixLocalOperatorAnalytical<FixDBCType, Soil, SolutionCase> LOP;               // operator including boundary
       //SubDomainEqualityCondition is true if the subdomain subset given is exactly the same as exists on the element
       typedef Dune::PDELab::MultiDomain::SubDomainEqualityCondition<MultiDomainGrid> BaseCondition;
       typedef Dune::PDELab::MultiDomain::SubDomainSubsetCondition<MultiDomainGrid> EnrichedCondition;
       typedef Dune::PDELab::MultiDomain::SubProblem<MultiGFS,MultiGFS,LOP,BaseCondition,0> BaseProblem;//at the end the indices of the GFS as defined in MultiGFS are given
       typedef typename MultiGFS::ConstraintsContainer<Scalar>::Type CC;
       typedef BCDirichletAnalytical<HGV,Scalar,AnalPSolFunctionMatrix> MBCDirichlet;
       typedef BCDirichletEnrichedAnalytical<HGV,Soil,Scalar,AnalPSolFunctionMatrix> BCDirichletEnriched;
       typedef Dune::PDELab::CompositeGridFunction<MBCDirichlet,BCDirichletEnriched,BCDirichletEnriched,BCDirichletEnriched,BCDirichletEnriched,BCDirichletEnriched> BCD;

       typedef MatrixLocalOperatorEnrichedAnalytical<CompositeBCType,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil,SolutionCase> LOPE;               // operator including boundary

       typedef Dune::PDELab::MultiDomain::SubProblem<MultiGFS,MultiGFS,LOPE,EnrichedCondition,0,1,2,3,4,5> EnrichedProblem;//at the end the indices of the GFS as defined in MultiGFS are given
       typedef Dune::PDELab::MultiDomain::GridOperator<MultiGFS,MultiGFS,MBE,Scalar,Scalar,Scalar,CC,CC,BaseProblem,EnrichedProblem> MultiGO;
       typedef typename MultiGO::Traits::Domain V;

       //v are the interpolated Dirichlet conditions ! TODO DOC ME!
       V v(multigfs,0.0);


       timing = timer.elapsed();
       std::cout << "=== preparation: " << timing << " s" << std::endl;
       timer.reset();

       /* ***********************************************************************
            matrix problem
        *********************************************************************** */

       LOP lop(fixDBC,soil,solutionCase);
       LOPE lope(compositeBC,multiDomainGrid,fractureGrid,intersectionPointsMap,couplingMap,soil,solutionCase);//additionally the integration order can be given as last argument

       EnrichedCondition cE(0,1,2,3,4);//cE is true if element is in at least one of the subdomains.
       BaseCondition c; //c is true, if there is no subdomain: needed for base operator
       BaseProblem bp(lop,c);
       EnrichedProblem ep(lope,cE);

       CC cc;
       cc.clear();

       auto constraints = Dune::PDELab::MultiDomain::constraints<Scalar>(multigfs,
               Dune::PDELab::MultiDomain::constrainSubProblem(bp,fixDBC)
               ,Dune::PDELab::MultiDomain::constrainSubProblem(ep,compositeBC)
       );
       constraints.assemble(cc);

       MBCDirichlet bcdirichlet(hostGridView,analPSolFunctionMatrix); //evaluates the value of the global Dirichlet boundary (also only on the HGV)
       BCDirichletEnriched bcdirichletenriched(hostGridView,soil,analPSolFunctionMatrix); //evaluates the value of the global Dirichlet boundary (also only on the HGV)
       BCD bcd(bcdirichlet,bcdirichletenriched,bcdirichletenriched,bcdirichletenriched,bcdirichletenriched,bcdirichletenriched);

       v*=0.0;
       Dune::PDELab::MultiDomain::interpolateOnTrialSpace(multigfs,v,bcdirichlet,bp,bcd,ep);//interpolate for both problems

       MultiGO multigo(multigfs,multigfs,cc,cc,bp,ep);

       typedef typename MultiGO::Traits::Jacobian GMatrix;
       GMatrix gMatrix(multigo);
       gMatrix = 0.0;

       timing = timer.elapsed();
       std::cout << "=== assembling matrix problem: " << timing << " s" << std::endl;
       timer.reset();

       /* ***********************************************************************
            fracture problem
        *********************************************************************** */
       typedef FractureLocalOperatorAnalytical<FBType,Soil,SolutionCase> LOPF;
       typedef Dune::PDELab::GridOperator<FGFS,FGFS,LOPF,MBE,Scalar,Scalar,Scalar,FCC,FCC> FGO;
       typedef typename FGO::Traits::Domain VF;
       VF vf(fgfs,0.0);
       FBType fbtype(globalBoundary);
       LOPF lopf(fbtype,soil,solutionCase);// fracture operator including boundary

       FCC fcc;
       fcc.clear();
       Dune::PDELab::constraints(fbtype,fgfs,fcc,false);//last argument is verbosity
       FBCDirichlet fbcdirichlet(fractureGridView,analPSolFunctionFracture);
       //set fracture solution vector to zero
       vf*=0.0;
       fgfs.ordering();
       Dune::PDELab::interpolate(fbcdirichlet,fgfs,vf);
       FGO fgo(fgfs,fcc,fgfs,fcc,lopf);
       //    FSLP fslp(fgo,vf,ls,1.0e-8);

       typedef typename FGO::Traits::Jacobian Mfracture;
       Mfracture gFracture(fgo);
       gFracture = 0.0;

       //Dune::writeMatrixToMatlab(gFracture.base(),"gFracture");

       timing = timer.elapsed();
       std::cout << "=== assembling fracture problem: " << timing << " s" << std::endl;
       timer.reset();
       std::cout << "=== assembling coupled problem: "<<std::endl;;

       /* ***********************************************************************
           coupling problem
        *********************************************************************** */

       typedef Dune::FieldMatrix<Scalar,1,1> M;
       typedef typename Dune::BCRSMatrix<M> ISTLM;

       typedef typename Dune::BlockVector< Dune::FieldVector<Scalar,1> >  VMonolithic;

       typedef CouplingLocalOperatorDarcy<ISTLM,VMonolithic,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,FGFS,VF,Soil,FBType> LOPC;

       typedef Dune::PDELab::MultiDomain::SubProblem<MultiGFS,MultiGFS,LOPC,EnrichedCondition,0,1,2,3,4,5> CouplingProblem;//at the end the indices of the GFS as defined in MultiGFS are given

       typedef typename MultiGFS::ConstraintsContainer<Scalar>::Type CCCoupling;
       CCCoupling cccoupling;
       cccoupling.clear();

       typedef Dune::PDELab::MultiDomain::GridOperator<MultiGFS,MultiGFS,MBE,Scalar,Scalar,Scalar,CC,CC,CouplingProblem> CouplingGO;

       /* ***********************************************************************
            assemble the monolithic system
        *********************************************************************** */

       //setting up the complete matrix

       /*
        * M is the number of columns, N is the number of rows. First argument is the number of rows, second the number of columns.
        */

       //get the sizes of the matrices
       const std::size_t gFractureNumberOfRows=gFracture.base().N();
       const std::size_t gFractureNumberOfColumns=gFracture.base().M();
       const std::size_t gMatrixNumberOfRows=gMatrix.base().N();
       const std::size_t gMatrixNumberOfColumns=gMatrix.base().M();
       const std::size_t gMonolithicNumberOfRows=gMatrixNumberOfRows+gFractureNumberOfRows;
       const std::size_t gMonolithicNumberOfColumns=gMatrixNumberOfColumns+gFractureNumberOfColumns;


       /*
        * pseudo operator only to calculate the size and position of the entries in the coupling blocks
        */
       typedef std::list<int> RowEntries;
       RowEntries empty;
       typedef std::vector<RowEntries> CouplingBlockEntries;
       CouplingBlockEntries couplingBlockEntries(gMonolithicNumberOfRows,empty);
       typedef SizeOfCouplingLOP<CouplingBlockEntries,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,FGFS,VF,Soil,FBType> SizeOfCLOP;
       SizeOfCLOP sizeOfclop(couplingBlockEntries,multiDomainGrid,fractureGrid,intersectionPointsMap,couplingMap,fgfs,vf,soil,fbtype);// matrix local coupling operator
       typedef Dune::PDELab::MultiDomain::SubProblem<MultiGFS,MultiGFS,SizeOfCLOP,EnrichedCondition,0,1,2,3,4,5> SizeOfCLOPProblem;//at the end the indices of the GFS as defined in MultiGFS are given
       typedef Dune::PDELab::MultiDomain::GridOperator<MultiGFS,MultiGFS,MBE,Scalar,Scalar,Scalar,CC,CC,SizeOfCLOPProblem> SizeOfCLOPGO;
       SizeOfCLOPProblem sizeOfCLOPProblem(sizeOfclop,cE);
       SizeOfCLOPGO sizeOfCLOPGO(multigfs,multigfs,cccoupling,cccoupling,sizeOfCLOPProblem);//give as last arguments all the subproblems
       sizeOfCLOPGO.jacobian(v,gMatrix);//This does not change gMatrix, but only calculates the row sizes and non-zero positions


       //Fix Dirichlet boundary conditions in coupling part of the monolithic matrix
//       std::cout << "constraints:" << std::endl;
       typedef typename FCC::iterator global_col_iteratorFracture;
//       std::cout << fcc.size() << " constrained fracture degrees of freedom" << std::endl;
//       for (global_col_iteratorFracture cit=fcc.begin(); cit!=fcc.end(); ++cit)
//       {
//         std::cout << (cit->first)+gMatrixNumberOfRows << std::endl;
//       }
//
       typedef typename CC::iterator global_col_iteratorMatrix;
//       std::cout << cc.size() << " constrained matrix degrees of freedom" << std::endl;
//       for (global_col_iteratorMatrix cit=cc.begin(); cit!=cc.end(); ++cit)
//       {
//         std::cout << cit->first << std::endl;
//       }

       //set the rowsize:
       ISTLM gMonolithic(gMonolithicNumberOfRows,gMonolithicNumberOfColumns,ISTLM::random);
       for (std::size_t j=0;j<gMatrixNumberOfRows;j++){//loop over rows of matrix block
           couplingBlockEntries[j].sort();
           couplingBlockEntries[j].unique();
           gMonolithic.setrowsize(j,gMatrix.base().getrowsize(j)+(couplingBlockEntries[j]).size() );//TODO this sets the coupling block to FULL entries!!!
//           std::cout << "set number of entries in row "<< j << " to old: " <<  gMatrix.base().getrowsize(j)+gFractureNumberOfColumns << "\t new: "<<(couplingBlockEntries[j]).size() << std::endl;
       }
       for (std::size_t j=gMatrixNumberOfRows;j<gMonolithicNumberOfRows;j++){//loop over rows of fracture block
           couplingBlockEntries[j].sort();
           couplingBlockEntries[j].unique();
           gMonolithic.setrowsize(j,gFracture.base().getrowsize(j-gMatrixNumberOfRows)+(couplingBlockEntries[j]).size() );//TODO this sets the coupling block to FULL entries!!!
//           std::cout << "set number of entries in row "<< j << " to: " <<  gFracture.base().getrowsize(j-gMatrixNumberOfRows)+gMatrixNumberOfColumns << "\t new: "<<(couplingBlockEntries[j]).size() << std::endl;
       }//incrementrowsize(row,increment size)

       //Dirichlet constraints
       for (global_col_iteratorMatrix cit=cc.begin(); cit!=cc.end(); ++cit)
       {
           gMonolithic.setrowsize(cit->first[0],1);
       }
       //set all matrix dofs to Dirichlet
//       for (int i=0;i<gMatrixNumberOfRows;++i)
//       {
//           gMonolithic.setrowsize(i,1);
//       }
       //standard Dirichlet constraints for the fracture dofs
       for (global_col_iteratorFracture cit=fcc.begin(); cit!=fcc.end(); ++cit)
       {
           gMonolithic.setrowsize((cit->first[0])+gMatrixNumberOfRows,1);
       }
       //set all fracture dofs to Dirichlet
//       for (int i=0;i<gFractureNumberOfRows;++i)
//       {
//           gMonolithic.setrowsize(i+gMatrixNumberOfRows,1);
//       }

       gMonolithic.endrowsizes();

       // add column indices to rows
       for (std::size_t j=0;j<gMatrixNumberOfRows;j++){//loop over rows of matrix block
           if ( gMonolithic.getrowsize(j)!=1 ){//only if this is not a Dirichlet row
               for (std::size_t k=0;k<gMatrixNumberOfColumns;k++){//loop over columns in row j
//               for (std::size_t k=0;k<gMonolithicNumberOfColumns;k++){//loop over columns in row j
//                   if (k>=gMatrixNumberOfColumns) {gMonolithic.addindex(j,k);}//TODO this sets the coupling block to FULL entries!!!
//                   else if (gMatrix.base().exists(j,k)) {gMonolithic.addindex(j,k);}
                   if (gMatrix.base().exists(j,k)) {gMonolithic.addindex(j,k);}
               }
               for (typename RowEntries::iterator it=(couplingBlockEntries[j]).begin();it!=(couplingBlockEntries[j]).end();++it ){
                   gMonolithic.addindex(j,*it);
               }
           }
           else {//for Dirichlet rows only add the main diagonal entry
               {gMonolithic.addindex(j,j);}
           }

       }
       for (std::size_t j=gMatrixNumberOfRows;j<gMonolithicNumberOfRows;j++){//loop over rows of fracture block
           if ( gMonolithic.getrowsize(j)!=1 ){//only if this is not a Dirichlet row
               for (std::size_t k=gMatrixNumberOfColumns;k<gMonolithicNumberOfColumns;k++){//loop over columns in row j
//               for (std::size_t k=0;k<gMonolithicNumberOfColumns;k++){//loop over columns in row j
//                   if (k<gMatrixNumberOfColumns) {gMonolithic.addindex(j,k);}//TODO this sets the coupling block to FULL entries!!!
//                   else if (gFracture.base().exists(j-gMatrixNumberOfRows,k-gMatrixNumberOfColumns)) {gMonolithic.addindex( j , k );}
                   if (gFracture.base().exists(j-gMatrixNumberOfRows,k-gMatrixNumberOfColumns)) {gMonolithic.addindex( j , k );}
               }
               for (typename RowEntries::iterator it=(couplingBlockEntries[j]).begin();it!=(couplingBlockEntries[j]).end();++it ){
                   gMonolithic.addindex(j,*it);
               }
           }
           else {//for Dirichlet rows only add the main diagonal entry
               {gMonolithic.addindex(j,j);}
           }
       }
       gMonolithic.endindices();

       //build the localCouplingOperator object and give the full monolithic system matrix to it and the full residual vector
       VMonolithic rMonolithic(multigo.trialGridFunctionSpace().globalSize()+fgo.trialGridFunctionSpace().globalSize());
       rMonolithic*=0.0;
       LOPC lopc(gMonolithic,rMonolithic,multiDomainGrid,fractureGrid,intersectionPointsMap,couplingMap,fgfs,vf,soil,fbtype);// matrix local coupling operator


       //set up the problem
       CouplingProblem cp(lopc,cE);

       //set up the coupling grid operator
       //TODO is it possible to construct the object without constraints (like in the pdelab documentation) if they are anyway always empty?
       CouplingGO couplinggo(multigfs,multigfs,cccoupling,cccoupling,cp);//give as last arguments all the subproblems
       multigo.jacobian(v,gMatrix);
       fgo.jacobian(vf,gFracture);

       //copy matrices
       for (std::size_t j=0;j<gMatrixNumberOfRows;j++){//loop over rows
           for (std::size_t k=0;k<gMonolithicNumberOfColumns;k++){//loop over columns in row j
               if (gMonolithic.exists(j,k)){
                   if (gMatrix.base().exists(j,k) ) {
                       gMonolithic[j][k]=(gMatrix.base())[j][k];
                   }
                   else gMonolithic[j][k]=0.0;
               }
           }
       }
       for (std::size_t j=gMatrixNumberOfRows;j<gMonolithicNumberOfRows;j++){//loop over rows
           for (std::size_t k=0;k<gMonolithicNumberOfColumns;k++){//loop over columns in row j
               if (gMonolithic.exists( j,k ) ) {
                   if(gFracture.base().exists( j-gMatrixNumberOfRows,k-gMatrixNumberOfColumns ) ){//if k<gMatrixNumberOfColumns the negative entry is not found
                       gMonolithic[j][k]=(gFracture.base())[j-gMatrixNumberOfRows][k-gMatrixNumberOfColumns];
                   }
                   else gMonolithic[j][k]=0.0;
               }
           }
       }

       /*
        * adding coupling blocks to monolithic matrix
        */
       couplinggo.jacobian(v,gMatrix);//This does not change gMatrix, but writes into gMonolithic

       typedef typename MultiGO::Traits::TrialGridFunctionSpace MultiGOTrialGridFunctionSpace;
       typedef typename Dune::PDELab::BackendVectorSelector<MultiGOTrialGridFunctionSpace,Scalar>::Type MultiGOW;
       MultiGOW rMatrix(multigo.testGridFunctionSpace(),0.0);
       multigo.residual(v,rMatrix);  // residual is additive

       typedef typename FGO::Traits::TrialGridFunctionSpace FGOTrialGridFunctionSpace;
       typedef typename Dune::PDELab::BackendVectorSelector<FGOTrialGridFunctionSpace,Scalar>::Type FGOW;
       FGOW rFracture(fgo.testGridFunctionSpace(),0.0);
       fgo.residual(vf,rFracture);  // residual is additive

       //copy residual implementation into monolithic vector
       for (int j=0;j<rMatrix.base().N();j++){
           (rMonolithic)[j]=(rMatrix.base())[j];
       }
       for (int j=0;j<rFracture.base().N();j++){
           (rMonolithic)[j+gMatrixNumberOfRows]=(rFracture.base())[j];
       }

       /*
        * adding coupling parts to monolithic residual vector
        * and taking care of Dirichlet boundaries in residual vector.
        * Remark: In contrast to standard local operators, Dirichlet boundaries in the coupling local
        *   operator in the standard alpha volume method to compute the residual are NOT considered.
        *   The jacobian volume however respects the Dirichlet boundaries directly!
        *
        * The residual is set to zero because the entry should be (u_initial - u_Dirichlet) -
        * u_initial is chosen as the interpolation of the Dirichlet boundaries into the domain.
        */
       couplinggo.residual(v,rMatrix);//This does not change v or rMatrix, but writes into rMonolithic

       for (global_col_iteratorMatrix cit=cc.begin(); cit!=cc.end(); ++cit)
       {
           rMonolithic[cit->first[0]]*=0.0;
       }
//       set all matrix dofs to Dirichlet
//       for (int i=0;i<gMatrixNumberOfRows;++i)
//       {
//           rMonolithic[i]*=0.0;
//       }
       //standard Dirichlet constraints for the fracture dofs
       for (global_col_iteratorFracture cit=fcc.begin(); cit!=fcc.end(); ++cit)
       {
           rMonolithic[(cit->first[0])+gMatrixNumberOfRows]=0.0;
       }
//       //set all fracture dofs Dirichlet
//       for (int i=0;i<gFractureNumberOfRows;++i)
//       {
//           rMonolithic[i+gMatrixNumberOfRows]=0.0;
//       }

       /*
        * Newton stuff, TODO doc me!
        */
       VMonolithic zMonolithic(multigo.trialGridFunctionSpace().globalSize()+fgo.trialGridFunctionSpace().globalSize());
       zMonolithic*=0.0;

       //copy vMatrix and vFracture into global vMonolithic
       VMonolithic vMonolithic(multigo.trialGridFunctionSpace().globalSize()+fgo.trialGridFunctionSpace().globalSize());
       for (int j=0;j<v.base().N();j++){//loop over entries of v
           (vMonolithic)[j]=(v.base())[j];
       }
       for (int j=0;j<vf.base().N();j++){//loop over entries of vf
           (vMonolithic)[j+gMatrixNumberOfRows]=(vf.base())[j];
       }


       gMonolithic=Dumux::manipulateMatrix(gMonolithic,matrixManipulationMap,gMatrixNumberOfRows);
//       Dune::writeMatrixToMatlab(gMonolithic,"gMonolithicManipulated");

       Dumux::manipulateResidual(rMonolithic,matrixManipulationMap,gMatrixNumberOfRows);

       timing = timer.elapsed();
       std::cout << "\t \t " << timing << " s" << std::endl;
       timer.reset();

       bool doMatrixScaling=true;//TODO change here the order because even if matrixscaling is not true
       if (doMatrixScaling){
//            std::cout << " m.base().N()= "<< m.base().N() << "   m.base().M()= " << m.base().M() <<std::endl;
           for (int j=0;j<gMonolithic.N();j++){//loop over rows
               Scalar mainDiagonalValue=1.0;
               if (gMonolithic.exists(j,j) ) {
                   mainDiagonalValue=(gMonolithic)[j][j];//find (j,j) main diagonal entry
//                    std::cout <<"mainDiagonalValue: " << mainDiagonalValue << std::endl;
               }
               else std::cout <<"ERROR: no entry in main diagonal!\n";
               bool noRowEntry=true;
               for (int k=0;k<gMonolithic.M();k++){//loop over columns in row j
                   if (gMonolithic.exists(j,k) ) {//TODO check in the whole file if it has to be .base().exists( or just .exists(
                       if (!Dumux::arePointsEqual(mainDiagonalValue, Scalar(0.0) )){
                           noRowEntry=false;
//                        std::cout << "original entry: " << (m.base())[j][k] <<"   ";
                           (gMonolithic)[j][k]/=mainDiagonalValue;//scale every entry to the main diagonal entry
//                        std::cout << "modified entry: " << (m.base())[j][k] <<std::endl;
                       }
                       else if ( !Dumux::arePointsEqual( ( (gMonolithic)[j][k])[0][0] ,0.0) ){
                           noRowEntry=false;
                           break;
                       }
                   }
               }
//                std::cout << "original rhs: " << (r.base())[j] <<"   ";
               /*
                * fix dofs with zero support
                */
               if (noRowEntry){
                   assert(Dumux::arePointsEqual( (rMonolithic)[j] [0],0.0 ) );
                   assert((gMonolithic).exists(j,j));
                   (gMonolithic)[j][j]=1.0;
                   (rMonolithic)[j]=-42.0;
               }
               else {
                   (rMonolithic)[j]/=mainDiagonalValue;//scale right hand side by the value of the main diagonal entry of the stiffness matrix
               }
//                std::cout << "modified rhs: " << (r.base())[j] <<std::endl;
           }
       }
       else{
           /*
            * fix dofs with zero support
            */
           Dune::writeMatrixToMatlab(gMonolithic,"gMonolithic");
           for (std::size_t row=0;row<gMonolithicNumberOfRows;row++){//loop over rows
               bool noRowEntry=true;
               for (std::size_t column=0;column<gMonolithicNumberOfColumns;column++){//loop over columns in row
                   if (gMonolithic.exists( row,column )) {
                       if ( !Dumux::arePointsEqual( (gMonolithic[row][column])[0][0] ,0.0) ){
                           noRowEntry=false;
                           break;
                       }
                   }
               }
               if (noRowEntry){
                   assert(Dumux::arePointsEqual((rMonolithic[row])[0],0.0));
                   assert(gMonolithic.exists(row,row));
                   gMonolithic[row][row]=1.0;
                   rMonolithic[row]=-42.0;
                   std::cout << "fixing empty row " << row << std::endl;
               }
           }
       }

       //solve the coupled system
       Dune::writeMatrixToMatlab(gMonolithic,"gMonolithic",18);//outputPrecision = 18

       // Select a linear solver backend
       typedef Dune::PDELab::ISTLBackend_SEQ_SuperLU LS;
       LS solver(2);//verbosity
//       typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_SSOR LS;
//       typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_ILU0 LS;
//       typedef Dune::PDELab::ISTLBackend_SEQ_MINRES_SSOR LS;
//       typedef Dune::PDELab::ISTLBackend_SEQ_LOOP_Jac LS;
//       LS solver(5000,2);//max iterations,verbosity
//       typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_AMG_SSOR<MultiGO> LS;
//       LS solver (/*maxiter*/5000, /*verbosity*/1, /*reuse*/false, /*usesuperlu*/false);

       //store before iterating
       const VMonolithic vMonolithicStore(vMonolithic);
       std::ofstream v0dat;
       v0dat.open ("v0.dat");
       v0dat << std::scientific<<std::setprecision(18) <<vMonolithic << std::endl;
       v0dat.close();

       std::ofstream r0dat;
       r0dat.open ("r0.dat");
       r0dat << std::scientific<<std::setprecision(18) <<rMonolithic << std::endl;
       r0dat.close();

       V rMonolithicContainer(multigfs,Dune::PDELab::tags::unattached_container());
       V zMonolithicContainer(multigfs,Dune::PDELab::tags::unattached_container());
       GMatrix gMonolithicContainer;
       rMonolithicContainer.attach(Dune::stackobject_to_shared_ptr(rMonolithic));
       zMonolithicContainer.attach(Dune::stackobject_to_shared_ptr(zMonolithic));
       gMonolithicContainer.attach(Dune::stackobject_to_shared_ptr(gMonolithic));

       //initial solution and matrix multiplication
       solver.apply(gMonolithicContainer, zMonolithicContainer, rMonolithicContainer, 1.0e-18);
       VMonolithic matrixTimesSolution0(multigo.trialGridFunctionSpace().globalSize()+fgo.trialGridFunctionSpace().globalSize());
       matrixTimesSolution0*=0.0;

       gMonolithicContainer.base().mv(zMonolithicContainer.base(),matrixTimesSolution0);

       //start further global iterations
       int numberOfGlobalIterations=0;
//       if ( inputParameters_.hasKey("convergence") ){
//           numberOfGlobalIterations = inputParameters_.get<int>("convergence");
//       }//TODO not working!
       for (int globalIteration=0;globalIteration<numberOfGlobalIterations;globalIteration++){

           rMonolithicContainer.base()-=matrixTimesSolution0;

           VMonolithic deltaV1(multigo.trialGridFunctionSpace().globalSize()+fgo.trialGridFunctionSpace().globalSize());
           deltaV1*=0.0;
           V deltaV1Container(multigfs,Dune::PDELab::tags::unattached_container());
           deltaV1Container.attach(Dune::stackobject_to_shared_ptr(deltaV1));

           solver.apply(gMonolithicContainer, deltaV1Container, rMonolithicContainer, 1.0e-10);

           std::cout << "deltaV: " << deltaV1Container.base().two_norm() << std::endl;

           zMonolithicContainer += deltaV1Container;

           //prepare for the next iteration
           matrixTimesSolution0*=0.0;
           gMonolithicContainer.base().mv(deltaV1Container.base(),matrixTimesSolution0);
       }
           //write output
           vMonolithic=vMonolithicStore;
           vMonolithic -= zMonolithicContainer;
//           std::cout << "rMonolithicStore: " << rMonolithic.two_norm()<<std::endl;
       // and update
//       vMonolithic -= zMonolithicContainer;

       timing = timer.elapsed();
       std::cout << "=== solving: "<< timing << " s" << std::endl;
       timer.reset();

       //copy back
       for (int j=0;j<v.base().N();j++){//loop over entries of rMatrix
           (v.base())[j]=(vMonolithic)[j];
       }
       for (int j=0;j<vf.base().N();j++){//loop over entries of rMatrix
           (vf.base())[j]=(vMonolithic)[j+gMatrixNumberOfRows];
       }

       /* ***********************************************************************
            output
        *********************************************************************** */

       std::cout << "Dirichlet constrained dofs=" << cc.size() << " of " << v.N()<< "\n";
       std::cout << hgfs.globalSize() << " standard  dofs" << "\n";
       std::cout << "Dirichlet constrained fracture dofs=" << fcc.size() << " of " << vf.N()<< "\n";

       std::ofstream matrixDofs;
       matrixDofs.open ("matrixDofs.dat");
       matrixDofs << v.N() << std::endl;
       matrixDofs.close();
       std::ofstream fractureDofs;
       fractureDofs.open ("fractureDofs.dat");
       fractureDofs << vf.N() << std::endl;
       fractureDofs.close();

       typedef Dune::PDELab::GridFunctionSubSpace<MultiGFS,Dune::TypeTree::TreePath<0> > SHGFS;
       typedef Dune::PDELab::GridFunctionSubSpace<MultiGFS,Dune::TypeTree::TreePath<1> > SEGFS0;

       typedef Dune::PDELab::DiscreteGridFunction<SHGFS,V> SHDGF;
       typedef Dune::PDELab::DiscreteGridFunction<SEGFS0,V> SEDGF0;

       SHGFS shgfs(multigfs);
       SEGFS0 segfs0(multigfs);

       SHDGF shdgf(shgfs,v);
       SEDGF0 sedgf0(segfs0,v);

       typedef Dune::PDELab::DiscreteGridFunction<FGFS,VF> FDGF;
       FDGF fdgf(fgfs,vf);

//       char iterationNumber[5];
//       std::sprintf (iterationNumber, "%d",globalIteration);
           {
               std::string outputName=simulationNameString;
               outputName+="_base";
//               outputName+=iterationNumber;
               Dune::VTKWriter<HGV> vtkwriter(hostGridView,Dune::VTK::conforming);
               vtkwriter.addVertexData(new Dune::PDELab::VTKGridFunctionAdapter<SHDGF>(shdgf,"pressure"));
               vtkwriter.write(outputName.c_str(),Dune::VTK::ascii);
           }
           {
               std::string outputName=simulationNameString;
               outputName+="_e0";
               Dune::VTKWriter<EGV> vtkwriter(enrichedGridView0,Dune::VTK::conforming);
               //Dune::SubsamplingVTKWriter<EGV> vtkwriter(enrichedGridView,5);
               vtkwriter.addVertexData(new Dune::PDELab::VTKGridFunctionAdapter<SEDGF0>(sedgf0,"pressure"));
               vtkwriter.write(outputName.c_str(),Dune::VTK::ascii);
           }
           {
               std::string outputName=simulationNameString;
               outputName+="_fracture";
//               outputName+=iterationNumber;
               Dune::VTKWriter<FGV> vtkwriter(fractureGridView,Dune::VTK::conforming);
               vtkwriter.addVertexData(new Dune::PDELab::VTKGridFunctionAdapter<FDGF>(fdgf,"fracture pressure"));
               vtkwriter.write(outputName.c_str(),Dune::VTK::ascii);
           }

           /*
            * Analytical solution for the matrix and the fracture
            */
//           int elementsMatrix = inputParameters_.get<int>("Grid.elementsMatrix");
//           Scalar domainLength=2.0;
//           Scalar elementLength=domainLength/elementsMatrix;
//           Scalar elementAreaMatrix= elementLength*elementLength;

           V analPSolVectorMatrix(v);
           analPSolVectorMatrix*=0.0;
           Dune::PDELab::interpolate(analPSolFunctionMatrix,shgfs,analPSolVectorMatrix);
//           std::cout << "analPSolVectorMatrix: " <<  analPSolVectorMatrix.base() << std::endl;
//           std::cout << "numericalSolMatrix: " <<  v.base() << std::endl;
           V analSolErrorVectorMatrix(analPSolVectorMatrix);
           analSolErrorVectorMatrix-=v;

           typedef Dune::PDELab::DiscreteGridFunction<SHGFS,V> AnalSolDGFMatrix;
           AnalSolDGFMatrix analSolDgfMatrix(shgfs,analSolErrorVectorMatrix);
           {
               std::string outputName=simulationNameString;
               outputName+="_analSolMatrix";
               Dune::VTKWriter<HGV> vtkwriter(hostGridView,Dune::VTK::conforming);
               vtkwriter.addVertexData(new Dune::PDELab::VTKGridFunctionAdapter<AnalSolDGFMatrix>(analSolDgfMatrix,"p_anal-p_num"));
               vtkwriter.write(outputName.c_str(),Dune::VTK::ascii);
           }


           VF analPSolVectorFracture(vf);
           analPSolVectorFracture*=0.0;
//           std::cout << "fgfs size: " << fgfs.globalSize() << "   analPSolVectorFracture size: " << analPSolVectorFracture.base().size() << std::endl;
           Dune::PDELab::interpolate(analPSolFunctionFracture,fgfs,analPSolVectorFracture);

           VF analSolErrorVectorFracture(analPSolVectorFracture);
           analSolErrorVectorFracture-=vf;

           typedef Dune::PDELab::DiscreteGridFunction<FGFS,VF> AnalSolDGFFracture;
           AnalSolDGFFracture analSolDgfFracture(fgfs,analSolErrorVectorFracture);
           {
                std::string outputName=simulationNameString;
                outputName+="_analSolFracture";
                Dune::VTKWriter<FGV> vtkwriter(fractureGridView,Dune::VTK::conforming);
                vtkwriter.addVertexData(new Dune::PDELab::VTKGridFunctionAdapter<AnalSolDGFFracture>(analSolDgfFracture,"p_anal-p_num"));
                vtkwriter.write(outputName.c_str(),Dune::VTK::ascii);
            }

           /*
            * error calculation
            *
            */

//           Scalar domainArea=domainLength*domainLength;
//           Scalar domainAreaFracture=domainLength*soil.fractureWidth();
//           Scalar elementAreaFracture= (domainAreaFracture)/( std::pow(2,levelFracture) );
//           std::cout << "domain area matrix: " << domainArea <<std::endl;
//           std::cout << "domain area fracture: " << domainAreaFracture <<std::endl;

           Scalar errorM=0;
           Scalar denominatorM=0;
           Scalar minM=1.0e4;
           Scalar maxM=-1.0e4;
//           for (int i=0;i<hgfs.globalSize();i++){
//
//               /*
//                * skip if xfem element nodes
//                */
//               if (!Dumux::arePointsEqual(analPSolVectorMatrix.base()[i][0],-2.3e7) ){
//                   if (analPSolVectorMatrix.base()[i]<minM) minM=analPSolVectorMatrix.base()[i];
//                   if (analPSolVectorMatrix.base()[i]>maxM) maxM=analPSolVectorMatrix.base()[i];
//                   errorM+= analSolErrorVectorMatrix.base()[i]*analSolErrorVectorMatrix.base()[i];
//                   denominatorM+= analPSolVectorMatrix.base()[i]*analPSolVectorMatrix.base()[i];
//               }
//               else {std::cout << "skip xfem element"<<std::endl;}
//           }

           typedef Dumux::MatrixError<MultiDomainGrid,IntersectionPointsMap,MultiGFS,EnrichedProblem,V,
        		   Soil,
                   AnalPSolFunctionMatrix,AnalVSolFunctionMatrix> MatrixError;
           MatrixError matrixError(multiDomainGrid,intersectionPointsMap,multigfs,ep,v,
        		   soil,
        		   analPSolFunctionMatrix,analVSolFunctionMatrix);

//           std::cout << "minM: " << minM << "  maxM: " << maxM <<std::endl;
//           std::cout << "matrix error: " << std::sqrt(errorM) << std::endl;
//           std::cout << "matrix L2-error/L2: " << std::sqrt(errorM/denominatorM) << std::endl;
           std::cout << "matrixerror = " << matrixError.evaluatePressure() << std::endl;
//           std::cout << "matrix error/Linf: " << std::sqrt(errorM)/maxM << std::endl;
//           std::cout << "matrix error/(Linf-minM): " << std::sqrt(errorM)/(maxM-minM) << std::endl;

           Scalar errorF=0;
           Scalar denominatorF=0;
           Scalar minF=1.0e4;
//           Scalar maxF=analPSolVectorFracture.infinity_norm();
           for (int i=0;i<fgfs.globalSize();i++){
               if (analPSolVectorFracture.base()[i]<minF) minF=analPSolVectorFracture.base()[i];
               errorF+= analSolErrorVectorFracture.base()[i]*analSolErrorVectorFracture.base()[i];
               denominatorF+= analPSolVectorFracture.base()[i]*analPSolVectorFracture.base()[i];
           }

           //iteration over all fracture elements
           typedef typename FGV::Codim<0>::Iterator FractureElementIterator;
           const int dim=1;
           Scalar errorF_infty=0.0;
           Scalar denominatorF_infty=0.0;
           FractureElementIterator fractureElementEndIt = fractureGrid.leafend<0>();//This is a loop over the grid because node ids are used
           for (FractureElementIterator fractureElementIterator = fractureGrid.leafbegin<0>();
                   fractureElementIterator != fractureElementEndIt; ++fractureElementIterator) {
               // select quadrature rule
               Dune::GeometryType gt = fractureElementIterator->geometry().type();
               const Dune::QuadratureRule<ctype,dim>& rule = Dune::QuadratureRules<ctype,dim>::rule(gt,6);
               // loop over quadrature points
               for (typename Dune::QuadratureRule<ctype,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it){
                   FDGF::Traits::RangeType numericalSolution=0.0;
                   fdgf.evaluate(*fractureElementIterator,it->position(),numericalSolution);
                   AnalPSolFunctionFracture::Traits::RangeType analyticalSolution=0.0;
                   analPSolFunctionFracture.evaluateGlobal(fractureElementIterator->geometry().global(it->position()),analyticalSolution);
                   typedef typename AnalPSolFunctionFracture::Traits::RangeType::value_type TempScalar;
                   TempScalar tempAnal(analyticalSolution);
                   TempScalar tempNum(numericalSolution);
                   errorF_infty+= std::pow(tempAnal-tempNum,2);
                   denominatorF_infty+= std::pow(tempAnal,2);
               }
           }

           typedef Dumux::FractureError<FractureGrid,FGFS,VF,AnalVSolFunctionFracture> FractureError;
           FractureError fractureError(fractureGrid,fgfs,vf,analVSolFunctionFracture);

//           std::cout << "minF: " << minF << "  maxF: " << maxF <<std::endl;
//           std::cout << "fracture error: " << std::sqrt(errorF) << std::endl;
//           std::cout << "fracture error/L2: " << std::sqrt(errorF/denominatorF) << std::endl;
           std::cout << "fractureerror = " << std::sqrt(errorF_infty/denominatorF_infty) << std::endl;
           std::cout << "H1-fracture error/H1: " << fractureError.evaluateVelocity(soil) << std::endl;
//           std::cout << "fracture error/Linf: " << std::sqrt(errorF)/maxF << std::endl;
//           std::cout << "fracture error/Linf_matrix: " << std::sqrt(errorF)/maxM << std::endl;
//           std::cout << "fracture error/(Linf-minM): " << std::sqrt(errorF)/(maxF-minF) << std::endl;

//           std::cout << "combined error/L2: " <<
//                   std::sqrt((errorF*domainAreaFracture+errorM*domainArea)/(denominatorF*domainAreaFracture+ denominatorM*domainArea ))
//           << std::endl;
//           std::cout << "combined error/Linf_matrix: " <<
//                              std::sqrt((errorF*domainAreaFracture+errorM*domainArea))/((domainAreaFracture+domainArea)*maxM)
//                      << std::endl;

           // end error calculation

           timing = timer.elapsed();
           std::cout << "=== writing output: " << timing << " s" << std::endl;
           timing = totalTimer.elapsed();
           std::cout << "=== TOTAL: " << timing << " s" << std::endl;

       return (0);

}//if helper size end

  }//try end
#ifdef NDEBUG
catch (Dune::Exception &e){
std::cerr << "Dune reported error: " << e << std::endl;
return (1);
}
catch (...){
std::cerr << "Unknown exception thrown!" << std::endl;
return (1);
}
}//main end
#endif


