#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/quadraturerules.hh>
#include<dune/pdelab/common/geometrywrapper.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>

/** a local operator for solving the equation
 * TODO: doc me!
 */
template<typename B, typename Soil, typename SolutionCase>
class MatrixLocalOperatorAnalytical :
    //derived from the following and using the CRTP-Trick!
  public Dune::PDELab::NumericalJacobianApplyVolume<MatrixLocalOperatorAnalytical<B, Soil, SolutionCase> >,
  public Dune::PDELab::NumericalJacobianVolume<MatrixLocalOperatorAnalytical<B, Soil, SolutionCase> >,
  public Dune::PDELab::NumericalJacobianApplyBoundary<MatrixLocalOperatorAnalytical<B, Soil, SolutionCase> >,
  public Dune::PDELab::NumericalJacobianBoundary<MatrixLocalOperatorAnalytical<B, Soil, SolutionCase> >,
  public Dune::PDELab::FullVolumePattern,
  public Dune::PDELab::LocalOperatorDefaultFlags

  {
  public:
  // pattern assembly flags
  enum { doPatternVolume = true };
  // residual assembly flags
  enum { doAlphaVolume  = true };
  enum { doAlphaBoundary  = false };

  MatrixLocalOperatorAnalytical (const B &b, const Soil &soil, const SolutionCase &solutionCase,
          unsigned int intorder=3)  // needs boundary cond. type
    :
    Dune::PDELab::NumericalJacobianApplyVolume<MatrixLocalOperatorAnalytical<B, Soil, SolutionCase> >(1e-1),
    Dune::PDELab::NumericalJacobianVolume<MatrixLocalOperatorAnalytical<B, Soil,SolutionCase> >(1e-1),
    Dune::PDELab::NumericalJacobianApplyBoundary<MatrixLocalOperatorAnalytical<B, Soil, SolutionCase> >(1e-1),
    Dune::PDELab::NumericalJacobianBoundary<MatrixLocalOperatorAnalytical<B, Soil, SolutionCase> >(1e-1),
        b_(b), soil_(soil),  solutionCase_(solutionCase),intorder_(intorder)
  {}

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {//std::cout << "hallo" <<std::endl;
    // extract some types
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::JacobianType JacobianType;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeType RangeType;
    typedef typename LFSU::Traits::SizeType size_type;
    typedef typename X::value_type Scalar;

    // dimensions
    const int dim = EG::Geometry::dimension;
    const int dimw = EG::Geometry::dimensionworld;

    Dune::FieldMatrix<Scalar,dim,dim> K=soil_.intrinsicPermeability();

    // select quadrature rule
    Dune::GeometryType gt = eg.geometry().type();
    const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder_);
    //the local operator is elementwise defined and therefore the loop over the quadrature points is elementwise
    for (typename Dune::QuadratureRule<DF,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it){
    // evaluate basis functions on reference element
    std::vector<RangeType> phi(lfsu.size());
    lfsu.finiteElement().localBasis().evaluateFunction(it->position(),phi);

    // evaluate gradient of basis functions on reference element
    std::vector<JacobianType> js(lfsu.size());
    lfsu.finiteElement().localBasis().evaluateJacobian(it->position(),js);
    // transform gradients from reference element to real element
    const Dune::FieldMatrix<DF,dimw,dim> jac = eg.geometry().jacobianInverseTransposed(it->position());
    std::vector<Dune::FieldVector<RF,dim> > gradphi(lfsu.size());
    for (size_type i=0; i<lfsu.size(); i++)
    {jac.mv(js[i][0],gradphi[i]);
//    std::cout << "gradphiM: " << gradphi[i]<<std::endl;
    }
    // compute gradient of u
    Dune::FieldVector<RF,dim> gradu(0.0);
    for (size_type i=0; i<lfsu.size(); i++){
        gradu.axpy(x(lfsu,i),gradphi[i]);}

    RF factor = it->weight()*eg.geometry().integrationElement(it->position());

    Dune::FieldVector<RF,dim> vector1(0.0);
    K.mv(gradu,vector1); //K * grad p
    for (size_type i=0; i<lfsu.size(); i++){
        r.accumulate(lfsu,i,( vector1 * gradphi[i])*factor);}

    RF factor2 = factor;
//    factor2*=0.0;
    /*
     * only for analytical solution comparison test case needed sink term:
     * (1-k) cos(x) cosh(a_f/(2 sqrt(a) )
     * with k=k_f /k_m and a_f the fracture aperture
     */

    if (solutionCase_==0){
//    Dune::FieldMatrix<Scalar,dim,dim> K=soil_.intrinsicPermeability();
    RF k_m=K[0][0];
    RF k_f=soil_.kFT();
    RF a_f=soil_.fractureWidth();
    RF k=k_f/k_m;
    RF sqrta=std::sqrt(K[0][0]/K[1][1]);
    typedef typename LFSU::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainType FV;
    FV global= eg.geometry().global(it->position());
    factor2*= (-1.0)* (1-k)*std::cos(global[0])*std::cosh(a_f/(2.0*sqrta));
//    factor2=0.0;
//    assert(Dumux::arePointsEqual(std::cosh(a_f/(2.0*sqrta)),1.0));
    factor*= (-1.0)* (1-k)*std::cos(global[0]);
    for (size_type i=0; i<lfsu.size(); i++){
        r.accumulate(lfsu,i,phi[i]*factor2);
    }
    }
  }//end loop over quadrature points
}//end alpha_volume

private:
  const B &b_;
  const Soil &soil_;
  const SolutionCase &solutionCase_;
  unsigned int intorder_;
};
