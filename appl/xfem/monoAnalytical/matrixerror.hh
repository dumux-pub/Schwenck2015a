#ifndef MATRIXERROR_HH_
#define MATRIXERROR_HH_

#include<dune/geometry/type.hh>

namespace Dumux{

template<typename MultiDomainGrid, typename Map, typename MultiGFS, typename EnrichedProblem, typename X,
typename Soil, typename AnalyticalPressureSolution, typename AnalyticalVelocitySolution>
class MatrixError{

    typedef typename MultiDomainGrid::LeafGridView MDGV;
    typedef typename MultiDomainGrid::ctype ctype;
    typedef typename MDGV::template Codim<0>::Iterator HostGridIterator;
    typedef typename Dune::PDELab::LocalFunctionSpace<MultiGFS> MultiLFSU;
    typedef typename EnrichedProblem::Traits::TrialLocalFunctionSpace EPLFS;
    typedef typename EPLFS::template Child<0>::Type LBFS;       // LBFS is the local base function space within the XFEM area (EnrichedProblem)
    typedef typename EPLFS::template Child<1>::Type LEFS0;
    typedef typename EPLFS::template Child<2>::Type LEFS1;
    typedef typename EPLFS::template Child<3>::Type LEFS2;
    typedef typename EPLFS::template Child<4>::Type LEFS3;
    typedef typename EPLFS::template Child<5>::Type LEFS4;

    typedef typename LBFS::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::DomainType FV;
//    typedef typename LBFS::Traits::FiniteElementType::
//            Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename LBFS::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LBFS::Traits::SizeType sizeType;
    typedef typename LBFS::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::JacobianType JacobianType;

    typedef Dune::PDELab::GridFunctionSubSpace<MultiGFS,Dune::TypeTree::TreePath<0> > SHGFS;
    typedef typename SHGFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits FTraits;
    typedef typename SHGFS::Traits::GridViewType GVB;
    typedef typename GVB::template Codim< 0 >::Entity E;
    typedef Dune::PDELab::ElementGeometry<E> EG;
    typedef typename FTraits::RangeFieldType RF;
    typedef typename FTraits::RangeType RangeType;
    typedef typename Dumux::modifiedBasisFunction<MultiDomainGrid, E, Map, MultiLFSU> MBF;

    typedef Dune::PDELab::LFSIndexCache<MultiLFSU> MultiLFSCache;
    typedef typename X::template LocalView<MultiLFSCache> XView;
    typedef typename X::template ConstLocalView<MultiLFSCache> ConstXView;
    typedef typename X::field_type Scalar;



public:
MatrixError(MultiDomainGrid& multiDomainGrid, Map& intersectionPointsMap,  MultiGFS& multigfs,EnrichedProblem& ep, X& x,
        const Soil &soil, AnalyticalPressureSolution &analyticalPressureSolution, AnalyticalVelocitySolution &analyticalVelocitySolution) //x FE solution
    : multiDomainGrid_(multiDomainGrid), intersectionPointsMap_(intersectionPointsMap), multigfs_(multigfs), ep_(ep),x_(x),
      soil_(soil),
      analyticalPressureSolution_(analyticalPressureSolution),analyticalVelocitySolution_(analyticalVelocitySolution)
    , multilfsu_(multigfs)
{}

RF evaluatePressure() {

    RangeType u_fe(0.0);//TODO check what happens if u_fe is returned without a proper value (u_fe=0.0)?
    RangeType error(0.0);
    RangeType denominator(0.0);

    MultiLFSCache multilfs_cache(multilfsu_);
    ConstXView x_view(x_);

//    int children=MultiLFSU::CHILDREN;
//    std::cout<< "children: " << children << std::endl; //TODO make this the correct number of spaces!

//iterate over the whole matrix multidomaingrid and find the element in which global lies.
    //TODO: in general global lies always within an enriched element. Check if it can happen that the element is not enriched.
HostGridIterator eendit = multiDomainGrid_.template leafend<0>();
for (HostGridIterator matrixElementIterator = multiDomainGrid_.template leafbegin<0>(); matrixElementIterator != eendit; ++matrixElementIterator) {
    EG eg(*matrixElementIterator);

    multilfsu_.bind(*matrixElementIterator);
    EPLFS eplfs(multilfsu_,ep_);

    const LBFS& lbfs = eplfs.template child<0>();
    const LEFS0& lefs0 = eplfs.template child<1>();
    const LEFS1& lefs1 = eplfs.template child<2>();
    const LEFS2& lefs2 = eplfs.template child<3>();
    const LEFS3& lefs3 = eplfs.template child<4>();
    const LEFS4& lefs4 = eplfs.template child<5>();

    eplfs.bind();

    std::vector<RF> xl(multilfsu_.maxSize());        // local coefficients
    multilfs_cache.update();
    x_view.bind(multilfs_cache);
    x_view.read(xl);// get local coefficients of the solution

    /*
     * for enriched elements account for all the function spaces
     */
    sizeType children=lefs0.size()+lefs1.size()+lefs2.size()+lefs3.size()+lefs4.size();
    if (children>0){
    	children/=4;
    	typedef Dune::GeometryType GT;
        const MBF mbf(multiDomainGrid_, eg.entity(), intersectionPointsMap_, multilfsu_);


        //get element Id
        typedef typename MultiDomainGrid::Traits::GlobalIdSet::IdType IdType;
        const IdType elementId =multiDomainGrid_.globalIdSet().id(eg.entity());

        typedef typename Map::mapped_type IntersectionPoints;
        IntersectionPoints intersectionPoints=(((*intersectionPointsMap_.find(elementId)).second));

        assert(children<6);
        if (children>2) --children;

        typedef typename std::vector<FV> PointsNotOnCorners;
        PointsNotOnCorners pointsNotOnCorners(children);//TODO check if this is true for cuts through a corner!
        std::vector<FV> xfMiddle;
        xfMiddle.clear();
        FV check(-1000.0);//check means no entry
        xfMiddle.push_back(intersectionPoints[4].intersection);

        /*
         * lmax is 1 for ending fractures, 2 for single fractures, 3 for 3-crossings and 4 for 4-crossings.
         * Two not-crossing fractures have an lmax of 2.
         */
    //    int lmax=pointsNotOnCorners.size();
        for (int i=0;i<pointsNotOnCorners.size();i++){
            pointsNotOnCorners[i]=(intersectionPoints[i].intersection);
        }
        if (Dumux::arePointsEqual(intersectionPoints[1].intersection,check) ){//this indicates an ending fracture
            pointsNotOnCorners[1]=(intersectionPoints[5].intersection);
    //        lmax=1;
        }

        bool sourceInThisElement=true;
        typedef typename std::vector<FV> SortedPoints;
        SortedPoints sortedPoints;
        sortedPoints=Dumux::sortPoints<FV>(pointsNotOnCorners, eg);
        typedef Dune::MultiLinearGeometry<ctype,dim_,dimw_> ElementGeometry;
        typedef Dune::GeometryType::BasicType BasicType;
        const BasicType simplex = Dune::GeometryType::simplex;
        const GT subSimplex(simplex,dimw_);
        std::vector<FV> listOfCornerCoordinates(3);//this is a vector which stores all corners of the used subtriangle
        /*
         * this scheme is based on the DUNE node ordering as defined for the generic reference element cube
         * create subtriangles depending on the fracture geoemtry
         * for straight, bending and ending fractures always 6 subtriangles are generated
         * for 3-crossings 7 subtriangles are generated
         * for 4-crossings 8 subtriangles are generated
         * and for two not-crossing fractures 10 subtriangles are generated
         */
        assert(xfMiddle.size()<=2);

        listOfCornerCoordinates[0]=eg.geometry().global(xfMiddle[0]);
        for (int i=0;i<4+children;i++){
            listOfCornerCoordinates[1]=eg.geometry().global(sortedPoints[i]);
            listOfCornerCoordinates[2]= (i<3+children ? eg.geometry().global(sortedPoints[i+1]) : eg.geometry().global(sortedPoints[0]) );

            Scalar a_f=soil_.fractureWidth();
            for (int index=1;index<=2;++index){
                const int indexPlusOne= (index==1  ? 2 : 1);
                if (!Dumux::equalToCorner( eg.geometry().global(listOfCornerCoordinates[index]) )){
                    if (Dumux::absDistance(listOfCornerCoordinates[index],
                            listOfCornerCoordinates[indexPlusOne])<a_f/2.0){
                        std::cout << "WARNING !!!!!!!!! \n aperture > h_matrix!!!"<<std::endl;
                        sourceInThisElement=false;
                        continue;
                    }
                    auto tempMiddleCornerPlus=(listOfCornerCoordinates[index]);
                    auto tempMiddleCornerMinus=(listOfCornerCoordinates[index]);
                    tempMiddleCornerPlus[1]+=a_f/2.0;
                    tempMiddleCornerMinus[1]-=a_f/2.0;
                    if (Dumux::absDistance(tempMiddleCornerPlus,(listOfCornerCoordinates[indexPlusOne]))<
                            Dumux::absDistance(tempMiddleCornerMinus,(listOfCornerCoordinates[indexPlusOne])) ){
                        listOfCornerCoordinates[index]=tempMiddleCornerPlus;
                    }
                    else{
                        listOfCornerCoordinates[index]=tempMiddleCornerMinus;
                    }
                }
                //center element
                else if(Dumux::equalToCorner( eg.geometry().global(listOfCornerCoordinates[index]) ) ||
                        Dumux::equalToCorner( eg.geometry().global(listOfCornerCoordinates[indexPlusOne]) ) )
                {
                    auto tempCenterPlus=(listOfCornerCoordinates[0]);
                    auto tempCenterMinus=(listOfCornerCoordinates[0]);
                    tempCenterPlus[1]+=a_f/2.0;
                    tempCenterMinus[1]-=a_f/2.0;
                    auto tempCornerMiddle(listOfCornerCoordinates[indexPlusOne]);
                    tempCornerMiddle+=listOfCornerCoordinates[index];
                    tempCornerMiddle/=2.0;
                    if ( Dumux::absDistance(tempCenterPlus, tempCornerMiddle) <
                            Dumux::absDistance(tempCenterMinus,tempCornerMiddle) ){
                        listOfCornerCoordinates[0]=tempCenterPlus;
                    }
                    else{
                        listOfCornerCoordinates[0]=tempCenterMinus;
                    }
                }
            }
            if (!sourceInThisElement) break;

            ElementGeometry subElement(subSimplex,listOfCornerCoordinates);


            // select quadrature rule
            GT gt=subElement.type();//subSimplex;
            const Dune::QuadratureRule<DF,dim_>& rule = Dune::QuadratureRules<DF,dim_>::rule(gt,6);
            //the local operator is elementwise defined and therefore the loop over the quadrature points is elementwise
            for (typename Dune::QuadratureRule<DF,dim_>::const_iterator it=rule.begin(); it!=rule.end(); ++it){
                u_fe*=0.0;
                /*
                 * it->position is on the local subelement
                 * subElement.global makes it world global
                 * eg.geometry().local transformes it into the local matrix element
                 */
                FV local = eg.geometry().local( subElement.global( it->position() ) );
        		RangeType u_fe_temp(0.0);

        		// evaluate finite element function at local coordinate (standard basis function)
        		//evaluating left hand side
        		std::vector<RangeType> b(lbfs.maxSize()); // shape function values
        		lbfs.finiteElement().localBasis().evaluateFunction(local,b);
        		b=mbf.modify(b,local,0);
        		for (int i=0; i<lbfs.size(); i++){
        			u_fe_temp.axpy(xl[lbfs.localIndex(i)],b[i]);
        		}
        		u_fe+=u_fe_temp;

        		// evaluate finite element function at local coordinate (enriched basis function)
        		//TODO: works only if lblfs.size=lelfs.size, i.e. only ONE additional dof at every standard dof, i.e. no fracture crossings and only one fracture per element
        		if (lefs0.size()>0){
        			//evaluating
        			u_fe_temp*=0.0;
        			std::vector<RangeType> e(lefs0.maxSize()); // shape function values
        			lefs0.finiteElement().localBasis().evaluateFunction(local,e);
        			e=mbf.modify(e,local,1);
        			for (int i=0; i<lefs0.size(); i++){
        				//            std::cout << "i: " << multiIndex << "\t localIndex: " <<multilfsu_.localIndex(multiIndex)<< std::endl;
        				u_fe_temp.axpy(xl[lefs0.localIndex(i)],e[i]);
        			}
        			u_fe+=u_fe_temp;
        		}
        		/*
        		 * for the anayltical case of a single horizontal fracture the following is not necessary -
        		 * it is here just for the sake of completeness
        		 */
        		if (lefs1.size()>0){
        			std::vector<RangeType> e(lefs1.maxSize()); // shape function values

        			//evaluating
        			u_fe_temp*=0.0;
        			lefs1.finiteElement().localBasis().evaluateFunction(local,e);
        			e=mbf.modify(e,local,2);
        			for (int i=0; i<lefs1.size(); i++){
        				u_fe_temp.axpy(xl[lefs1.localIndex(i)],e[i]);
        			}
        			u_fe+=u_fe_temp;
        		}
        		if (lefs2.size()>0){
        			std::vector<RangeType> e(lefs2.maxSize()); // shape function values

        			//evaluating left hand side
        			u_fe_temp*=0.0;
        			lefs2.finiteElement().localBasis().evaluateFunction(local,e);
        			e=mbf.modify(e,local,3);
        			for (int i=0; i<lefs2.size(); i++){
        				u_fe_temp.axpy(xl[lefs2.localIndex(i)],e[i]);
        			}
        			u_fe+=u_fe_temp;
        		}
        		if (lefs3.size()>0){
        			std::vector<RangeType> e(lefs3.maxSize()); // shape function values

        			//evaluating left hand side
        			u_fe_temp*=0.0;
        			lefs3.finiteElement().localBasis().evaluateFunction(local,e);
        			e=mbf.modify(e,local,4);
        			for (int i=0; i<lefs3.size(); i++){
        				u_fe_temp.axpy(xl[lefs3.localIndex(i)],e[i]);
        			}
        			u_fe+=u_fe_temp;
        		}
        		if (lefs4.size()>0){
        			std::vector<RangeType> e(lefs4.maxSize()); // shape function values

        			//evaluating left hand side
        			u_fe_temp*=0.0;
        			lefs4.finiteElement().localBasis().evaluateFunction(local,e);
        			e=mbf.modify(e,local,5);
        			for (int i=0; i<lefs4.size(); i++){
        				u_fe_temp.axpy(xl[lefs4.localIndex(i)],e[i]);
        			}
        			u_fe+=u_fe_temp;
        		}
        		typename AnalyticalPressureSolution::Traits::RangeType analSol=0.0;
        		analyticalPressureSolution_.evaluateGlobal(eg.geometry().global(local),analSol);
        		typedef typename AnalyticalPressureSolution::Traits::RangeType::value_type TempScalar;
        		TempScalar tempAnalSol=analSol;
        		TempScalar tempNumSol=u_fe;
        		error+= std::pow(tempAnalSol-tempNumSol,2);
        		denominator+= std::pow(tempAnalSol,2);

        	}//end quadrature rule
        }//end loop over all subelements
    }//end if
    else {//all the not-enriched elements
        // select quadrature rule
        Dune::GeometryType gt = eg.geometry().type();
        const Dune::QuadratureRule<DF,dim_>& rule = Dune::QuadratureRules<DF,dim_>::rule(gt,6);
        //the local operator is elementwise defined and therefore the loop over the quadrature points is elementwise
        for (typename Dune::QuadratureRule<DF,dim_>::const_iterator it=rule.begin(); it!=rule.end(); ++it){
            u_fe*=0.0;
            Dune::FieldVector<ctype,dimw_> local=it->position();
            RangeType u_fe_temp(0.0);

            // evaluate finite element function at local coordinate (standard basis function)
            std::vector<RangeType> b(lbfs.maxSize()); // shape function values
            lbfs.finiteElement().localBasis().evaluateFunction(local,b);
            for (int i=0; i<lbfs.size(); i++){
//                std::cout << "i: " << multiIndex << "\t localIndex: " <<multilfsu_.localIndex(multiIndex)<< std::endl;
                u_fe_temp.axpy(xl[lbfs.localIndex(i)],b[i]);
            }
            u_fe+=u_fe_temp;

            typename AnalyticalPressureSolution::Traits::RangeType analSol=0.0;
            analyticalPressureSolution_.evaluateGlobal(eg.geometry().global(local),analSol);
            typedef typename AnalyticalPressureSolution::Traits::RangeType::value_type TempScalar;
            TempScalar tempAnalSol=analSol;
            TempScalar tempNumSol=u_fe;
            error+= std::pow(tempAnalSol-tempNumSol,2);
            denominator+= std::pow(tempAnalSol,2);
        }//end quadrature rules
    }//end all the not-enriched elements
}//end loop over all elements

return (std::sqrt(error/denominator));
}//end evaluate pressure function

RF evaluateVelocity() {

    RangeType u_fe(0.0);//TODO check what happens if u_fe is returned without a proper value (u_fe=0.0)?
    RangeType error(0.0);
    RangeType denominator(0.0);

    MultiLFSU multilfsu_(multigfs_);
    MultiLFSCache multilfs_cache(multilfsu_);
    ConstXView x_view(x_);

//    int children=MultiLFSU::CHILDREN;
//    std::cout<< "children: " << children << std::endl; //TODO make this the correct number of spaces!

//iterate over the whole matrix multidomaingrid and find the element in which global lies.
    //TODO: in general global lies always within an enriched element. Check if it can happen that the element is not enriched.
HostGridIterator eendit = multiDomainGrid_.template leafend<0>();
for (HostGridIterator matrixElementIterator = multiDomainGrid_.template leafbegin<0>(); matrixElementIterator != eendit; ++matrixElementIterator) {
    typedef typename Dune::FieldVector<RF,dimw_> GradP;
    typedef typename std::vector<GradP> GradPhi;

    MultiLFSCache multilfs_cache_(multilfsu_);
    ConstXView x_view_(x_);

    RangeType u_fe(0.0);//TODO check what happens if u_fe is returned without a proper value (u_fe=0.0)?

    multilfsu_.bind(*matrixElementIterator);
    EPLFS eplfs(multilfsu_,ep_);

    const LBFS& lbfs = eplfs.template child<0>();
    const LEFS0& lefs0 = eplfs.template child<1>();
    const LEFS1& lefs1 = eplfs.template child<2>();
    const LEFS2& lefs2 = eplfs.template child<3>();
    const LEFS3& lefs3 = eplfs.template child<4>();
    const LEFS4& lefs4 = eplfs.template child<5>();

    bool normalElement=true;
    if (lefs0.size()+lefs1.size()+lefs2.size()+lefs3.size()+lefs4.size() > 0){
        normalElement=false;
    }

    eplfs.bind();

    std::vector<RF> xl(lbfs.maxSize());        // local coefficients
    multilfs_cache_.update();
    x_view_.bind(multilfs_cache_);
    x_view_.read(xl);// get local coefficients of the solution

    if (normalElement){

        Dune::FieldVector<ctype,dimw_> globalElementCenter = matrixElementIterator->geometry().center();
        FV localElementCenter=matrixElementIterator->geometry().local(globalElementCenter);

        // evaluate gradient of basis functions on reference element
        std::vector<JacobianType> js(lbfs.size());
        lbfs.finiteElement().localBasis().evaluateJacobian(localElementCenter,js);
        // transform gradients from reference element to real element
        const Dune::FieldMatrix<DF,dimw_,dim_> jac = matrixElementIterator->geometry().jacobianInverseTransposed(localElementCenter);
        GradPhi gradphi(lbfs.size());
        for (sizeType i=0; i<lbfs.size(); i++){
            jac.mv(js[i][0],gradphi[i]);
        }

        GradP gradP(0.0);
        for (sizeType i=0; i<lbfs.size(); i++){
            gradP.axpy(xl[lbfs.localIndex(i)],gradphi[i]);
        }
        const Dune::FieldMatrix<Scalar,dimw_,dimw_> K=soil_.intrinsicPermeability();
        FV u(0.0);
        K.umv(gradP,u);

//            std::cout << "u("<<globalElementCenter<<")= " << u << std::endl;
        typename AnalyticalVelocitySolution::Traits::RangeType analSol(0.0);
        analyticalVelocitySolution_.evaluateGlobal(globalElementCenter,analSol);
        std::cout << "analSol: " << analSol << "\t u: " << u<< std::endl;
        denominator+= analSol.two_norm()*matrixElementIterator->geometry().integrationElement(localElementCenter);
        analSol-=u;
        error+= analSol.two_norm()* matrixElementIterator->geometry().integrationElement(localElementCenter);

    }// end here loop for normal elements


    else{

        MBF mbf(multiDomainGrid_, matrixElementIterator->entity(), intersectionPointsMap_, multilfsu_);

        //get element Id
        typedef typename MultiDomainGrid::Traits::GlobalIdSet::IdType IdType;
        const IdType elementId =multiDomainGrid_.globalIdSet().id(*matrixElementIterator);

        typedef typename LBFS::Traits::SizeType sizeType;

        typedef typename Map::mapped_type IntersectionPoints;
        IntersectionPoints intersectionPoints=(((*intersectionPointsMap_.find(elementId)).second));
        sizeType children=(lbfs.size()+lefs0.size()+lefs1.size()+lefs2.size()+lefs3.size()+lefs4.size())/4;
        assert(children<6);
        if (children>2) --children;

        typedef typename std::vector<FV> PointsNotOnCorners;
        PointsNotOnCorners pointsNotOnCorners(children);//TODO check if this is true for cuts through a corner!
        std::vector<FV> xfMiddle;
        xfMiddle.clear();
        FV check(-1000.0);//check means no entry
        xfMiddle.push_back(intersectionPoints[4].intersection);

        /*
         * lmax is 1 for ending fractures, 2 for single fractures, 3 for 3-crossings and 4 for 4-crossings.
         * Two not-crossing fractures have an lmax of 2.
         */
    //    int lmax=pointsNotOnCorners.size();
        for (int i=0;i<pointsNotOnCorners.size();i++){
            pointsNotOnCorners[i]=(intersectionPoints[i].intersection);
        }
        if (Dumux::arePointsEqual(intersectionPoints[1].intersection,check) ){//this indicates an ending fracture
            pointsNotOnCorners[1]=(intersectionPoints[5].intersection);
    //        lmax=1;
        }
        else if( !Dumux::arePointsEqual(intersectionPoints[5].intersection,check) ){//this indicates two not-crossing fractures
            xfMiddle.push_back(intersectionPoints[5].intersection);
            /*
             * there are 4 points on the boundary of the element,
             * but there are also two centre points, so that the for each xfMiddle there are only two pointsNotOnCorners.
             */
    //        lmax=2;
        }

        typedef typename std::vector<FV> SortedPoints;
        /*
         * for ending fractures the sub-triangulation is the same as for one single not bending fracture through the element
         * the fracture is extrapolated to the end of the element and a virtual exit point is determined
         * this happens in the gridCoupling and in line 62,
         * so that the vector pointsNotOnCorners does look the same as in the single not bending fracture case
         */
        SortedPoints sortedPoints;
        sortedPoints=Dumux::sortPoints<FV>(pointsNotOnCorners, *matrixElementIterator);
	typedef Dune::MultiLinearGeometry<ctype,dim_,dimw_> ElementGeometry;
        typedef Dune::GeometryType::BasicType BasicType;
        const BasicType simplex = Dune::GeometryType::simplex;
        typedef Dune::GeometryType GT;
        const GT subSimplex(simplex,dimw_);
        std::vector<FV> listOfCornerCoordinates(3);//this is a vector which stores all corners of the used subtriangle
        /*
         * this scheme is based on the DUNE node ordering as defined for the generic reference element cube
         * create subtriangles depending on the fracture geoemtry
         * for straight, bending and ending fractures always 6 subtriangles are generated
         * for 3-crossings 7 subtriangles are generated
         * for 4-crossings 8 subtriangles are generated
         * and for two not-crossing fractures 10 subtriangles are generated
         */
        assert(xfMiddle.size()<2);
        if (xfMiddle.size()==1){
            listOfCornerCoordinates[0]=matrixElementIterator->geometry().global(xfMiddle[0]);
            for (int i=0;i<4+children;i++){
                listOfCornerCoordinates[1]=matrixElementIterator->geometry().global(sortedPoints[i]);
                listOfCornerCoordinates[2]= (i<3+children ? matrixElementIterator->geometry().global(sortedPoints[i+1]) : matrixElementIterator->geometry().global(sortedPoints[0]) );
                ElementGeometry subElement(subSimplex,listOfCornerCoordinates);
                // calculate the centroid for the subtriangle
                Dune::FieldVector<ctype,dimw_> positionGlobal= (listOfCornerCoordinates[0]+listOfCornerCoordinates[1]+listOfCornerCoordinates[2]);
                positionGlobal/=3.0;
                FV position=matrixElementIterator->geometry().local(positionGlobal);

                GradP gradP(0.0);
                {
                    // evaluate gradient of basis functions on reference element
                    std::vector<JacobianType> js(lbfs.size());
                    lbfs.finiteElement().localBasis().evaluateJacobian(position,js);
                    // transform gradients from reference element to real element
                    const Dune::FieldMatrix<DF,dimw_,dim_> jac = matrixElementIterator->geometry().jacobianInverseTransposed(position);
                    GradPhi gradphi(lbfs.size());
                    for (sizeType i=0; i<lbfs.size(); i++){
                        jac.mv(js[i][0],gradphi[i]);
                    }
                    GradPhi gradPhiModified(lbfs.size());
                    gradPhiModified=mbf.modify(gradphi,position,0);

                    GradP gradPTemp(0.0);
                    for (sizeType i=0; i<lbfs.size(); i++){
                        gradPTemp.axpy(xl[lbfs.localIndex(i)],gradPhiModified[i]);
                    }
                    gradP+=gradPTemp;
                }

                // evaluate finite element function at local coordinate (enriched basis function)
                //TODO: works only if lblfs.size=lelfs.size, i.e. only ONE additional dof at every standard dof, i.e. no fracture crossings and only one fracture per element
                if (lefs0.size()>0){
                    std::vector<JacobianType> js(lefs0.size());
                    lefs0.finiteElement().localBasis().evaluateJacobian(position,js);
                    // transform gradients from reference element to real element
                    const Dune::FieldMatrix<DF,dimw_,dim_> jac = matrixElementIterator->geometry().jacobianInverseTransposed(position);
                    GradPhi gradphi(lefs0.size());
                    for (sizeType i=0; i<lefs0.size(); i++){
                        jac.mv(js[i][0],gradphi[i]);
                    }
                    GradPhi gradPhiModified(lefs0.size());
                    gradPhiModified=mbf.modify(gradphi,position,1);

                    GradP gradPTemp(0.0);
                    for (sizeType i=0; i<lefs0.size(); i++){
                        gradPTemp.axpy(xl[lefs0.localIndex(i)],gradPhiModified[i]);
                    }
                    gradP+=gradPTemp;
                }

                // evaluate finite element function at local coordinate (enriched basis function)
                //TODO: works only if lblfs.size=lelfs.size, i.e. only ONE additional dof at every standard dof, i.e. no fracture crossings and only one fracture per element
                if (lefs1.size()>0){
                    std::vector<JacobianType> js(lefs1.size());
                    lefs1.finiteElement().localBasis().evaluateJacobian(position,js);
                    // transform gradients from reference element to real element
                    const Dune::FieldMatrix<DF,dimw_,dim_> jac = matrixElementIterator->geometry().jacobianInverseTransposed(position);
                    GradPhi gradphi(lefs1.size());
                    for (sizeType i=0; i<lefs1.size(); i++){
                        jac.mv(js[i][0],gradphi[i]);
                    }
                    GradPhi gradPhiModified(lefs1.size());
                    gradPhiModified=mbf.modify(gradphi,position,2);

                    GradP gradPTemp(0.0);
                    for (sizeType i=0; i<lefs1.size(); i++){
                        gradPTemp.axpy(xl[lefs1.localIndex(i)],gradPhiModified[i]);
                    }
                    gradP+=gradPTemp;
                }
                // evaluate finite element function at local coordinate (enriched basis function)
                //TODO: works only if lblfs.size=lelfs.size, i.e. only ONE additional dof at every standard dof, i.e. no fracture crossings and only one fracture per element
                if (lefs2.size()>0){
                    std::vector<JacobianType> js(lefs2.size());
                    lefs2.finiteElement().localBasis().evaluateJacobian(position,js);
                    // transform gradients from reference element to real element
                    const Dune::FieldMatrix<DF,dimw_,dim_> jac = matrixElementIterator->geometry().jacobianInverseTransposed(position);
                    GradPhi gradphi(lefs2.size());
                    for (sizeType i=0; i<lefs2.size(); i++){
                        jac.mv(js[i][0],gradphi[i]);
                    }
                    GradPhi gradPhiModified(lefs2.size());
                    gradPhiModified=mbf.modify(gradphi,position,3);

                    GradP gradPTemp(0.0);
                    for (sizeType i=0; i<lefs2.size(); i++){
                        gradPTemp.axpy(xl[lefs2.localIndex(i)],gradPhiModified[i]);
                    }
                    gradP+=gradPTemp;
                }
                // evaluate finite element function at local coordinate (enriched basis function)
                //TODO: works only if lblfs.size=lelfs.size, i.e. only ONE additional dof at every standard dof, i.e. no fracture crossings and only one fracture per element
                if (lefs3.size()>0){
                    std::vector<JacobianType> js(lefs3.size());
                    lefs3.finiteElement().localBasis().evaluateJacobian(position,js);
                    // transform gradients from reference element to real element
                    const Dune::FieldMatrix<DF,dimw_,dim_> jac = matrixElementIterator->geometry().jacobianInverseTransposed(position);
                    GradPhi gradphi(lefs3.size());
                    for (sizeType i=0; i<lefs3.size(); i++){
                        jac.mv(js[i][0],gradphi[i]);
                    }
                    GradPhi gradPhiModified(lefs3.size());
                    gradPhiModified=mbf.modify(gradphi,position,4);

                    GradP gradPTemp(0.0);
                    for (sizeType i=0; i<lefs3.size(); i++){
                        gradPTemp.axpy(xl[lefs3.localIndex(i)],gradPhiModified[i]);
                    }
                    gradP+=gradPTemp;
                }
                // evaluate finite element function at local coordinate (enriched basis function)
                //TODO: works only if lblfs.size=lelfs.size, i.e. only ONE additional dof at every standard dof, i.e. no fracture crossings and only one fracture per element
                if (lefs4.size()>0){
                    std::vector<JacobianType> js(lefs4.size());
                    lefs4.finiteElement().localBasis().evaluateJacobian(position,js);
                    // transform gradients from reference element to real element
                    const Dune::FieldMatrix<DF,dimw_,dim_> jac = matrixElementIterator->geometry().jacobianInverseTransposed(position);
                    GradPhi gradphi(lefs4.size());
                    for (sizeType i=0; i<lefs4.size(); i++){
                        jac.mv(js[i][0],gradphi[i]);
                    }
                    GradPhi gradPhiModified(lefs4.size());
                    gradPhiModified=mbf.modify(gradphi,position,5);

                    GradP gradPTemp(0.0);
                    for (sizeType i=0; i<lefs4.size(); i++){
                        gradPTemp.axpy(xl[lefs4.localIndex(i)],gradPhiModified[i]);
                    }
                    gradP+=gradPTemp;
                }

                const Dune::FieldMatrix<Scalar,dimw_,dimw_> K=soil_.intrinsicPermeability();
                FV u(0.0);
                K.umv(gradP,u);
//    std::cout << "u("<<positionGlobal<<")= " << u << std::endl;
                typename AnalyticalVelocitySolution::Traits::RangeType analSol(0.0);
                analyticalVelocitySolution_.evaluateGlobal(positionGlobal,analSol);
                std::cout << "analSol: " << analSol << "\t u: " << u<< std::endl;
                denominator+= analSol.two_norm()*subElement.integrationElement(position);
                analSol-=u;
                error+= analSol.two_norm()* subElement.integrationElement(position);

            }//end loop over all subelements
        }//end if for all cases except two-not crossing fractures
    }//end else
}//end loop over all elements

return (std::sqrt(error/denominator));

}//end evaluate velocities function

private:
    MultiDomainGrid& multiDomainGrid_;
    Map& intersectionPointsMap_;
    MultiGFS& multigfs_;
    EnrichedProblem& ep_;
    X& x_;
    const Soil &soil_;
    AnalyticalPressureSolution &analyticalPressureSolution_;
    AnalyticalVelocitySolution &analyticalVelocitySolution_;
    MultiLFSU multilfsu_;

    static const int dimw_=MultiDomainGrid::dimensionworld;
    static const int dim_=MultiDomainGrid::dimension;


};//end class MatrixError
}//end namespace Dumux
#endif /* MATRIXERROR_HH_ */
