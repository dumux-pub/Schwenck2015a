template<typename B, typename Soil>
class FractureLocalOperatorDarcy : //derived from the following and using the CRTP-Trick!
  public Dune::PDELab::NumericalJacobianApplyVolume<FractureLocalOperatorDarcy<B,Soil> >,
  public Dune::PDELab::NumericalJacobianVolume<FractureLocalOperatorDarcy<B,Soil> >,
  public Dune::PDELab::NumericalJacobianApplyBoundary<FractureLocalOperatorDarcy<B,Soil> >,
  public Dune::PDELab::NumericalJacobianBoundary<FractureLocalOperatorDarcy<B,Soil> >,
  public Dune::PDELab::FullVolumePattern,
  public Dune::PDELab::LocalOperatorDefaultFlags


  {
  public:
    // pattern assembly flags
    enum { doPatternVolume = true };
//    enum { doPatternBoundary = true };TODO does not work

    // residual assembly flags
    enum { doAlphaVolume  = true };
    enum { doAlphaBoundary  = true };
    enum { doAlphaSkeleton  = false };                            // interface integrals

  FractureLocalOperatorDarcy (const B &b, const Soil &soil, unsigned int intorder=2)
    // integration order=2 means two evaluation points which is exact for linear functions
    :   Dune::PDELab::NumericalJacobianApplyVolume<FractureLocalOperatorDarcy<B,Soil> > (1e-1),
        Dune::PDELab::NumericalJacobianVolume<FractureLocalOperatorDarcy<B,Soil> > (1e-1),
        Dune::PDELab::NumericalJacobianApplyBoundary<FractureLocalOperatorDarcy<B,Soil> >(1e-1),
        Dune::PDELab::NumericalJacobianBoundary<FractureLocalOperatorDarcy<B,Soil> >(1e-1),
        b_(b), soil_(soil), intorder_(intorder)
  {}

  /*
   * Use this alpha_volume only for real elements
   */
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume (const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, R &r) const
  {
      typedef typename LFSU::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::RangeFieldType RF;
      typedef typename LFSU::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::DomainFieldType DF;
      typedef typename LFSU::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::JacobianType JacobianType;
      typedef typename LFSU::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::RangeType RangeType;
      typedef typename LFSU::Traits::SizeType size_type;

      const RF angle=soil_.angle(eg.entity());

      //TODO check gradient in fracture operator

    // dimensions
    const int dim = EG::Geometry::dimension;
    const int dimw = EG::Geometry::dimensionworld;

    typedef Dune::FieldVector<RF,dimw> FVGlobal;
    typedef Dune::FieldVector<RF,dim> FVLocal;
    if (angle<-0.5){
    const RF KFT=soil_.kFT(eg.entity());

    //extract soil parameter
    const RF alphaF=soil_.alphaF(eg.entity());//a large alphaF means high permeability normal through the fracture and small fracture width
    const RF xi=soil_.xi();//fumble parameter 0.5 < xi <= 1.0
    const RF d=soil_.fractureWidth(eg.entity());

    // select quadrature rule
    Dune::GeometryType gt = eg.geometry().type();
    const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder_);
    //the local operator is elementwise defined and therefore the loop over the quadrature points is elementwise
    // loop over quadrature points
    for (typename Dune::QuadratureRule<DF,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it){
        // evaluate basis functions on reference element
        std::vector<RangeType> phif(lfsu.size());
        lfsu.finiteElement().localBasis().evaluateFunction(it->position(),phif);

        // compute pf at integration point
        RF pf=0.0;
        for (size_type i=0; i<lfsu.size(); i++){
            pf += x(lfsu,i)*phif[i];
        }

        // evaluate gradient of basis functions on reference element
        std::vector<JacobianType> js(lfsu.size());
        lfsu.finiteElement().localBasis().evaluateJacobian(it->position(),js);

        // transform gradients from reference element to real element, but in fracture coordinates (i.e. adjusting only the length)
        RF jac= eg.geometry().integrationElement(it->position());//TODO integrationElement is constant at every integration point. so it could be placed outside this loop
        std::vector<FVLocal> gradphif(lfsu.size());//lfsu.size()=2 for 1D line elements here
        for (size_type i=0; i<lfsu.size(); i++)
        {
            gradphif[i]=js[i][0]/jac;//TODO: Only 1D, re-think for planes in 3D environment
//            std::cout << "gradphiF: " << gradphif[i] << std::endl;
        }
//        std::cout << "elementSize: " << jac << std::endl;

        // compute gradient of pf
        Dune::FieldVector<RF,dim> gradpf(0.0);
        for (size_type i=0; i<lfsu.size(); i++){
          gradpf.axpy(x(lfsu,i),gradphif[i]);
//          std::cout <<"gradphi("<<i<<"): " << gradphif[i] << "\t xs: "<<x(lfsu,i)<< std::endl;
        }

        // integrate KFT * grad p * grad phi_i and store it in r[i], i loops over every vertex of the current element
        RF factor = it->weight()*jac;

        //residual assembling
        //loop over local basis functions (one basis function per vertex)
        for (size_type i=0; i<lfsu.size(); i++)
        {r.accumulate(lfsu,i, ( ( d*KFT*gradpf * gradphif[i]) )*factor);}//note that gradpf, gradphif are scalar if the fracture is 1D!
        for (size_type i=0; i<lfsu.size(); i++)
        {r.accumulate(lfsu,i, ( (alphaF/(xi-0.5)* pf*phif[i]) )*factor);}
//        //natural source/sink term
//        RF k_m=1.0;
//        RF k=KFT/k_m;
////        typedef typename LFSU::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainType FV;
//        FVGlobal global= eg.geometry().global(it->position());
//        RF qf=(1.0-(1.0/k))*std::cos(global[0])*std::cosh(d/2.0)*(-1.0);
//        for (size_type i=0; i<lfsu.size(); i++)
//        {r.accumulate(lfsu,i, qf*phif[i]*factor*d);}

    }//end loop over quadrature points
  }//end if only for real elements
      else{

          //THIS PART IS NOT USED BECAUSE ALPHA VOLUME IS ONLY CALLED FOR NORMAL ELEMENTS, VIRTUAL ELEMENTS ARE ALWAYS ASSEMBLED IN JACOBIAN VOLUME
          typedef typename Dune::FieldMatrix<typename X::value_type,2,2> FM; //TODO replace 2,2 by dimworld,dimworld
          const FM KI=soil_.intrinsicPermeabilityCrossing(eg.entity());
          const RF angle=soil_.angle(eg.entity());
          const RF li=soil_.li(eg.entity());
          const FVGlobal ti=soil_.ti(eg.entity());
          const FM D=rotationMatrix<FM>(angle);

          /*
           * find pIIndex by checking
           * if the scalar product of the fracture orientation vector
           * and the vector between the element corners is one or minus one.
           * If it is 1 than both vectors point in the same direction and the
           * corner 1 is the crossing node (pIIndex=1 and pIOppositeIndex=0).
           * add tempEntry in position pI,pI
           * substract tempEntry in position pI,pi
           */
          size_type pIIndex;
          FVGlobal fi=Dumux::fractureOrientationVector<FVGlobal>(angle);
          FVGlobal elementOrientation=(eg.geometry().corner(1)-eg.geometry().corner(0));
          elementOrientation/=elementOrientation.two_norm();
          RF orientation=elementOrientation*fi;
          pIIndex = Dumux::arePointsEqual(orientation,1.0);
          size_type pIOppositeIndex = (pIIndex==0);
          /*
           * the fracture local gradient is always pI-pIOpposite,
           * but because of Darcy's law this has to be multiplied by (-1).
           */
          const RF tempEntry=-((KI[0][0]*D[0][0]+KI[0][1]*D[1][0])*ti[0] + (KI[0][1]*D[0][0]+KI[1][1]*D[1][0])*ti[1])*(x(lfsu,pIIndex)-x(lfsu,pIOppositeIndex))/li;
//          const RF tempEntry2=-(x(lfsu,pIIndex)-x(lfsu,pIOppositeIndex));
//          const RF tempEntry2=-(1.0e-4)*(x(lfsu,pIIndex)-x(lfsu,pIOppositeIndex));
//          std::cout << "tempEntry: " << tempEntry << std::endl;//"  const: " << tempEntry2 << std::endl;
          r.accumulate(lfsu,pIIndex,tempEntry);
          r.accumulate(lfsu,pIOppositeIndex,-tempEntry);
      }

  }//end alpha_volume


  //    template<typename EG, typename LFSV, typename R>
  //    void lambda_volume (const EG& eg, const LFSV& lfsv, R& r) const

  // boundary integral
  // for Neumann b.c. and inner nodes
    template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
    void alpha_boundary (const IG& ig, const LFSU& lfsu_s, const X& x_s,
                         const LFSV& lfsv_s, R& r_s) const

    {
        typedef typename LFSU::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename LFSU::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;
        typedef typename LFSU::Traits::SizeType size_type;

      // dimensions
      const int dim = IG::dimension;
//      const int dimw = IG::dimensionworld;

//      typedef Dune::FieldVector<RF,dimw> FVGlobal;

      // select quadrature rule for face
      Dune::GeometryType gtface = ig.geometryInInside().type();
      /*
       * The integration order for fracture boundary, which is a point in 2D, is set to one.
       */
      const Dune::QuadratureRule<DF,dim-1>&
        rule = Dune::QuadratureRules<DF,dim-1>::rule(gtface,1);

      // loop over quadrature points and integrate normal flux
      for (typename Dune::QuadratureRule<DF,dim-1>::const_iterator it=rule.begin();
     it!=rule.end(); ++it)
        {
          /*
           *  position of quadrature point on intersection in LOCAL coordinates of element
           */
          auto local = ig.geometryInInside().global(it->position());
          assert(Dumux::arePointsEqual( ig.inside()->geometry().local( ig.geometry().global(it->position()) ) ,local ) );
          // skip rest if we are on Dirichlet boundary
          //if this is a Neumann boundary assemble Neumann boundary term
          if ( b_.isNeumann(ig,it->position() ) ){
              RF jac= ig.geometry().integrationElement(it->position());
              RF factor = it->weight()*jac;
              // evaluate basis functions at integration point
              std::vector<RangeType> phif(lfsu_s.size());
              lfsu_s.finiteElement().localBasis().evaluateFunction(local,phif);
              // evaluate flux boundary condition
              RF j;
              j=b_.template neumannFlux<RF>(ig,it->position());
              for (size_type i=0; i<lfsu_s.size(); i++)
                  r_s.accumulate(lfsu_s,i, j*phif[i]*factor);
          }
          //this is neither Dirichlet nor Neumann, so treat that as free node with non-zero flux
          else if (b_.isEnding(ig,it->position())){
              typedef typename LFSU::Traits::FiniteElementType::
                      Traits::LocalBasisType::Traits::JacobianType JacobianType;
              typedef Dune::FieldVector<RF,dim> FVLocal;
              std::cout << "ending node at: "<< ig.geometry().global(it->position()) <<std::endl;
              // evaluate basis functions at integration point
              std::vector<RangeType> phif(lfsu_s.size());
              lfsu_s.finiteElement().localBasis().evaluateFunction(local,phif);

              // evaluate gradient of basis functions on reference element
              std::vector<JacobianType> js(lfsu_s.size());
              lfsu_s.finiteElement().localBasis().evaluateJacobian(local,js);

              // transform gradients from reference element to real element, but in fracture coordinates (i.e. adjusting only the length)
              RF jac= ig.geometry().integrationElement(it->position());
              std::vector<FVLocal> gradphif(lfsu_s.size());//lfsu.size()=2 for 1D line elements here
              for (size_type i=0; i<lfsu_s.size(); i++)
              {
                  gradphif[i]=js[i][0]/jac;//TODO: Only 1D, re-think for planes in 3D environment
              }

              // compute gradient of pf
              Dune::FieldVector<RF,dim> gradpf(0.0);
              for (size_type i=0; i<lfsu_s.size(); i++){
                  gradpf.axpy(x_s(lfsu_s,i),gradphif[i]);
              }

              RF factor = it->weight()*jac;

              /*
               * integrate - KFT * grad p * phi_i on the boundary of gamma
               * and store it in r[i], i loops over every vertex of the current element
               */
              const RF KFT=soil_.kFT(ig.inside());
//              std::cout << "KFT: " << KFT << "\t gradp: " << gradpf << "\t factor: "<<factor << std::endl;
              for (size_type i=0; i<lfsu_s.size(); i++){
//                  std::cout << "ending node boundary term: " << (-KFT)*gradpf*phif[i]*factor<<std::endl;
                  r_s.accumulate(lfsu_s,i, (-1.0)*(KFT)*gradpf*phif[i]*factor );//TODO VORZEICHEN???
              }
          }//end if ending node boundary term

        }
    }

  template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
   void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv,
                         M& mat) const
   {
      typedef typename LFSU::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::RangeFieldType RF;
      const RF angle=soil_.angle(eg.entity());

      if (angle>-0.5){
//          typedef typename LFSU::Traits::FiniteElementType::
//                  Traits::LocalBasisType::Traits::DomainFieldType DF;
//          typedef typename LFSU::Traits::FiniteElementType::
//                  Traits::LocalBasisType::Traits::JacobianType JacobianType;
//          typedef typename LFSU::Traits::FiniteElementType::
//                  Traits::LocalBasisType::Traits::RangeType RangeType;
          typedef typename LFSU::Traits::SizeType size_type;
          typedef typename Dune::FieldMatrix<typename X::value_type,2,2> FM; //TODO replace 2,2 by dimworld,dimworld
          typedef Dune::FieldVector<RF,2> FVGlobal;//TODO replace 2 by dimworld

          /*
           * for virtual elements accumulate for every fracture i the corresponding part of the sum:
           * \begin{equation}
           * \sum_i \left[ \left(  - \mathbf{K}_{\mathrm{I}} \left( \mathbf{D}(\theta_i) \left[(\grad{p_i})_{\mathrm{I}}\right]_f \right) \right) \mathbf{t}_{i,\mathrm{I}}^* \right]\mathrm{d}_{i,\mathrm{I}}^*=0
           * \end{equation}
           */

          const FM KI=soil_.intrinsicPermeabilityCrossing(eg.entity());
          const RF angle=soil_.angle(eg.entity());
          const RF li=soil_.li(eg.entity());
          const FVGlobal ti=soil_.ti(eg.entity());
          const FM D=rotationMatrix<FM>(angle);
//          std::cout << "ti: " << ti << "\t li: " << li << std::endl;

          const RF tempEntry=(-(KI[0][0]*D[0][0]+KI[0][1]*D[1][0])*ti[0] + (KI[0][1]*D[0][0]+KI[1][1]*D[1][0])*ti[1])/li;
//          const RF tempEntry=(-1.0e-4);
//          const RF tempEntry2=(1.0);
//          std::cout << "now accumulating in jacobian: " << tempEntry << " complex: " << tempEntry2<<  std::endl;
          /*
           * find pI line by checking
           * if the scalar product of the fracture orientation vector
           * and the vector between the element corners is one or minus one.
           * If it is one than both vectors point in the same direction and the
           * corner one is the crossing node.
           * add tempEntry in position pI,pI
           * substract tempEntry in position pI,pi
           */
          size_type pIIndex;
          FVGlobal fi=Dumux::fractureOrientationVector<FVGlobal>(angle);
          FVGlobal elementOrientation=(eg.geometry().corner(1)-eg.geometry().corner(0));
          elementOrientation/=elementOrientation.two_norm();
          RF orientation=elementOrientation*fi;
          pIIndex = Dumux::arePointsEqual(orientation,1.0);
          size_type pIOppositeIndex=(pIIndex==0);
//          std::cout << "tempEntry: " << tempEntry << std::endl;//"  const: " << tempEntry2 << std::endl;
          mat.accumulate(lfsu,pIIndex,lfsu,pIIndex,tempEntry);//pI,pI
          mat.accumulate(lfsu,pIIndex,lfsu,pIOppositeIndex,(-1.0)*tempEntry);//pI,pi
          mat.accumulate(lfsu,pIOppositeIndex,lfsu,pIIndex,(-1.0)*tempEntry);//pi,pI
          mat.accumulate(lfsu,pIOppositeIndex,lfsu,pIOppositeIndex,tempEntry);//pi,pi
      }//end if only for virtual elements
      else{//call alpha_volume
//          std::cout <<"now numerical jacobian"<<std::endl;
          typedef FractureLocalOperatorDarcy<B, Soil> ThisType;
          typedef Dune::PDELab::NumericalJacobianVolume<ThisType> NumericalJacobian;
          NumericalJacobian::jacobian_volume (eg, lfsu, x, lfsv, mat);
      }//end else for real elements
   }//end jacobian_volume

//  // jacobian of boundary term
//  template<typename IG, typename LFSU, typename X, typename LFSV, typename M>
//  void jacobian_boundary (const IG& ig, const LFSU& lfsu, const X& x, const LFSV& lfsv, M& mat) const
//  {}//end jacobian_boundary

private:
  const B &b_;
  const Soil &soil_;
  unsigned int intorder_;
  /*
   * returns the rotation matrix for a given angle theta in radians.
   */
  template<typename FM>
  FM rotationMatrix(const typename FM::value_type &theta) const{
      FM D;
      D[0][0]=std::cos(theta);
      D[1][1]=std::cos(theta);
      D[1][0]=std::sin(theta);
      D[0][1]=(-1.0)*std::sin(theta);
      return (D);
  }
};
