/*
 * The coupling is symmetric.
 * It happens only on the geometrical position of the fractures.
 * Nevertheless the coupling is formulated from the point of view of local matrix elements, because it
 * is much easier to get and assemble the correct fracture dofs than the stacked (enriched) matrix dofs.
 */


template<typename CouplingBlockEntries, typename MultiDomainGrid, typename FractureGrid,
    typename IntersectionPointsMap, typename CouplingMap, typename FractureGFS, typename XF, typename Soil, typename FB>
class SizeOfCouplingLOP : //derived from the following and using the CRTP-Trick!
  public Dune::PDELab::JacobianBasedAlphaVolume<
      SizeOfCouplingLOP<CouplingBlockEntries,MultiDomainGrid,FractureGrid,
      IntersectionPointsMap,CouplingMap,FractureGFS,XF,Soil, FB> >,
  public Dune::PDELab::JacobianBasedAlphaBoundary<
      SizeOfCouplingLOP<CouplingBlockEntries,MultiDomainGrid,FractureGrid,
      IntersectionPointsMap,CouplingMap,FractureGFS,XF,Soil, FB> >,
  public Dune::PDELab::FullVolumePattern,
  public Dune::PDELab::LocalOperatorDefaultFlags

  {
  public:
  // pattern assembly flags
  enum { doPatternVolume = true };
  // residual assembly flags
  enum { doAlphaVolume  = true };
  enum { doAlphaBoundary  = false };

  SizeOfCouplingLOP (CouplingBlockEntries &couplingBlockEntries, const MultiDomainGrid &multiDomainGrid, const FractureGrid &fractureGrid,
          IntersectionPointsMap &intersectionPointsMap, CouplingMap &couplingMap,
          const FractureGFS &fractureGFS, const XF& xf, const Soil &soil, const FB &fb, unsigned int intorder=3)
  :
      couplingBlockEntries_(couplingBlockEntries), multiDomainGrid_(multiDomainGrid), fractureGrid_(fractureGrid), intersectionPointsMap_(intersectionPointsMap),
    couplingMap_(couplingMap), fractureGFS_(fractureGFS), xf_(xf), soil_(soil), fb_(fb), intorder_(intorder), fix_pf_(false)
  {
      typedef typename MultiDomainGrid::SubDomainGrid SubDomainGrid;
      typedef typename MultiDomainGrid::LeafGridView MDGV;
      MDGV mdgv = multiDomainGrid.leafGridView();
      typedef typename SubDomainGrid::LeafGridView SDGV;
      const SubDomainGrid &sdg0 = multiDomainGrid.subDomain(0);
      SDGV sdgv0 = sdg0.leafGridView();
      const SubDomainGrid &sdg1 = multiDomainGrid.subDomain(1);
      SDGV sdgv1 = sdg1.leafGridView();
      const SubDomainGrid &sdg2 = multiDomainGrid.subDomain(2);
      SDGV sdgv2 = sdg2.leafGridView();
      const SubDomainGrid &sdg3 = multiDomainGrid.subDomain(3);
      SDGV sdgv3 = sdg3.leafGridView();
      const SubDomainGrid &sdg4 = multiDomainGrid.subDomain(4);
      SDGV sdgv4 = sdg4.leafGridView();
      offset_=mdgv.size(2)+sdgv0.size(2)+sdgv1.size(2)+sdgv2.size(2)+sdgv3.size(2)+sdgv4.size(2);
  }
  private:
      int offset_;

  public:

  template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
  void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv,
                        M& mat) const
  {
      //here it is assumed that the LFSU is a powergridfunctionspace,
      //i.e., all individual function spaces are of the same order!
      // domain and range field type (assume both components have same RF)
      typedef typename LFSU::template Child<0>::Type LGFSChild0;
      typedef typename LGFSChild0::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::DomainFieldType DF;
      typedef typename LGFSChild0::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::RangeFieldType RF;
//      typedef typename LGFSChild0::Traits::FiniteElementType::
//        Traits::LocalBasisType::Traits::JacobianType JacobianType;
      typedef typename LGFSChild0::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::RangeType RangeType;
      typedef typename LGFSChild0::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::DomainType FV;
      typedef typename LGFSChild0::Traits::SizeType sizeType;

      //get element Id
      typedef typename MultiDomainGrid::Traits::GlobalIdSet::IdType IdType;
      const IdType elementId =multiDomainGrid_.globalIdSet().id(eg.entity());

      typedef typename IntersectionPointsMap::mapped_type IntersectionPoints;
      IntersectionPoints intersectionPoints=(((*intersectionPointsMap_.find(elementId)).second));
      FV check(-1000.0);//check means no entry

      typedef typename CouplingMap::iterator CouplingMapIterator;
      CouplingMapIterator cmit=couplingMap_.find(elementId);
      assert(cmit != couplingMap_.end());
//      typedef std::list<int> FractureNodeIdxList;
//      typedef typename FractureNodeIdxList::iterator FractureNodeIdxLisIdxListIterator;

      typedef typename FractureGrid::Traits::template Codim<0>::EntitySeed FGES;
      typedef std::vector<FGES> VectorOfEntitySeeds;
      typedef typename VectorOfEntitySeeds::iterator VectorOfEntitySeedsIterator;


      typedef typename FractureGrid::LeafGridView FGV;
      typedef typename FGV::IntersectionIterator FractureIntersectionIterator;
      FGV fgv=fractureGrid_.leafGridView();

      typedef typename Dune::PDELab::LocalFunctionSpace<FractureGFS> FractureLFS;
      FractureLFS fracturelfs(fractureGFS_);

      const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();
      typedef typename LFSU::template Child<1>::Type LGFSChild1;
      const LGFSChild1 &lgfsChild1 = lfsu.template child<1>();
      typedef typename LFSU::template Child<2>::Type LGFSChild2;
      const LGFSChild2 &lgfsChild2 = lfsu.template child<2>();
      typedef typename LFSU::template Child<3>::Type LGFSChild3;
      const LGFSChild3 &lgfsChild3 = lfsu.template child<3>();
      typedef typename LFSU::template Child<4>::Type LGFSChild4;
      const LGFSChild4 &lgfsChild4 = lfsu.template child<4>();
      typedef typename LFSU::template Child<5>::Type LGFSChild5;
      const LGFSChild5 &lgfsChild5 = lfsu.template child<5>();

//      sizeType numberOfSpaces=(lgfsChild0.size()+lgfsChild1.size()+lgfsChild2.size()+lgfsChild3.size()+lgfsChild4.size()+lgfsChild5.size())/4;
      assert( ((lgfsChild0.size()+lgfsChild1.size()+lgfsChild2.size()+lgfsChild3.size()+lgfsChild4.size()+lgfsChild5.size())/4) <6);

      //create object which modifies the basis and test functions correctly
      typedef Dumux::modifiedBasisFunction<MultiDomainGrid, typename EG::Entity, IntersectionPointsMap, LFSU> MBF;
      const MBF mbf(multiDomainGrid_, eg.entity(), intersectionPointsMap_, lfsu);
      //extract soil parameters
      const RF xi=soil_.xi();//fumble parameter 0.5 < xi <= 1.0

      //create subelements of codimension one depending on the fracture geoemtry
      typedef typename Dune::ReferenceElements<ctype,dimw_> GRE;
      typedef Dune::GeometryType::BasicType BasicType;
      const BasicType simplex = Dune::GeometryType::simplex;
      typedef Dune::GeometryType GT;
      GT matrixElementGeometryType =eg.geometry().type();
      typedef Dune::MultiLinearGeometry<ctype,dimw_-1,dimw_> ElementGeometryFracture;
      const GT subSimplexFracture(simplex,dimw_-1);
      std::vector<FV> listOfCornerCoordinatesFracture(2);
      typedef typename std::vector<int> IndexVector;
      IndexVector listOfCornerIndicesFracture(2);
//      typedef typename IndexVector::iterator IndexVectorIterator;
      //iterate over all fracture subelements
      VectorOfEntitySeeds vectorOfEntitySeeds=(((*cmit).second));//FractureNodeIndexType
      for (VectorOfEntitySeedsIterator esIt=vectorOfEntitySeeds.begin(); esIt!=vectorOfEntitySeeds.end();esIt++ ){
          fracturelfs.bind(*(fractureGrid_.entityPointer(*esIt)));
          const RF alphaF=soil_.alphaF(*(fractureGrid_.entityPointer(*esIt)));
          FractureIntersectionIterator isItEnd = fgv.iend( *(fractureGrid_.entityPointer(*esIt)) );
          std::vector<bool> endingNodeTreatment(2,false);
          for (FractureIntersectionIterator isIt = fgv.ibegin( *(fractureGrid_.entityPointer(*esIt)) ); isIt != isItEnd; ++isIt){
              //TODO this is only for 1d ending fractures
              Dune::FieldVector<RF,0> position(0.0);
              if ( fb_.isEnding( *isIt, position) ){
                  endingNodeTreatment[isIt->indexInInside()]=true;
              }
              listOfCornerCoordinatesFracture[isIt->indexInInside() ]= isIt->geometry().center();
          }//end loop over fracture nodes

          /*
           * if the fracture element node is within this matrix element, then use it as node for the integration subelement.
           * Otherwise use the intersection point with the element boundary.
           */
          std::vector<bool> notInside(2,false);
          for (int i=0;i<2;i++){
              if ( !GRE::general(matrixElementGeometryType).checkInside( eg.geometry().local( listOfCornerCoordinatesFracture[i] ) ) ){
                  notInside[i]=true;
                  RF distanceOnLine=1.0e6;
                  FV closestPoint(check);
                  FV thisClosestPoint(check);
                  FV minClosestPoint(check);
                  RF thisDistance=1.0e6;
                  RF minDistance=1.0e6;
//                  std::cout << "_________________________________"<<std::endl;
                  for (int j=0;j<4;j++){//index 4 and 5 only store the midpoints/virtual exit points
                      /*
                       * if there is an empty entry there are no more relevant entries after that
                       * for ending fractures the projected exit is not to be used!
                       */
                      if (Dumux::arePointsEqual(intersectionPoints[j].intersection,check)){
                          break;
                      }
                      else if( Dumux::pointOnLine(listOfCornerCoordinatesFracture[0],
                              listOfCornerCoordinatesFracture[1],
                              eg.geometry().global(intersectionPoints[j].intersection)) //make the comparison in global coordinates
//      ||
//      Dumux::pointOnLine(eg.geometry().local(listOfCornerCoordinatesFracture[0]),
//                              eg.geometry().local(listOfCornerCoordinatesFracture[1]),
//                              intersectionPoints[j].intersection) //make the comparison in local coordinates
                      ) {
                          /*
                           * make sure that always the intersectionPoint closest to the old fracture node is chosen
                           */
                          if (Dumux::arePointsEqual(closestPoint,check) ) {
                              closestPoint=eg.geometry().global(intersectionPoints[j].intersection);//but store the global coordinates
                              //clean up closest Point zeros
                              for (int indexK=0;indexK<2;indexK++){
                                  if (Dumux::arePointsEqual(closestPoint[indexK],0.0)) closestPoint[indexK]=0.0;
                              }
                              distanceOnLine=Dumux::absDistance(listOfCornerCoordinatesFracture[i],closestPoint);
                          }
                          else if (Dumux::absDistance(listOfCornerCoordinatesFracture[i],eg.geometry().global(intersectionPoints[j].intersection)) < distanceOnLine){
                              //if the distance to the second intersectionPoint is smaller than to the first, replace the closestPoint by this intersectionPoint
                              closestPoint=eg.geometry().global(intersectionPoints[j].intersection);
                          }
                      }
                      //debug output
                      else{
                          thisClosestPoint=Dumux::closestPointOnLine(listOfCornerCoordinatesFracture[0],
                                                                  listOfCornerCoordinatesFracture[1],
                                                                  eg.geometry().global(intersectionPoints[j].intersection));
                          thisDistance=Dumux::absDistance(eg.geometry().global(intersectionPoints[j].intersection),thisClosestPoint);
//                          std::cout << "this distance: " << thisDistance << s" from: " << thisClosestPoint << " to: " << eg.geometry().global(intersectionPoints[j].intersection)<<std::endl;
                          if (thisDistance<minDistance){
                              minDistance=thisDistance;
                              minClosestPoint=eg.geometry().global(intersectionPoints[j].intersection);
                          }
                      }
                  }//end loop over intersectionPoints
                  if (Dumux::arePointsEqual(closestPoint,check)){
                      std::cout << "WARNING: point is not on line but "<< minDistance << " away!!!!!!!!!!!!!!!" << std::endl;
                      closestPoint=minClosestPoint;
                  }
                  assert(!Dumux::arePointsEqual(closestPoint,check));
                  listOfCornerCoordinatesFracture[i] = closestPoint;
              }//end if one fracture node does not lie within this matrix element
          }//end loop over fracture nodes
          //every subelement is defined by its corners
          if (Dumux::arePointsEqual(listOfCornerCoordinatesFracture[0],listOfCornerCoordinatesFracture[1])) {continue;}

          for (sizeType i=0; i<lgfsChild0.size(); i++){
              auto globalIndexRow=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild0.dofIndex(i)) )[0];
              for (sizeType j=0; j<fracturelfs.size();j++){
                  auto globalIndexColumn=(fracturelfs.gridFunctionSpace().ordering().mapIndex( fracturelfs.dofIndex(j)))[0]+offset_;
                  couplingBlockEntries_[globalIndexRow].push_back(globalIndexColumn);
                  couplingBlockEntries_[globalIndexColumn].push_back(globalIndexRow);
              }
          }

          if (lgfsChild1.size()!=0){
              for (sizeType i=0; i<lgfsChild1.size(); i++){
                  auto globalIndexRow=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild1.dofIndex(i)) )[0];
                  for (sizeType j=0; j<fracturelfs.size();j++){
                      auto globalIndexColumn=(fracturelfs.gridFunctionSpace().ordering().mapIndex( fracturelfs.dofIndex(j)))[0]+offset_;
                      couplingBlockEntries_[globalIndexRow].push_back(globalIndexColumn);
                      couplingBlockEntries_[globalIndexColumn].push_back(globalIndexRow);
                  }
              }
          }
          if (lgfsChild2.size()!=0){
              for (sizeType i=0; i<lgfsChild2.size(); i++){
                  auto globalIndexRow=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild2.dofIndex(i)) )[0];
                  for (sizeType j=0; j<fracturelfs.size();j++){
                      auto globalIndexColumn=(fracturelfs.gridFunctionSpace().ordering().mapIndex( fracturelfs.dofIndex(j)))[0]+offset_;
                      couplingBlockEntries_[globalIndexRow].push_back(globalIndexColumn);
                      couplingBlockEntries_[globalIndexColumn].push_back(globalIndexRow);
                  }
              }
          }
          if (lgfsChild3.size()!=0){
              for (sizeType i=0; i<lgfsChild3.size(); i++){
                  auto globalIndexRow=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild3.dofIndex(i)) )[0];
                  for (sizeType j=0; j<fracturelfs.size();j++){
                      auto globalIndexColumn=(fracturelfs.gridFunctionSpace().ordering().mapIndex( fracturelfs.dofIndex(j)))[0]+offset_;
                      couplingBlockEntries_[globalIndexRow].push_back(globalIndexColumn);
                      couplingBlockEntries_[globalIndexColumn].push_back(globalIndexRow);
                  }
              }
          }
          if (lgfsChild4.size()!=0){
              for (sizeType i=0; i<lgfsChild4.size(); i++){
                  auto globalIndexRow=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild4.dofIndex(i)) )[0];
                  for (sizeType j=0; j<fracturelfs.size();j++){
                      auto globalIndexColumn=(fracturelfs.gridFunctionSpace().ordering().mapIndex( fracturelfs.dofIndex(j)))[0]+offset_;
                      couplingBlockEntries_[globalIndexRow].push_back(globalIndexColumn);
                      couplingBlockEntries_[globalIndexColumn].push_back(globalIndexRow);
                  }
              }
          }
          if (lgfsChild5.size()!=0){
              for (sizeType i=0; i<lgfsChild5.size(); i++){
                  auto globalIndexRow=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild5.dofIndex(i)) )[0];
                  for (sizeType j=0; j<fracturelfs.size();j++){
                      auto globalIndexColumn=(fracturelfs.gridFunctionSpace().ordering().mapIndex( fracturelfs.dofIndex(j)))[0]+offset_;
                      couplingBlockEntries_[globalIndexRow].push_back(globalIndexColumn);
                      couplingBlockEntries_[globalIndexColumn].push_back(globalIndexRow);
                  }
              }
          }
      }//end loop over fracture subelements
  }//end jacobian_volume

protected:
  static const int dimw_=MultiDomainGrid::dimensionworld;

private:
  CouplingBlockEntries &couplingBlockEntries_;
  const MultiDomainGrid &multiDomainGrid_;
  const FractureGrid &fractureGrid_;
  IntersectionPointsMap &intersectionPointsMap_;
  CouplingMap &couplingMap_;
  const FractureGFS &fractureGFS_;
  const XF &xf_;
  const Soil &soil_;
  const FB &fb_;
  unsigned int intorder_;
  bool fix_pf_;

  //some type extraction
  typedef typename MultiDomainGrid::ctype ctype;
};
