#ifndef MATRIXLOCALOPERATORENRICHEDDARCY_HH_
#define MATRIXLOCALOPERATORENRICHEDDARCY_HH_

/*
 * TODO: DOC ME!
 */

template <typename B, typename MultiDomainGrid, typename FractureGrid, typename IntersectionPointsMap, typename CouplingMap, typename Soil>
class MatrixLocalOperatorEnrichedDarcy :
//  public Dune::PDELab::NumericalJacobianApplyVolume<MatrixLocalOperatorEnrichedDarcy<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil> >,
//  public Dune::PDELab::NumericalJacobianVolume<MatrixLocalOperatorEnrichedDarcy<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil> >,
  public Dune::PDELab::JacobianBasedAlphaVolume<MatrixLocalOperatorEnrichedDarcy<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil> >,
  public Dune::PDELab::NumericalJacobianApplyBoundary<MatrixLocalOperatorEnrichedDarcy<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil> >,
  public Dune::PDELab::NumericalJacobianBoundary<MatrixLocalOperatorEnrichedDarcy<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil> >,
//  public Dune::PDELab::JacobianBasedAlphaBoundary<MatrixLocalOperatorEnrichedDarcy<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil> >,

  public Dune::PDELab::FullVolumePattern,
  public Dune::PDELab::LocalOperatorDefaultFlags

  {
    typedef typename MultiDomainGrid::ctype ctype;
public:
  // pattern assembly flags
  enum { doPatternVolume = true };
//  enum { doPatternBoundary = true };//TODO does not work

  // residual assembly flags
  enum { doAlphaVolume = true };
  enum { doAlphaBoundary  = true };//TODO only homogeneous Neumann bc are possible!

  MatrixLocalOperatorEnrichedDarcy (const B &b,const MultiDomainGrid &multiDomainGrid, const FractureGrid &fractureGrid, IntersectionPointsMap &intersectionPointsMap,
          CouplingMap &couplingMap, const Soil &soil, unsigned int intorder=6)
    :
//        Dune::PDELab::NumericalJacobianApplyVolume<MatrixLocalOperatorEnrichedDarcy<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil> >(1e-4),// takes as argument the epsilon for the evaluation of numerical Jacobian
//        Dune::PDELab::NumericalJacobianVolume<MatrixLocalOperatorEnrichedDarcy<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil> >(1e-4),
        Dune::PDELab::NumericalJacobianApplyBoundary<MatrixLocalOperatorEnrichedDarcy<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil> >(1e-2),
        Dune::PDELab::NumericalJacobianBoundary<MatrixLocalOperatorEnrichedDarcy<B,MultiDomainGrid,FractureGrid,IntersectionPointsMap,CouplingMap,Soil> >(1e-2),
        b_(b),
        multiDomainGrid_(multiDomainGrid),
        fractureGrid_(fractureGrid),
        intersectionPointsMapLocal_(intersectionPointsMap),
        couplingMap_(couplingMap),
        soil_(soil),
        xi_(soil_.xi()),
        intorder_(intorder),
        intorderF_(2)
  {}

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {
      /*
       * here it is assumed that the LFSU is a powergridfunctionspace,
       * i.e., all individual function spaces are of the same order!
       * domain and range field type (assume both components have same Scalar)
       */
      typedef typename LFSU::template Child<0>::Type LGFSChild; //always child 0 is used for type extraction even if child zero is not "present" at this element
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::DomainType FV;
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::RangeFieldType Scalar;
      typedef typename LGFSChild::Traits::SizeType sizeType;


      typedef typename LFSU::template Child<0>::Type LGFSChild0;
      typedef typename LFSU::template Child<1>::Type LGFSChild1;
      typedef typename LFSU::template Child<2>::Type LGFSChild2;
      typedef typename LFSU::template Child<3>::Type LGFSChild3;
      typedef typename LFSU::template Child<4>::Type LGFSChild4;
      typedef typename LFSU::template Child<5>::Type LGFSChild5;
      const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();
      const LGFSChild1 &lgfsChild1 = lfsu.template child<1>();
      const LGFSChild2 &lgfsChild2 = lfsu.template child<2>();
      const LGFSChild3 &lgfsChild3 = lfsu.template child<3>();
      const LGFSChild4 &lgfsChild4 = lfsu.template child<4>();
      const LGFSChild5 &lgfsChild5 = lfsu.template child<5>();

    //get element Id
    typedef typename MultiDomainGrid::Traits::GlobalIdSet::IdType IdType;
    const IdType elementId =multiDomainGrid_.globalIdSet().id(eg.entity());

    typedef typename IntersectionPointsMap::mapped_type IntersectionPoints;
    IntersectionPoints intersectionPoints=(((*intersectionPointsMapLocal_.find(elementId)).second));
    sizeType children=(lgfsChild0.size()+lgfsChild1.size()+lgfsChild2.size()+lgfsChild3.size()+lgfsChild4.size()+lgfsChild5.size())/4;
    assert(children<6);
    if (children>2) --children;

//    typedef typename std::vector<FV> PointsNotOnCorners;
    typedef typename IntersectionPoints::value_type Intersection;
    typedef typename std::vector<Intersection> StdVecOfIntersections;
    StdVecOfIntersections pointsNotOnCorners(children);//TODO check if this is true for cuts through a corner!
    std::vector<FV> xfMiddle;
    xfMiddle.clear();
    FV check(-1000.0);//check means no entry
    xfMiddle.push_back(intersectionPoints[4].intersection);

    /*
     * lmax is 1 for ending fractures, 2 for single fractures, 3 for 3-crossings and 4 for 4-crossings.
     * Two not-crossing fractures have an lmax of 2.
     */
//    int lmax=pointsNotOnCorners.size();
    for (int i=0;i<pointsNotOnCorners.size();i++){
//        pointsNotOnCorners[i]=(intersectionPoints[i].intersection);
        pointsNotOnCorners[i]=(intersectionPoints[i]);
    }
    if (Dumux::arePointsEqual(intersectionPoints[1].intersection,check) ){//this indicates an ending fracture
//        pointsNotOnCorners[1]=(intersectionPoints[5].intersection);
        pointsNotOnCorners[1]=(intersectionPoints[5]);
//        lmax=1;
    }
    else if( !Dumux::arePointsEqual(intersectionPoints[5].intersection,check) ){//this indicates two not-crossing fractures
        xfMiddle.push_back(intersectionPoints[5].intersection);
        std::cout << "xfMiddle.size(): " << xfMiddle.size() <<std::endl;
        /*
         * there are 4 points on the boundary of the element,
         * but there are also two center points, so that the for each xfMiddle there are only two pointsNotOnCorners.
         */
//        lmax=2;
    }

    //create object which modifies the basis and test functions correctly
    typedef Dumux::modifiedBasisFunction<MultiDomainGrid, typename EG::Entity, IntersectionPointsMap, LFSU> MBF;
    MBF mbf(multiDomainGrid_, eg.entity(), intersectionPointsMapLocal_, lfsu, true);











    //create sub-elements of co-dimension one depending on the fracture geometry
    typedef typename CouplingMap::iterator CouplingMapIterator;
    CouplingMapIterator cmit=couplingMap_.find(elementId);
    assert(cmit != couplingMap_.end());

    typedef typename FractureGrid::Traits::template Codim<0>::EntitySeed FGES;
    typedef std::vector<FGES> VectorOfEntitySeeds;
    typedef typename VectorOfEntitySeeds::iterator VectorOfEntitySeedsIterator;

    typedef typename FractureGrid::LeafGridView FGV;
    typedef typename FGV::IntersectionIterator FractureIntersectionIterator;
    FGV fgv=fractureGrid_.leafGridView();
    typedef typename MultiDomainGrid::ctype ctype;
    const int worlddim=MultiDomainGrid::dimensionworld;
    typedef typename Dune::ReferenceElements<ctype,worlddim> GRE;
    typedef Dune::GeometryType GT;
    GT matrixElementGeometryType =eg.geometry().type();

    typedef Dune::MultiLinearGeometry<ctype,dimw_-1,dimw_> ElementGeometryFracture;
    typedef Dune::GeometryType::BasicType BasicType;
    const BasicType simplex = Dune::GeometryType::simplex;
    const GT subSimplexFracture(simplex,dimw_-1);
    std::vector<FV> listOfCornerCoordinatesFracture(2);
    //iterate over all fracture sub-elements
    VectorOfEntitySeeds vectorOfEntitySeeds=(((*cmit).second));//FractureNodeIndexType
    for (VectorOfEntitySeedsIterator esIt=vectorOfEntitySeeds.begin(); esIt!=vectorOfEntitySeeds.end();esIt++ ){
    	const Scalar alphaF=this->template alphaF<Scalar>(*(fractureGrid_.entityPointer(*esIt)));
    	FractureIntersectionIterator isItEnd = fgv.iend( *(fractureGrid_.entityPointer(*esIt)) );
    	for (FractureIntersectionIterator isIt = fgv.ibegin( *(fractureGrid_.entityPointer(*esIt)) ); isIt != isItEnd; ++isIt){
    		listOfCornerCoordinatesFracture[isIt->indexInInside()]= isIt->geometry().center();
    	}//end loop over fracture nodes

    	/*
    	 * if the fracture element node is within this matrix element, then use it as node for the integration subelement.
    	 * Otherwise use the intersection point with the element boundary.
    	 */
    	for (int i=0;i<2;i++){
    		if ( !GRE::general(matrixElementGeometryType).checkInside( eg.geometry().local( listOfCornerCoordinatesFracture[i] ) ) ){
    			Scalar distanceOnLine=1.0e6;
    			FV closestPoint(check);
    			FV thisClosestPoint(check);
    			FV minClosestPoint(check);
    			Scalar thisDistance=1.0e6;
    			Scalar minDistance=1.0e6;
    			for (int j=0;j<4;j++){//index 4 and 5 only store the midpoints/virtual exit points
    				/*
    				 * if there is an empty entry there are no more relevant entries after that
    				 * for ending fractures the projected exit is not to be used!
    				 */
    				if (Dumux::arePointsEqual(intersectionPoints[j].intersection,check)){
    					break;
    				}
    				else if(Dumux::pointOnLine(listOfCornerCoordinatesFracture[0],
    						listOfCornerCoordinatesFracture[1],
    						eg.geometry().global(intersectionPoints[j].intersection)) //make the comparison in global coordinates
    				) {
    					/*
    					 * make sure that always the intersectionPoint closest to the old fracture node is chosen
    					 */
    					if (Dumux::arePointsEqual(closestPoint,check) ) {
    						closestPoint=eg.geometry().global(intersectionPoints[j].intersection);//and store the global coordinates
    						//clean up closest Point zeros
    						for (int indexK=0;indexK<2;indexK++){
    							if (Dumux::arePointsEqual(closestPoint[indexK],0.0)) closestPoint[indexK]=0.0;
    						}
    						distanceOnLine=Dumux::absDistance(listOfCornerCoordinatesFracture[i],closestPoint);
    					}
    					else if (Dumux::absDistance(listOfCornerCoordinatesFracture[i],eg.geometry().global(intersectionPoints[j].intersection)) < distanceOnLine){
    						//if the distance to the second intersectionPoint is smaller than to the first, replace the closestPoint by this intersectionPoint
    						closestPoint=eg.geometry().global(intersectionPoints[j].intersection);
    					}
    				}
    				//debug output
    				else{
    					thisClosestPoint=Dumux::closestPointOnLine(listOfCornerCoordinatesFracture[0],
    							listOfCornerCoordinatesFracture[1],
    							eg.geometry().global(intersectionPoints[j].intersection));
    					thisDistance=Dumux::absDistance(eg.geometry().global(intersectionPoints[j].intersection),thisClosestPoint);
    					if (thisDistance<minDistance){
    						minDistance=thisDistance;
    						minClosestPoint=eg.geometry().global(intersectionPoints[j].intersection);
    					}
    				}
    			}//end loop over intersectionPoints
    			if (Dumux::arePointsEqual(closestPoint,check)){
    				std::cout << "A WARNING: point is not on line but "<< minDistance << " away!!!!!!!!!!!!!!!" << std::endl;
    				closestPoint=minClosestPoint;
    			}
    			assert(!Dumux::arePointsEqual(closestPoint,check));
    			listOfCornerCoordinatesFracture[i] = closestPoint;
    		}//end if one fracture node does not lie within this matrix element
    	}//end loop over fracture nodes
    	//every sub-element is defined by its corners
    	if (Dumux::arePointsEqual(listOfCornerCoordinatesFracture[0],listOfCornerCoordinatesFracture[1])) {continue;}
    	ElementGeometryFracture subElementFracture(subSimplexFracture,listOfCornerCoordinatesFracture);


    	GT gtf=subElementFracture.type();//subSimplex;
    	// select quadrature rule for the subelement
    	const Dune::QuadratureRule<ctype,dimw_-1>& ruleFace = Dune::QuadratureRules<ctype,dimw_-1>::rule(gtf,intorderF_); //integration on type, integration order
    	//start loop for integration over fracture subelement
    	for (typename Dune::QuadratureRule<ctype,dimw_-1>::const_iterator it=ruleFace.begin(); it!=ruleFace.end(); ++it)
    	{
    		// position of quadrature point in local coordinates of element
    		Dune::FieldVector<ctype,dimw_-1> const localOnFracture=it->position();
    		//transforms coordinates of integration point on the lower dimensional fracture into the dimw_orld dimensional global coordinates
    		FV const globalPos = subElementFracture.global( localOnFracture );
    		//transforms global coordinates of integration point into local rock matrix element coordinates
    		FV const xQP = eg.geometry().local( globalPos );
    		/*
    		 * For bi-linear transformations of non-standard quadrilateral elements,
    		 * the integration point is slightly moved so that
    		 * the evaluation for [[phi]] and {{phi}} works correctly.
    		 */
    		FV intersectionPointForThisBranch(intersectionPoints[4].intersection);
    		for (int k=0;k<4;++k){
    			if(Dumux::arePointsEqual(intersectionPoints[k].intersection,check)) break;
    			intersectionPointForThisBranch=intersectionPoints[k].intersection;
    			if (!Dumux::pointOnLine(intersectionPoints[4].intersection,intersectionPointForThisBranch,xQP)
    			&& Dumux::pointOnLine(eg.geometry().global(intersectionPoints[4].intersection),eg.geometry().global(intersectionPointForThisBranch),globalPos)){
    				mbf.insertSubpolygonPoint(intersectionPointForThisBranch,xQP);
    			}
    		}

    		Scalar factor = it->weight()*subElementFracture.integrationElement(localOnFracture);//global fracture length within this element
    		Scalar uJump=0.0;
    		Scalar uAverage=0.0;
    		evaluateU(lfsu, mbf, x, xQP, uJump, uAverage);
    		accumulateMatrixResidualCouplingTerm(lfsu, mbf, x, xQP, factor, alphaF, uJump, uAverage, r);
    	}

    }//end loop over fracture entities connected to this matrix element




//    //old
//    StdVecOfIntersections sortedPoints;
//    sortedPoints=Dumux::sortPoints(pointsNotOnCorners, eg);
//    //    typedef Dune::GenericGeometry::BasicGeometry<dimw_, Dune::GenericGeometry::DefaultGeometryTraits<ctype,dimw_,dimw_> > ElementGeometry;
//    typedef Dune::MultiLinearGeometry<ctype,dim_,dimw_> ElementGeometry;
//    typedef Dune::GeometryType::BasicType BasicType;
//    const GT subSimplex(simplex,dimw_);
//    std::vector<FV> listOfCornerCoordinates(3);//this is a vector which stores all corners of the used subtriangle
//    /*
//     * this scheme is based on the DUNE node ordering as defined for the generic reference element cube
//     * create subtriangles depending on the fracture geoemtry
//     * for straight, bending and ending fractures always 6 subtriangles are generated
//     * for 3-crossings 7 subtriangles are generated
//     * for 4-crossings 8 subtriangles are generated
//     * and for two not-crossing fractures 10 subtriangles are generated
//     */
//    listOfCornerCoordinates[0]=eg.geometry().global(xfMiddle[0]);
//    for (int i=0;i<4+children;i++){
//    	listOfCornerCoordinates[1]=eg.geometry().global(sortedPoints[i].intersection);
//    	listOfCornerCoordinates[2]= (i<3+children ? eg.geometry().global(sortedPoints[i+1].intersection) : eg.geometry().global(sortedPoints[0].intersection) );
//    	ElementGeometry subElement(subSimplex,listOfCornerCoordinates);
//    	//accumulate the residuals for the matrix part of the element (without coupling terms)
//    	for (int cIdx=0;cIdx<3;++cIdx){
//    		std::cout << "old: " << listOfCornerCoordinates[cIdx] << "\t";
//    	}
//    	std::cout << std::endl;
//    	evaluateMatrixResidual(eg, lfsu, mbf, x, r, subElement);
//    }//end loop over all subelements









    /*
     * for ending fractures the sub-triangulation is the same as for one single not bending fracture through the element
     * the fracture is extrapolated to the end of the element and a virtual exit point is determined
     * this happens in the gridCoupling and in line 62,
     * so that the vector pointsNotOnCorners does look the same as in the single not bending fracture case
     *
     * this scheme is based on the DUNE node ordering as defined for the generic reference element cube
     * create sub-triangles depending on the fracture geometry
     * for straight, bending and ending fractures always 6 sub-triangles are generated
     * for 3-crossings 7 sub-triangles are generated
     * for 4-crossings 8 sub-triangles are generated
     * and for two not-crossing fractures 10 sub-triangles are generated
     */
    mbf.createSimplicesFromSubpolygons();
    for (int sIdx=0; sIdx<mbf.subSimplices.size();++sIdx){
//    	for (int cIdx=0;cIdx<3;++cIdx){
//    		std::cout << "new: " << (mbf.subSimplices[sIdx]).corner(cIdx) << "\t";
//    	}
//    	std::cout << std::endl;
        //accumulate the residuals for the matrix part of the element (without coupling terms)
        evaluateMatrixResidual(eg, lfsu, mbf, x, r, mbf.subSimplices[sIdx]);
    }
    assert(xfMiddle.size()==1);
    assert(xfMiddle.size()<=2);


  }//end alpha volume


  template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
  void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv,
                        M& m) const
  {

      /*
       * here it is assumed that the LFSU is a powergridfunctionspace,
       * i.e., all individual function spaces are of the same order!
       * domain and range field type (assume both components have same Scalar)
       */
      typedef typename LFSU::template Child<0>::Type LGFSChild; //always child 0 is used for type extraction even if child zero is not "present" at this element
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::DomainType FV;
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::RangeFieldType Scalar;
      typedef typename LGFSChild::Traits::SizeType sizeType;



      typedef typename LFSU::template Child<0>::Type LGFSChild0;
      typedef typename LFSU::template Child<1>::Type LGFSChild1;
      typedef typename LFSU::template Child<2>::Type LGFSChild2;
      typedef typename LFSU::template Child<3>::Type LGFSChild3;
      typedef typename LFSU::template Child<4>::Type LGFSChild4;
      typedef typename LFSU::template Child<5>::Type LGFSChild5;
      const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();
      const LGFSChild1 &lgfsChild1 = lfsu.template child<1>();
      const LGFSChild2 &lgfsChild2 = lfsu.template child<2>();
      const LGFSChild3 &lgfsChild3 = lfsu.template child<3>();
      const LGFSChild4 &lgfsChild4 = lfsu.template child<4>();
      const LGFSChild5 &lgfsChild5 = lfsu.template child<5>();

    //get element Id
    typedef typename MultiDomainGrid::Traits::GlobalIdSet::IdType IdType;
    const IdType elementId =multiDomainGrid_.globalIdSet().id(eg.entity());

    typedef typename IntersectionPointsMap::mapped_type IntersectionPoints;
    IntersectionPoints intersectionPoints=(((*intersectionPointsMapLocal_.find(elementId)).second));
    sizeType children=(lgfsChild0.size()+lgfsChild1.size()+lgfsChild2.size()+lgfsChild3.size()+lgfsChild4.size()+lgfsChild5.size())/4;
    assert(children<6);
    if (children>2) --children;

    typedef typename IntersectionPoints::value_type Intersection;
    typedef typename std::vector<Intersection> StdVecOfIntersections;
    StdVecOfIntersections pointsNotOnCorners(children);//TODO check if this is true for cuts through a corner!
    std::vector<FV> xfMiddle;
    xfMiddle.clear();
    FV check(-1000.0);//check means no entry
    xfMiddle.push_back(intersectionPoints[4].intersection);

    /*
     * lmax is 1 for ending fractures, 2 for single fractures, 3 for 3-crossings and 4 for 4-crossings.
     * Two not-crossing fractures have an lmax of 2.
     */
//    int lmax=pointsNotOnCorners.size();
    for (int i=0;i<pointsNotOnCorners.size();i++){
//        pointsNotOnCorners[i]=(intersectionPoints[i].intersection);
        pointsNotOnCorners[i]=(intersectionPoints[i]);
    }
    if (Dumux::arePointsEqual(intersectionPoints[1].intersection,check) ){//this indicates an ending fracture
//        pointsNotOnCorners[1]=(intersectionPoints[5].intersection);
        pointsNotOnCorners[1]=(intersectionPoints[5]);
//        lmax=1;
    }
    else if( !Dumux::arePointsEqual(intersectionPoints[5].intersection,check) ){//this indicates two not-crossing fractures
        xfMiddle.push_back(intersectionPoints[5].intersection);
        /*
         * there are 4 points on the boundary of the element,
         * but there are also two center points, so that the for each xfMiddle there are only two pointsNotOnCorners.
         */
//        lmax=2;
    }

    //create object which modifies the basis and test functions correctly
    typedef Dumux::modifiedBasisFunction<MultiDomainGrid, typename EG::Entity, IntersectionPointsMap, LFSU> MBF;
    MBF mbf(multiDomainGrid_, eg.entity(), intersectionPointsMapLocal_, lfsu, true);





    //create sub-elements of co-dimension one depending on the fracture geometry
    typedef typename CouplingMap::iterator CouplingMapIterator;
    CouplingMapIterator cmit=couplingMap_.find(elementId);
    assert(cmit != couplingMap_.end());

    typedef typename FractureGrid::Traits::template Codim<0>::EntitySeed FGES;
    typedef std::vector<FGES> VectorOfEntitySeeds;
    typedef typename VectorOfEntitySeeds::iterator VectorOfEntitySeedsIterator;

    typedef typename FractureGrid::LeafGridView FGV;
    typedef typename FGV::IntersectionIterator FractureIntersectionIterator;
    FGV fgv=fractureGrid_.leafGridView();
    const int worlddim=MultiDomainGrid::dimensionworld;
    typedef typename Dune::ReferenceElements<ctype,worlddim> GRE;
    typedef Dune::GeometryType GT;
    GT matrixElementGeometryType =eg.geometry().type();

    typedef Dune::MultiLinearGeometry<ctype,dimw_-1,dimw_> ElementGeometryFracture;
    typedef Dune::GeometryType::BasicType BasicType;
    const BasicType simplex = Dune::GeometryType::simplex;
    const GT subSimplexFracture(simplex,dimw_-1);
    std::vector<FV> listOfCornerCoordinatesFracture(2);
    //iterate over all fracture sub-elements
    VectorOfEntitySeeds vectorOfEntitySeeds=(((*cmit).second));//FractureNodeIndexType
    for (VectorOfEntitySeedsIterator esIt=vectorOfEntitySeeds.begin(); esIt!=vectorOfEntitySeeds.end();esIt++ ){
    	const Scalar alphaF=this->template alphaF<Scalar>(*(fractureGrid_.entityPointer(*esIt)));
    	FractureIntersectionIterator isItEnd = fgv.iend( *(fractureGrid_.entityPointer(*esIt)) );
    	int i=0;
    	for (FractureIntersectionIterator isIt = fgv.ibegin( *(fractureGrid_.entityPointer(*esIt)) ); isIt != isItEnd; ++isIt){
    		listOfCornerCoordinatesFracture[i]= isIt->geometry().center();
    		i++;
    	}//end loop over fracture nodes

    	/*
    	 * if the fracture element node is within this matrix element, then use it as node for the integration sub-element.
    	 * Otherwise use the intersection point with the element boundary.
    	 */
    	for (int i=0;i<2;i++){
    		if ( !GRE::general(matrixElementGeometryType).checkInside( eg.geometry().local( listOfCornerCoordinatesFracture[i] ) ) ){
    			Scalar distanceOnLine=1.0e6;
    			FV closestPoint(check);
    			FV thisClosestPoint(check);
    			FV minClosestPoint(check);
    			Scalar thisDistance=1.0e6;
    			Scalar minDistance=1.0e6;
    			for (int j=0;j<4;j++){//index 4 and 5 only store the midpoints/virtual exit points
    				/*
    				 * if there is an empty entry there are no more relevant entries after that
    				 * for ending fractures the projected exit is not to be used!
    				 */
    				if (Dumux::arePointsEqual(intersectionPoints[j].intersection,check)){
    					break;
    				}
    				else if( Dumux::pointOnLine(listOfCornerCoordinatesFracture[0],
    						listOfCornerCoordinatesFracture[1],
    						eg.geometry().global(intersectionPoints[j].intersection)) //make the comparison in global coordinates
    				) {
    					/*
    					 * make sure that always the intersectionPoint closest to the old fracture node is chosen
    					 */
    					if (Dumux::arePointsEqual(closestPoint,check) ) {
    						closestPoint=eg.geometry().global(intersectionPoints[j].intersection);//and store the global coordinates
    						//clean up closest Point zeros
    						for (int indexK=0;indexK<2;indexK++){
    							if (Dumux::arePointsEqual(closestPoint[indexK],0.0)) closestPoint[indexK]=0.0;
    						}
    						distanceOnLine=Dumux::absDistance(listOfCornerCoordinatesFracture[i],closestPoint);
    					}
    					else if (Dumux::absDistance(listOfCornerCoordinatesFracture[i],eg.geometry().global(intersectionPoints[j].intersection)) < distanceOnLine){
    						//if the distance to the second intersectionPoint is smaller than to the first, replace the closestPoint by this intersectionPoint
    						closestPoint=eg.geometry().global(intersectionPoints[j].intersection);
    					}
    				}
    				//debug output
    				else{
    					thisClosestPoint=Dumux::closestPointOnLine(listOfCornerCoordinatesFracture[0],
    							listOfCornerCoordinatesFracture[1],
    							eg.geometry().global(intersectionPoints[j].intersection));
    					thisDistance=Dumux::absDistance(eg.geometry().global(intersectionPoints[j].intersection),thisClosestPoint);
    					if (thisDistance<minDistance){
    						minDistance=thisDistance;
    						minClosestPoint=eg.geometry().global(intersectionPoints[j].intersection);
    					}
    				}
    			}//end loop over intersectionPoints
    			if (Dumux::arePointsEqual(closestPoint,check)){
    				std::cout << "B WARNING: point is not on line but "<< minDistance << " away!!!!!!!!!!!!!!!" << std::endl;
    				closestPoint=minClosestPoint;
    			}
    			assert(!Dumux::arePointsEqual(closestPoint,check));
    			listOfCornerCoordinatesFracture[i] = closestPoint;
    		}//end if one fracture node does not lie within this matrix element
    	}//end loop over fracture nodes
    	if (Dumux::arePointsEqual(listOfCornerCoordinatesFracture[0],listOfCornerCoordinatesFracture[1])) {continue;}
    	//every sub-element is defined by its corners
    	ElementGeometryFracture subElementFracture(subSimplexFracture,listOfCornerCoordinatesFracture);


    	GT gtf=subElementFracture.type();//subSimplex;
    	// select quadrature rule for the subelement
    	const Dune::QuadratureRule<ctype,dimw_-1>& ruleFace = Dune::QuadratureRules<ctype,dimw_-1>::rule(gtf,intorderF_); //integration on type, integration order
    	//start loop for integration over fracture subelement
    	for (typename Dune::QuadratureRule<ctype,dimw_-1>::const_iterator it=ruleFace.begin(); it!=ruleFace.end(); ++it)
    	{
    		// position of quadrature point in local coordinates of element
    		Dune::FieldVector<ctype,dimw_-1> const localOnFracture=it->position();
    		//transforms coordinates of integration point on the lower dimensional fracture into the dimw_orld dimensional global coordinates
    		FV const globalPos = subElementFracture.global( localOnFracture );
    		//transforms global coordinates of integration point into local rock matrix element coordinates
    		FV const xQP = eg.geometry().local( globalPos );
    		/*
    		 * For bi-linear transformations of non-standard quadrilateral elements,
    		 * the integration point is slightly moved so that
    		 * the evaluation for [[phi]] and {{phi}} works correctly.
    		 */
    		FV intersectionPointForThisBranch(intersectionPoints[4].intersection);
    		for (int k=0;k<4;++k){
    			if(Dumux::arePointsEqual(intersectionPoints[k].intersection,check)) break;
    			intersectionPointForThisBranch=intersectionPoints[k].intersection;
    			if (!Dumux::pointOnLine(intersectionPoints[4].intersection,intersectionPointForThisBranch,xQP)
    			&& Dumux::pointOnLine(eg.geometry().global(intersectionPoints[4].intersection),eg.geometry().global(intersectionPointForThisBranch),globalPos)){
    				mbf.insertSubpolygonPoint(intersectionPointForThisBranch,xQP);
    			}
    		}
    		Scalar factor = it->weight()*subElementFracture.integrationElement(localOnFracture);//global fracture length within this element
    		accumulateMatrixJacobianCouplingTerm(lfsu, mbf, x, xQP, factor, alphaF, m);
    	}

    }//end loop over fracture entities connected to this matrix element





    //old
//    StdVecOfIntersections sortedPoints;
//    sortedPoints=Dumux::sortPoints(pointsNotOnCorners, eg);
//    //    typedef Dune::GenericGeometry::BasicGeometry<dimw_, Dune::GenericGeometry::DefaultGeometryTraits<ctype,dimw_,dimw_> > ElementGeometry;
//    typedef Dune::MultiLinearGeometry<ctype,dim_,dimw_> ElementGeometry;
//    typedef Dune::GeometryType::BasicType BasicType;
//    const GT subSimplex(simplex,dimw_);
//    std::vector<FV> listOfCornerCoordinates(3);//this is a vector which stores all corners of the used subtriangle
//    /*
//     * this scheme is based on the DUNE node ordering as defined for the generic reference element cube
//     * create subtriangles depending on the fracture geoemtry
//     * for straight, bending and ending fractures always 6 subtriangles are generated
//     * for 3-crossings 7 subtriangles are generated
//     * for 4-crossings 8 subtriangles are generated
//     * and for two not-crossing fractures 10 subtriangles are generated
//     */
//    listOfCornerCoordinates[0]=eg.geometry().global(xfMiddle[0]);
//    for (int i=0;i<4+children;i++){
//    	listOfCornerCoordinates[1]=eg.geometry().global(sortedPoints[i].intersection);
//    	listOfCornerCoordinates[2]= (i<3+children ? eg.geometry().global(sortedPoints[i+1].intersection) : eg.geometry().global(sortedPoints[0].intersection) );
//    	ElementGeometry subElement(subSimplex,listOfCornerCoordinates);
//    	//accumulate the residuals for the matrix part of the element (without coupling terms)
//    	for (int cIdx=0;cIdx<3;++cIdx){
//    		std::cout << "old: " << listOfCornerCoordinates[cIdx] << "\t";
//    	}
//    	std::cout << std::endl;
//    	evaluateMatrixJacobian(eg, lfsu, mbf, x, m,subElement);
//    }//end loop over all subelements








    /*
     * for ending fractures the sub-triangulation is the same as for one single not bending fracture through the element
     * the fracture is extrapolated to the end of the element and a virtual exit point is determined
     * this happens in the gridCoupling and in line 62,
     * so that the vector pointsNotOnCorners does look the same as in the single not bending fracture case
     *
     * this scheme is based on the DUNE node ordering as defined for the generic reference element cube
     * create sub-triangles depending on the fracture geometry
     * for straight, bending and ending fractures always 6 sub-triangles are generated
     * for 3-crossings 7 sub-triangles are generated
     * for 4-crossings 8 sub-triangles are generated
     * and for two not-crossing fractures 10 sub-triangles are generated
     */
    mbf.createSimplicesFromSubpolygons();
    for (int sIdx=0; sIdx<mbf.subSimplices.size();++sIdx){
    	//accumulate the residuals for the matrix part of the element (without coupling terms)
    	evaluateMatrixJacobian(eg, lfsu, mbf, x, m,mbf.subSimplices[sIdx]);
//    	for (int cIdx=0;cIdx<3;++cIdx){
//    		std::cout << "new: " << (mbf.subSimplices[sIdx]).corner(cIdx) << "\t";
//    	}
//    	std::cout << std::endl;
    }
    assert(xfMiddle.size()==1);
    assert(xfMiddle.size()<=2);

  }//end jacobian_volume

protected:

  template<typename EG, typename LFSU, typename MBF, typename X, typename R, typename SubEG>
  void evaluateMatrixResidual(const EG &eg, const LFSU &lfsu, const MBF &mbf, const X &x, R &r, const SubEG &subElement) const{
      typedef typename LFSU::template Child<0>::Type LGFSChild;       //HACK: Assuming that every type is the same
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::RangeFieldType Scalar;
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::DomainType FV;
      GT gt=subElement.type();//subSimplex;
      const Dune::QuadratureRule<ctype,dimw_>& rule = Dune::QuadratureRules<ctype,dim_>::rule(gt,intorder_);
      for (typename Dune::QuadratureRule<ctype,dimw_>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
      {
          /*
           * it->position is on the local sub-element
           * subElement.global makes it world global
           * eg.geometry().local transforms it into the local matrix element
           */
          FV xQP = eg.geometry().local( subElement.global( it->position() ) );
          /*
           * factor consists of the weight and the integrationElement, which is for triangles DOUBLE the element area.
           * and for cubes just the element area.
           * here integrationElement of the sub-element is called with the local position of the quadrature point within this sub-element
           */
          Scalar factor = it->weight()*subElement.integrationElement(it->position());
          typedef typename Dune::FieldVector<Scalar,dimw_> GradU;
          const GradU gradu=evaluateGradU< GradU > (eg, lfsu, mbf, x, xQP);
          const Dune::FieldMatrix<Scalar,dimw_,dimw_> K=soil_.intrinsicPermeability();
          GradU vector1(0.0);
          K.umv(gradu,vector1);
          accumulateMatrixResidual(eg, lfsu, mbf, x, xQP, factor, vector1, r);
      }
  }//end evaluateMatrixResidual

  template<typename EG, typename LFSU, typename MBF, typename X, typename M, typename SubEG>
  void evaluateMatrixJacobian(const EG &eg, const LFSU &lfsu, const MBF &mbf, const X &x, M &m, const SubEG &subElement) const{
      typedef typename LFSU::template Child<0>::Type LGFSChild;       //HACK: Assuming that every type is the same
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::RangeFieldType Scalar;
      typedef typename LGFSChild::Traits::FiniteElementType::
              Traits::LocalBasisType::Traits::DomainType FV;

      GT gt=subElement.type();//subSimplex;
      const Dune::QuadratureRule<ctype,dimw_>& rule = Dune::QuadratureRules<ctype,dim_>::rule(gt,intorder_);
      for (typename Dune::QuadratureRule<ctype,dimw_>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
      {
          /*
           * it->position is on the local sub-element
           * subElement.global makes it world global
           * eg.geometry().local transforms it into the local matrix element
           */
          FV xQP = eg.geometry().local( subElement.global( it->position() ) );
          /*
           * factor consists of the weight and the integrationElement, which is for triangles DOUBLE the element area.
           * and for cubes just the element area.
           * here integrationElement of the sub-element is called with the local position of the quadrature point within this sub-element
           */
          Scalar factor = it->weight()*subElement.integrationElement(it->position());
          accumulateMatrixJacobian(eg, lfsu, mbf, x, xQP, factor, m);
      }
  }//end evaluateMatrixJacobian

  //evaluateGradPhi needs to get the child function space!!! and the position of it
  template<typename GradPhi, typename EG, typename LGFSChild, typename MBF, typename FV >
  GradPhi evaluateGradPhi(const EG &eg, const LGFSChild &lgfsChild, const MBF &mbf, const FV &xQP, const int &spaceNumber) const{
//    typedef typename LGFSChild::Traits::FiniteElementType::
//            Traits::LocalBasisType::Traits::RangeFieldType Scalar;
    typedef typename LGFSChild::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::JacobianType JacobianType;
    typedef typename LGFSChild::Traits::SizeType sizeType;
    // evaluate gradient of basis functions on reference element
    std::vector<JacobianType> js(lgfsChild.size());
    lgfsChild.finiteElement().localBasis().evaluateJacobian(xQP,js);
    // transform gradients from reference element to real element
    const Dune::FieldMatrix<ctype,dimw_,dim_> jac = eg.geometry().jacobianInverseTransposed(xQP);
    GradPhi gradphi(lgfsChild.size());
    for (sizeType i=0; i<lgfsChild.size(); i++){
        jac.mv(js[i][0],gradphi[i]);
    }
    //modify gradients of basis functions
    GradPhi gradphiModified=mbf.modify(gradphi,xQP,spaceNumber);
    return (gradphiModified);
}

template<typename GradU, typename EG, typename LGFS, typename MBF, typename X, typename FV >
GradU evaluateGradU(const EG &eg, const LGFS &lgfs, const MBF &mbf, const X& x, const FV &xQP) const{
    typedef typename LGFS::template Child<0>::Type LGFSChild0;
    typedef typename LGFSChild0::Traits::SizeType sizeType0;
    typedef typename LGFS::template Child<1>::Type LGFSChild1;
    typedef typename LGFSChild1::Traits::SizeType sizeType1;
    typedef typename LGFS::template Child<2>::Type LGFSChild2;
    typedef typename LGFSChild2::Traits::SizeType sizeType2;
    typedef typename LGFS::template Child<3>::Type LGFSChild3;
    typedef typename LGFSChild3::Traits::SizeType sizeType3;
    typedef typename LGFS::template Child<4>::Type LGFSChild4;
    typedef typename LGFSChild4::Traits::SizeType sizeType4;
    typedef typename LGFS::template Child<5>::Type LGFSChild5;
    typedef typename LGFSChild5::Traits::SizeType sizeType5;


    typedef typename std::vector<GradU> GradPhi;
    GradU gradu(0.0);

    const LGFSChild0 &lgfsChild0 = lgfs.template child<0>();
    GradPhi gradPhi=evaluateGradPhi< GradPhi > (eg, lgfsChild0, mbf, xQP, 0);
    for (sizeType0 i=0; i<lgfsChild0.size(); i++){
        gradu.axpy(x(lgfsChild0,i),gradPhi[i]);
    }
    const LGFSChild1 &lgfsChild1 = lgfs.template child<1>();
    if (lgfsChild1.size()!=0){
        GradPhi gradPhi=evaluateGradPhi< GradPhi > (eg, lgfsChild1, mbf, xQP, 1);
        for (sizeType1 i=0; i<lgfsChild1.size(); i++){
            gradu.axpy(x(lgfsChild1,i),gradPhi[i]);
        }
    }
    const LGFSChild2 &lgfsChild2 = lgfs.template child<2>();
    if (lgfsChild2.size()!=0){
        GradPhi gradPhi=evaluateGradPhi< GradPhi > (eg, lgfsChild2, mbf, xQP, 2);
        for (sizeType2 i=0; i<lgfsChild2.size(); i++){
            gradu.axpy(x(lgfsChild2,i),gradPhi[i]);
        }
    }
    const LGFSChild3 &lgfsChild3 = lgfs.template child<3>();
    if (lgfsChild3.size()!=0){
        GradPhi gradPhi=evaluateGradPhi< GradPhi > (eg, lgfsChild3, mbf, xQP, 3);
        for (sizeType3 i=0; i<lgfsChild3.size(); i++){
            gradu.axpy(x(lgfsChild3,i),gradPhi[i]);
        }
    }
    const LGFSChild4 &lgfsChild4 = lgfs.template child<4>();
    if (lgfsChild4.size()!=0){
        GradPhi gradPhi=evaluateGradPhi< GradPhi > (eg, lgfsChild4, mbf, xQP, 4);
        for (sizeType4 i=0; i<lgfsChild4.size(); i++){
            gradu.axpy(x(lgfsChild4,i),gradPhi[i]);
        }
    }
    const LGFSChild5 &lgfsChild5 = lgfs.template child<5>();
    if (lgfsChild5.size()!=0){
        GradPhi gradPhi=evaluateGradPhi< GradPhi > (eg, lgfsChild5, mbf, xQP, 5);
        for (sizeType5 i=0; i<lgfsChild5.size(); i++){
            gradu.axpy(x(lgfsChild5,i),gradPhi[i]);
        }
    }

    return (gradu);
}

template<typename EG, typename LFSU, typename MBF, typename X, typename FV, typename Factor,typename U, typename R>
void accumulateMatrixResidual(const EG &eg, const LFSU &lfsu, const MBF &mbf,
        const X& x, const FV &xQP, const Factor &factor, const U &u, R &r) const{
    typedef typename LFSU::template Child<0>::Type LGFSChild0;
    typedef typename LGFSChild0::Traits::SizeType sizeType0;
    typedef typename LFSU::template Child<1>::Type LGFSChild1;
    typedef typename LGFSChild1::Traits::SizeType sizeType1;
    typedef typename LFSU::template Child<2>::Type LGFSChild2;
    typedef typename LGFSChild2::Traits::SizeType sizeType2;
    typedef typename LFSU::template Child<3>::Type LGFSChild3;
    typedef typename LGFSChild3::Traits::SizeType sizeType3;
    typedef typename LFSU::template Child<4>::Type LGFSChild4;
    typedef typename LGFSChild4::Traits::SizeType sizeType4;
    typedef typename LFSU::template Child<5>::Type LGFSChild5;
    typedef typename LGFSChild5::Traits::SizeType sizeType5;
//    typedef typename LGFSChild0::Traits::FiniteElementType::
//            Traits::LocalBasisType::Traits::RangeFieldType Scalar;

    typedef typename std::vector<U> GradPhi;

    const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();
    const GradPhi gradPhi= evaluateGradPhi< GradPhi >(eg, lgfsChild0, mbf, xQP,0);
    for (sizeType0 i=0; i<lgfsChild0.size(); i++){
        r.accumulate(lgfsChild0,i,( u * gradPhi[i] )*factor);
    }

    const LGFSChild1 &lgfsChild1 = lfsu.template child<1>();
    if (lgfsChild1.size()!=0){
        const GradPhi gradPhi= evaluateGradPhi< GradPhi >(eg, lgfsChild1, mbf, xQP,1);
        for (sizeType1 i=0; i<lgfsChild1.size(); i++){
            r.accumulate(lgfsChild1,i,( u * gradPhi[i] )*factor);
        }
    }
    const LGFSChild2 &lgfsChild2 = lfsu.template child<2>();
    if (lgfsChild2.size()!=0){
        const GradPhi gradPhi= evaluateGradPhi< GradPhi >(eg, lgfsChild2, mbf, xQP,2);
        for (sizeType2 i=0; i<lgfsChild2.size(); i++){
            r.accumulate(lgfsChild2,i,( u * gradPhi[i] )*factor);
        }
    }
    const LGFSChild3 &lgfsChild3 = lfsu.template child<3>();
    if (lgfsChild3.size()!=0){
        const GradPhi gradPhi= evaluateGradPhi< GradPhi >(eg, lgfsChild3, mbf, xQP,3);
        for (sizeType3 i=0; i<lgfsChild3.size(); i++){
            r.accumulate(lgfsChild3,i,( u * gradPhi[i] )*factor);
        }
    }
    const LGFSChild4 &lgfsChild4 = lfsu.template child<4>();
    if (lgfsChild4.size()!=0){
        const GradPhi gradPhi= evaluateGradPhi< GradPhi >(eg, lgfsChild4, mbf, xQP,4);
        for (sizeType4 i=0; i<lgfsChild4.size(); i++){
            r.accumulate(lgfsChild4,i,( u * gradPhi[i] )*factor);
        }
    }
    const LGFSChild5 &lgfsChild5 = lfsu.template child<5>();
    if (lgfsChild5.size()!=0){
        const GradPhi gradPhi= evaluateGradPhi< GradPhi >(eg, lgfsChild5, mbf, xQP,5);
        for (sizeType5 i=0; i<lgfsChild5.size(); i++){
            r.accumulate(lgfsChild5,i,( u * gradPhi[i] )*factor);
        }
    }

}//end accumulateMatrixResidual

template<typename EG, typename LFSU, typename MBF, typename X, typename FV, typename Factor, typename M>
void accumulateMatrixJacobian(const EG &eg, const LFSU &lfsu, const MBF &mbf,
        const X& x, const FV &xQP, const Factor &factor, M &m) const{
    typedef typename LFSU::template Child<0>::Type LGFSChild0;
    typedef typename LGFSChild0::Traits::SizeType sizeType0;
    typedef typename LFSU::template Child<1>::Type LGFSChild1;
    typedef typename LGFSChild1::Traits::SizeType sizeType1;
    typedef typename LFSU::template Child<2>::Type LGFSChild2;
    typedef typename LGFSChild2::Traits::SizeType sizeType2;
    typedef typename LFSU::template Child<3>::Type LGFSChild3;
    typedef typename LGFSChild3::Traits::SizeType sizeType3;
    typedef typename LFSU::template Child<4>::Type LGFSChild4;
    typedef typename LGFSChild4::Traits::SizeType sizeType4;
    typedef typename LFSU::template Child<5>::Type LGFSChild5;
    typedef typename LGFSChild5::Traits::SizeType sizeType5;

    typedef typename LGFSChild0::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::RangeFieldType Scalar;
    typedef typename Dune::FieldVector<Scalar,dimw_> GradU;
    typedef typename std::vector<GradU> GradPhi;

    const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();
    const LGFSChild1 &lgfsChild1 = lfsu.template child<1>();
    const LGFSChild2 &lgfsChild2 = lfsu.template child<2>();
    const LGFSChild3 &lgfsChild3 = lfsu.template child<3>();
    const LGFSChild4 &lgfsChild4 = lfsu.template child<4>();
    const LGFSChild5 &lgfsChild5 = lfsu.template child<5>();

    const Dune::FieldMatrix<Scalar,dimw_,dimw_> K=soil_.intrinsicPermeability();

    const GradPhi gradPhi0= evaluateGradPhi< GradPhi >(eg, lgfsChild0, mbf, xQP,0);
    GradPhi kGradPhi0(lgfsChild0.size());
    for (sizeType0 i=0; i<lgfsChild0.size(); i++){
        K.mv(gradPhi0[i],kGradPhi0[i]);
    }

    for (sizeType0 rowIdx=0; rowIdx<lgfsChild0.size(); rowIdx++){
        for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
            m.accumulate(lgfsChild0,rowIdx,lgfsChild0,colIdx,
                    gradPhi0[colIdx]*kGradPhi0[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
        }
        if (lgfsChild1.size()!=0){
            const GradPhi gradPhi1= evaluateGradPhi< GradPhi >(eg, lgfsChild1, mbf, xQP,1);
            for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild1,colIdx
                                                  ,gradPhi1[colIdx]*kGradPhi0[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
        }
        if (lgfsChild2.size()!=0){
            const GradPhi gradPhi2= evaluateGradPhi< GradPhi >(eg, lgfsChild2, mbf, xQP,2);
            for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild2,colIdx
                                                  ,gradPhi2[colIdx]*kGradPhi0[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
        }
        if (lgfsChild3.size()!=0){
            const GradPhi gradPhi3= evaluateGradPhi< GradPhi >(eg, lgfsChild3, mbf, xQP,3);
            for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild3,colIdx
                                                  ,gradPhi3[colIdx]*kGradPhi0[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
        }
        if (lgfsChild4.size()!=0){
            const GradPhi gradPhi4= evaluateGradPhi< GradPhi >(eg, lgfsChild4, mbf, xQP,4);
            for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild4,colIdx
                                                  ,gradPhi4[colIdx]*kGradPhi0[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
        }
        if (lgfsChild5.size()!=0){
            const GradPhi gradPhi5= evaluateGradPhi< GradPhi >(eg, lgfsChild5, mbf, xQP,5);
            for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild5,colIdx
                                                  ,gradPhi5[colIdx]*kGradPhi0[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
        }
    }//end loop over test function lines associated to child space 0

    if (lgfsChild1.size()!=0){
        const GradPhi gradPhi1= evaluateGradPhi< GradPhi >(eg, lgfsChild1, mbf, xQP,1);
        GradPhi kGradPhi1(lgfsChild1.size());
        for (sizeType1 i=0; i<lgfsChild1.size(); i++){
            K.mv(gradPhi1[i],kGradPhi1[i]);
        }
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild1.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild1,rowIdx,lgfsChild0,colIdx
                                                  ,gradPhi0[colIdx]*kGradPhi1[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
            if (lgfsChild1.size()!=0){
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild1,colIdx
                                                      ,gradPhi1[colIdx]*kGradPhi1[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild2.size()!=0){
                const GradPhi gradPhi2= evaluateGradPhi< GradPhi >(eg, lgfsChild2, mbf, xQP,2);
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild2,colIdx
                                                      ,gradPhi2[colIdx]*kGradPhi1[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild3.size()!=0){
                const GradPhi gradPhi3= evaluateGradPhi< GradPhi >(eg, lgfsChild3, mbf, xQP,3);
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild3,colIdx
                                                      ,gradPhi3[colIdx]*kGradPhi1[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild4.size()!=0){
                const GradPhi gradPhi4= evaluateGradPhi< GradPhi >(eg, lgfsChild4, mbf, xQP,4);
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild4,colIdx
                                                      ,gradPhi4[colIdx]*kGradPhi1[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild5.size()!=0){
                const GradPhi gradPhi5= evaluateGradPhi< GradPhi >(eg, lgfsChild5, mbf, xQP,5);
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild5,colIdx
                                                      ,gradPhi5[colIdx]*kGradPhi1[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
        }//end loop over test function lines associated to child space 1
    }//end if lgfsChild!=0
    if (lgfsChild2.size()!=0){
        const GradPhi gradPhi2= evaluateGradPhi< GradPhi >(eg, lgfsChild2, mbf, xQP,2);
        GradPhi kGradPhi2(lgfsChild2.size());
        for (sizeType2 i=0; i<lgfsChild2.size(); i++){
            K.mv(gradPhi2[i],kGradPhi2[i]);
        }
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild2.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild2,rowIdx,lgfsChild0,colIdx
                                                  ,gradPhi0[colIdx]*kGradPhi2[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
            if (lgfsChild1.size()!=0){
                const GradPhi gradPhi1= evaluateGradPhi< GradPhi >(eg, lgfsChild1, mbf, xQP,1);
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild1,colIdx
                                                      ,gradPhi1[colIdx]*kGradPhi2[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild2.size()!=0){
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild2,colIdx
                                                      ,gradPhi2[colIdx]*kGradPhi2[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild3.size()!=0){
                const GradPhi gradPhi3= evaluateGradPhi< GradPhi >(eg, lgfsChild3, mbf, xQP,3);
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild3,colIdx
                                                      ,gradPhi3[colIdx]*kGradPhi2[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild4.size()!=0){
                const GradPhi gradPhi4= evaluateGradPhi< GradPhi >(eg, lgfsChild4, mbf, xQP,4);
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild4,colIdx
                                                      ,gradPhi4[colIdx]*kGradPhi2[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild5.size()!=0){
                const GradPhi gradPhi5= evaluateGradPhi< GradPhi >(eg, lgfsChild5, mbf, xQP,5);
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild5,colIdx
                                                      ,gradPhi5[colIdx]*kGradPhi2[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
        }//end loop over test function lines associated to child space 2
    }//end if lgfsChild!=0
    if (lgfsChild3.size()!=0){
        const GradPhi gradPhi3= evaluateGradPhi< GradPhi >(eg, lgfsChild3, mbf, xQP,3);
        GradPhi kGradPhi3(lgfsChild3.size());
        for (sizeType3 i=0; i<lgfsChild3.size(); i++){
            K.mv(gradPhi3[i],kGradPhi3[i]);
        }
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild3.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild3,rowIdx,lgfsChild0,colIdx
                                                  ,gradPhi0[colIdx]*kGradPhi3[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
            if (lgfsChild1.size()!=0){
                const GradPhi gradPhi1= evaluateGradPhi< GradPhi >(eg, lgfsChild1, mbf, xQP,1);
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild1,colIdx
                                                      ,gradPhi1[colIdx]*kGradPhi3[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild2.size()!=0){
                const GradPhi gradPhi2= evaluateGradPhi< GradPhi >(eg, lgfsChild2, mbf, xQP,2);
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild2,colIdx
                                                      ,gradPhi2[colIdx]*kGradPhi3[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild3.size()!=0){
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild3,colIdx
                                                      ,gradPhi3[colIdx]*kGradPhi3[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild4.size()!=0){
                const GradPhi gradPhi4= evaluateGradPhi< GradPhi >(eg, lgfsChild4, mbf, xQP,4);
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild4,colIdx
                                                      ,gradPhi4[colIdx]*kGradPhi3[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild5.size()!=0){
                const GradPhi gradPhi5= evaluateGradPhi< GradPhi >(eg, lgfsChild5, mbf, xQP,5);
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild5,colIdx
                                                      ,gradPhi5[colIdx]*kGradPhi3[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
        }//end loop over test function lines associated to child space 3
    }//end if lgfsChild!=0
    if (lgfsChild4.size()!=0){
        const GradPhi gradPhi4= evaluateGradPhi< GradPhi >(eg, lgfsChild4, mbf, xQP,4);
        GradPhi kGradPhi4(lgfsChild4.size());
        for (sizeType4 i=0; i<lgfsChild4.size(); i++){
            K.mv(gradPhi4[i],kGradPhi4[i]);
        }
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild4.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild4,rowIdx,lgfsChild0,colIdx
                                                  ,gradPhi0[colIdx]*kGradPhi4[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
            if (lgfsChild1.size()!=0){
                const GradPhi gradPhi1= evaluateGradPhi< GradPhi >(eg, lgfsChild1, mbf, xQP,1);
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild1,colIdx
                                                      ,gradPhi1[colIdx]*kGradPhi4[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild2.size()!=0){
                const GradPhi gradPhi2= evaluateGradPhi< GradPhi >(eg, lgfsChild2, mbf, xQP,2);
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild2,colIdx
                                                      ,gradPhi2[colIdx]*kGradPhi4[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild3.size()!=0){
                const GradPhi gradPhi3= evaluateGradPhi< GradPhi >(eg, lgfsChild3, mbf, xQP,3);
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild3,colIdx
                                                      ,gradPhi3[colIdx]*kGradPhi4[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild4.size()!=0){
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild4,colIdx
                                                      ,gradPhi4[colIdx]*kGradPhi4[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild5.size()!=0){
                const GradPhi gradPhi5= evaluateGradPhi< GradPhi >(eg, lgfsChild5, mbf, xQP,5);
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild5,colIdx
                                                      ,gradPhi5[colIdx]*kGradPhi4[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
        }//end loop over test function lines associated to child space 4
    }//end if lgfsChild!=0
    if (lgfsChild5.size()!=0){
        const GradPhi gradPhi5= evaluateGradPhi< GradPhi >(eg, lgfsChild5, mbf, xQP,5);
        GradPhi kGradPhi5(lgfsChild5.size());
        for (sizeType5 i=0; i<lgfsChild5.size(); i++){
            K.mv(gradPhi5[i],kGradPhi5[i]);
        }
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild5.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild5,rowIdx,lgfsChild0,colIdx
                                                  ,gradPhi0[colIdx]*kGradPhi5[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
            }
            if (lgfsChild1.size()!=0){
                const GradPhi gradPhi1= evaluateGradPhi< GradPhi >(eg, lgfsChild1, mbf, xQP,1);
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild1,colIdx
                                                      ,gradPhi1[colIdx]*kGradPhi5[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild2.size()!=0){
                const GradPhi gradPhi2= evaluateGradPhi< GradPhi >(eg, lgfsChild2, mbf, xQP,2);
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild2,colIdx
                                                      ,gradPhi2[colIdx]*kGradPhi5[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild3.size()!=0){
                const GradPhi gradPhi3= evaluateGradPhi< GradPhi >(eg, lgfsChild3, mbf, xQP,3);
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild3,colIdx
                                                      ,gradPhi3[colIdx]*kGradPhi5[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild4.size()!=0){
                const GradPhi gradPhi4= evaluateGradPhi< GradPhi >(eg, lgfsChild4, mbf, xQP,4);
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild4,colIdx
                                                      ,gradPhi4[colIdx]*kGradPhi5[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
            if (lgfsChild5.size()!=0){
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild5,colIdx
                                                      ,gradPhi5[colIdx]*kGradPhi5[rowIdx]*factor);//gradPhi[rowIdx] is testfunctions
                }
            }
        }//end loop over test function lines associated to child space 5
    }//end if lgfsChild!=0


}//end accumulateMatrixJacobian

template<typename U, typename LGFS, typename MBF, typename X, typename FV >
void evaluateU(const LGFS &lgfs, const MBF &mbf, const X& x, const FV &xQP, U &uJump, U &uAverage) const{
    typedef typename LGFS::template Child<0>::Type LGFSChild0;
    typedef typename LGFSChild0::Traits::SizeType sizeType0;
    typedef typename LGFS::template Child<1>::Type LGFSChild1;
    typedef typename LGFSChild1::Traits::SizeType sizeType1;
    typedef typename LGFS::template Child<2>::Type LGFSChild2;
    typedef typename LGFSChild2::Traits::SizeType sizeType2;
    typedef typename LGFS::template Child<3>::Type LGFSChild3;
    typedef typename LGFSChild3::Traits::SizeType sizeType3;
    typedef typename LGFS::template Child<4>::Type LGFSChild4;
    typedef typename LGFSChild4::Traits::SizeType sizeType4;
    typedef typename LGFS::template Child<5>::Type LGFSChild5;
    typedef typename LGFSChild5::Traits::SizeType sizeType5;

//    typedef typename LGFSChild0::Traits::FiniteElementType::
//            Traits::LocalBasisType::Traits::RangeFieldType Scalar;
    typedef typename LGFSChild0::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::RangeType RangeType;

    const LGFSChild0 &lgfsChild0 = lgfs.template child<0>();
    // evaluate basis functions on reference element
    std::vector<RangeType> phi(lgfsChild0.size());
    lgfsChild0.finiteElement().localBasis().evaluateFunction(xQP,phi);
    //evaluate the jump
    std::vector<RangeType> phiJump(lgfsChild0.size());
    phiJump=mbf.jump(phi,xQP,0);
    for (sizeType0 i=0; i<lgfsChild0.size(); i++){
        uJump += x(lgfsChild0,i)*phiJump[i];
    }
    //evaluate the average
    std::vector<RangeType> phiAverage(lgfsChild0.size());
    phiAverage=mbf.average(phi,xQP,0);
    for (sizeType0 i=0; i<lgfsChild0.size(); i++){
        uAverage += x(lgfsChild0,i)*phiAverage[i];
    }
    const LGFSChild1 &lgfsChild1 = lgfs.template child<1>();
    if (lgfsChild1.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild1.size());
        lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi);
        //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild1.size());
        phiJump=mbf.jump(phi,xQP,1);
        for (sizeType1 i=0; i<lgfsChild1.size(); i++){
            uJump += x(lgfsChild1,i)*phiJump[i];
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild1.size());
        phiAverage=mbf.average(phi,xQP,1);
        for (sizeType1 i=0; i<lgfsChild1.size(); i++){
            uAverage += x(lgfsChild1,i)*phiAverage[i];
        }
    }
    const LGFSChild2 &lgfsChild2 = lgfs.template child<2>();
    if (lgfsChild2.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild2.size());
        lgfsChild2.finiteElement().localBasis().evaluateFunction(xQP,phi);
        //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild2.size());
        phiJump=mbf.jump(phi,xQP,2);
        for (sizeType2 i=0; i<lgfsChild2.size(); i++){
            uJump += x(lgfsChild2,i)*phiJump[i];
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild2.size());
        phiAverage=mbf.average(phi,xQP,2);
        for (sizeType2 i=0; i<lgfsChild2.size(); i++){
            uAverage += x(lgfsChild2,i)*phiAverage[i];
        }
    }
    const LGFSChild3 &lgfsChild3 = lgfs.template child<3>();
    if (lgfsChild3.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild3.size());
        lgfsChild3.finiteElement().localBasis().evaluateFunction(xQP,phi);
        //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild3.size());
        phiJump=mbf.jump(phi,xQP,3);
        for (sizeType3 i=0; i<lgfsChild3.size(); i++){
            uJump += x(lgfsChild3,i)*phiJump[i];
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild3.size());
        phiAverage=mbf.average(phi,xQP,3);
        for (sizeType3 i=0; i<lgfsChild3.size(); i++){
            uAverage += x(lgfsChild3,i)*phiAverage[i];
        }
    }
    const LGFSChild4 &lgfsChild4 = lgfs.template child<4>();
    if (lgfsChild4.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild4.size());
        lgfsChild4.finiteElement().localBasis().evaluateFunction(xQP,phi);
        //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild4.size());
        phiJump=mbf.jump(phi,xQP,4);
        for (sizeType4 i=0; i<lgfsChild4.size(); i++){
            uJump += x(lgfsChild4,i)*phiJump[i];
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild4.size());
        phiAverage=mbf.average(phi,xQP,4);
        for (sizeType4 i=0; i<lgfsChild4.size(); i++){
            uAverage += x(lgfsChild4,i)*phiAverage[i];
        }
    }
    const LGFSChild5 &lgfsChild5 = lgfs.template child<5>();
    if (lgfsChild5.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild5.size());
        lgfsChild5.finiteElement().localBasis().evaluateFunction(xQP,phi);
        //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild5.size());
        phiJump=mbf.jump(phi,xQP,5);
        for (sizeType5 i=0; i<lgfsChild5.size(); i++){
            uJump += x(lgfsChild5,i)*phiJump[i];
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild5.size());
        phiAverage=mbf.average(phi,xQP,5);
        for (sizeType5 i=0; i<lgfsChild5.size(); i++){
            uAverage += x(lgfsChild5,i)*phiAverage[i];
        }
    }

}//end evaluateU

template<typename LFSU, typename MBF, typename X, typename FV, typename U, typename Factor, typename AlphaF, typename R>
void accumulateMatrixResidualCouplingTerm(const LFSU &lfsu, const MBF &mbf,
        const X& x, const FV &xQP, const Factor &factor, const AlphaF &alphaF, const U &uJump, const U &uAverage, R &r) const{
    typedef typename LFSU::template Child<0>::Type LGFSChild0;
    typedef typename LGFSChild0::Traits::SizeType sizeType0;
    typedef typename LFSU::template Child<1>::Type LGFSChild1;
    typedef typename LGFSChild1::Traits::SizeType sizeType1;
    typedef typename LFSU::template Child<2>::Type LGFSChild2;
    typedef typename LGFSChild2::Traits::SizeType sizeType2;
    typedef typename LFSU::template Child<3>::Type LGFSChild3;
    typedef typename LGFSChild3::Traits::SizeType sizeType3;
    typedef typename LFSU::template Child<4>::Type LGFSChild4;
    typedef typename LGFSChild4::Traits::SizeType sizeType4;
    typedef typename LFSU::template Child<5>::Type LGFSChild5;
    typedef typename LGFSChild5::Traits::SizeType sizeType5;

//    typedef typename LGFSChild0::Traits::FiniteElementType::
//            Traits::LocalBasisType::Traits::RangeFieldType Scalar;
    typedef typename LGFSChild0::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::RangeType RangeType;

    const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();
    // evaluate basis functions on reference element
    std::vector<RangeType> phi(lgfsChild0.size());
    lgfsChild0.finiteElement().localBasis().evaluateFunction(xQP,phi);
    //evaluate the jump
    std::vector<RangeType> phiJump(lgfsChild0.size());
    phiJump=mbf.jump(phi,xQP,0);
    //accumulate the jump
    for (sizeType0 i=0; i<lgfsChild0.size(); i++)
    {r.accumulate(lgfsChild0,i,(alphaF*0.5)*uJump*phiJump[i]*factor);
    }
    //evaluate the average
    std::vector<RangeType> phiAverage(lgfsChild0.size());
    phiAverage=mbf.average(phi,xQP,0);
    //accumulate the average
    for (sizeType0 i=0; i<lgfsChild0.size(); i++)
    {r.accumulate(lgfsChild0,i,(alphaF/(xi_-0.5))*uAverage*phiAverage[i]*factor);
    }
    const LGFSChild1 &lgfsChild1 = lfsu.template child<1>();
    if (lgfsChild1.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild1.size());
        lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi);
       //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild1.size());
        phiJump=mbf.jump(phi,xQP,1);
        //accumulate the jump
        for (sizeType1 i=0; i<lgfsChild1.size(); i++)
        {r.accumulate(lgfsChild1,i,(alphaF*0.5)*uJump*phiJump[i]*factor);
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild1.size());
        phiAverage=mbf.average(phi,xQP,1);
        //accumulate the average
        for (sizeType1 i=0; i<lgfsChild1.size(); i++)
        {r.accumulate(lgfsChild1,i,(alphaF/(xi_-0.5))*uAverage*phiAverage[i]*factor);
        }
    }
    const LGFSChild2 &lgfsChild2 = lfsu.template child<2>();
    if (lgfsChild2.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild2.size());
        lgfsChild2.finiteElement().localBasis().evaluateFunction(xQP,phi);
       //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild2.size());
        phiJump=mbf.jump(phi,xQP,2);
        //accumulate the jump
        for (sizeType2 i=0; i<lgfsChild2.size(); i++)
        {r.accumulate(lgfsChild2,i,(alphaF*0.5)*uJump*phiJump[i]*factor);
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild2.size());
        phiAverage=mbf.average(phi,xQP,2);
        //accumulate the average
        for (sizeType2 i=0; i<lgfsChild2.size(); i++)
        {r.accumulate(lgfsChild2,i,(alphaF/(xi_-0.5))*uAverage*phiAverage[i]*factor);
        }
    }
    const LGFSChild3 &lgfsChild3 = lfsu.template child<3>();
    if (lgfsChild3.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild3.size());
        lgfsChild3.finiteElement().localBasis().evaluateFunction(xQP,phi);
       //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild3.size());
        phiJump=mbf.jump(phi,xQP,3);
        //accumulate the jump
        for (sizeType3 i=0; i<lgfsChild3.size(); i++)
        {r.accumulate(lgfsChild3,i,(alphaF*0.5)*uJump*phiJump[i]*factor);
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild3.size());
        phiAverage=mbf.average(phi,xQP,3);
        //accumulate the average
        for (sizeType3 i=0; i<lgfsChild3.size(); i++)
        {r.accumulate(lgfsChild3,i,(alphaF/(xi_-0.5))*uAverage*phiAverage[i]*factor);
        }
    }
    const LGFSChild4 &lgfsChild4 = lfsu.template child<4>();
    if (lgfsChild4.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild4.size());
        lgfsChild4.finiteElement().localBasis().evaluateFunction(xQP,phi);
       //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild4.size());
        phiJump=mbf.jump(phi,xQP,4);
        //accumulate the jump
        for (sizeType4 i=0; i<lgfsChild4.size(); i++)
        {r.accumulate(lgfsChild4,i,(alphaF*0.5)*uJump*phiJump[i]*factor);
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild4.size());
        phiAverage=mbf.average(phi,xQP,4);
        //accumulate the average
        for (sizeType4 i=0; i<lgfsChild4.size(); i++)
        {r.accumulate(lgfsChild4,i,(alphaF/(xi_-0.5))*uAverage*phiAverage[i]*factor);
        }
    }
    const LGFSChild5 &lgfsChild5 = lfsu.template child<5>();
    if (lgfsChild5.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lgfsChild5.size());
        lgfsChild5.finiteElement().localBasis().evaluateFunction(xQP,phi);
       //evaluate the jump
        std::vector<RangeType> phiJump(lgfsChild5.size());
        phiJump=mbf.jump(phi,xQP,5);
        //accumulate the jump
        for (sizeType5 i=0; i<lgfsChild5.size(); i++)
        {r.accumulate(lgfsChild5,i,(alphaF*0.5)*uJump*phiJump[i]*factor);
        }
        //evaluate the average
        std::vector<RangeType> phiAverage(lgfsChild5.size());
        phiAverage=mbf.average(phi,xQP,5);
        //accumulate the average
        for (sizeType5 i=0; i<lgfsChild5.size(); i++)
        {r.accumulate(lgfsChild5,i,(alphaF/(xi_-0.5))*uAverage*phiAverage[i]*factor);
        }
    }

}//end accumulateMatrixResidualCouplingTerm

template<typename LFSU, typename MBF, typename X, typename FV, typename Factor, typename AlphaF, typename M>
void accumulateMatrixJacobianCouplingTerm(const LFSU &lfsu, const MBF &mbf,
        const X& x, const FV &xQP, const Factor &factor, const AlphaF &alphaF, M &m) const{
    typedef typename LFSU::template Child<0>::Type LGFSChild0;
    typedef typename LGFSChild0::Traits::SizeType sizeType0;
    typedef typename LFSU::template Child<1>::Type LGFSChild1;
    typedef typename LGFSChild1::Traits::SizeType sizeType1;
    typedef typename LFSU::template Child<2>::Type LGFSChild2;
    typedef typename LGFSChild2::Traits::SizeType sizeType2;
    typedef typename LFSU::template Child<3>::Type LGFSChild3;
    typedef typename LGFSChild3::Traits::SizeType sizeType3;
    typedef typename LFSU::template Child<4>::Type LGFSChild4;
    typedef typename LGFSChild4::Traits::SizeType sizeType4;
    typedef typename LFSU::template Child<5>::Type LGFSChild5;
    typedef typename LGFSChild5::Traits::SizeType sizeType5;
    typedef typename LGFSChild0::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::RangeType RangeType;

    const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();
    const LGFSChild1 &lgfsChild1 = lfsu.template child<1>();
    const LGFSChild2 &lgfsChild2 = lfsu.template child<2>();
    const LGFSChild3 &lgfsChild3 = lfsu.template child<3>();
    const LGFSChild4 &lgfsChild4 = lfsu.template child<4>();
    const LGFSChild5 &lgfsChild5 = lfsu.template child<5>();

    RangeType jumpFactor=(alphaF*0.5);
    RangeType averageFactor=(alphaF/(xi_-0.5));

    // evaluate basis functions on reference element
    std::vector<RangeType> phi0(lgfsChild0.size());
    lgfsChild0.finiteElement().localBasis().evaluateFunction(xQP,phi0);
    //evaluate the jump
    std::vector<RangeType> phiJump0(lgfsChild0.size());
    phiJump0=mbf.jump(phi0,xQP,0);
    //evaluate the average
    std::vector<RangeType> phiAverage0(lgfsChild0.size());
    phiAverage0=mbf.average(phi0,xQP,0);

    for (sizeType0 rowIdx=0; rowIdx<lgfsChild0.size(); rowIdx++){
        for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
            m.accumulate(lgfsChild0,rowIdx,lgfsChild0,colIdx
                                              ,phiAverage0[colIdx]*phiAverage0[rowIdx]*factor*averageFactor
                                              + phiJump0[colIdx]*phiJump0[rowIdx]*factor*jumpFactor);
        }
        if (lgfsChild1.size()!=0){
            // evaluate basis functions on reference element
            std::vector<RangeType> phi1(lgfsChild1.size());
            lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi1);
            //evaluate the jump
            std::vector<RangeType> phiJump1(lgfsChild1.size());
            phiJump1=mbf.jump(phi1,xQP,1);
            //evaluate the average
            std::vector<RangeType> phiAverage1(lgfsChild1.size());
            phiAverage1=mbf.average(phi1,xQP,1);
            for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild1,colIdx
                                                  ,phiAverage1[colIdx]*phiAverage0[rowIdx]*factor*averageFactor
                                                  + phiJump1[colIdx]*phiJump0[rowIdx]*factor*jumpFactor);
            }
        }
        if (lgfsChild2.size()!=0){
            // evaluate basis functions on reference element
            std::vector<RangeType> phi2(lgfsChild2.size());
            lgfsChild2.finiteElement().localBasis().evaluateFunction(xQP,phi2);
            //evaluate the jump
            std::vector<RangeType> phiJump2(lgfsChild2.size());
            phiJump2=mbf.jump(phi2,xQP,2);
            //evaluate the average
            std::vector<RangeType> phiAverage2(lgfsChild2.size());
            phiAverage2=mbf.average(phi2,xQP,2);
            for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild2,colIdx
                                                  ,phiAverage2[colIdx]*phiAverage0[rowIdx]*factor*averageFactor
                                                  + phiJump2[colIdx]*phiJump0[rowIdx]*factor*jumpFactor);
            }
        }
        if (lgfsChild3.size()!=0){
            // evaluate basis functions on reference element
            std::vector<RangeType> phi3(lgfsChild3.size());
            lgfsChild3.finiteElement().localBasis().evaluateFunction(xQP,phi3);
            //evaluate the jump
            std::vector<RangeType> phiJump3(lgfsChild3.size());
            phiJump3=mbf.jump(phi3,xQP,3);
            //evaluate the average
            std::vector<RangeType> phiAverage3(lgfsChild3.size());
            phiAverage3=mbf.average(phi3,xQP,3);
            for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild3,colIdx
                                                  ,phiAverage3[colIdx]*phiAverage0[rowIdx]*factor*averageFactor
                                                  + phiJump3[colIdx]*phiJump0[rowIdx]*factor*jumpFactor);
            }
        }
        if (lgfsChild4.size()!=0){
            // evaluate basis functions on reference element
            std::vector<RangeType> phi4(lgfsChild4.size());
            lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi4);
            //evaluate the jump
            std::vector<RangeType> phiJump4(lgfsChild4.size());
            phiJump4=mbf.jump(phi4,xQP,4);
            //evaluate the average
            std::vector<RangeType> phiAverage4(lgfsChild4.size());
            phiAverage4=mbf.average(phi4,xQP,4);
            for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild4,colIdx
                                                  ,phiAverage4[colIdx]*phiAverage0[rowIdx]*factor*averageFactor
                                                  + phiJump4[colIdx]*phiJump0[rowIdx]*factor*jumpFactor);
            }
        }
        if (lgfsChild5.size()!=0){
            // evaluate basis functions on reference element
            std::vector<RangeType> phi5(lgfsChild5.size());
            lgfsChild5.finiteElement().localBasis().evaluateFunction(xQP,phi5);
            //evaluate the jump
            std::vector<RangeType> phiJump5(lgfsChild5.size());
            phiJump5=mbf.jump(phi5,xQP,5);
            //evaluate the average
            std::vector<RangeType> phiAverage5(lgfsChild5.size());
            phiAverage5=mbf.average(phi5,xQP,5);
            for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                m.accumulate(lgfsChild0,rowIdx,lgfsChild5,colIdx
                                                  ,phiAverage5[colIdx]*phiAverage0[rowIdx]*factor*averageFactor
                                                  + phiJump5[colIdx]*phiJump0[rowIdx]*factor*jumpFactor);
            }
        }
    }//end loop over test function lines associated to child space 0

    if (lgfsChild1.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi1(lgfsChild1.size());
        lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi1);
        //evaluate the jump
        std::vector<RangeType> phiJump1(lgfsChild1.size());
        phiJump1=mbf.jump(phi1,xQP,1);
        //evaluate the average
        std::vector<RangeType> phiAverage1(lgfsChild1.size());
        phiAverage1=mbf.average(phi1,xQP,1);
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild1.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild1,rowIdx,lgfsChild0,colIdx
                                                  ,phiAverage0[colIdx]*phiAverage1[rowIdx]*factor*averageFactor
                                                  + phiJump0[colIdx]*phiJump1[rowIdx]*factor*jumpFactor);
            }
            if (lgfsChild1.size()!=0){
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild1,colIdx
                                                      ,phiAverage1[colIdx]*phiAverage1[rowIdx]*factor*averageFactor
                                                      + phiJump1[colIdx]*phiJump1[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild2.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi2(lgfsChild2.size());
                lgfsChild2.finiteElement().localBasis().evaluateFunction(xQP,phi2);
                //evaluate the jump
                std::vector<RangeType> phiJump2(lgfsChild2.size());
                phiJump2=mbf.jump(phi2,xQP,2);
                //evaluate the average
                std::vector<RangeType> phiAverage2(lgfsChild2.size());
                phiAverage2=mbf.average(phi2,xQP,2);
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild2,colIdx
                                                      ,phiAverage2[colIdx]*phiAverage1[rowIdx]*factor*averageFactor
                                                      + phiJump2[colIdx]*phiJump1[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild3.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi3(lgfsChild3.size());
                lgfsChild3.finiteElement().localBasis().evaluateFunction(xQP,phi3);
                //evaluate the jump
                std::vector<RangeType> phiJump3(lgfsChild3.size());
                phiJump3=mbf.jump(phi3,xQP,3);
                //evaluate the average
                std::vector<RangeType> phiAverage3(lgfsChild3.size());
                phiAverage3=mbf.average(phi3,xQP,3);
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild3,colIdx
                                                      ,phiAverage3[colIdx]*phiAverage1[rowIdx]*factor*averageFactor
                                                      + phiJump3[colIdx]*phiJump1[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild4.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi4(lgfsChild4.size());
                lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi4);
                //evaluate the jump
                std::vector<RangeType> phiJump4(lgfsChild4.size());
                phiJump4=mbf.jump(phi4,xQP,4);
                //evaluate the average
                std::vector<RangeType> phiAverage4(lgfsChild4.size());
                phiAverage4=mbf.average(phi4,xQP,4);
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild4,colIdx
                                                      ,phiAverage4[colIdx]*phiAverage1[rowIdx]*factor*averageFactor
                                                      + phiJump4[colIdx]*phiJump1[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild5.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi5(lgfsChild5.size());
                lgfsChild5.finiteElement().localBasis().evaluateFunction(xQP,phi5);
                //evaluate the jump
                std::vector<RangeType> phiJump5(lgfsChild5.size());
                phiJump5=mbf.jump(phi5,xQP,5);
                //evaluate the average
                std::vector<RangeType> phiAverage5(lgfsChild5.size());
                phiAverage5=mbf.average(phi5,xQP,5);
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild1,rowIdx,lgfsChild5,colIdx
                                                      ,phiAverage5[colIdx]*phiAverage1[rowIdx]*factor*averageFactor
                                                      + phiJump5[colIdx]*phiJump1[rowIdx]*factor*jumpFactor);
                }
            }
        }//end loop over test function lines associated to child space 1
    }//end if lgfsChild!=0
    if (lgfsChild2.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi2(lgfsChild2.size());
        lgfsChild2.finiteElement().localBasis().evaluateFunction(xQP,phi2);
        //evaluate the jump
        std::vector<RangeType> phiJump2(lgfsChild2.size());
        phiJump2=mbf.jump(phi2,xQP,2);
        //evaluate the average
        std::vector<RangeType> phiAverage2(lgfsChild2.size());
        phiAverage2=mbf.average(phi2,xQP,2);
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild2.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild2,rowIdx,lgfsChild0,colIdx
                                                  ,phiAverage0[colIdx]*phiAverage2[rowIdx]*factor*averageFactor
                                                  + phiJump0[colIdx]*phiJump2[rowIdx]*factor*jumpFactor);
            }
            if (lgfsChild1.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi1(lgfsChild1.size());
                lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi1);
                //evaluate the jump
                std::vector<RangeType> phiJump1(lgfsChild1.size());
                phiJump1=mbf.jump(phi1,xQP,1);
                //evaluate the average
                std::vector<RangeType> phiAverage1(lgfsChild1.size());
                phiAverage1=mbf.average(phi1,xQP,1);
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild1,colIdx
                                                      ,phiAverage1[colIdx]*phiAverage2[rowIdx]*factor*averageFactor
                                                      + phiJump1[colIdx]*phiJump2[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild2.size()!=0){
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild2,colIdx
                                                      ,phiAverage2[colIdx]*phiAverage2[rowIdx]*factor*averageFactor
                                                      + phiJump2[colIdx]*phiJump2[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild3.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi3(lgfsChild3.size());
                lgfsChild3.finiteElement().localBasis().evaluateFunction(xQP,phi3);
                //evaluate the jump
                std::vector<RangeType> phiJump3(lgfsChild3.size());
                phiJump3=mbf.jump(phi3,xQP,3);
                //evaluate the average
                std::vector<RangeType> phiAverage3(lgfsChild3.size());
                phiAverage3=mbf.average(phi3,xQP,3);
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild3,colIdx
                                                      ,phiAverage3[colIdx]*phiAverage2[rowIdx]*factor*averageFactor
                                                      + phiJump3[colIdx]*phiJump2[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild4.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi4(lgfsChild4.size());
                lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi4);
                //evaluate the jump
                std::vector<RangeType> phiJump4(lgfsChild4.size());
                phiJump4=mbf.jump(phi4,xQP,4);
                //evaluate the average
                std::vector<RangeType> phiAverage4(lgfsChild4.size());
                phiAverage4=mbf.average(phi4,xQP,4);
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild4,colIdx
                                                      ,phiAverage4[colIdx]*phiAverage2[rowIdx]*factor*averageFactor
                                                      + phiJump4[colIdx]*phiJump2[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild5.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi5(lgfsChild5.size());
                lgfsChild5.finiteElement().localBasis().evaluateFunction(xQP,phi5);
                //evaluate the jump
                std::vector<RangeType> phiJump5(lgfsChild5.size());
                phiJump5=mbf.jump(phi5,xQP,5);
                //evaluate the average
                std::vector<RangeType> phiAverage5(lgfsChild5.size());
                phiAverage5=mbf.average(phi5,xQP,5);
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild2,rowIdx,lgfsChild5,colIdx
                                                      ,phiAverage5[colIdx]*phiAverage2[rowIdx]*factor*averageFactor
                                                      + phiJump5[colIdx]*phiJump2[rowIdx]*factor*jumpFactor);
                }
            }
        }//end loop over test function lines associated to child space 2
    }//end if lgfsChild!=0
    if (lgfsChild3.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi3(lgfsChild3.size());
        lgfsChild3.finiteElement().localBasis().evaluateFunction(xQP,phi3);
        //evaluate the jump
        std::vector<RangeType> phiJump3(lgfsChild3.size());
        phiJump3=mbf.jump(phi3,xQP,3);
        //evaluate the average
        std::vector<RangeType> phiAverage3(lgfsChild3.size());
        phiAverage3=mbf.average(phi3,xQP,3);
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild3.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild3,rowIdx,lgfsChild0,colIdx
                                                  ,phiAverage0[colIdx]*phiAverage3[rowIdx]*factor*averageFactor
                                                  + phiJump0[colIdx]*phiJump3[rowIdx]*factor*jumpFactor);
            }
            if (lgfsChild1.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi1(lgfsChild1.size());
                lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi1);
                //evaluate the jump
                std::vector<RangeType> phiJump1(lgfsChild1.size());
                phiJump1=mbf.jump(phi1,xQP,1);
                //evaluate the average
                std::vector<RangeType> phiAverage1(lgfsChild1.size());
                phiAverage1=mbf.average(phi1,xQP,1);
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild1,colIdx
                                                      ,phiAverage1[colIdx]*phiAverage3[rowIdx]*factor*averageFactor
                                                      + phiJump1[colIdx]*phiJump3[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild2.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi2(lgfsChild2.size());
                lgfsChild2.finiteElement().localBasis().evaluateFunction(xQP,phi2);
                //evaluate the jump
                std::vector<RangeType> phiJump2(lgfsChild2.size());
                phiJump2=mbf.jump(phi2,xQP,2);
                //evaluate the average
                std::vector<RangeType> phiAverage2(lgfsChild2.size());
                phiAverage2=mbf.average(phi2,xQP,2);
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild2,colIdx
                                                      ,phiAverage2[colIdx]*phiAverage3[rowIdx]*factor*averageFactor
                                                      + phiJump2[colIdx]*phiJump3[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild3.size()!=0){
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild3,colIdx
                                                      ,phiAverage3[colIdx]*phiAverage3[rowIdx]*factor*averageFactor
                                                      + phiJump3[colIdx]*phiJump3[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild4.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi4(lgfsChild4.size());
                lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi4);
                //evaluate the jump
                std::vector<RangeType> phiJump4(lgfsChild4.size());
                phiJump4=mbf.jump(phi4,xQP,4);
                //evaluate the average
                std::vector<RangeType> phiAverage4(lgfsChild4.size());
                phiAverage4=mbf.average(phi4,xQP,4);
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild4,colIdx
                                                      ,phiAverage4[colIdx]*phiAverage3[rowIdx]*factor*averageFactor
                                                      + phiJump4[colIdx]*phiJump3[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild5.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi5(lgfsChild5.size());
                lgfsChild5.finiteElement().localBasis().evaluateFunction(xQP,phi5);
                //evaluate the jump
                std::vector<RangeType> phiJump5(lgfsChild5.size());
                phiJump5=mbf.jump(phi5,xQP,5);
                //evaluate the average
                std::vector<RangeType> phiAverage5(lgfsChild5.size());
                phiAverage5=mbf.average(phi5,xQP,5);
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild3,rowIdx,lgfsChild5,colIdx
                                                      ,phiAverage5[colIdx]*phiAverage3[rowIdx]*factor*averageFactor
                                                      + phiJump5[colIdx]*phiJump3[rowIdx]*factor*jumpFactor);
                }
            }
        }//end loop over test function lines associated to child space 3
    }//end if lgfsChild!=0
    if (lgfsChild4.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi4(lgfsChild4.size());
        lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi4);
        //evaluate the jump
        std::vector<RangeType> phiJump4(lgfsChild4.size());
        phiJump4=mbf.jump(phi4,xQP,4);
        //evaluate the average
        std::vector<RangeType> phiAverage4(lgfsChild4.size());
        phiAverage4=mbf.average(phi4,xQP,4);
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild4.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild4,rowIdx,lgfsChild0,colIdx
                                                  ,phiAverage0[colIdx]*phiAverage4[rowIdx]*factor*averageFactor
                                                  + phiJump0[colIdx]*phiJump4[rowIdx]*factor*jumpFactor);
            }
            if (lgfsChild1.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi1(lgfsChild1.size());
                lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi1);
                //evaluate the jump
                std::vector<RangeType> phiJump1(lgfsChild1.size());
                phiJump1=mbf.jump(phi1,xQP,1);
                //evaluate the average
                std::vector<RangeType> phiAverage1(lgfsChild1.size());
                phiAverage1=mbf.average(phi1,xQP,1);
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild1,colIdx
                                                      ,phiAverage1[colIdx]*phiAverage4[rowIdx]*factor*averageFactor
                                                      + phiJump1[colIdx]*phiJump4[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild2.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi2(lgfsChild2.size());
                lgfsChild2.finiteElement().localBasis().evaluateFunction(xQP,phi2);
                //evaluate the jump
                std::vector<RangeType> phiJump2(lgfsChild2.size());
                phiJump2=mbf.jump(phi2,xQP,2);
                //evaluate the average
                std::vector<RangeType> phiAverage2(lgfsChild2.size());
                phiAverage2=mbf.average(phi2,xQP,2);
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild2,colIdx
                                                      ,phiAverage2[colIdx]*phiAverage4[rowIdx]*factor*averageFactor
                                                      + phiJump2[colIdx]*phiJump4[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild3.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi3(lgfsChild3.size());
                lgfsChild3.finiteElement().localBasis().evaluateFunction(xQP,phi3);
                //evaluate the jump
                std::vector<RangeType> phiJump3(lgfsChild3.size());
                phiJump3=mbf.jump(phi3,xQP,3);
                //evaluate the average
                std::vector<RangeType> phiAverage3(lgfsChild3.size());
                phiAverage3=mbf.average(phi3,xQP,3);
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild3,colIdx
                                                      ,phiAverage3[colIdx]*phiAverage4[rowIdx]*factor*averageFactor
                                                      + phiJump3[colIdx]*phiJump4[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild4.size()!=0){
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild4,colIdx
                                                      ,phiAverage4[colIdx]*phiAverage4[rowIdx]*factor*averageFactor
                                                      + phiJump4[colIdx]*phiJump4[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild5.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi5(lgfsChild5.size());
                lgfsChild5.finiteElement().localBasis().evaluateFunction(xQP,phi5);
                //evaluate the jump
                std::vector<RangeType> phiJump5(lgfsChild5.size());
                phiJump5=mbf.jump(phi5,xQP,5);
                //evaluate the average
                std::vector<RangeType> phiAverage5(lgfsChild5.size());
                phiAverage5=mbf.average(phi5,xQP,5);
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild4,rowIdx,lgfsChild5,colIdx
                                                      ,phiAverage5[colIdx]*phiAverage4[rowIdx]*factor*averageFactor
                                                      + phiJump5[colIdx]*phiJump4[rowIdx]*factor*jumpFactor);
                }
            }
        }//end loop over test function lines associated to child space 4
    }//end if lgfsChild!=0
    if (lgfsChild5.size()!=0){
        // evaluate basis functions on reference element
        std::vector<RangeType> phi5(lgfsChild5.size());
        lgfsChild5.finiteElement().localBasis().evaluateFunction(xQP,phi5);
        //evaluate the jump
        std::vector<RangeType> phiJump5(lgfsChild5.size());
        phiJump5=mbf.jump(phi5,xQP,5);
        //evaluate the average
        std::vector<RangeType> phiAverage5(lgfsChild5.size());
        phiAverage5=mbf.average(phi5,xQP,5);
        for (sizeType1 rowIdx=0; rowIdx<lgfsChild5.size(); rowIdx++){
            for (sizeType0 colIdx=0; colIdx<lgfsChild0.size();colIdx++){
                m.accumulate(lgfsChild5,rowIdx,lgfsChild0,colIdx
                                                  ,phiAverage0[colIdx]*phiAverage5[rowIdx]*factor*averageFactor
                                                  + phiJump0[colIdx]*phiJump5[rowIdx]*factor*jumpFactor);
            }
            if (lgfsChild1.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi1(lgfsChild1.size());
                lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi1);
                //evaluate the jump
                std::vector<RangeType> phiJump1(lgfsChild1.size());
                phiJump1=mbf.jump(phi1,xQP,1);
                //evaluate the average
                std::vector<RangeType> phiAverage1(lgfsChild1.size());
                phiAverage1=mbf.average(phi1,xQP,1);
                for (sizeType1 colIdx=0; colIdx<lgfsChild1.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild1,colIdx
                                                      ,phiAverage1[colIdx]*phiAverage5[rowIdx]*factor*averageFactor
                                                      + phiJump1[colIdx]*phiJump5[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild2.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi2(lgfsChild2.size());
                lgfsChild2.finiteElement().localBasis().evaluateFunction(xQP,phi2);
                //evaluate the jump
                std::vector<RangeType> phiJump2(lgfsChild2.size());
                phiJump2=mbf.jump(phi2,xQP,2);
                //evaluate the average
                std::vector<RangeType> phiAverage2(lgfsChild2.size());
                phiAverage2=mbf.average(phi2,xQP,2);
                for (sizeType2 colIdx=0; colIdx<lgfsChild2.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild2,colIdx
                                                      ,phiAverage2[colIdx]*phiAverage5[rowIdx]*factor*averageFactor
                                                      + phiJump2[colIdx]*phiJump5[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild3.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi3(lgfsChild3.size());
                lgfsChild3.finiteElement().localBasis().evaluateFunction(xQP,phi3);
                //evaluate the jump
                std::vector<RangeType> phiJump3(lgfsChild3.size());
                phiJump3=mbf.jump(phi3,xQP,3);
                //evaluate the average
                std::vector<RangeType> phiAverage3(lgfsChild3.size());
                phiAverage3=mbf.average(phi3,xQP,3);
                for (sizeType3 colIdx=0; colIdx<lgfsChild3.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild3,colIdx
                                                      ,phiAverage3[colIdx]*phiAverage5[rowIdx]*factor*averageFactor
                                                      + phiJump3[colIdx]*phiJump5[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild4.size()!=0){
                // evaluate basis functions on reference element
                std::vector<RangeType> phi4(lgfsChild4.size());
                lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phi4);
                //evaluate the jump
                std::vector<RangeType> phiJump4(lgfsChild4.size());
                phiJump4=mbf.jump(phi4,xQP,4);
                //evaluate the average
                std::vector<RangeType> phiAverage4(lgfsChild4.size());
                phiAverage4=mbf.average(phi4,xQP,4);
                for (sizeType4 colIdx=0; colIdx<lgfsChild4.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild4,colIdx
                                                      ,phiAverage4[colIdx]*phiAverage5[rowIdx]*factor*averageFactor
                                                      + phiJump4[colIdx]*phiJump5[rowIdx]*factor*jumpFactor);
                }
            }
            if (lgfsChild5.size()!=0){
                for (sizeType5 colIdx=0; colIdx<lgfsChild5.size();colIdx++){
                    m.accumulate(lgfsChild5,rowIdx,lgfsChild5,colIdx
                                                      ,phiAverage5[colIdx]*phiAverage5[rowIdx]*factor*averageFactor
                                                      + phiJump5[colIdx]*phiJump5[rowIdx]*factor*jumpFactor);
                }
            }
        }//end loop over test function lines associated to child space 5
    }//end if lgfsChild!=0




}//end accumulateMatrixJacobianCouplingTerm



public:
// boundary integral
// for Neumann b.c.
template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
void alpha_boundary (const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s) const

{
    typedef typename LFSU::template Child<0>::Type LGFSChild0;
    typedef typename LGFSChild0::Traits::SizeType sizeType0;
    typedef typename LFSU::template Child<1>::Type LGFSChild1;
    typedef typename LGFSChild1::Traits::SizeType sizeType1;
    typedef typename LFSU::template Child<2>::Type LGFSChild2;
    typedef typename LGFSChild2::Traits::SizeType sizeType2;
    typedef typename LFSU::template Child<3>::Type LGFSChild3;
    typedef typename LGFSChild3::Traits::SizeType sizeType3;
    typedef typename LFSU::template Child<4>::Type LGFSChild4;
    typedef typename LGFSChild4::Traits::SizeType sizeType4;
    typedef typename LFSU::template Child<5>::Type LGFSChild5;
    typedef typename LGFSChild0::Traits::SizeType sizeType;
    typedef typename LGFSChild0::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::RangeType RangeType;
    typedef typename LGFSChild0::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::RangeFieldType RF;

    const LGFSChild0 &lgfsChild0 = lfsu_s.template child<0>();
    const LGFSChild1 &lgfsChild1 = lfsu_s.template child<1>();
    const LGFSChild2 &lgfsChild2 = lfsu_s.template child<2>();
    const LGFSChild3 &lgfsChild3 = lfsu_s.template child<3>();
    const LGFSChild4 &lgfsChild4 = lfsu_s.template child<4>();
    const LGFSChild5 &lgfsChild5 = lfsu_s.template child<5>();



  typedef typename B::template Child<0>::Type BCType;
  const BCType& bctype= b_.template child<0>();


  // dimensions
  const int dim_ = IG::dimension;

  typedef Dumux::modifiedBasisFunction<MultiDomainGrid, typename IG::Entity, IntersectionPointsMap, LFSU> MBF;
  //TODO get eg from ig
  const MBF mbf(multiDomainGrid_, *(ig.inside()), intersectionPointsMapLocal_, lfsu_s, true);

  // select quadrature rule for face
  Dune::GeometryType gtface = ig.geometryInInside().type();
  const Dune::QuadratureRule<ctype,dim_-1>&
  rule = Dune::QuadratureRules<ctype,dim_-1>::rule(gtface,intorder_);

  //loop over quadrature points and integrate normal flux
  for (typename Dune::QuadratureRule<ctype,dim_-1>::const_iterator it=rule.begin();it!=rule.end(); ++it)
  {
      // position of quadrature point in local coordinates of element
      Dune::FieldVector<ctype,dim_> elementLocal = ig.geometryInInside().global(it->position());
      // skip rest if we are on Dirichlet boundary
      //TODO: here it is assumed, that if there is a Neumann boundary for the standard dofs, there is also a boundary for the enriched.
      //if this is not true, you have to use bctypeEnriched to check!
      if (!bctype.isNeumann(ig,it->position())) continue;
      // evaluate flux boundary condition
      RF j=bctype.template neumannFlux<RF>(ig,it->position());
      // integrate j
      RF factor = it->weight()*ig.geometry().integrationElement(it->position());
      {//space 0
          // evaluate basis functions on reference element
          std::vector<RangeType> phi0(lgfsChild0.size());
          lgfsChild0.finiteElement().localBasis().evaluateFunction(elementLocal,phi0);
          phi0=mbf.modify(phi0,elementLocal,0);
          for (sizeType i=0; i<lgfsChild0.size(); i++)
          {r_s.accumulate(lgfsChild0,i,j*phi0[i]*factor);
          }
      }
      if (lgfsChild1.size()!=0){
          // evaluate basis functions on reference element
          std::vector<RangeType> phi1(lgfsChild1.size());
          lgfsChild1.finiteElement().localBasis().evaluateFunction(elementLocal,phi1);
          phi1=mbf.modify(phi1,elementLocal,1);
          for (sizeType i=0; i<lgfsChild1.size(); i++)
          {r_s.accumulate(lgfsChild1,i,j*phi1[i]*factor);
          }
      }
      if (lgfsChild2.size()!=0){
          // evaluate basis functions on reference element
          std::vector<RangeType> phi2(lgfsChild2.size());
          lgfsChild2.finiteElement().localBasis().evaluateFunction(elementLocal,phi2);
          phi2=mbf.modify(phi2,elementLocal,2);
          for (sizeType i=0; i<lgfsChild2.size(); i++)
          {r_s.accumulate(lgfsChild2,i,j*phi2[i]*factor);
          }
      }
      if (lgfsChild3.size()!=0){
          // evaluate basis functions on reference element
          std::vector<RangeType> phi3(lgfsChild3.size());
          lgfsChild3.finiteElement().localBasis().evaluateFunction(elementLocal,phi3);
          phi3=mbf.modify(phi3,elementLocal,3);
          for (sizeType i=0; i<lgfsChild3.size(); i++)
          {r_s.accumulate(lgfsChild3,i,j*phi3[i]*factor);
          }
      }
      if (lgfsChild4.size()!=0){
          // evaluate basis functions on reference element
          std::vector<RangeType> phi4(lgfsChild4.size());
          lgfsChild4.finiteElement().localBasis().evaluateFunction(elementLocal,phi4);
          phi4=mbf.modify(phi4,elementLocal,4);
          for (sizeType i=0; i<lgfsChild4.size(); i++)
          {r_s.accumulate(lgfsChild4,i,j*phi4[i]*factor);
          }
      }
      if (lgfsChild5.size()!=0){
          // evaluate basis functions on reference element
          std::vector<RangeType> phi5(lgfsChild5.size());
          lgfsChild5.finiteElement().localBasis().evaluateFunction(elementLocal,phi5);
          phi5=mbf.modify(phi5,elementLocal,5);
          for (sizeType i=0; i<lgfsChild5.size(); i++)
          {r_s.accumulate(lgfsChild5,i,j*phi5[i]*factor);
          }
      }
  }//end quadrature point loop

}//end alpha boundary

protected:
/*
* extract soil parameters
* alphaF depends on the fracture element
*/
template<typename Scalar, typename Entity>
const Scalar alphaF(const Entity &entity) const{
    return (soil_.alphaF(entity));//a large alphaF means high permeability normal through the fracture and small fracture width
}

typedef Dune::GeometryType GT;
typedef typename MultiDomainGrid::Traits::GlobalIdSet::IdType IdType;
//here it is assumed that the grid dimension is unique, i.e., elements of different dimensions are not allowed
static const int dim_=MultiDomainGrid::dimension;
static const int dimw_=MultiDomainGrid::dimensionworld;

private:
  const B &b_;
  const MultiDomainGrid &multiDomainGrid_;
  const FractureGrid &fractureGrid_;
  IntersectionPointsMap &intersectionPointsMapLocal_;
  CouplingMap &couplingMap_;
protected:
  const Soil &soil_;
  const typename MultiDomainGrid::ctype xi_;
private:
  const unsigned int intorder_;
  const unsigned int intorderF_;
};

#endif /* MATRIXLOCALOPERATORENRICHEDDARCY_HH_ */
