/*
 * The coupling is symmetric.
 * It happens only on the geometrical position of the fractures.
 * Nevertheless the coupling is formulated from the point of view of local matrix elements, because it
 * is much easier to get and assemble the correct fracture dofs than the stacked (enriched) matrix dofs.
 */


template<typename MonolithicMatrix, typename MonolithicResidual, typename MultiDomainGrid, typename FractureGrid,
    typename IntersectionPointsMap, typename CouplingMap, typename FractureGFS, typename XF, typename Soil, typename FB>
class CouplingLocalOperatorDarcy : //derived from the following and using the CRTP-Trick!
  public Dune::PDELab::JacobianBasedAlphaVolume<
      CouplingLocalOperatorDarcy<MonolithicMatrix,MonolithicResidual,MultiDomainGrid,FractureGrid,
      IntersectionPointsMap,CouplingMap,FractureGFS,XF,Soil, FB> >,
  public Dune::PDELab::JacobianBasedAlphaBoundary<
      CouplingLocalOperatorDarcy<MonolithicMatrix,MonolithicResidual,MultiDomainGrid,FractureGrid,
      IntersectionPointsMap,CouplingMap,FractureGFS,XF,Soil, FB> >,
  public Dune::PDELab::FullVolumePattern,
  public Dune::PDELab::LocalOperatorDefaultFlags

  {
  protected:
    static const int dimw_=MultiDomainGrid::dimensionworld;

  public:
  // pattern assembly flags
  enum { doPatternVolume = true };
  // residual assembly flags
  enum { doAlphaVolume  = true };
  enum { doAlphaBoundary  = false };

  CouplingLocalOperatorDarcy (MonolithicMatrix &gMonolithic, MonolithicResidual &rMonolithic,// gMonolithic and rMonolithic are references, not copies and not const!
          const MultiDomainGrid &multiDomainGrid, const FractureGrid &fractureGrid,
          IntersectionPointsMap &intersectionPointsMap, CouplingMap &couplingMap,
          const FractureGFS &fractureGFS, const XF& xf, const Soil &soil, const FB &fb, unsigned int intorder=3)
  :
      gMonolithic_(gMonolithic), rMonolithic_(rMonolithic), multiDomainGrid_(multiDomainGrid), fractureGrid_(fractureGrid), intersectionPointsMap_(intersectionPointsMap),
    couplingMap_(couplingMap), fractureGFS_(fractureGFS), xf_(xf), soil_(soil), fb_(fb), intorder_(intorder), fix_pf_(false)
  {
      typedef typename MultiDomainGrid::SubDomainGrid SubDomainGrid;
      typedef typename MultiDomainGrid::LeafGridView MDGV;
      MDGV mdgv = multiDomainGrid.leafGridView();
      typedef typename SubDomainGrid::LeafGridView SDGV;
      const SubDomainGrid &sdg0 = multiDomainGrid.subDomain(0);
      SDGV sdgv0 = sdg0.leafGridView();
      const SubDomainGrid &sdg1 = multiDomainGrid.subDomain(1);
      SDGV sdgv1 = sdg1.leafGridView();
      const SubDomainGrid &sdg2 = multiDomainGrid.subDomain(2);
      SDGV sdgv2 = sdg2.leafGridView();
      const SubDomainGrid &sdg3 = multiDomainGrid.subDomain(3);
      SDGV sdgv3 = sdg3.leafGridView();
      const SubDomainGrid &sdg4 = multiDomainGrid.subDomain(4);
      SDGV sdgv4 = sdg4.leafGridView();
      offset_=mdgv.size(dimw_)+sdgv0.size(dimw_)+sdgv1.size(dimw_)+sdgv2.size(dimw_)+sdgv3.size(dimw_)+sdgv4.size(dimw_);
  }
  private:
      int offset_;

  public:

      // volume integral depending on test and ansatz functions
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
      {
          bool endingNEW=false;
    	  //here it is assumed that the LFSU is a powergridfunctionspace,
    	  //i.e., all individual function spaces are of the same order!
    	  // domain and range field type (assume both components have same RF)
    	  typedef typename LFSU::template Child<0>::Type LGFSChild0;
    	  typedef typename LFSU::template Child<1>::Type LGFSChild1;
    	  typedef typename LFSU::template Child<2>::Type LGFSChild2;
    	  typedef typename LFSU::template Child<3>::Type LGFSChild3;
    	  typedef typename LFSU::template Child<4>::Type LGFSChild4;
    	  typedef typename LFSU::template Child<5>::Type LGFSChild5;
    	  typedef typename LGFSChild0::Traits::FiniteElementType::
    			  Traits::LocalBasisType::Traits::DomainFieldType DF;
    	  typedef typename LGFSChild0::Traits::FiniteElementType::
    			  Traits::LocalBasisType::Traits::RangeFieldType RF;
    	  //      typedef typename LGFSChild0::Traits::FiniteElementType::
    	  //        Traits::LocalBasisType::Traits::JacobianType JacobianType;
    	  typedef typename LGFSChild0::Traits::FiniteElementType::
    			  Traits::LocalBasisType::Traits::RangeType RangeType;
    	  typedef typename LGFSChild0::Traits::FiniteElementType::
    			  Traits::LocalBasisType::Traits::DomainType FV;
    	  typedef typename LGFSChild0::Traits::SizeType sizeType;

    	  //get element Id
    	  typedef typename MultiDomainGrid::Traits::GlobalIdSet::IdType IdType;
    	  const IdType elementId =multiDomainGrid_.globalIdSet().id(eg.entity());

    	  typedef typename IntersectionPointsMap::mapped_type IntersectionPoints;
    	  IntersectionPoints intersectionPoints=(((*intersectionPointsMap_.find(elementId)).second));
    	  FV check(-1000.0);//check means no entry

    	  typedef typename CouplingMap::iterator CouplingMapIterator;
    	  CouplingMapIterator cmit=couplingMap_.find(elementId);
    	  assert(cmit != couplingMap_.end());
    	  //      typedef std::list<int> FractureNodeIdxList;
    	  //      typedef typename FractureNodeIdxList::iterator FractureNodeIdxLisIdxListIterator;

    	  typedef typename FractureGrid::Traits::template Codim<0>::EntitySeed FGES;
    	  typedef std::vector<FGES> VectorOfEntitySeeds;
    	  typedef typename VectorOfEntitySeeds::iterator VectorOfEntitySeedsIterator;


    	  typedef typename FractureGrid::LeafGridView FGV;
    	  typedef typename FGV::IntersectionIterator FractureIntersectionIterator;
    	  FGV fgv=fractureGrid_.leafGridView();

    	  typedef typename MultiDomainGrid::ctype ctype;

    	  typedef typename Dune::PDELab::LocalFunctionSpace<FractureGFS> FractureLFS;
    	  FractureLFS fracturelfs(fractureGFS_);

    	  const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();
    	  const LGFSChild1 &lgfsChild1 = lfsu.template child<1>();
    	  const LGFSChild2 &lgfsChild2 = lfsu.template child<2>();
    	  const LGFSChild3 &lgfsChild3 = lfsu.template child<3>();
    	  const LGFSChild4 &lgfsChild4 = lfsu.template child<4>();
    	  const LGFSChild5 &lgfsChild5 = lfsu.template child<5>();

    	  //      sizeType numberOfSpaces=(lgfsChild0.size()+lgfsChild1.size()+lgfsChild2.size()+lgfsChild3.size()+lgfsChild4.size()+lgfsChild5.size())/4;
    	  assert( ((lgfsChild0.size()+lgfsChild1.size()+lgfsChild2.size()+lgfsChild3.size()+lgfsChild4.size()+lgfsChild5.size())/4) <6);

    	  //create object which modifies the basis and test functions correctly
    	  typedef Dumux::modifiedBasisFunction<MultiDomainGrid, typename EG::Entity, IntersectionPointsMap, LFSU> MBF;
    	  MBF mbf(multiDomainGrid_, eg.entity(), intersectionPointsMap_, lfsu, true);
    	  //extract soil parameters
    	  const RF xi=soil_.xi();//fumble parameter 0.5 < xi <= 1.0


    	  //create sub-elements of co-dimension one depending on the fracture geometry
    	  typedef typename Dune::ReferenceElements<ctype,dimw_> GRE;
    	  typedef Dune::GeometryType::BasicType BasicType;
    	  const BasicType simplex = Dune::GeometryType::simplex;
    	  typedef Dune::GeometryType GT;
    	  GT matrixElementGeometryType =eg.geometry().type();
    	  typedef Dune::MultiLinearGeometry<ctype,dimw_-1,dimw_> ElementGeometryFracture;
    	  const GT subSimplexFracture(simplex,dimw_-1);
    	  std::vector<FV> listOfCornerCoordinatesFracture(2);
    	  typedef typename std::vector<int> IndexVector;
    	  IndexVector listOfCornerIndicesFracture(2);
    	  //iterate over all fracture sub-elements
    	  VectorOfEntitySeeds vectorOfEntitySeeds=(((*cmit).second));//FractureNodeIndexType
          Dune::FieldVector<DF,dimw_-1> localFractureEndInElement;
          RF source=0.0;
    	  for (VectorOfEntitySeedsIterator esIt=vectorOfEntitySeeds.begin(); esIt!=vectorOfEntitySeeds.end();esIt++ ){
    		  fracturelfs.bind(*(fractureGrid_.entityPointer(*esIt)));
    		  std::vector<bool> endingNodeTreatment(2,false);
    		  const RF alphaF=soil_.alphaF(*(fractureGrid_.entityPointer(*esIt)));
    		  FractureIntersectionIterator isItEnd = fgv.iend( *(fractureGrid_.entityPointer(*esIt)) );
    		  for (FractureIntersectionIterator isIt = fgv.ibegin( *(fractureGrid_.entityPointer(*esIt)) ); isIt != isItEnd; ++isIt){
    			  //TODO this is only for 1d ending fractures
    			  Dune::FieldVector<RF,0> position(0.0);
    			  if ( fb_.isEnding( *isIt, position) ){
    				  endingNodeTreatment[isIt->indexInInside()]=true;
    				  /*
    				   * localFractureEndInElement is either zero or one,
    				   * extremly short fracture elements which have two ends in one matrix cell are not allowed
    				   */
    				  localFractureEndInElement=  (*(isIt->inside())).geometry().local(isIt->geometry().center());
    			  }
    			  listOfCornerCoordinatesFracture[isIt->indexInInside()]= isIt->geometry().center();
    		  }//end loop over fracture nodes

    		  /*
    		   * if the fracture element node is within this matrix element, then use it as node for the integration subelement.
    		   * Otherwise use the intersection point with the element boundary.
    		   */
    		  for (int i=0;i<2;i++){
    			  if ( !GRE::general(matrixElementGeometryType).checkInside( eg.geometry().local( listOfCornerCoordinatesFracture[i] ) ) ){
    				  //                  endingNodeTreatment[i]=false;
    				  RF distanceOnLine=1.0e6;
    				  FV closestPoint(check);
    				  FV thisClosestPoint(check);
    				  FV minClosestPoint(check);
    				  RF thisDistance=1.0e6;
    				  RF minDistance=1.0e6;
    				  for (int j=0;j<4;j++){//index 4 and 5 only store the midpoints/virtual exit points
    					  /*
    					   * if there is an empty entry there are no more relevant entries after that
    					   * for ending fractures the projected exit is not to be used!
    					   */
    					  if (Dumux::arePointsEqual(intersectionPoints[j].intersection,check)){
    						  break;
    					  }
    					  else if(Dumux::pointOnLine(listOfCornerCoordinatesFracture[0],
    							  listOfCornerCoordinatesFracture[1],
    							  eg.geometry().global(intersectionPoints[j].intersection)) //make the comparison in global coordinates
    					  ) {
    						  /*
    						   * make sure that always the intersectionPoint closest to the old fracture node is chosen
    						   */
    						  if (Dumux::arePointsEqual(closestPoint,check) ) {
    							  closestPoint=eg.geometry().global(intersectionPoints[j].intersection);//but store the global coordinates
    							  //clean up closest Point zeros
    							  for (int indexK=0;indexK<2;indexK++){
    								  if (Dumux::arePointsEqual(closestPoint[indexK],0.0)) closestPoint[indexK]=0.0;
    							  }
    							  distanceOnLine=Dumux::absDistance(listOfCornerCoordinatesFracture[i],closestPoint);
    						  }
    						  else if (Dumux::absDistance(listOfCornerCoordinatesFracture[i],eg.geometry().global(intersectionPoints[j].intersection)) < distanceOnLine){
    							  //if the distance to the second intersectionPoint is smaller than to the first, replace the closestPoint by this intersectionPoint
    							  closestPoint=eg.geometry().global(intersectionPoints[j].intersection);
    	    					}
    					  }
    					  //debug output
    					  else{
//    						  std::cout << "not on line: intersection: " << eg.geometry().global(intersectionPoints[j].intersection) << std::endl;
    						  thisClosestPoint=Dumux::closestPointOnLine(listOfCornerCoordinatesFracture[0],
    								  listOfCornerCoordinatesFracture[1],
    								  eg.geometry().global(intersectionPoints[j].intersection));
    						  thisDistance=Dumux::absDistance(eg.geometry().global(intersectionPoints[j].intersection),thisClosestPoint);
    						  if (thisDistance<minDistance){
    							  minDistance=thisDistance;
    							  minClosestPoint=eg.geometry().global(intersectionPoints[j].intersection);
    						  }
    					  }
    				  }//end loop over intersectionPoints
    				  if (Dumux::arePointsEqual(closestPoint,check)){
    					  std::cout << "C WARNING: point is not on line but "<< minDistance << " away!!!!!!!!!!!!!!!" << std::endl;
    					  closestPoint=minClosestPoint;
    				  }
    				  assert(!Dumux::arePointsEqual(closestPoint,check));
    				  listOfCornerCoordinatesFracture[i] = closestPoint;
    			  }//end if one fracture node does not lie within this matrix element
    		  }//end loop over fracture nodes

    		  //every subelement is defined by its corners
    		  if (Dumux::arePointsEqual(listOfCornerCoordinatesFracture[0],listOfCornerCoordinatesFracture[1])) {continue;}
    		  ElementGeometryFracture subElementFracture(subSimplexFracture,listOfCornerCoordinatesFracture);
    		  GT gtf=subElementFracture.type();//subSimplex;
    		  // select quadrature rule for the subelement
    		  const Dune::QuadratureRule<DF,dimw_-1>& ruleFace = Dune::QuadratureRules<DF,dimw_-1>::rule(gtf,2); //integration on type, integration order
    		  //start loop for integration over fracture subelement
    		  for (typename Dune::QuadratureRule<DF,dimw_-1>::const_iterator it=ruleFace.begin(); it!=ruleFace.end(); ++it)
    		  {
    			  // position of quadrature point in local coordinates of element
    			  Dune::FieldVector<DF,dimw_-1> const localOnSubFracture=it->position();

    			  /*
    			   * transforms coordinates of integration point on the lower dimensional fracture
    			   * into the dimworld dimensional global coordinates
    			   * This transformations are exact.
    			   */
    			  FV const globalPos = subElementFracture.global( localOnSubFracture );
    			  RF integrationFactor = it->weight()*subElementFracture.integrationElement(localOnSubFracture);//global fracture length within this element
    			  Dune::FieldVector<DF,dimw_-1> const localOnFracture= (fractureGrid_.entityPointer(*esIt))->geometry().local(globalPos);

    			  // evaluate fracture basis functions on reference element
    			  std::vector<RangeType> phiF( fracturelfs.size() );
    			  fracturelfs.finiteElement().localBasis().evaluateFunction(localOnFracture,phiF);
                  std::vector<RangeType> phiFAtEnd( fracturelfs.size() );
                  fracturelfs.finiteElement().localBasis().evaluateFunction(localFractureEndInElement,phiFAtEnd);

    			  //debug: evaluate gradient on fracture
    			  // evaluate gradient of basis functions on reference element
    			  typedef typename FractureLFS::Traits::FiniteElementType::
    					  Traits::LocalBasisType::Traits::JacobianType JacobianTypeF;
    			  typedef typename FractureLFS::Traits::FiniteElementType::
    					  Traits::LocalBasisType::Traits::RangeFieldType RFF;
    			  std::vector<JacobianTypeF> js(fracturelfs.size());
    			  fracturelfs.finiteElement().localBasis().evaluateJacobian(localOnFracture,js);
    			  std::vector<JacobianTypeF> jsAtEnd(fracturelfs.size());
    			  fracturelfs.finiteElement().localBasis().evaluateJacobian(localFractureEndInElement,jsAtEnd);
    			  RFF jac= subElementFracture.integrationElement(localOnFracture);//TODO integrationElement is constant at every integration point. so it could be placed outside this loop
    			  std::vector<Dune::FieldVector<RFF,1>> gradphif(fracturelfs.size());//lfsu.size()=2 for 1D line elements here
    			  std::vector<Dune::FieldVector<RFF,1>> gradphifAtEnd(fracturelfs.size());//lfsu.size()=2 for 1D line elements here
    			  for (sizeType i=0; i<fracturelfs.size(); i++)
    			  {
    			      auto temp=js[i][0];
    			      temp/=jac;
    				  gradphif[i]=temp;//TODO: Only 1D, re-think for planes in 3D environment
    				  gradphifAtEnd[i]=jsAtEnd[i][0];//WARNING: jac for boundary term is actually 1, thus it is not used here
    			  }

    			  /*
    			   * transforms global coordinates of integration point into local rock matrix element coordinates
    			   * This transformation is also exact, but the rock matrix sub-element division is not carried out exactly
    			   * for non-standard (non-quadratic/rectangular) elements. To avoid miscalculations of the basis/test function evaluation
    			   * the global xQP is moved on a matrix element locally straight line between intersection and midpoint
    			   * (the real fracture might not be a straight line in matrix element local coordinates).
    			   */
    			  FV xQP = eg.geometry().local( globalPos );

    			  FV intersectionPointForThisBranch(intersectionPoints[4].intersection);
    			  for (int k=0;k<4;++k){
    				  if(Dumux::arePointsEqual(intersectionPoints[k].intersection,check)) break;
    				  intersectionPointForThisBranch=intersectionPoints[k].intersection;
    				  if (!Dumux::pointOnLine(intersectionPoints[4].intersection,intersectionPointForThisBranch,xQP)
    				  && Dumux::pointOnLine(eg.geometry().global(intersectionPoints[4].intersection),eg.geometry().global(intersectionPointForThisBranch),globalPos)){
    					  mbf.insertSubpolygonPoint(intersectionPointForThisBranch,xQP);
    				  }
    			  }

    			  // evaluate basis functions on reference element
    			  std::vector<RangeType> phi(lgfsChild0.size());
    			  lgfsChild0.finiteElement().localBasis().evaluateFunction(xQP,phi);

    			  //evaluate phiAverage
    			  //phiAverage is zero if the integration point is outside the current matrix element (independent of the support of the basis function).
    			  std::vector<RangeType> phiAverage(lgfsChild0.size());
    			  phiAverage=mbf.average(phi,xQP,0);

    			  RF pAverage=0.0;
    			  for (sizeType i=0; i<lgfsChild0.size(); i++){
    				  pAverage += x(lgfsChild0,i)*phiAverage[i];
    			  }
    			  RF pF=0.0;

                  // compute pf and gradient of pf
                  Dune::FieldVector<RF,dimw_-1> gradpf(0.0);
    			  for (sizeType i=0; i<fracturelfs.size(); i++){
    				  auto globalIndexFracture=(fracturelfs.gridFunctionSpace().ordering().mapIndex( fracturelfs.dofIndex(i)))[0];
    				  pF += xf_.base()[globalIndexFracture]*phiF[i];
    				  //for fracture tip treatment
    				  //std::cout << "xf_.base()[globalIndexFracture]: " << xf_.base()[globalIndexFracture] << std::endl;
    				  gradpf.axpy(xf_.base()[globalIndexFracture],gradphifAtEnd[i]);
    				  //end fracture tip treatment
    			  }

    			  /*
    			   * for fracture tip treatment
    			   * in general jac is dependent on the position, but
    			   * in this case it is element-wise constant so that the same jac
    			   * as for the element integration is used.
    			   * Because the fracture is a 1d element and integration should be across the boundary
    			   * weight is simply always one. For the total 2d flux it has to multiplied by the aperture.
    			   */
                  const RF KFT=1.0e-2;//soil_.kFT(ig.inside());
                  const RF d=1.0e-2;//soil_.fractureWidth(ig.inside());
                  RF factorF = it->weight()*jac;
    			  //end fracture tip treatment

    			  RF factor = ( alphaF / (xi-0.5) ) * integrationFactor*(-1.0);
    			  for (sizeType i=0; i<lgfsChild0.size(); i++){
    				  auto globalIndexMatrix=(lfsu.gridFunctionSpace().ordering().mapIndex( lgfsChild0.dofIndex(i)))[0];
    				  rMonolithic_[globalIndexMatrix]+=pF*phiAverage[i]*factor;
    			  }

    			  if (lgfsChild1.size()!=0){
    				  // evaluate enriched basis functions on reference element
    				  std::vector<RangeType> phiE0(lgfsChild1.size());
    				  lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phiE0);
    				  //evaluate phiAverage
    				  std::vector<RangeType> phiE0Average(lgfsChild1.size());
    				  phiE0Average=mbf.average(phiE0,xQP,1);

    				  for (sizeType i=0; i<lgfsChild1.size(); i++){
    					  pAverage += x(lgfsChild1,i)*phiE0Average[i];
    				  }
    				  for (sizeType i=0; i<lgfsChild1.size(); i++){
    					  auto globalIndexMatrix=(lfsu.gridFunctionSpace().ordering().mapIndex( lgfsChild1.dofIndex(i)))[0];
    					  rMonolithic_[globalIndexMatrix]+=pF*phiE0Average[i]*factor;
    				  }
    			  }
    			  if (lgfsChild2.size()!=0){
    				  // evaluate enriched basis functions on reference element
    				  std::vector<RangeType> phiE1(lgfsChild2.size());
    				  lgfsChild2.finiteElement().localBasis().evaluateFunction(xQP,phiE1);
    				  //evaluate phiAverage
    				  std::vector<RangeType> phiE1Average(lgfsChild2.size());
    				  phiE1Average=mbf.average(phiE1,xQP,2);

    				  for (sizeType i=0; i<lgfsChild2.size(); i++){
    					  pAverage += x(lgfsChild2,i)*phiE1Average[i];
    				  }
    				  for (sizeType i=0; i<lgfsChild2.size(); i++){
    					  auto globalIndexMatrix=(lfsu.gridFunctionSpace().ordering().mapIndex( lgfsChild2.dofIndex(i)))[0];
    					  rMonolithic_[globalIndexMatrix]+=pF*phiE1Average[i]*factor;
    				  }
    			  }
    			  if (lgfsChild3.size()!=0){
    				  // evaluate enriched basis functions on reference element
    				  std::vector<RangeType> phiE2(lgfsChild3.size());
    				  lgfsChild3.finiteElement().localBasis().evaluateFunction(xQP,phiE2);
    				  //evaluate phiAverage
    				  std::vector<RangeType> phiE2Average(lgfsChild3.size());
    				  phiE2Average=mbf.average(phiE2,xQP,3);

    				  for (sizeType i=0; i<lgfsChild3.size(); i++){
    					  pAverage += x(lgfsChild3,i)*phiE2Average[i];
    				  }
    				  for (sizeType i=0; i<lgfsChild3.size(); i++){
    					  auto globalIndexMatrix=(lfsu.gridFunctionSpace().ordering().mapIndex( lgfsChild3.dofIndex(i)))[0];
    					  rMonolithic_[globalIndexMatrix]+=pF*phiE2Average[i]*factor;
    				  }
    			  }
    			  if (lgfsChild4.size()!=0){
    				  // evaluate enriched basis functions on reference element
    				  std::vector<RangeType> phiE3(lgfsChild4.size());
    				  lgfsChild4.finiteElement().localBasis().evaluateFunction(xQP,phiE3);
    				  //evaluate phiAverage
    				  std::vector<RangeType> phiE3Average(lgfsChild4.size());
    				  phiE3Average=mbf.average(phiE3,xQP,4);

    				  for (sizeType i=0; i<lgfsChild4.size(); i++){
    					  pAverage += x(lgfsChild4,i)*phiE3Average[i];
    				  }
    				  for (sizeType i=0; i<lgfsChild4.size(); i++){
    					  auto globalIndexMatrix=(lfsu.gridFunctionSpace().ordering().mapIndex( lgfsChild4.dofIndex(i)))[0];
    					  rMonolithic_[globalIndexMatrix]+=pF*phiE3Average[i]*factor;
    				  }
    			  }
    			  if (lgfsChild5.size()!=0){
    				  // evaluate enriched basis functions on reference element
    				  std::vector<RangeType> phiE4(lgfsChild5.size());
    				  lgfsChild5.finiteElement().localBasis().evaluateFunction(xQP,phiE4);
    				  //evaluate phiAverage
    				  std::vector<RangeType> phiE4Average(lgfsChild5.size());
    				  phiE4Average=mbf.average(phiE4,xQP,5);

    				  for (sizeType i=0; i<lgfsChild5.size(); i++){
    					  pAverage += x(lgfsChild5,i)*phiE4Average[i];
    				  }
    				  for (sizeType i=0; i<lgfsChild5.size(); i++){
    					  auto globalIndexMatrix=(lfsu.gridFunctionSpace().ordering().mapIndex( lgfsChild5.dofIndex(i)))[0];
    					  rMonolithic_[globalIndexMatrix]+=pF*phiE4Average[i]*factor;
    				  }
    			  }

    			  //              // integration
    			  //TODO handle Dirichlet boundaries???
    			  for (sizeType i=0; i<fracturelfs.size(); i++){
    			      auto globalIndexFracture=(fracturelfs.gridFunctionSpace().ordering().mapIndex( fracturelfs.dofIndex(i)))[0];
    			      if (endingNodeTreatment[i] && fix_pf_){
    			          rMonolithic_[globalIndexFracture+offset_]=0.0;
    			      }
    			      else rMonolithic_[globalIndexFracture+offset_]+=pAverage*phiF[i]*factor;
    			      if (endingNodeTreatment[i] && !fix_pf_){
//    			          std::cout << d << "\t ending node boundary term in coupling: " << d*(-KFT)*gradpf*phiFAtEnd[i]<<std::endl;
    			          auto gradpfphiFAtEndi=gradpf*phiFAtEnd[i];
    			          source+=d*(KFT)*gradpfphiFAtEndi;//TODO VORZEICHEN???
                      }
    			  }

    		  }//end loop over fracture quadrature points







    	  //ending fracture source term
    	  if (endingNEW && (endingNodeTreatment[0] || endingNodeTreatment[1]) ){
              source=1.0;
    	      std::cout << "source: " << source << std::endl;
              /*
               * integrate - KFT * grad p * phi_i on the boundary of gamma
               * and couple it to the matrix by inserting a source term
               */

    	      //loop over subsimplices
    	      mbf.createSimplicesFromSubpolygons();
    	      for (int sIdx=0; sIdx<mbf.subSimplices.size();++sIdx){
                  std::cout << "sIdx: " << sIdx << std::endl;
    	          //loop over integration points in subsimplex
    	          GT gt=mbf.subSimplices[sIdx].type();//subSimplex;
    	          const Dune::QuadratureRule<ctype,dimw_>& rule = Dune::QuadratureRules<ctype,dimw_>::rule(gt,intorder_);
    	          for (typename Dune::QuadratureRule<ctype,dimw_>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
    	          {
    	              /*
    	               * it->position is on the local subelement
    	               * subElement.global makes it world global
    	               * eg.geometry().local transformes it into the local matrix element
    	               */
    	              FV xQP = eg.geometry().local( mbf.subSimplices[sIdx].global( it->position() ) );
    	              //the source term is absolut and has to be given per matrix element volume
                      source /= eg.geometry().integrationElement(xQP);
    	              /*
    	               * factor consists of the weight and the integrationElement, which is for triangles DOUBLE the element area.
    	               * and for cubes just the element area.
    	               * here integrationElement of the subelement is called with the local position of the quadrature point within this subelement
    	               */
    	              RF factor = it->weight()*mbf.subSimplices[sIdx].integrationElement(it->position());


    	              const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();

    	              // evaluate basis functions on reference element
    	              std::vector<RangeType> phi(lgfsChild0.size());
    	              lgfsChild0.finiteElement().localBasis().evaluateFunction(xQP,phi);
    	              std::vector<RangeType> phiModified(lgfsChild0.size());
    	              phiModified=mbf.modify(phi,xQP,0);

                      for (sizeType i=0; i<lgfsChild0.size(); i++){
                      std::cout << phiModified[i] << std::endl;
    	                  auto globalIndexMatrix=(lfsu.gridFunctionSpace().ordering().mapIndex( lgfsChild0.dofIndex(i)))[0];
    	                  rMonolithic_[globalIndexMatrix]+=source*phiModified[i]*factor;
    	              }


    	              if (lgfsChild1.size()!=0){
    	                  // evaluate enriched basis functions on reference element
    	                  std::vector<RangeType> phiE0(lgfsChild1.size());
    	                  lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phiE0);
    	                  std::vector<RangeType> phiE0Modified(lgfsChild1.size());
    	                  phiE0Modified=mbf.modify(phiE0,xQP,1);

    	                  for (sizeType i=0; i<lgfsChild1.size(); i++){
                      std::cout << phiE0Modified[i] << std::endl;
    	                      auto globalIndexMatrix=(lfsu.gridFunctionSpace().ordering().mapIndex( lgfsChild1.dofIndex(i)))[0];
    	                      rMonolithic_[globalIndexMatrix]+=source*phiE0Modified[i]*factor;
    	                  }
    	              }
    	          }
    	      }//end loop over sub-simplices for volume source term
    	  }//end ending node source term treatment




    	  }//end loop over fracture subelements




      }//end alpha_volume

  // boundary integral
  // for Neumann b.c.
  template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_boundary (const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s) const
  {}//end alpha_boundary


  template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
  void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv,
		  M& mat) const
  {

      bool endingNEW=false;
	  //here it is assumed that the LFSU is a powergridfunctionspace,
	  //i.e., all individual function spaces are of the same order!
	  // domain and range field type (assume both components have same RF)
	  typedef typename LFSU::template Child<0>::Type LGFSChild0;
	  typedef typename LGFSChild0::Traits::FiniteElementType::
			  Traits::LocalBasisType::Traits::DomainFieldType DF;
	  typedef typename LGFSChild0::Traits::FiniteElementType::
			  Traits::LocalBasisType::Traits::RangeFieldType RF;
	  typedef typename LGFSChild0::Traits::FiniteElementType::
			  Traits::LocalBasisType::Traits::RangeType RangeType;
	  typedef typename LGFSChild0::Traits::FiniteElementType::
			  Traits::LocalBasisType::Traits::DomainType FV;
	  typedef typename LGFSChild0::Traits::SizeType sizeType;

	  //get element Id
	  typedef typename MultiDomainGrid::Traits::GlobalIdSet::IdType IdType;
	  const IdType elementId =multiDomainGrid_.globalIdSet().id(eg.entity());

	  typedef typename IntersectionPointsMap::mapped_type IntersectionPoints;
	  IntersectionPoints intersectionPoints=(((*intersectionPointsMap_.find(elementId)).second));
	  FV check(-1000.0);//check means no entry

	  typedef typename CouplingMap::iterator CouplingMapIterator;
	  CouplingMapIterator cmit=couplingMap_.find(elementId);
	  assert(cmit != couplingMap_.end());

	  typedef typename FractureGrid::Traits::template Codim<0>::EntitySeed FGES;
	  typedef std::vector<FGES> VectorOfEntitySeeds;
	  typedef typename VectorOfEntitySeeds::iterator VectorOfEntitySeedsIterator;


	  typedef typename FractureGrid::LeafGridView FGV;
	  typedef typename FGV::IntersectionIterator FractureIntersectionIterator;
	  FGV fgv=fractureGrid_.leafGridView();

	  typedef typename Dune::PDELab::LocalFunctionSpace<FractureGFS> FractureLFS;
	  FractureLFS fracturelfs(fractureGFS_);

	  const LGFSChild0 &lgfsChild0 = lfsu.template child<0>();
	  typedef typename LFSU::template Child<1>::Type LGFSChild1;
	  const LGFSChild1 &lgfsChild1 = lfsu.template child<1>();
	  typedef typename LFSU::template Child<2>::Type LGFSChild2;
	  const LGFSChild2 &lgfsChild2 = lfsu.template child<2>();
	  typedef typename LFSU::template Child<3>::Type LGFSChild3;
	  const LGFSChild3 &lgfsChild3 = lfsu.template child<3>();
	  typedef typename LFSU::template Child<4>::Type LGFSChild4;
	  const LGFSChild4 &lgfsChild4 = lfsu.template child<4>();
	  typedef typename LFSU::template Child<5>::Type LGFSChild5;
	  const LGFSChild5 &lgfsChild5 = lfsu.template child<5>();

	  assert( ((lgfsChild0.size()+lgfsChild1.size()+lgfsChild2.size()+lgfsChild3.size()+lgfsChild4.size()+lgfsChild5.size())/4) <6);

	  //create object which modifies the basis and test functions correctly
	  typedef Dumux::modifiedBasisFunction<MultiDomainGrid, typename EG::Entity, IntersectionPointsMap, LFSU> MBF;
	  MBF mbf(multiDomainGrid_, eg.entity(), intersectionPointsMap_, lfsu, true);
	  //extract soil parameters
	  const RF xi=soil_.xi();//fumble parameter 0.5 < xi <= 1.0

	  //create sub-elements of co-dimension one depending on the fracture geometry
	  typedef typename Dune::ReferenceElements<ctype,dimw_> GRE;
	  typedef Dune::GeometryType::BasicType BasicType;
	  const BasicType simplex = Dune::GeometryType::simplex;
	  typedef Dune::GeometryType GT;
	  GT matrixElementGeometryType =eg.geometry().type();
	  typedef Dune::MultiLinearGeometry<ctype,dimw_-1,dimw_> ElementGeometryFracture;
	  const GT subSimplexFracture(simplex,dimw_-1);
	  std::vector<FV> listOfCornerCoordinatesFracture(2);
	  typedef typename std::vector<int> IndexVector;
	  IndexVector listOfCornerIndicesFracture(2);
	  //iterate over all fracture sub-elements
	  VectorOfEntitySeeds vectorOfEntitySeeds=(((*cmit).second));//FractureNodeIndexType
      Dune::FieldVector<DF,dimw_-1> localFractureEndInElement;
      RF source=0.0;
	  for (VectorOfEntitySeedsIterator esIt=vectorOfEntitySeeds.begin(); esIt!=vectorOfEntitySeeds.end();esIt++ ){
		  fracturelfs.bind(*(fractureGrid_.entityPointer(*esIt)));
		  const RF alphaF=soil_.alphaF(*(fractureGrid_.entityPointer(*esIt)));
		  FractureIntersectionIterator isItEnd = fgv.iend( *(fractureGrid_.entityPointer(*esIt)) );
		  std::vector<bool> endingNodeTreatment(2,false);
		  for (FractureIntersectionIterator isIt = fgv.ibegin( *(fractureGrid_.entityPointer(*esIt)) ); isIt != isItEnd; ++isIt){
			  //TODO this is only for 1d ending fractures
			  Dune::FieldVector<RF,0> position(0.0);
			  if ( fb_.isEnding( *isIt, position) ){
				  endingNodeTreatment[isIt->indexInInside()]=true;
				  /*
				   * localFractureEndInElement is either zero or one,
				   * extremly short fracture elements which have to ends in one matrix cell are not allowed
				   */
				  localFractureEndInElement=  (*(isIt->inside())).geometry().local(isIt->geometry().center());
			  }
			  listOfCornerCoordinatesFracture[isIt->indexInInside() ]= isIt->geometry().center();
		  }//end loop over fracture nodes

		  /*
		   * if the fracture element node is within this matrix element, then use it as node for the integration subelement.
		   * Otherwise use the intersection point with the element boundary.
		   */
		  std::vector<bool> notInside(2,false);
		  for (int i=0;i<2;i++){
			  if ( !GRE::general(matrixElementGeometryType).checkInside( eg.geometry().local( listOfCornerCoordinatesFracture[i] ) ) ){
				  notInside[i]=true;
				  RF distanceOnLine=1.0e6;
				  FV closestPoint(check);
				  FV thisClosestPoint(check);
				  FV minClosestPoint(check);
				  RF thisDistance=1.0e6;
				  RF minDistance=1.0e6;
				  for (int j=0;j<4;j++){//index 4 and 5 only store the midpoints/virtual exit points
					  /*
					   * if there is an empty entry there are no more relevant entries after that
					   * for ending fractures the projected exit is not to be used!
					   */
					  if (Dumux::arePointsEqual(intersectionPoints[j].intersection,check)){
						  break;
					  }
					  else if( Dumux::pointOnLine(listOfCornerCoordinatesFracture[0],
							  listOfCornerCoordinatesFracture[1],
							  eg.geometry().global(intersectionPoints[j].intersection)) //make the comparison in global coordinates
					  ) {
						  /*
						   * make sure that always the intersectionPoint closest to the old fracture node is chosen
						   */
						  if (Dumux::arePointsEqual(closestPoint,check) ) {
							  closestPoint=eg.geometry().global(intersectionPoints[j].intersection);//but store the global coordinates
							  //clean up closest Point zeros
							  for (int indexK=0;indexK<2;indexK++){
								  if (Dumux::arePointsEqual(closestPoint[indexK],0.0)) closestPoint[indexK]=0.0;
							  }
							  distanceOnLine=Dumux::absDistance(listOfCornerCoordinatesFracture[i],closestPoint);
						  }
						  else if (Dumux::absDistance(listOfCornerCoordinatesFracture[i],eg.geometry().global(intersectionPoints[j].intersection)) < distanceOnLine){
							  //if the distance to the second intersectionPoint is smaller than to the first, replace the closestPoint by this intersectionPoint
							  closestPoint=eg.geometry().global(intersectionPoints[j].intersection);
						  }
					  }
					  //debug output
					  else{
						  thisClosestPoint=Dumux::closestPointOnLine(listOfCornerCoordinatesFracture[0],
								  listOfCornerCoordinatesFracture[1],
								  eg.geometry().global(intersectionPoints[j].intersection));
						  thisDistance=Dumux::absDistance(eg.geometry().global(intersectionPoints[j].intersection),thisClosestPoint);
						  if (thisDistance<minDistance){
							  minDistance=thisDistance;
							  minClosestPoint=eg.geometry().global(intersectionPoints[j].intersection);
						  }
					  }
				  }//end loop over intersectionPoints
				  if (Dumux::arePointsEqual(closestPoint,check)){
					  std::cout << "D WARNING: point is not on line but "<< minDistance << " away!!!!!!!!!!!!!!!" << std::endl;
					  closestPoint=minClosestPoint;
				  }
				  assert(!Dumux::arePointsEqual(closestPoint,check));
				  listOfCornerCoordinatesFracture[i] = closestPoint;
			  }//end if one fracture node does not lie within this matrix element
		  }//end loop over fracture nodes

		  //every subelement is defined by its corners
		  if (Dumux::arePointsEqual(listOfCornerCoordinatesFracture[0],listOfCornerCoordinatesFracture[1])) {continue;}
		  ElementGeometryFracture subElementFracture(subSimplexFracture,listOfCornerCoordinatesFracture);
		  GT gtf=subElementFracture.type();//subSimplex;
		  // select quadrature rule for the subelement
		  const Dune::QuadratureRule<DF,dimw_-1>& ruleFace = Dune::QuadratureRules<DF,dimw_-1>::rule(gtf,2); //integration on type, integration order
		  //start loop for integration over fracture subelement
		  for (typename Dune::QuadratureRule<DF,dimw_-1>::const_iterator it=ruleFace.begin(); it!=ruleFace.end(); ++it)
		  {
			  // position of quadrature point in local coordinates of element
			  Dune::FieldVector<DF,dimw_-1> const localOnSubFracture=it->position();

			  //transforms coordinates of integration point on the lower dimensional fracture into the dimw_orld dimensional global coordinates
			  FV const globalPos = subElementFracture.global( localOnSubFracture );
			  Dune::FieldVector<DF,dimw_-1> const localOnFracture= (fractureGrid_.entityPointer(*esIt))->geometry().local(globalPos);
			  //transforms global coordinates of integration point into local rock matrix element coordinates
			  FV xQP = eg.geometry().local( globalPos );

			  FV intersectionPointForThisBranch(intersectionPoints[4].intersection);
			  for (int k=0;k<4;++k){
				  if(Dumux::arePointsEqual(intersectionPoints[k].intersection,check)) break;
				  intersectionPointForThisBranch=intersectionPoints[k].intersection;
				  if (!Dumux::pointOnLine(intersectionPoints[4].intersection,intersectionPointForThisBranch,xQP)
				  && Dumux::pointOnLine(eg.geometry().global(intersectionPoints[4].intersection),eg.geometry().global(intersectionPointForThisBranch),globalPos)){
					  mbf.insertSubpolygonPoint(intersectionPointForThisBranch,xQP);
				  }
			  }

			  RF integrationFactor = it->weight()*subElementFracture.integrationElement(localOnSubFracture);//global fracture length within this element
			  // evaluate fracture basis functions on reference element
			  std::vector<RangeType> phiF( fracturelfs.size() );
			  fracturelfs.finiteElement().localBasis().evaluateFunction(localOnFracture,phiF);
              std::vector<RangeType> phiFAtEnd( fracturelfs.size() );
              fracturelfs.finiteElement().localBasis().evaluateFunction(localFractureEndInElement,phiFAtEnd);

              // evaluate gradient of basis functions on reference element
              typedef typename FractureLFS::Traits::FiniteElementType::
                      Traits::LocalBasisType::Traits::JacobianType JacobianTypeF;
              typedef typename FractureLFS::Traits::FiniteElementType::
                      Traits::LocalBasisType::Traits::RangeFieldType RFF;
              std::vector<JacobianTypeF> js(fracturelfs.size());
              fracturelfs.finiteElement().localBasis().evaluateJacobian(localOnFracture,js);
              std::vector<JacobianTypeF> jsAtEnd(fracturelfs.size());
              fracturelfs.finiteElement().localBasis().evaluateJacobian(localFractureEndInElement,jsAtEnd);
              RFF jac= subElementFracture.integrationElement(localOnFracture);//TODO integrationElement is constant at every integration point. so it could be placed outside this loop
              std::vector<Dune::FieldVector<RFF,1>> gradphif(fracturelfs.size());//lfsu.size()=2 for 1D line elements here
              std::vector<Dune::FieldVector<RFF,1>> gradphifAtEnd(fracturelfs.size());//lfsu.size()=2 for 1D line elements here
              for (sizeType i=0; i<fracturelfs.size(); i++)
              {
                  auto temp=js[i][0];
                  temp/=jac;
                  gradphif[i]=temp;//TODO: Only 1D, re-think for planes in 3D environment
                  gradphifAtEnd[i]=jsAtEnd[i][0];//WARNING: jac for boundary term is actually 1, thus it is not used here
              }

			  // evaluate basis functions on reference element
			  std::vector<RangeType> phi(lgfsChild0.size());
			  lgfsChild0.finiteElement().localBasis().evaluateFunction(xQP,phi);

			  //evaluate phiAverage
			  //phiAverage is zero if the integration point is outside the current matrix element (independent of the support of the basis function).
			  std::vector<RangeType> phiAverage(lgfsChild0.size());
			  phiAverage=mbf.average(phi,xQP,0);

			  // integration
			  RF factor = ( alphaF / (xi-0.5) ) * integrationFactor*(-1.0);
			  for (sizeType i=0; i<lgfsChild0.size(); i++){
				  //ordering must be called from the very root gridfunctionspace
				  auto globalIndexRow=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild0.dofIndex(i)) )[0];
				  for (sizeType j=0; j<fracturelfs.size();j++){
					  auto globalIndexColumn=(fracturelfs.gridFunctionSpace().ordering().mapIndex( fracturelfs.dofIndex(j)))[0]+offset_;
					  if ( gMonolithic_.exists( globalIndexRow, globalIndexColumn ) ){
						  gMonolithic_[globalIndexRow][globalIndexColumn] += ( phiF[j] * phiAverage[i] * factor ) ;
					  }
					  //make it symmetric. it might be not because of Dirichlet boundary conditions
					  if ( gMonolithic_.exists( globalIndexColumn,globalIndexRow ) ){
						  if (!endingNodeTreatment[j] || !fix_pf_) gMonolithic_[globalIndexColumn][globalIndexRow] += ( phiF[j] * phiAverage[i] * factor ) ;
					  }
				  }
			  }

			  if (lgfsChild1.size()!=0){

				  // evaluate basis functions on reference element
				  std::vector<RangeType> phiE0(lgfsChild1.size());
				  lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phiE0);
				  //evaluate Average
				  std::vector<RangeType> phiE0Average(lgfsChild1.size());
				  phiE0Average=mbf.average(phiE0,xQP,1);

				  for (sizeType i=0; i<lgfsChild1.size(); i++){
					  auto globalIndexRow=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild1.dofIndex(i)) )[0];
					  for (sizeType j=0; j<fracturelfs.size();j++){
						  auto globalIndexColumn=(fracturelfs.gridFunctionSpace().ordering().mapIndex( fracturelfs.dofIndex(j)))[0]+offset_;
						  if ( gMonolithic_.exists( globalIndexRow,globalIndexColumn ) ){
							  gMonolithic_[globalIndexRow][globalIndexColumn] += ( phiF[j] * phiE0Average[i] *factor );
						  }
						  //make it symmetric. it might be not because of Dirichlet boundary conditions
						  if ( gMonolithic_.exists( globalIndexColumn,globalIndexRow ) ){
							  if (!endingNodeTreatment[j] || !fix_pf_) gMonolithic_[globalIndexColumn][globalIndexRow] += ( phiF[j] * phiE0Average[i] *factor  );
						  }
					  }
				  }
			  }
			  if (lgfsChild2.size()!=0){

				  // evaluate basis functions on reference element
				  std::vector<RangeType> phiE1(lgfsChild2.size());
				  lgfsChild2.finiteElement().localBasis().evaluateFunction(xQP,phiE1);
				  //evaluate Average
				  std::vector<RangeType> phiE1Average(lgfsChild2.size());
				  phiE1Average=mbf.average(phiE1,xQP,2);

				  for (sizeType i=0; i<lgfsChild2.size(); i++){
					  auto globalIndexRow=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild2.dofIndex(i)) )[0];
					  for (sizeType j=0; j<fracturelfs.size();j++){
						  auto globalIndexColumn=(fracturelfs.gridFunctionSpace().ordering().mapIndex( fracturelfs.dofIndex(j)))[0]+offset_;
						  if ( gMonolithic_.exists( globalIndexRow,globalIndexColumn ) ){
							  gMonolithic_[globalIndexRow][globalIndexColumn] += ( phiF[j] * phiE1Average[i] *factor );
						  }
						  //make it symmetric. it might be not because of Dirichlet boundary conditions
						  if ( gMonolithic_.exists( globalIndexColumn,globalIndexRow ) ){
							  if (!endingNodeTreatment[j] || !fix_pf_) gMonolithic_[globalIndexColumn][globalIndexRow] += ( phiF[j] * phiE1Average[i] *factor  );
						  }
					  }
				  }
			  }
			  if (lgfsChild3.size()!=0){

				  // evaluate basis functions on reference element
				  std::vector<RangeType> phiE2(lgfsChild3.size());
				  lgfsChild3.finiteElement().localBasis().evaluateFunction(xQP,phiE2);
				  //evaluate Average
				  std::vector<RangeType> phiE2Average(lgfsChild3.size());
				  phiE2Average=mbf.average(phiE2,xQP,3);

				  for (sizeType i=0; i<lgfsChild3.size(); i++){
					  auto globalIndexRow=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild3.dofIndex(i)) )[0];
					  for (sizeType j=0; j<fracturelfs.size();j++){
						  auto globalIndexColumn=(fracturelfs.gridFunctionSpace().ordering().mapIndex( fracturelfs.dofIndex(j)))[0]+offset_;
						  if ( gMonolithic_.exists( globalIndexRow,globalIndexColumn ) ){
							  gMonolithic_[globalIndexRow][globalIndexColumn] += ( phiF[j] * phiE2Average[i] *factor );
						  }
						  //make it symmetric. it might be not because of Dirichlet boundary conditions
						  if ( gMonolithic_.exists( globalIndexColumn,globalIndexRow ) ){
							  if (!endingNodeTreatment[j] || !fix_pf_) gMonolithic_[globalIndexColumn][globalIndexRow] += ( phiF[j] * phiE2Average[i] *factor  );
						  }
					  }
				  }
			  }
			  if (lgfsChild4.size()!=0){

				  // evaluate basis functions on reference element
				  std::vector<RangeType> phiE3(lgfsChild4.size());
				  lgfsChild4.finiteElement().localBasis().evaluateFunction(xQP,phiE3);
				  //evaluate Average
				  std::vector<RangeType> phiE3Average(lgfsChild4.size());
				  phiE3Average=mbf.average(phiE3,xQP,4);

				  for (sizeType i=0; i<lgfsChild4.size(); i++){
					  auto globalIndexRow=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild4.dofIndex(i)) )[0];
					  for (sizeType j=0; j<fracturelfs.size();j++){
						  auto globalIndexColumn=(fracturelfs.gridFunctionSpace().ordering().mapIndex( fracturelfs.dofIndex(j)))[0]+offset_;
						  if ( gMonolithic_.exists( globalIndexRow,globalIndexColumn ) ){
							  gMonolithic_[globalIndexRow][globalIndexColumn] += ( phiF[j] * phiE3Average[i] *factor );
						  }
						  //make it symmetric. it might be not because of Dirichlet boundary conditions
						  if ( gMonolithic_.exists( globalIndexColumn,globalIndexRow ) ){
							  if (!endingNodeTreatment[j] || !fix_pf_) gMonolithic_[globalIndexColumn][globalIndexRow] += ( phiF[j] * phiE3Average[i] *factor  );
						  }
					  }
				  }
			  }
			  if (lgfsChild5.size()!=0){

				  // evaluate basis functions on reference element
				  std::vector<RangeType> phiE4(lgfsChild5.size());
				  lgfsChild5.finiteElement().localBasis().evaluateFunction(xQP,phiE4);
				  //evaluate Average
				  std::vector<RangeType> phiE4Average(lgfsChild5.size());
				  phiE4Average=mbf.average(phiE4,xQP,5);

				  for (sizeType i=0; i<lgfsChild5.size(); i++){
					  auto globalIndexRow=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild5.dofIndex(i)) )[0];
					  for (sizeType j=0; j<fracturelfs.size();j++){
						  auto globalIndexColumn=(fracturelfs.gridFunctionSpace().ordering().mapIndex( fracturelfs.dofIndex(j)))[0]+offset_;
						  if ( gMonolithic_.exists( globalIndexRow,globalIndexColumn ) ){
							  gMonolithic_[globalIndexRow][globalIndexColumn] += ( phiF[j] * phiE4Average[i] *factor );
						  }
						  //make it symmetric. it might be not because of Dirichlet boundary conditions
						  if ( gMonolithic_.exists(globalIndexColumn,globalIndexRow ) ){
							  if (!endingNodeTreatment[j] || !fix_pf_) gMonolithic_[globalIndexColumn][globalIndexRow] += ( phiF[j] * phiE4Average[i] *factor  );
						  }
					  }
				  }
			  }

		  }//end loop over fracture quadrature points

		  /*
		   * if this is an ending node set it to the average of pm
		   * TODO this only works for p1/q1 elements in 1d!?
		   * this is in an extra loop because no integration is necessary.
		   */
		  if (fix_pf_){
			  for (sizeType j=0; j<fracturelfs.size();j++){
				  if (endingNodeTreatment[j] && !notInside[j] ){
					  std::cout << "ENDING NODE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<std::endl;
					  auto globalIndexRow=(fracturelfs.gridFunctionSpace().ordering().mapIndex( fracturelfs.dofIndex(j)))[0]+offset_;
					  FV matrixElementLocalPos=eg.geometry().local(listOfCornerCoordinatesFracture[j]);
					  const int jPlusOne= (j==0);
					  FV f=matrixElementLocalPos-eg.geometry().local(listOfCornerCoordinatesFracture[jPlusOne]);
					  f/=f.two_norm();
					  f*=(mbf.template normalEpsilon<ctype>());
					  matrixElementLocalPos+=f;
					  //set all fracture-fracture coupling entries to zero except for the main diagonal
					  for (int fractureIndex=offset_;fractureIndex<gMonolithic_.M();fractureIndex++){
						  if ( gMonolithic_.exists(globalIndexRow,fractureIndex) )
							  gMonolithic_[globalIndexRow][fractureIndex]=0.0;
					  }
					  gMonolithic_[globalIndexRow][globalIndexRow]=1.0;

					  for (sizeType i=0; i<lgfsChild0.size(); i++){
						  auto globalIndexColumn=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild0.dofIndex(i)) )[0];
						  if ( gMonolithic_.exists( globalIndexRow,globalIndexColumn ) ){

							  /*
							   * if this is an ending node set it to the average of pm
							   * TODO this only works for p1/q1 elements in 1d!?
							   */

							  // evaluate basis functions on reference element
							  std::vector<RangeType> phi(lgfsChild0.size());
							  lgfsChild0.finiteElement().localBasis().evaluateFunction(matrixElementLocalPos,phi);

							  //evaluate phiAverage
							  //phiAverage is zero if the integration point is outside the current matrix element (independent of the support of the basis function).
							  std::vector<RangeType> phiAverage(lgfsChild0.size());
							  phiAverage=mbf.average(phi,matrixElementLocalPos,0);

							  //set the fracture-matrix coupling entries to the average matrix pressure
							  gMonolithic_[globalIndexRow][globalIndexColumn]=phiAverage[i];
						  }
					  }//end loop over matrix functions space 0

					  if (lgfsChild1.size()!=0){

						  // evaluate basis functions on reference element
						  std::vector<RangeType> phiE0(lgfsChild1.size());
						  lgfsChild1.finiteElement().localBasis().evaluateFunction(matrixElementLocalPos,phiE0);
						  //evaluate Average
						  std::vector<RangeType> phiE0Average(lgfsChild1.size());
						  phiE0Average=mbf.average(phiE0,matrixElementLocalPos,1);

						  for (sizeType i=0; i<lgfsChild1.size(); i++){
							  auto globalIndexColumn=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild1.dofIndex(i)) )[0];
							  if ( gMonolithic_.exists( globalIndexRow,globalIndexColumn ) ){
								  //set the fracture-matrix coupling entries to the average matrix pressure
								  gMonolithic_[globalIndexRow][globalIndexColumn]=phiE0Average[i];
							  }
						  }//end loop over matrix functions space 1
					  }//end if condition for checking if functions space 1 exists

					  if (lgfsChild2.size()!=0){

						  // evaluate basis functions on reference element
						  std::vector<RangeType> phiE1(lgfsChild2.size());
						  lgfsChild2.finiteElement().localBasis().evaluateFunction(matrixElementLocalPos,phiE1);
						  //evaluate Average
						  std::vector<RangeType> phiE1Average(lgfsChild2.size());
						  phiE1Average=mbf.average(phiE1,matrixElementLocalPos,2);

						  for (sizeType i=0; i<lgfsChild2.size(); i++){
							  auto globalIndexColumn=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild2.dofIndex(i)) )[0];
							  if ( gMonolithic_.exists( globalIndexRow,globalIndexColumn ) ){
								  //set the fracture-matrix coupling entries to the average matrix pressure
								  gMonolithic_[globalIndexRow][globalIndexColumn]=phiE1Average[i];
							  }
						  }//end loop over matrix functions space 2
					  }//end if condition for checking if functions space 2 exists

					  if (lgfsChild3.size()!=0){

						  // evaluate basis functions on reference element
						  std::vector<RangeType> phiE2(lgfsChild3.size());
						  lgfsChild3.finiteElement().localBasis().evaluateFunction(matrixElementLocalPos,phiE2);
						  //evaluate Average
						  std::vector<RangeType> phiE2Average(lgfsChild3.size());
						  phiE2Average=mbf.average(phiE2,matrixElementLocalPos,3);

						  for (sizeType i=0; i<lgfsChild3.size(); i++){
							  auto globalIndexColumn=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild3.dofIndex(i)) )[0];
							  if ( gMonolithic_.exists( globalIndexRow,globalIndexColumn ) ){
								  //set the fracture-matrix coupling entries to the average matrix pressure
								  gMonolithic_[globalIndexRow][globalIndexColumn]=phiE2Average[i];
							  }
						  }//end loop over matrix functions space 3
					  }//end if condition for checking if functions space 3 exists

					  if (lgfsChild4.size()!=0){

						  // evaluate basis functions on reference element
						  std::vector<RangeType> phiE3(lgfsChild4.size());
						  lgfsChild4.finiteElement().localBasis().evaluateFunction(matrixElementLocalPos,phiE3);
						  //evaluate Average
						  std::vector<RangeType> phiE3Average(lgfsChild4.size());
						  phiE3Average=mbf.average(phiE3,matrixElementLocalPos,4);

						  for (sizeType i=0; i<lgfsChild4.size(); i++){
							  auto globalIndexColumn=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild4.dofIndex(i)) )[0];
							  if ( gMonolithic_.exists( globalIndexRow,globalIndexColumn ) ){
								  //set the fracture-matrix coupling entries to the average matrix pressure
								  gMonolithic_[globalIndexRow][globalIndexColumn]=phiE3Average[i];
							  }
						  }//end loop over matrix functions space 4
					  }//end if condition for checking if functions space 4 exists

					  if (lgfsChild5.size()!=0){

						  // evaluate basis functions on reference element
						  std::vector<RangeType> phiE4(lgfsChild5.size());
						  lgfsChild5.finiteElement().localBasis().evaluateFunction(matrixElementLocalPos,phiE4);
						  //evaluate Average
						  std::vector<RangeType> phiE4Average(lgfsChild5.size());
						  phiE4Average=mbf.average(phiE4,matrixElementLocalPos,5);

						  for (sizeType i=0; i<lgfsChild5.size(); i++){
							  auto globalIndexColumn=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild5.dofIndex(i)) )[0];
							  if ( gMonolithic_.exists( globalIndexRow,globalIndexColumn ) ){
								  //set the fracture-matrix coupling entries to the average matrix pressure
								  gMonolithic_[globalIndexRow][globalIndexColumn]=phiE4Average[i];
							  }
						  }//end loop over matrix functions space 5
					  }//end if condition for checking if functions space 5 exists

				  }//end if loop for special treatment of ending fractures
			  }//end loop over fracture boundaries
		  }//end if fix_pf


      //ending fracture source term
        if (endingNEW && (endingNodeTreatment[0] || endingNodeTreatment[1])){
            RF source = - 100*0.01;//-KFT*d
            std::cout << "source: " << source << std::endl;
            /*
             * integrate - KFT * grad p * phi_i on the boundary of gamma
             * and couple it to the matrix by inserting a source term
             */


            // evaluate fracture basis functions on reference element
            std::vector<RangeType> phiFAtEnd( fracturelfs.size() );
            fracturelfs.finiteElement().localBasis().evaluateFunction(localFractureEndInElement,phiFAtEnd);

            // evaluate gradient of basis functions on reference element
            typedef typename FractureLFS::Traits::FiniteElementType::
                    Traits::LocalBasisType::Traits::JacobianType JacobianTypeF;
            typedef typename FractureLFS::Traits::FiniteElementType::
                    Traits::LocalBasisType::Traits::RangeFieldType RFF;
            std::vector<JacobianTypeF> jsAtEnd(fracturelfs.size());
            fracturelfs.finiteElement().localBasis().evaluateJacobian(localFractureEndInElement,jsAtEnd);
            RFF jac= subElementFracture.integrationElement(localFractureEndInElement);//TODO integrationElement is constant at every integration point. so it could be placed outside this loop
            std::vector<Dune::FieldVector<RFF,1>> gradphifAtEnd(fracturelfs.size());//lfsu.size()=2 for 1D line elements here
            for (sizeType i=0; i<fracturelfs.size(); i++)
            {
                gradphifAtEnd[i]=jsAtEnd[i][0];//WARNING: jac for boundary term is actually 1, thus it is not used here
            }


            //loop over subsimplices
            mbf.createSimplicesFromSubpolygons();
            for (int sIdx=0; sIdx<mbf.subSimplices.size();++sIdx){

                //loop over integration points in subsimplex
                GT gt=mbf.subSimplices[sIdx].type();//subSimplex;
                const Dune::QuadratureRule<ctype,dimw_>& rule = Dune::QuadratureRules<ctype,dimw_>::rule(gt,intorder_);
                for (typename Dune::QuadratureRule<ctype,dimw_>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
                {
                    /*
                     * it->position is on the local subelement
                     * subElement.global makes it world global
                     * eg.geometry().local transformes it into the local matrix element
                     */
                    FV xQP = eg.geometry().local( mbf.subSimplices[sIdx].global( it->position() ) );
                    //the source term is absolut and has to be given per matrix element volume
                    source /= eg.geometry().integrationElement(xQP);
                    /*
                     * factor consists of the weight and the integrationElement, which is for triangles DOUBLE the element area.
                     * and for cubes just the element area.
                     * here integrationElement of the subelement is called with the local position of the quadrature point within this subelement
                     */
                    RF integrationFactor = it->weight()*mbf.subSimplices[sIdx].integrationElement(it->position());
                    // evaluate basis functions on reference element
                    std::vector<RangeType> phi(lgfsChild0.size());
                    lgfsChild0.finiteElement().localBasis().evaluateFunction(xQP,phi);
                    std::vector<RangeType> phiModified(lgfsChild0.size());
                    phiModified=mbf.modify(phi,xQP,0);

                    // integration
                    RF factor = source*integrationFactor;
                    for (sizeType i=0; i<lgfsChild0.size(); i++){
                        //ordering must be called from the very root gridfunctionspace
                        auto globalIndexRow=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild0.dofIndex(i)) )[0];
                        for (sizeType j=0; j<fracturelfs.size();j++){
                            auto globalIndexColumn=(fracturelfs.gridFunctionSpace().ordering().mapIndex( fracturelfs.dofIndex(j)))[0]+offset_;
                            if (endingNodeTreatment[j]){
                                gMonolithic_[globalIndexRow][globalIndexColumn] += ( gradphifAtEnd[j] * phiModified[i] * factor);
                            }
                        }
                    }

                    if (lgfsChild1.size()!=0){

                        // evaluate basis functions on reference element
                        std::vector<RangeType> phiE0(lgfsChild1.size());
                        lgfsChild1.finiteElement().localBasis().evaluateFunction(xQP,phiE0);

                        std::vector<RangeType> phiModifiedE0(lgfsChild1.size());
                        phiModifiedE0=mbf.modify(phiE0,xQP,1);

                        for (sizeType i=0; i<lgfsChild1.size(); i++){
                            auto globalIndexRow=(lfsu.gridFunctionSpace().ordering().mapIndex(lgfsChild1.dofIndex(i)) )[0];
                            for (sizeType j=0; j<fracturelfs.size();j++){
                                auto globalIndexColumn=(fracturelfs.gridFunctionSpace().ordering().mapIndex( fracturelfs.dofIndex(j)))[0]+offset_;
                                if (endingNodeTreatment[j]){
                                    gMonolithic_[globalIndexRow][globalIndexColumn] += ( gradphifAtEnd[j] * phiModifiedE0[i] * factor );
                                }
                            }
                        }
                    }
                }
            }//end loop over sub-simplices for volume source term
        }//end ending node source term treatment


	  }//end loop over fracture subelements


  }//end jacobian_volume

//  // jacobian of boundary term
//  template<typename IG, typename LFSU, typename X, typename LFSV, typename M>
//  void jacobian_boundary (const IG& ig, const LFSU& lfsu, const X& x, const LFSV& lfsv, M& mat) const
//  {}//end jacobian_boundary


private:
  MonolithicMatrix &gMonolithic_;
  MonolithicResidual &rMonolithic_;
  const MultiDomainGrid &multiDomainGrid_;
  const FractureGrid &fractureGrid_;
  IntersectionPointsMap &intersectionPointsMap_;
  CouplingMap &couplingMap_;
  const FractureGFS &fractureGFS_;
  const XF &xf_;
  const Soil &soil_;
  const FB &fb_;
  unsigned int intorder_;
  bool fix_pf_;

  //some type extraction
  typedef typename MultiDomainGrid::ctype ctype;
};
