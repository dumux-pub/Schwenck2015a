// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \ingroup IMPETtests
 * \brief test for diffusion models
 */
#include "config.h"
#include <iostream>

#include <dune/common/exceptions.hh>
#include <dune/common/version.hh>
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 3)
#include <dune/common/parallel/mpihelper.hh>
#else
#include <dune/common/mpihelper.hh>
#endif
//#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/grid/io/file/dgfparser.hh>

#include "test_diffusionproblem.hh"
#include "../resultevaluation.hh"

////////////////////////
// the main function
////////////////////////
void usage(const char *progname)
{
    std::cout << "usage: " << progname << " #refine [delta]\n";
    exit(1);
}

int main(int argc, char** argv)
{
    try {
        typedef TTAG(FVVelocity2PTestProblem) TypeTag;
        typedef GET_PROP_TYPE(TypeTag, Scalar) Scalar;
        typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;
        typedef GET_PROP_TYPE(TypeTag, Grid) Grid;
        static const int dim = Grid::dimension;
        typedef Dune::FieldVector<Scalar, dim> GlobalPosition;

        // initialize MPI, finalize is done automatically on exit
        Dune::MPIHelper::instance(argc, argv);

        ////////////////////////////////////////////////////////////
        // parse the command line arguments
        ////////////////////////////////////////////////////////////
        if (argc != 1 && argc != 2)
            usage(argv[0]);

        double delta = 1e-3;
        if (argc == 2)
            std::istringstream(argv[1]) >> delta;

        ////////////////////////////////////////////////////////////
        // create the grid
        ////////////////////////////////////////////////////////////

        // try to create a grid (from the given grid file)
        GridCreator::makeGrid("../grids/final.dgf");
        GridCreator::loadBalance();

        ////////////////////////////////////////////////////////////
        // instantiate and run the concrete problem
        ////////////////////////////////////////////////////////////
        Dune::Timer timer;
        bool consecutiveNumbering = true;

        typedef GET_PROP_TYPE(TTAG(FVVelocity2PTestProblem), Problem) FVProblem;
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 3)
        FVProblem fvProblem(GridCreator::grid().leafGridView(), delta);
#else
        FVProblem fvProblem(GridCreator::grid().leafView(), delta);
#endif
        fvProblem.setName("fvdiffusion");
        fvProblem.init();
        fvProblem.calculateFVVelocity();
        fvProblem.writeOutput();
        Dumux::ResultEvaluation fvResult;

        typedef GET_PROP_TYPE(TTAG(MimeticPressure2PTestProblem), Problem) MimeticProblem;
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 3)
        fvResult.evaluate(GridCreator::grid().leafGridView(), fvProblem, consecutiveNumbering);

        MimeticProblem mimeticProblem(GridCreator::grid().leafGridView(), delta);
#else
        fvResult.evaluate(GridCreator::grid().leafView(), fvProblem, consecutiveNumbering);

        MimeticProblem mimeticProblem(GridCreator::grid().leafView(), delta);
#endif

        mimeticProblem.setName("mimeticdiffusion");
        mimeticProblem.init();
        mimeticProblem.writeOutput();
        Dumux::ResultEvaluation mimeticResult;
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 3)
        mimeticResult.evaluate(GridCreator::grid().leafGridView(), mimeticProblem, consecutiveNumbering);
#else
        mimeticResult.evaluate(GridCreator::grid().leafView(), mimeticProblem, consecutiveNumbering);
#endif

        return 0;
    }
    catch (Dune::Exception &e) {
        std::cerr << "Dune reported error: " << e << std::endl;
    }
    catch (...) {
        std::cerr << "Unknown exception thrown!\n";
        throw;
    }

    return 3;
}
