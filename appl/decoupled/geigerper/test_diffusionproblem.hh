// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief test problem for diffusion models from the FVCA5 benchmark.
 */
#ifndef DUMUX_TEST_2P_PROBLEM_HH
#define DUMUX_TEST_2P_PROBLEM_HH

//#if HAVE_UG
//#include <dune/grid/uggrid.hh>
//#endif

//#include <dune/grid/yaspgrid.hh>
//#include <dune/grid/sgrid.hh>

#if HAVE_UG
#include <dune/grid/io/file/dgfparser/dgfug.hh>
#endif
#if HAVE_ALUGRID
#include <dune/grid/io/file/dgfparser/dgfalu.hh>
#endif
#include <dune/grid/io/file/dgfparser/dgfs.hh>
#include <dune/grid/io/file/dgfparser/dgfyasp.hh>
#include <dune/grid/io/file/gmshreader.hh>

#include <dumux/material/components/unit.hh>
#include <dumux/io/dgfgridcreator.hh>

#include <dumux/decoupled/2p/diffusion/fv/fvpressureproperties2p.hh>
#include <dumux/decoupled/2p/diffusion/fvmpfa/omethod/fvmpfao2dpressureproperties2p.hh>
#include <dumux/decoupled/2p/diffusion/mimetic/mimeticpressureproperties2p.hh>
#include <dumux/decoupled/2p/diffusion/diffusionproblem2p.hh>
#include <dumux/decoupled/common/fv/fvvelocity.hh>

#include "test_diffusionspatialparams.hh"

namespace Dumux
{
/*!
 * \ingroup IMPETtests
 */
template<class TypeTag>
class TestDiffusionProblem;

//////////
// Specify the properties
//////////
namespace Properties
{
//// set the types for the 2PFA FV method
NEW_TYPE_TAG(FVVelocity2PTestProblem,
        INHERITS_FROM(FVPressureTwoP, TestDiffusionSpatialParams));
SET_TYPE_PROP(FVVelocity2PTestProblem, Problem,
        Dumux::TestDiffusionProblem<TypeTag>);

//set the GridCreator property
SET_TYPE_PROP(FVVelocity2PTestProblem, GridCreator, DgfGridCreator<TypeTag>);

// Set the grid type
SET_PROP(FVVelocity2PTestProblem, Grid){
typedef Dune::UGGrid<2> type;
//    typedef Dune::YaspGrid<2> type;
//typedef Dune::SGrid<2, 2> type;
};
SET_TYPE_PROP(FVVelocity2PTestProblem, LinearSolver, SuperLUBackend<TypeTag>);
// Set the wetting phase
SET_PROP(FVVelocity2PTestProblem, WettingPhase){
private:
typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};

// Set the non-wetting phase
SET_PROP(FVVelocity2PTestProblem, NonwettingPhase){
private:
typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};

// Enable gravity
SET_BOOL_PROP(FVVelocity2PTestProblem, ProblemEnableGravity, false);

// set the types for the MPFA-O FV method
NEW_TYPE_TAG(FVMPFAOVelocity2PTestProblem, INHERITS_FROM(FvMpfaO2dPressureTwoP, TestDiffusionSpatialParams))
;
//SET_TYPE_PROP(FVMPFAOVelocity2PTestProblem, LinearSolver, Dumux::ILUnBiCGSTABBackend<TypeTag>);
//SET_TYPE_PROP(FVMPFAOVelocity2PTestProblem, LinearSolver, Dumux::SSORBiCGSTABBackend<TypeTag>);
SET_TYPE_PROP(FVMPFAOVelocity2PTestProblem, LinearSolver,
        SuperLUBackend<TypeTag>);SET_INT_PROP
        (FVMPFAOVelocity2PTestProblem, LinearSolverPreconditionerIterations, 2);
SET_TYPE_PROP        (FVMPFAOVelocity2PTestProblem, Problem, Dumux::TestDiffusionProblem<TypeTag>);

//set the GridCreator property
SET_TYPE_PROP(FVMPFAOVelocity2PTestProblem, GridCreator, DgfGridCreator<TypeTag>);

// Set the grid type
SET_PROP(FVMPFAOVelocity2PTestProblem, Grid){
typedef Dune::UGGrid<2> type;
//    typedef Dune::YaspGrid<2> type;
//typedef Dune::SGrid<2, 2> type;
};

// Set the wetting phase
SET_PROP(FVMPFAOVelocity2PTestProblem, WettingPhase){
private:
typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};

// Set the non-wetting phase
SET_PROP(FVMPFAOVelocity2PTestProblem, NonwettingPhase){
private:
typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};

// Enable gravity
SET_BOOL_PROP(FVMPFAOVelocity2PTestProblem, ProblemEnableGravity, false);

// set the types for the mimetic FD method
NEW_TYPE_TAG(MimeticPressure2PTestProblem, INHERITS_FROM(MimeticPressureTwoP, TestDiffusionSpatialParams))
;
SET_TYPE_PROP(MimeticPressure2PTestProblem, LinearSolver,
        SuperLUBackend<TypeTag>);SET_TYPE_PROP
        (MimeticPressure2PTestProblem, Problem, Dumux::TestDiffusionProblem<TypeTag>);

//set the GridCreator property
SET_TYPE_PROP(MimeticPressure2PTestProblem, GridCreator, DgfGridCreator<TypeTag>);

// Set the grid type
SET_PROP(MimeticPressure2PTestProblem, Grid){
typedef Dune::UGGrid<2> type;
//        typedef Dune::YaspGrid<2> type;
//typedef Dune::SGrid<2, 2> type;
};

// Set the wetting phase
SET_PROP(MimeticPressure2PTestProblem, WettingPhase){
private:
typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};

// Set the non-wetting phase
SET_PROP(MimeticPressure2PTestProblem, NonwettingPhase){
private:
typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};

// Enable gravity
SET_BOOL_PROP(MimeticPressure2PTestProblem, ProblemEnableGravity, false);

}/*!
 * \ingroup DecoupledProblems
 *
 * \brief test problem for diffusion models from the FVCA5 benchmark.
 *
 * The problem corresponds to Test 2 of the FVCA5 benchmark
 * session, http://www.latp.univ-mrs.fr/fvca5/benchmark/index.html.
 */
template<class TypeTag>
class TestDiffusionProblem: public DiffusionProblem2P<TypeTag>
{
    typedef DiffusionProblem2P<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView)GridView;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, WettingPhase) WettingPhase;
    typedef typename GET_PROP_TYPE(TypeTag, WettingPhase) NonwettingPhase;

    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;

    enum
    {
        dim = GridView::dimension, dimWorld = GridView::dimensionworld
    };

    enum
    {
        wPhaseIdx = Indices::wPhaseIdx,
        pwIdx = Indices::pwIdx,
        swIdx = Indices::swIdx
    };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dim> LocalPosition;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;

public:
    typedef typename GET_PROP(TypeTag, SolutionTypes) SolutionTypes;
    typedef typename SolutionTypes::PrimaryVariables PrimaryVariables;
    typedef typename SolutionTypes::ScalarSolution ScalarSolution;

    TestDiffusionProblem(const GridView &gridView) :
    ParentType(gridView), eps_(1e-8), velocity_(*this)
    {}

    //!for this specific problem: initialize the saturation and afterwards the model
    void init()
    {
        this->variables().initialize();
        this->spatialParams().initialize();
        for (int i = 0; i < this->gridView().size(0); i++)
        {
            this->variables().cellData(i).setSaturation(wPhaseIdx, 1.0);
        }
        this->model().initialize();
        velocity_.initialize();
    }

    /*!
     * \name Problem parameters
     */
    // \{
    bool shouldWriteRestartFile() const
    {   return false;}

    void calculateFVVelocity()
    {
        velocity_.calculateVelocity();
//        velocity_.addOutputVtkFields(this->resultWriter());
    }

//     \copydoc ParentType::addOutputVtkFields()
     void addOutputVtkFields()
     {
//         ScalarSolution *exactPressure = this->resultWriter().allocateManagedBuffer(this->gridView().size(0));
//
//         ElementIterator eIt = this->gridView().template begin<0>();
//         ElementIterator eEndIt = this->gridView().template end<0>();
//         for(;eIt != eEndIt; ++eIt)
//         {
//             (*exactPressure)[this->elementMapper().map(*eIt)][0] = exact(eIt->geometry().center());
//         }
//
// //         this->resultWriter().attachCellData(*exactPressure, "exact pressure");
//
         this->spatialParams().addOutputVtkFields(this->resultWriter());

         return;
     }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperatureAtPos(const GlobalPosition& globalPos) const
    {
        return 273.15 + 10; // -> 10°C
    }

    // \}

    //! Returns the reference pressure for evaluation of constitutive relations
    Scalar referencePressureAtPos(const GlobalPosition& globalPos) const
    {
        return 1; // -> 10°C
    }

    void source(PrimaryVariables &values,const Element& element) const
    {
        values = 0;

//         values[wPhaseIdx] = integratedSource_(element, 4);
    }

    /*!
     * \brief Returns the type of boundary condition.
     *
     *
     * BC for saturation equation can be dirichlet (saturation), neumann (flux), or outflow.
     */
    void boundaryTypesAtPos(BoundaryTypes &bcTypes, const GlobalPosition& globalPos) const
    {
        if (globalPos[1] > this->bBoxMax()[1] - eps_ ) //Top boundary
        {
            bcTypes.setAllNeumann(); //matrix
        }
        else if (globalPos[1] < this->bBoxMin()[1] + eps_) //Bottom boundary
        {
            bcTypes.setAllNeumann(); //matrix
        }
        else if (globalPos[0] < this->bBoxMin()[0] + eps_) //left boundary
        {
            bcTypes.setAllNeumann(); //matrix
        }
        else
        bcTypes.setAllDirichlet();
//	if (globalPos[0] > this->bBoxMax()[0] - eps_)//Right boundary
//	{
//		bcTypes.setAllDirichlet();
//	}
//	if (globalPos[0] < this->bBoxMin()[0] + eps_)//Left boundary
//	{
//		bcTypes.setAllDirichlet();
//	}
    }

    //! set dirichlet condition  (saturation [-])
    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition& globalPos) const
    {
        values[pwIdx] = 1.0; //pressure at right boundary
    }

    //! set neumann condition for phases (flux, [kg/(m^2 s)])
    void neumannAtPos(PrimaryVariables &values, const GlobalPosition& globalPos) const
    {
        values = 0;
        if(globalPos[0] < this->bBoxMin()[0] + eps_){
//           values = -1.0e6;
           values[0] = -1.0e0;
           values[1] =  0.0;
        }
    }

    Scalar exact (const GlobalPosition& globalPos) const
    {
        Scalar pi = 4.0*atan(1.0);

        return (sin(pi*globalPos[0])*sin(pi*globalPos[1]));
    }

    Dune::FieldVector<Scalar,dim> exactGrad (const GlobalPosition& globalPos) const
    {
        Dune::FieldVector<Scalar,dim> grad(0);
        Scalar pi = 4.0*atan(1.0);
        grad[0] = pi*cos(pi*globalPos[0])*sin(pi*globalPos[1]);
        grad[1] = pi*cos(pi*globalPos[1])*sin(pi*globalPos[0]);

        return grad;
    }

private:

    const Scalar eps_;
    Dumux::FVVelocity<TypeTag, typename GET_PROP_TYPE(TypeTag, Velocity) > velocity_;
};
}
 //end namespace

#endif
