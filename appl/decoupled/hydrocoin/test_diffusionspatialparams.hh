// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief spatial parameters for the test problem for diffusion models.
 */
#ifndef TEST_DIFFUSION_SPATIALPARAMS_HH
#define TEST_DIFFUSION_SPATIALPARAMS_HH

#include <dumux/decoupled/common/decoupledproperties.hh>
#include <dumux/material/spatialparams/fvspatialparams.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class TestDiffusionSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(TestDiffusionSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(TestDiffusionSpatialParams, SpatialParams, Dumux::TestDiffusionSpatialParams<TypeTag>);

// Set the material law
SET_PROP(TestDiffusionSpatialParams, MaterialLaw)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef LinearMaterial<Scalar> RawMaterialLaw;
public:
    typedef EffToAbsLaw<RawMaterialLaw> type;
};
}

/*!
 * \ingroup IMPETtests
 * \brief spatial parameters for the test problem for diffusion models.
 */
template<class TypeTag>
class TestDiffusionSpatialParams: public FVSpatialParams<TypeTag>
{
    typedef FVSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::IndexSet IndexSet;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;
    ///@cond false
    typedef typename GET_PROP(TypeTag, SolutionTypes)::ScalarSolution ScalarSolution;
    ///@endcond
    enum
        {dim=Grid::dimension, dimWorld=Grid::dimensionworld};
    typedef typename Grid::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;

    typedef Dune::FieldVector<CoordScalar, dimWorld> GlobalPosition;
    typedef Dune::FieldMatrix<Scalar,dim,dim> FieldMatrix;

public:
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;

    const FieldMatrix& intrinsicPermeabilityAtPos (const GlobalPosition& globalPos) const
    {
    	if ((globalPos[1]>=-globalPos[0]+495) && (globalPos[1]<=-globalPos[0]+505)){
		return KFracture_;
	}
    	else if ((globalPos[1]<=5.5*globalPos[0]-6458.75) && (globalPos[1]>= globalPos[0]*5.5-6541.25)){
		return KFracture_;
	}
   	else {
		return KMatrix_;
	}
    }

    double porosity(const Element& element) const
    {
        return 1.0;
    }


    // return the parameter object for the Brooks-Corey material law which depends on the position
    const MaterialLawParams& materialLawParams(const Element &element) const
    {
            return materialLawParams_;
    }

    void initialize(const double delta)
    {
        delta_ = delta;
        permeability_.resize(gridView_.size(0));

        ElementIterator eIt = gridView_.template begin<0>();
        ElementIterator eEndIt = gridView_.template end<0>();
        for(;eIt != eEndIt; ++eIt)
        {
            perm(permeability_[indexSet_.index(*eIt)], eIt->geometry().center());
        }

    }

    template<class Writer>
    void addOutputVtkFields(Writer& writer)
    {
        ScalarSolution *permXX = writer.allocateManagedBuffer(gridView_.size(0));
        ScalarSolution *permXY = writer.allocateManagedBuffer(gridView_.size(0));
        ScalarSolution *permYY = writer.allocateManagedBuffer(gridView_.size(0));

        ElementIterator eIt = gridView_.template begin<0>();
        ElementIterator eEndIt = gridView_.template end<0>();
        for(;eIt != eEndIt; ++eIt)
        {
            int globalIdx = indexSet_.index(*eIt);
            (*permXX)[globalIdx][0] = permeability_[globalIdx][0][0];
            (*permXY)[globalIdx][0] = permeability_[globalIdx][0][1];
            (*permYY)[globalIdx][0] = permeability_[globalIdx][1][1];
        }

        writer.attachCellData(*permXX, "permeability-X");
        writer.attachCellData(*permYY, "permeability-Y");
        writer.attachCellData(*permXY, "permeability-Offdiagonal");

        return;
    }

//   TestDiffusionSpatialParams(const GridView& gridView)
//   : ParentType(gridView),gridView_(gridView), indexSet_(gridView.indexSet()), permeability_(0)
//   {
        // residual saturations
//        materialLawParams_.setSwr(0.0);
//        materialLawParams_.setSnr(0.0);

        // parameters for the linear entry pressure function
//        materialLawParams_.setEntryPc(0);
//        materialLawParams_.setMaxPc(0);
//    }

    TestDiffusionSpatialParams(const GridView& gridView)
    : ParentType(gridView), gridView_(gridView), indexSet_(gridView.indexSet()), KFracture_(0), KMatrix_(0), delta_(0)
    {
    		KMatrix_[0][0] = 1.0;
    		KMatrix_[0][1] = 0.0;
    		KMatrix_[1][0] = 0.0;
    		KMatrix_[1][1] = 1.0;

            /*
             * 1 darcy is equivalent to 9.869233×10−13 m² or 0.9869233 (µm)².
             * This conversion is usually approximated as 1 (µm)².
             */
    		KFracture_[0][0] = 1e2;
    		KFracture_[0][1] = 0.0;
    		KFracture_[1][0] = 0.0;
    		KFracture_[1][1] = 1e2;
    }
private:
    void perm (FieldMatrix& perm, const GlobalPosition& globalPos) const
    {
          perm = intrinsicPermeabilityAtPos(globalPos);
    }

    const GridView gridView_;
    const IndexSet& indexSet_;
    MaterialLawParams materialLawParams_;
    std::vector<FieldMatrix> permeability_;
    FieldMatrix KFracture_;
    FieldMatrix KMatrix_;
    double delta_;
};

} // end namespace
#endif
