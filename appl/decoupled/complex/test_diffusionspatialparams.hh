// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief spatial parameters for the test problem for diffusion models.
 */
#ifndef TEST_DIFFUSION_SPATIALPARAMS_HH
#define TEST_DIFFUSION_SPATIALPARAMS_HH

#include <dumux/decoupled/common/decoupledproperties.hh>
#include <dumux/material/spatialparams/fvspatialparams.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

#include <appl/xfem/common/helperfunctions.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class TestDiffusionSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(TestDiffusionSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(TestDiffusionSpatialParams, SpatialParams, Dumux::TestDiffusionSpatialParams<TypeTag>);

// Set the material law
SET_PROP(TestDiffusionSpatialParams, MaterialLaw)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef LinearMaterial<Scalar> RawMaterialLaw;
public:
    typedef EffToAbsLaw<RawMaterialLaw> type;
};
}

/*!
 * \ingroup IMPETtests
 * \brief spatial parameters for the test problem for diffusion models.
 */
template<class TypeTag>
class TestDiffusionSpatialParams: public FVSpatialParams<TypeTag>
{
    typedef FVSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::IndexSet IndexSet;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;
    ///@cond false
    typedef typename GET_PROP(TypeTag, SolutionTypes)::ScalarSolution ScalarSolution;
    ///@endcond
    enum
        {dim=Grid::dimension, dimWorld=Grid::dimensionworld};
    typedef typename Grid::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;

    typedef Dune::FieldVector<CoordScalar, dimWorld> GlobalPosition;
    typedef Dune::FieldMatrix<Scalar,dim,dim> FieldMatrix;

public:
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;

    const FieldMatrix& intrinsicPermeabilityAtPos (const GlobalPosition& globalPos) const
    {

        const CoordScalar a=1.0e-4;
        // single inclined fracture between (0.8,0.3) and (0.45,0.95)
        GlobalPosition x1(0.45);
        x1[1]=0.95;
        GlobalPosition x2(0.8);
        x2[1]=0.3;
        GlobalPosition r(x1);
        r-=x2;
        GlobalPosition n(r);
        n[0]=-1.0*r[1];
        n[1]=r[0];
        n/=n.two_norm();
        n*=a;
        std::vector<GlobalPosition> fracture;
        fracture.push_back(x2+n);
        fracture.push_back(x2-n);
        fracture.push_back(x1-n);
        fracture.push_back(x1+n);

        if ( Dumux::pointInPolygon(fracture,globalPos) ){
            return KFractureI_;
        }

        // single cross lower right with center at (0.75,0.225)
        // and endings at (0.95,0.3) and (0.45,0.95)
        // and (0.5,0.05) and (0.75,0.1)
        x1[0]=0.75;
        x1[1]=0.225;

        std::vector<GlobalPosition> vectorOfX2;

        x2[0]=0.95;
        x2[1]=0.3;
        vectorOfX2.push_back(x2);
        x2[0]=0.6;
        x2[1]=0.4;
        vectorOfX2.push_back(x2);
        x2[0]=0.5;
        x2[1]=0.05;
        vectorOfX2.push_back(x2);
        x2[0]=0.75;
        x2[1]=0.1;
        vectorOfX2.push_back(x2);

        for (int i=0;i<vectorOfX2.size();++i){
            r=x1;
            r-=vectorOfX2[i];
            n[0]=-1.0*r[1];
            n[1]=r[0];
            n/=n.two_norm();
            n*=a;
            fracture.clear();
            fracture.push_back(vectorOfX2[i]+n);
            fracture.push_back(vectorOfX2[i]-n);
            fracture.push_back(x1-n);
            fracture.push_back(x1+n);

            if ( Dumux::pointInPolygon(fracture,globalPos) ){
                return KFractureP_;
            }
        }
        //4-crossing lower left with center at (0.2,0.31)
        //and endings at (0.05,0.4) and (0.1,0.0) 
        //and (0.55,0.2) and (0.3,0.695)
        x1[0]=0.2;
        x1[1]=0.31;
        
        vectorOfX2.clear();
        x2[0]=0.05;
        x2[1]=0.4;
        vectorOfX2.push_back(x2);
        x2[0]=0.1;
        x2[1]=0.0;
        vectorOfX2.push_back(x2);
        x2[0]=0.55;
        x2[1]=0.2;
        vectorOfX2.push_back(x2);
        x2[0]=0.3;
        x2[1]=0.695;
        vectorOfX2.push_back(x2);

        for (int i=0;i<vectorOfX2.size();++i){
            r=x1;
            r-=vectorOfX2[i];
            n[0]=-1.0*r[1];
            n[1]=r[0];
            n/=n.two_norm();
            n*=a;
            fracture.clear();
            fracture.push_back(vectorOfX2[i]+n);
            fracture.push_back(vectorOfX2[i]-n);
            fracture.push_back(x1-n);
            fracture.push_back(x1+n);

            if ( Dumux::pointInPolygon(fracture,globalPos) ){
                return KFractureP_;
            }
        } 
        
        // remaining two fracture branches upper left
        // with center at (0.3,0.695)
        // and endings at (0.1,1.0) and (0.4,0.75)
        x1=x2; 
    
        vectorOfX2.clear();
        x2[0]=0.1;
        x2[1]=1.0;
        vectorOfX2.push_back(x2);
        x2[0]=0.4;
        x2[1]=0.75;
        vectorOfX2.push_back(x2);

        for (int i=0;i<vectorOfX2.size();++i){
            r=x1;
            r-=vectorOfX2[i];
            n[0]=-1.0*r[1];
            n[1]=r[0];
            n/=n.two_norm();
            n*=a;
            fracture.clear();
            fracture.push_back(vectorOfX2[i]+n);
            fracture.push_back(vectorOfX2[i]-n);
            fracture.push_back(x1-n);
            fracture.push_back(x1+n);

            if ( Dumux::pointInPolygon(fracture,globalPos) ){
                return KFractureP_;
            }
        } 

        //    	if ( (globalPos[0]>=0.5-0.5e-4 && globalPos[0]<=0.5+0.5e-4) 
//                || ( globalPos[1] >= 0.5-0.5e-4 && globalPos[1] <= 0.5+0.5e-4) )
//    				return KFractureH_;//largest cross
//        else if ( globalPos[0] >= 0.5 && globalPos[1] >= 0.5){
//            //second largest cross
//    	    if ( (globalPos[0]>=0.75-0.5e-4 && globalPos[0]<=0.75+0.5e-4) 
//                    || ( globalPos[1] >= 0.75-0.5e-4 && globalPos[1] <= 0.75+0.5e-4) ){
//   				return KFractureH_;
//            }
//            //smallest cross
//            else if ( globalPos[0] <= 0.75 && globalPos[1] <= 0.75){
//                if ( (globalPos[0]>=0.625-0.5e-4 && globalPos[0]<=0.625+0.5e-4)
//                        || ( globalPos[1] >= 0.625-0.5e-4 && globalPos[1] <= 0.625+0.5e-4) ){
//                return KFractureH_;
//                                                      }
//            }
//        }
   		return KMatrix_;
    }
    double porosity(const Element& element) const
    {
        return 0.2;
    }


    // return the parameter object for the Brooks-Corey material law which depends on the position
    const MaterialLawParams& materialLawParams(const Element &element) const
    {
            return materialLawParams_;
    }

    void initialize(const double delta)
    {
        delta_ = delta;
        permeability_.resize(gridView_.size(0));

        ElementIterator eIt = gridView_.template begin<0>();
        ElementIterator eEndIt = gridView_.template end<0>();
        for(;eIt != eEndIt; ++eIt)
        {
            perm(permeability_[indexSet_.index(*eIt)], eIt->geometry().center());
        }

    }

    template<class Writer>
    void addOutputVtkFields(Writer& writer)
    {
        ScalarSolution *permXX = writer.allocateManagedBuffer(gridView_.size(0));
        ScalarSolution *permXY = writer.allocateManagedBuffer(gridView_.size(0));
        ScalarSolution *permYY = writer.allocateManagedBuffer(gridView_.size(0));

        ElementIterator eIt = gridView_.template begin<0>();
        ElementIterator eEndIt = gridView_.template end<0>();
        for(;eIt != eEndIt; ++eIt)
        {
            int globalIdx = indexSet_.index(*eIt);
            (*permXX)[globalIdx][0] = permeability_[globalIdx][0][0];
            (*permXY)[globalIdx][0] = permeability_[globalIdx][0][1];
            (*permYY)[globalIdx][0] = permeability_[globalIdx][1][1];
        }

        writer.attachCellData(*permXX, "permeability-X");
        writer.attachCellData(*permYY, "permeability-Y");
        writer.attachCellData(*permXY, "permeability-Offdiagonal");

        return;
    }

//   TestDiffusionSpatialParams(const GridView& gridView)
//   : ParentType(gridView),gridView_(gridView), indexSet_(gridView.indexSet()), permeability_(0)
//   {
        // residual saturations
//        materialLawParams_.setSwr(0.0);
//        materialLawParams_.setSnr(0.0);

        // parameters for the linear entry pressure function
//        materialLawParams_.setEntryPc(0);
//        materialLawParams_.setMaxPc(0);
//    }

    TestDiffusionSpatialParams(const GridView& gridView)
    : ParentType(gridView), gridView_(gridView), indexSet_(gridView.indexSet()), KFractureP_(0), KFractureI_(0), KMatrix_(0), delta_(0)
    {
    		KMatrix_[0][0] = 1.0;
    		KMatrix_[0][1] = 0.0;
    		KMatrix_[1][0] = 0.0;
    		KMatrix_[1][1] = 1.0;
 
            /*
             * 1 darcy is equivalent to 9.869233×10−13 m² or 0.9869233 (µm)².
             * This conversion is usually approximated as 1 (µm)².
             */
    		KFractureP_[0][0] = 1.0e4; // (mD)
    		KFractureP_[0][1] = 0.0;
    		KFractureP_[1][0] = 0.0;
    		KFractureP_[1][1] = 1.0e4;

            KFractureI_[0][0] = 1.0e-4;
    		KFractureI_[0][1] = 0.0;
    		KFractureI_[1][0] = 0.0;
    		KFractureI_[1][1] = 1.0e-4;
    }
private:
    void perm (FieldMatrix& perm, const GlobalPosition& globalPos) const
    {
//        double rt = globalPos[0]*globalPos[0]+globalPos[1]*globalPos[1];
//        perm[0][0] = (delta_*globalPos[0]*globalPos[0] + globalPos[1]*globalPos[1])/rt;
//        perm[0][1] = -(1.0 - delta_)*globalPos[0]*globalPos[1]/rt;
//        perm[1][0] = perm[0][1];
//        perm[1][1] = (globalPos[0]*globalPos[0] + delta_*globalPos[1]*globalPos[1])/rt;
          perm = intrinsicPermeabilityAtPos(globalPos);
    }

    const GridView gridView_;
    const IndexSet& indexSet_;
    MaterialLawParams materialLawParams_;
    std::vector<FieldMatrix> permeability_;
    FieldMatrix KFractureP_;
    FieldMatrix KFractureI_;
    FieldMatrix KMatrix_;
    double delta_;
};

} // end namespace
#endif
