// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \ingroup IMPETtests
 * \brief test for diffusion models
 */
#include "config.h"
#include <iostream>

#include <dune/common/exceptions.hh>
#include <dune/common/version.hh>
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 3)
#include <dune/common/parallel/mpihelper.hh>
#else
#include <dune/common/mpihelper.hh>
#endif
//#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/grid/io/file/dgfparser.hh>
#include <dumux/common/start.hh>

#include "test_diffusionproblem.hh"
#include "../resultevaluation.hh"

void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
                    errorMessageOut += progName;
                    errorMessageOut += " [options]\n";
                    errorMessageOut += errorMsg;
                    errorMessageOut += "\n\nThe list of mandatory arguments for this program is:\n"
                                       "\t-Grid.File      name of the dgf file \n";

        std::cout << errorMessageOut
                  << "\n";
    }
}

int main(int argc, char** argv)
{
    try {
        typedef TTAG(FVVelocity2PTestProblem) TypeTag;
        typedef GET_PROP_TYPE(TypeTag, Scalar) Scalar;
        typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;
        typedef GET_PROP_TYPE(TypeTag, Grid) Grid;
        static const int dim = Grid::dimension;
        typedef Dune::FieldVector<Scalar, dim> GlobalPosition;

        // initialize MPI, finalize is done automatically on exit
        Dune::MPIHelper::instance(argc, argv);

        // fill the parameter tree with the options from the command line
        typedef typename GET_PROP(TypeTag, ParameterTree) ParameterTree;
        std::string s = Dumux::readOptions_(argc, argv, ParameterTree::tree());
        if (!s.empty()) {
            std::string usageMessage = s ;
            usageMessage += Dumux::usageTextBlock();
            usage(argv[0], usageMessage);
            return 1;
        }

        // obtain the name of the parameter file
        std::string parameterFileName;
        if (ParameterTree::tree().hasKey("ParameterFile"))
        {
            // set the name to the one provided by the user
            parameterFileName = GET_RUNTIME_PARAM(TypeTag, std::string, ParameterFile); // otherwise we read from the command line
        }
        else
        {
            // set the name to the default ./<programname>.input
            parameterFileName = argv[0];
            parameterFileName += ".input";
        }

        // open and check whether the parameter file exists.
        std::ifstream parameterFile(parameterFileName.c_str());
        if (not parameterFile.is_open()) {
            // if the name of the file has been specified,
            // this must be an error. Otherwise proceed.
            if (ParameterTree::tree().hasKey("ParameterFile"))
            {
                std::cout << "\n\t -> Could not open file '"
                          << parameterFileName
                          << "'. <- \n\n\n\n";
                usage(argv[0], Dumux::usageTextBlock());
                return 1;
            }
        }
        else
        {
            // read parameters from the file without overwriting
            Dune::ParameterTreeParser::readINITree(parameterFileName,
                                                   ParameterTree::tree(),
                                                   /*overwrite=*/false);
        }
        parameterFile.close();

        ////////////////////////////////////////////////////////////
        // create the grid
        ////////////////////////////////////////////////////////////

        // try to create a grid (from the given grid file)
        try { GridCreator::makeGrid(); }
        catch (...) {
            std::string usageMessage = "\n\t -> Creation of the grid failed! <- \n\n\n\n";
            usageMessage += Dumux::usageTextBlock();
            usage(argv[0], usageMessage);
            throw;
        }
        GridCreator::loadBalance();

        ////////////////////////////////////////////////////////////
        // instantiate and run the concrete problem
        ////////////////////////////////////////////////////////////
        bool consecutiveNumbering = true;

        typedef GET_PROP_TYPE(TTAG(FVVelocity2PTestProblem), Problem) FVProblem;
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 3)
        FVProblem fvProblem(GridCreator::grid().leafGridView());
#else
        FVProblem fvProblem(GridCreator::grid().leafView());
#endif
        fvProblem.setName("fvdiffusion");
        fvProblem.init();
        fvProblem.calculateFVVelocity();
        fvProblem.writeOutput();
        Dumux::ResultEvaluation fvResult;

        typedef GET_PROP_TYPE(TTAG(FVMPFAOVelocity2PTestProblem), Problem) MPFAOProblem;
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 3)
        fvResult.evaluate(GridCreator::grid().leafGridView(), fvProblem, consecutiveNumbering);

        MPFAOProblem mpfaProblem(GridCreator::grid().leafGridView());
#else
        fvResult.evaluate(GridCreator::grid().leafView(), fvProblem, consecutiveNumbering);
#endif

        mpfaProblem.setName("fvmpfaodiffusion");
        mpfaProblem.init();
        mpfaProblem.writeOutput();
        Dumux::ResultEvaluation mpfaResult;
 #if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 3)
        mpfaResult.evaluate(GridCreator::grid().leafGridView(), mpfaProblem, consecutiveNumbering);
 #else
        mpfaResult.evaluate(GridCreator::grid().leafView(), mpfaProblem, consecutiveNumbering);
 #endif

        typedef GET_PROP_TYPE(TTAG(MimeticPressure2PTestProblem), Problem) MimeticProblem;
        MimeticProblem mimeticProblem(GridCreator::grid().leafView());
        mimeticProblem.setName("mimeticdiffusion");
        mimeticProblem.init();
        mimeticProblem.writeOutput();
        Dumux::ResultEvaluation mimeticResult;
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 3)
        mimeticResult.evaluate(GridCreator::grid().leafGridView(), mimeticProblem, consecutiveNumbering);
#else
        mimeticResult.evaluate(GridCreator::grid().leafView(), mimeticProblem, consecutiveNumbering);
#endif

        return 0;
    }
    catch (Dune::Exception &e) {
        std::cerr << "Dune reported error: " << e << std::endl;
    }
    catch (...) {
        std::cerr << "Unknown exception thrown!\n";
        throw;
    }

    return 3;
}
