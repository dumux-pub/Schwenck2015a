#!/bin/sh

### Create a folder for the dune and dumux modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Please run this script preferably in an empty directory."
  echo "Aborting."
  exit 1
fi

# DUNE core modules
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-common.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-geometry.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-grid.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-istl.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-localfunctions.git

# DUNE multidomain(grid)
git clone -b releases/2.3 git://github.com/smuething/dune-multidomaingrid.git
git clone -b releases/2.0 git://github.com/smuething/dune-multidomain.git

# DUNE PDELab
git clone -b releases/2.3 https://gitlab.dune-project.org/pdelab/dune-typetree.git
git clone -b releases/2.0 https://gitlab.dune-project.org/pdelab/dune-pdelab.git

# DuMuX
git clone -b releases/2.7 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Schwenck2015a.git

# apply patches
cd dune-grid
patch -p1 <../Schwenck2015a/patches/grid-2.3.patch
cd ..
cd dune-multidomaingrid
patch -p1 <../Schwenck2015a/patches/multidomaingrid-2.3.patch
cd ..
cd dune-multidomain
patch -p1 <../Schwenck2015a/patches/multidomain-2.0.patch
cd ..

# external modules: UG
mkdir external
cd external
wget http://conan.iwr.uni-heidelberg.de/download/ug-3.12.1.tar.gz
tar zxvf ug-3.12.1.tar.gz
cd ug-3.12.1
sed -i 's/NDELEM_BLKS_MAX                 100/NDELEM_BLKS_MAX                 2000/g' gm/gm.h
autoreconf -is
OPTIM_FLAGS="-O3 -DNDEBUG -march=native -finline-functions -funroll-loops"
CFLAGS="$OPTIM_FLAGS"
CXXFLAGS="$OPTIM_FLAGS -std=c++0x -fno-strict-aliasing"
OPTS="--enable-dune --prefix=$PWD --without-mpi"
./configure CC=g++ CFLAGS="$CFLAGS" CXXFLAGS="$CXXFLAGS" $OPTS
make -j3
make install

# external modules: Alberta
cd ..
wget http://www.mathematik.uni-stuttgart.de/fak8/ians/lehrstuhl/nmh/downloads/alberta/alberta-3.0.1.tar.gz
tar zxvf alberta-3.0.1.tar.gz
cd alberta-3.0.1
./configure --prefix=$PWD --disable-fem-toolbox --disable-vector-basis-functions --disable-chained-basis-functions CFLAGS="-O2" CXXFLAGS="-O2"
make -j3
make install
cd ../..

# run dunecontrol
pwd > temp.txt
sed -i 's/\//\\\//g' temp.txt
EXTPATH=`cat temp.txt`
/usr/bin/rm temp.txt
sed "s/EXTDIR=.*/EXTDIR=$EXTPATH\/external/" Schwenck2015a/optim.opts >optim.opts
sed "s/EXTDIR=.*/EXTDIR=$EXTPATH\/external/" Schwenck2015a/debug.opts >debug.opts
./dune-common/bin/dunecontrol --opts=optim.opts all

