Summary
=======
This is the DuMuX module containing the code for producing the results
published in:

N. Schwenck<br>
[An XFEM-based model for fluid flow in fractured porous media]
(http://elib.uni-stuttgart.de/handle/11682/638?locale=en)<br>
PhD thesis, 2015<br>
DOI: 10.18419/opus-621

Installation
============

The easiest way to install this module is to create a new folder and to execute
the file [installSchwenck2015a.sh]
(https://git.iws.uni-stuttgart.de/dumux-pub/Schwenck2015a/raw/master/installSchwenck2015a.sh)
in this folder.
You can copy the following to a terminal:
```bash
mkdir -p Schwenck2015a && cd Schwenck2015a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Schwenck2015a/raw/master/installSchwenck2015a.sh
chmod +x installSchwenck2015a.sh && ./installSchwenck2015a.sh
```
For more detailed informations on installation, have a look at the
[DuMuX installation guide](http://www.dumux.org/installation.php)
or use the [DuMuX handbook]
(http://www.dumux.org/documents/dumux-handbook-2.9.pdf), chapter 2.

Applications
============

The applications can be found in the folder `appl`, particularly in the
following subfolders. Each program is related to a corresponding section in the
PhD thesis.

* `decoupled`: programs for calculating reference solutions on equi-dimensional
  grids.
    - `complex`: Section 7.4.
    - `crossing1`: Section 5.2, but crossing permeability = diag(1e2, 1e2).
      No grid file found.
    - `crossing2`: Section 5.2. No grid file found.
    - `ending`: Sections 4.3 and 4.4.
    - `geigerimp`: Section 7.2.
    - `geigerper`: Section 7.1.
    - `hydrocoin`: Section 7.3.
* `dfm`: programs using the discrete fracture matrix (DFM) method.
    - `complex`: Section 7.4.
    - `geiger`: Sections 7.1 and 7.2.
    - `haegland`: Section 3.2.
    - `hydrocoin`: Section 7.3.
* `xfem`: programs using the extended finite element method (XFEM) as proposed
  in the thesis.
    - `mono`: Sections 4.3 and 4.4.
    - `monoAnalytical`: Section 3.2.
    - `comparison`:
        + `bc`: Section 6.3.
        + `complex`: Section 7.4.
        + `crossing`: Section 5.2.
        + `geiger`: Sections 7.1 and 7.2.
        + `hydrocoin`: Section 5.4.


Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules as well as
grid managers, have a look at [installSchwenck2015a.sh]
(https://git.iws.uni-stuttgart.de/dumux-pub/Schwenck2015a/raw/master/installSchwenck2015a.sh).

In addition, the linear solver SuperLU has to be installed.

The module has been tested successfully with GCC 4.8.
The autotools-based DUNE buildsystem has been employed.
